﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Diagnostics;
using System.IO.MemoryMappedFiles;
using System.Runtime.InteropServices;
using Microsoft.VisualBasic.FileIO;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Reflection.Emit;

namespace DataConversionAutomation
{
    public static class Template
    {
        static int ActualRecordCount = 0;
        static int HeaderCount = 0;
        static int FooterCount = 0;
        static string[] abbreviations = { "NFS", "DPR", "TEST"};
        static string[] HeaderTypeList = { "Header| ","Header|", "header|", "HEADER|", "HEADER| "};
        static string[] FooterTypeList = { "Footer|", "FOOTER|"};
        public static string GetFileNameWithExtension(string fullpath)
        {
            string result = Path.GetFileName(fullpath);
            return result;
        }

        public static string GetFileNameWithoutExtension(string fullpath)
        {
            string result = Path.GetFileNameWithoutExtension(fullpath);
            return result;
        }

        public static List<string> GetFileLines(Stream stream)
        {
            string line = string.Empty;
            List<string> lines = new List<string>();
            using (BufferedStream bs = new BufferedStream(stream))
            {
                try
                {
                    using (StreamReader sr = new StreamReader(bs))
                    {
                        // Read and display lines from the file until the end of the file is reached.
                        while ((line = sr.ReadLine()) != null)
                        {
                            lines.Add(line);
                            Console.WriteLine(line);
                        }
                    }
                }
                catch (Exception e)
                {
                    // Let the user know what went wrong.
                    Console.WriteLine("The file could not be read:");
                    Console.WriteLine(e.Message);
                }
            }
            return lines;
        }

        public static List<string> GetFileLines(Stream stream, String sectionHeader)
        {
            string line = string.Empty;
            bool sectionFound = false;
            string sectionFooter = string.Empty;


            //sectionHeader = Regex.Replace(sectionHeader, "_header[|]", string.Empty, RegexOptions.IgnoreCase);
            sectionHeader = Regex.Replace(sectionHeader, "header[|]", string.Empty, RegexOptions.IgnoreCase);

            if (TestSetup.module.Equals("AIR"))
            {
                sectionFooter = sectionHeader.Replace(" ", String.Empty) + "_Footer|";
                sectionHeader = sectionHeader.Replace(" ", String.Empty) + "_Header|";
            }
            else if (TestSetup.module.Equals("AccountMaster"))
            {
                sectionFooter = sectionHeader.Replace(" ", String.Empty) + "Footer|";
                sectionHeader = sectionHeader.Replace(" ", String.Empty) + "Header|";
            }
            else
            {
                sectionFooter = sectionHeader.Replace(" ", String.Empty) + "Footer|";
                sectionHeader = sectionHeader.Replace(" ", String.Empty) + "Header|";
            }


                List<string> lines = new List<string>();
            using (BufferedStream bs = new BufferedStream(stream))
            {
                try
                {
                    CultureInfo culture = new CultureInfo("es-ES", false);
                    using (StreamReader sr = new StreamReader(bs))
                    {
                        // Read and display lines from the file until the end of the file is reached.

                        while ((line = sr.ReadLine()) != null)
                        {
                            //if (line.Contains(sectionHeader))
                            if(culture.CompareInfo.IndexOf(line, sectionHeader, CompareOptions.IgnoreCase) >= 0)
                            {
                                sectionFound = true;
                                continue;
                            }
                            else if (culture.CompareInfo.IndexOf(line, sectionFooter, CompareOptions.IgnoreCase) >= 0)
                            {
                                sectionFound = false;
                                break;
                            }
                                
                            if (sectionFound)
                            {
                                lines.Add(line);
                                //Console.WriteLine(line);
                            }

                        }
                    }
                }
                catch (Exception e)
                {
                    // Let the user know what went wrong.
                    Console.WriteLine("The file could not be read:");
                    Console.WriteLine(e.Message);
                }
            }
            if (TestSetup.module.Equals("AIR"))
            {
                lines.RemoveAt(0);
                return lines;
            }
            else if (TestSetup.module.Equals("AccountMaster"))
            {
                return lines;
            }
            else
            {
                return lines;
            }

        }

        public static DataTable ParseExtractToDataTable(string filepath, string strHeader)
        {
            var dt = new DataTable();
            List<string> ExtractData;
            // Get max columns.
            using (FileStream file = File.Open(filepath, FileMode.Open))
            {
                ExtractData = GetFileLines(file, strHeader);
            }
            try
            {
                string FirstRowDataValue = ExtractData.First().ToString();
                int ExtractDataColCount = FirstRowDataValue.Count(f => f == '|') + 1;
                // Add columns.
                for (int i = 0; i < ExtractDataColCount; i++)
                {
                    dt.Columns.Add();
                }

                // Add rows.
                foreach (var line in ExtractData)
                {
                    string[] field;
                    if (TestSetup.module.Equals("AIR"))
                    {
                        field = line.Split('|'); // Select(value => value.Trim()).ToArray(); //using string.split() method to split the string.
                    }
                    else
                    {
                        field = line.Split('|').Select(value => value.Trim()).ToArray(); //using string.split() method to split the string.
                    }

                    if (ExtractDataColCount == field.Length)
                    {
                        dt.Rows.Add(field);
                    }
                    else
                    {
                        Console.WriteLine("Column Size Miss Match");
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return dt;
        }

        public static string ToPascalCase(this string str)
        {   
            if (!string.IsNullOrEmpty(str) && str.Length > 1 && !abbreviations.Any(str.Contains))
            {
                str=str.ToLowerInvariant();
                return Char.ToUpperInvariant(str[0]) + str.Substring(1);
            }
            return str;
        }


        public static string GetHeaderFooterCount(string prefix, string filepath)
        {
            int TotalLine = 0;
            List<string> HeaderList = new List<String>();
            List<string> ExpectedFooterList = new List<string>();
            List<string> FooterList = new List<String>();
            string sectionName = String.Empty;
            int sectionRecordCount = 0;
            int expectedSectionRecordCount =0;

            DirectoryInfo d = new DirectoryInfo(filepath);
            FileInfo[] Files = d.GetFiles("*.txt"); //Getting Text files
            int fileCount = Files.Length;
            int PassCount = 0;
            bool ColumnHeaders = false;
            foreach (var file in Files)
            {
                ColumnHeaders = false;
                String fullFilePath = filepath + @"\" + file.Name;
                String filename = GetFileNameWithoutExtension(fullFilePath);
                String DataHeader = String.Empty;
                foreach (string word in filename.Split('_'))
                {
                    DataHeader = DataHeader + ToPascalCase(word);
                }
                DataHeader = DataHeader + "Header";

                var lines = File.ReadLines(fullFilePath);
                string FileHeader = lines.Skip(1).ToString();
                HeaderCount++;
                string FileFooter = lines.Last().ToString();
                FooterCount++;
                TotalLine = lines.Count();

                foreach (var line in lines)
                {
                    if (prefix.Equals("AccountMaster"))
                    {
                        if (HeaderTypeList.Any(line.EndsWith))
                        {
                            HeaderList.Add(line.Replace("Header|", string.Empty).Trim());
                            ExpectedFooterList.Add(line.Replace("Header|", string.Empty).Trim() + "Footer|");
                            HeaderCount++;
                            sectionRecordCount = 0;
                            sectionName = line.Replace("Header|", string.Empty).Trim();
                        }
                        else if (ExpectedFooterList.Any(line.Contains))
                        {
                            FooterList.Add(line);
                            FooterCount++;
                            expectedSectionRecordCount = int.Parse(Regex.Match(line, @"\d+").Value);
                            Console.WriteLine("Section Name " + sectionName + "  | Actual RecordCount  : " + sectionRecordCount + " | Record Count Given : " + expectedSectionRecordCount);
                        }
                        else
                        {
                            sectionRecordCount++;
                        }
                    }
                    else if (prefix.Equals("AIR"))
                    {   //Don't put "line" into a list or collection. Just make your processing on it.
                        if(ColumnHeaders)
                        {
                            if (HeaderTypeList.Any(line.Contains))
                            {
                                HeaderList.Add(line);
                                ExpectedFooterList.Add(line.Replace("Header|", string.Empty).Trim() + "Footer|");
                                //HeaderCount++;
                            }
                            else if (ExpectedFooterList.Any(line.Contains))
                            {
                                FooterList.Add(line);
                                //FooterCount++;
                            }
                        }
                        else
                        {
                            HeaderCount++;
                            ColumnHeaders = true;
                        }
                    }       

                }
                
                ActualRecordCount = TotalLine - (HeaderCount + FooterCount);
                HeaderCount = HeaderCount - Convert.ToInt32(ColumnHeaders);
                int RecordCountInTemplate = int.Parse(Regex.Match(FileFooter, @"\d+").Value);

                if ((ActualRecordCount == RecordCountInTemplate) && (HeaderCount  == FooterCount))
                {
                    Console.WriteLine("FileName :  " + file.Name + "  | RecordCount Matches Expected | : " + ActualRecordCount + " | All : " + HeaderCount + " Header/Headers has Footer/Footers");
                    PassCount++;
                }
                else {
                    Console.WriteLine("FileName :  " + file.Name);
                    Console.WriteLine("RecordCount Does NOT Matches. >   Actual Count  : " + ActualRecordCount + " | Expected Count: " + RecordCountInTemplate);
                    Console.WriteLine("Header Count: " + HeaderCount + " | Footer Count: " + FooterCount);
                    //PassCount++;
                }
                ActualRecordCount = 0;
                HeaderCount = 0;
                FooterCount = 0;
            }

            if (fileCount == PassCount)
                return "Pass";
            else
                return "Fail";
            //Console.Write(RecordCount);
        }
        public static List<string> UsingFileStream(string filepath)
        {
            // FileStream.
            using (FileStream file = File.Open(filepath, FileMode.Open))
            {
                //intTotalRecordCount = file.
                return GetFileLines(file);
            }
        }

        public static List<string> UsingMemoryMapped(string filepath)
        {
            // MemoryMappedFile.
            using (MemoryMappedFile file = MemoryMappedFile.CreateFromFile(filepath))
            using (MemoryMappedViewStream stream = file.CreateViewStream())
            {
                return GetFileLines(stream);
            }
        }

        public static void UsingMemoryStream(string filepath)
        {
            // MemoryStream.
            using (MemoryStream stream = new MemoryStream(File.ReadAllBytes(filepath)))
            {
                List<String> lines = GetFileLines(stream);
                foreach (string line in lines)
                {
                    //Console.WriteLine(line);
                }
            }

        }

        public static string ParseExtractFileIntoDataSet(string file)
        {
            //long countOfLine = 0;
            long countOfHeaders = 0;
            long countOfFooters = 0;
            long countOfCurrentRecord = 0;
            long countOftotalRecords = 0;
            string currentHeader = null;
            List<long> headerIndex = new List<long>();
            List<long> footerIndex = new List<long>();
            Boolean headerRow = true;
            var dt = new DataTable();

            using (StreamReader sr = File.OpenText(file))
            {
                var parse = new TextFieldParser(sr);
                string s = String.Empty;
                while ((s = sr.ReadLine()) != null)
                {
                    if (s.Contains("Header|MML") || s.EndsWith("Header| "))
                    {
                        countOfHeaders++;//headerIndex.Add(parser.LineNumber);
                        countOfCurrentRecord = 0;
                        currentHeader = s.Substring(0, s.IndexOf("Header|"));
                        Console.WriteLine(s);
                        headerRow = true;
                        Console.WriteLine("Found Header: " + countOfHeaders + "Name: " + currentHeader);
                        System.Threading.Thread.Sleep(1000);
                    }
                    else if (s.Contains(currentHeader + "Footer|"))
                    {
                        countOfFooters++;//footerIndex.Add(parser.LineNumber);
                        countOfCurrentRecord = 0;
                        Console.WriteLine(s);
                        Console.WriteLine("Found Footer: " + countOfFooters);
                        headerRow = true;
                        System.Threading.Thread.Sleep(1000);
                    }
                    else
                    {
                        parse.SetDelimiters("|");
                        while (!parse.EndOfData)
                        {
                            var currentRow = parse.ReadFields();
                            if (headerRow)
                            {
                                headerRow = false;
                            }
                            else
                            {
                                dt.Rows.Add(currentRow);
                            }
                        }
                    }
                }
                countOftotalRecords = countOfCurrentRecord + countOftotalRecords;
                Console.WriteLine("Total Records in Section: " + countOfCurrentRecord);
            }
            var parser = new TextFieldParser(file);
            return "Total Records : " + countOftotalRecords;
        }

        //Remove
        public static DataTable ParseRawDataTable(DataTable dt)
        {
            string RowDataValue = dt.Rows[0][0].ToString();
            int dtColCount = RowDataValue.Count(f => f == '|');
            DataTable parsedDatatable = new DataTable();

            for (int i = 0; i <= dtColCount; i++)
            {
                parsedDatatable.Columns.Add("column" + i);
            }
            RowDataValue = String.Empty;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                RowDataValue = dt.Rows[i][0].ToString();
                string[] splitPipeValue = RowDataValue.Split('|');
                DataRow drow = parsedDatatable.NewRow();
                for (int j = 0; j <= dtColCount; j++)
                {
                    drow[j] = splitPipeValue[j].ToString();
                }
                parsedDatatable.Rows.Add(drow);
            }
            return parsedDatatable;
        }

        public static string CheckFileNameFormat(string prefix, string filepath)
        {
            String[] arrTable = XDocument.Load(@"DataConversionTemplate.xml").Descendants(prefix + "_Tables").Elements().Select(x => (string)x).ToArray();
            arrTable = arrTable.Select(s => s.ToUpperInvariant()).ToArray();

            string clientPrefix = XDocument.Load(@"DataConversionTemplate.xml")
                .Descendants("Client")
                .Select(e => (string)e.Value.ToString())
                .FirstOrDefault();

            arrTable = arrTable.Select(s => s.ToUpperInvariant()).ToArray();
            string tablename = string.Empty;

            DirectoryInfo d = new DirectoryInfo(filepath);
            FileInfo[] Files = d.GetFiles("*.txt"); //Getting Text files
            int CountOfFile = Files.Length;
            int counter = 0;
            Regex pattern1;
            Regex pattern2;
            bool PatterMatch = false;

            foreach (FileInfo file in Files)
            {
                PatterMatch = false;
                foreach (var item in arrTable)
                {
                    PatterMatch = false; 
                    tablename = item.ToString();
                    if (prefix.Equals("AIR"))
                    {
                        pattern1 = new Regex(@"^" + clientPrefix + "_" + prefix.ToUpper() + "_" + tablename + "_[0-9]{8}_[0-9]{9}.TXT$");
                        pattern2 = new Regex(@"^" + clientPrefix + "_" + prefix.ToUpper() + "_" + tablename + "_[0-9]{8}_[0-9]{9} - COPY.TXT$");
                    }
                    else if (prefix.Equals("AccountMaster"))
                    {
                        pattern1 = new Regex(@"^" + "ACCOUNT_MASTER_" + tablename + ".TXT$");
                        pattern2 = new Regex(@"^" + "ACCOUNT_MASTER_" + tablename + ". - COPY.TXT$");
                    }
                    else if (prefix.Equals("HouseHold"))
                    {
                        pattern1 = new Regex(@"^" + clientPrefix + "_" + tablename + "_[0-9]{2}_[0-9]{8}_[0-9]{10}.TXT$");
                        pattern2 = new Regex(@"^" + clientPrefix + "_" +  tablename + "_[0-9]{2}_[0-9]{8}_[0-9]{10}. - COPY.TXT$");
                    }
                    else
                    {
                        pattern1 = new Regex(@"^" + clientPrefix + "_" + prefix.ToUpper() + "_" + tablename + "_[0-9]{8}_[0-9]{9}.TXT$");
                        pattern2 = new Regex(@"^" + clientPrefix + "_" + prefix.ToUpper() + "_" + tablename + "_[0-9]{8}_[0-9]{9} - COPY.TXT$");
                    }

                    if (counter < CountOfFile && (pattern1.IsMatch(file.Name.ToUpper()) || pattern2.IsMatch(file.Name.ToUpper())))
                    {
                        PatterMatch = true;
                        counter++;
                        break;
                    }

                }
               
                if(PatterMatch)
                    Console.WriteLine("FileName :  " + file.Name + "  -> Matches Expected FileName Format"); 
                else
                    Console.WriteLine("FileName :  " + file.Name + "  -> Does NOT Match Expected FileName Format");
            }

            if (counter == CountOfFile) { return "Pass"; }
            else { return "Fail"; }
        }

        public static void Get_Template_Checksum(string strModule, string strFilePath, string strSection, int[] DelCol)
        {
            String sectionName = strSection + "HEADER|";
            DataTable t = ParseExtractToDataTable(strFilePath, sectionName);
            if (DelCol != null)
            {
                foreach (int i in DelCol)
                    t.Columns.RemoveAt(i);
            }
            //Get Extract Row Cheksum List
            List<string> rawData = Methods.GetAllRowChecksumArray(t);
            //rawData = rawData.OrderBy(q => q).ToList();
            string OutputPath = string.Empty;
            WriteListToFile(OutputPath, rawData);
        }
        public static void Get_DB_Checksum(string strModule, string strQuery, string strSection)
        {
            string conString = Database.GetConnectionString(strModule, TestSetup.environment);
            //Get Table in SQL Row Cheksum List
            List<string> TableData = Methods.GetAllRowChecksumArray(Database.RunQuery(strQuery, conString));
            strSection = Regex.Replace(strSection, "", string.Empty, RegexOptions.IgnoreCase);
            string OutputPath = String.Empty;

            if (TestSetup.txt2db || TestSetup.PartialRun)
            {
                if (Regex.IsMatch(TestSetup.TargetChecksumFilePath, strSection, RegexOptions.IgnoreCase))
                    OutputPath = TestSetup.TargetChecksumFilePath;
                WriteListToFile(OutputPath, TableData);
            }
            else //default TestSetup.txt2db = false and TestSetup.PartialRun = False
            {
                if (Regex.IsMatch(TestSetup.SourceChecksumFilePath, strSection, RegexOptions.IgnoreCase))
                    OutputPath = TestSetup.SourceChecksumFilePath;
                WriteListToFile(OutputPath, TableData);
            }
            TestSetup.txt2db = true;
        }

        public static void SetupChecksumComparisionTest()
        {
           String strTime = Methods.GetCurrentTime("yyyyMMddHHmmssfff");
            if (TestSetup.module.Equals("AIR"))
            {
                TestSetup.SourceChecksumFilePath = TestSetup.checksumStorePath + @"\" + TestSetup.module + @"\" + TestSetup.section + @"\sourceChecksum" + "_" + strTime + @".txt";
                TestSetup.TargetChecksumFilePath = TestSetup.checksumStorePath + @"\" + TestSetup.module + @"\" + TestSetup.section + @"\targetChecksum" + "_" + strTime + @".txt";
            }
            else
            {
                TestSetup.SourceChecksumFilePath = TestSetup.checksumStorePath + @"\" + TestSetup.module + @"\" + TestSetup.section + @"\" + TestSetup.table + @"\sourceChecksum" + "_" + strTime + @".txt";
                TestSetup.TargetChecksumFilePath = TestSetup.checksumStorePath + @"\" + TestSetup.module + @"\" + TestSetup.section + @"\" + TestSetup.table + @"\targetChecksum" + "_" + strTime + @".txt";
            }

        }
        public static void WriteListToFile(string OutputPath, List<string> lstData)
        {
            if (File.Exists(OutputPath))
            {
                // Create a file to write to.
                File.WriteAllLines(OutputPath, lstData, Encoding.UTF8);
            }
            else
            {
                Directory.CreateDirectory(Path.GetDirectoryName(OutputPath));
                File.WriteAllLines(OutputPath, lstData, Encoding.UTF8);
                Console.WriteLine("Created File : " + OutputPath);
            }

         }

        public static void TestResult(String ResultFilePath)
        {
            bool boolFail = false;
                //Define Failure Condition
            boolFail = (TestSetup.missingInDB.Any() || TestSetup.missingInRaw.Any());
            var duplicateInRaw = TestSetup.missingInRaw.GroupBy(x => x)
              .Where(g => g.Count() > 1)
              .ToDictionary(x => x.Key, y => y.Count());
            var duplicateInDB = TestSetup.missingInDB.GroupBy(x => x)
              .Where(g => g.Count() > 1)
              .ToDictionary(x => x.Key, y => y.Count());

            if (boolFail)
            {
                //Write to Result if any record failed
                using (StreamWriter tw = new StreamWriter(ResultFilePath))
                {

                   tw.WriteLine("START LOG");
                    tw.WriteLine("Date Time: " + DateTime.Now);
                    tw.WriteLine(" ");
                    tw.WriteLine("-------------------------------------------------------------------");
                    tw.WriteLine(" ");
                    tw.WriteLine("Template(s) File Path : " + TestSetup.TempFilePath);
                    tw.WriteLine("DB Environment : " + TestSetup.environment);
                    tw.WriteLine("DB Table : " + TestSetup.table);
                    tw.WriteLine("DB Query File : " + TestSetup.strSQLFile);
                    tw.WriteLine(" ");
                    tw.WriteLine("-------------------------------------------------------------------");
                    tw.WriteLine(" ");
                    tw.WriteLine("Total Records Missing in DB: " + TestSetup.missingInDB.Count());
                    tw.WriteLine("-------------------------------------------------------------------");
                    int counter = 0;
                    foreach (string missing in TestSetup.missingInDB)
                    {
                        counter++;
                        foreach (string line in TestSetup.file1Lines)
                        {
                            if (line.Contains(missing))
                                tw.WriteLine(line);
                        }
                        
                    }
                    tw.WriteLine("-------------------------------------------------------------------");
                    tw.WriteLine("Total Records Missing in Raw: " + TestSetup.missingInRaw.Count());
                    tw.WriteLine("-------------------------------------------------------------------");
                    counter = 0;
                    foreach (string missing in TestSetup.missingInRaw)
                    {
                        counter++;
                        foreach (string line in TestSetup.file2Lines)
                        {
                            if (line.Contains(missing))
                                tw.WriteLine(line);
                        }
                    }
                    tw.WriteLine("-------------------------------------------------------------------");

                    if (duplicateInRaw.Count > 0)
                        foreach (KeyValuePair<string, int> kvp in duplicateInRaw)
                        {
                            tw.WriteLine("Duplicate in Raw: ", kvp.Key, kvp.Value);
                        }

                    if (duplicateInDB.Count > 0)
                        foreach (KeyValuePair<string, int> kvp in duplicateInDB)
                        {
                            tw.WriteLine("Duplicate in DB: ", kvp.Key, kvp.Value);
                        }
                    tw.WriteLine("-------------------------------------------------------------------");
                    tw.WriteLine("END LOG");
                }
                //Report Failure
                Assert.Fail("Checksum is not same!!!");
            }
            else
            {
                using (StreamWriter tw = new StreamWriter(ResultFilePath))
                {
                    tw.WriteLine("Comparision Passed. No Missing Records Found!");
                }
                //Report Pass
                Trace.WriteLine("Test Passed");
                Debug.WriteLine("Test Passed");
            }
        }

        public static void Get_Template_Checksum(string strSection, DataTable dt, int[] DelCol)
        {
            if (DelCol != null)
            {
                foreach (int i in DelCol)
                    dt.Columns.RemoveAt(i);
            }
            //Get Extract Row Cheksum List
            List<string> rawData = Methods.GetAllRowChecksumArray(dt);
            string OutputPath = string.Empty;

            if (Regex.IsMatch(TestSetup.SourceChecksumFilePath, strSection, RegexOptions.IgnoreCase))
                OutputPath = TestSetup.SourceChecksumFilePath;
            WriteListToFile(OutputPath, rawData);
        }

       public static void ComputeTemplateChecksum()
        {
            TestSetup.txt2db = true;
            DirectoryInfo d = new DirectoryInfo(TestSetup.templateFilePath);
            FileInfo[] Files = null;

            if (TestSetup.module.Equals("AIR"))
                Files = d.GetFiles("*_" + TestSetup.section + "_2*.txt");
            else if (TestSetup.module.Equals("AccountMaster"))
                Files = d.GetFiles("*" + TestSetup.section + "*.txt"); 
            else if (TestSetup.module.Equals("ManualInsurance"))
                Files = d.GetFiles("*" + TestSetup.module + "*.txt");
            else if (TestSetup.module.Equals("HouseHold"))
                Files = d.GetFiles("*" + TestSetup.section + "*.txt");
            else if (TestSetup.module.Equals("PPS"))
                Files = d.GetFiles("*" + TestSetup.section + "*.txt");
            else
                Console.WriteLine("File Not Identified! Check Configuration!!!");

            DataSet dsMaster = new DataSet();

            foreach (var file in Files)
            {
                TestSetup.TempFilePath = file.FullName;
                dsMaster.Tables.Add(ParseExtractToDataTable(TestSetup.TempFilePath, TestSetup.table));
            }
            DataTable dtFinal = Methods.MergeDataSet(dsMaster);

            //strOBARawChecksumPath;
            int[] DeleteColumns = { };
            Get_Template_Checksum(TestSetup.section, dtFinal, DeleteColumns);
            //ExcelDataReader.SaveAsCSV(dtFinal);
            dtFinal.Clear();
        }

        public static void ComputeDBChecksum()
        {
            //Get Checksum File To 
            Get_DB_Checksum(TestSetup.module, TestSetup.query, TestSetup.section);
        }

        public static void ComputeChecksumDifference()
        {
            String strTime = Methods.GetCurrentTime("yyyyMMddHHmmssfff");

            if (string.IsNullOrEmpty(TestSetup.ResultFilePath))
            {
                if (TestSetup.module.Equals("AIR"))
                    TestSetup.ResultFilePath = TestSetup.checksumStorePath + @"\" + TestSetup.module + @"\" + TestSetup.section + @"\ChecksumCompareResult" + "_" + strTime + @".txt";
                else
                    TestSetup.ResultFilePath = TestSetup.checksumStorePath + @"\" + TestSetup.module + @"\" + TestSetup.section + @"\" + TestSetup.table + @"\ChecksumCompareResult" + "_" + strTime + @".txt";
            }
            //Get List of Checksum from Source and Target
            TestSetup.file1Lines = File.ReadLines(TestSetup.SourceChecksumFilePath).ToList();

            List<string> RawFileChecksum = new List<string>();
            List<string> DBFileChecksum = new List<string>();
            string strTemp = string.Empty;
            foreach (string eachLine in TestSetup.file1Lines)
            {
                strTemp = eachLine.Split('|').Last().Trim();
                RawFileChecksum.Add(strTemp);
            }

            TestSetup.file2Lines = File.ReadLines(TestSetup.TargetChecksumFilePath).ToList();
            foreach (string eachLine in TestSetup.file2Lines)
            {
                strTemp = eachLine.Split('|').Last().Trim();
                DBFileChecksum.Add(strTemp);
            }

            //Get Diff between Source and Target Checksum using LINQ
            TestSetup.missingInDB = RawFileChecksum.Except(DBFileChecksum);
            TestSetup.missingInRaw = DBFileChecksum.Except(RawFileChecksum);

            bool isCountSame = false;
            isCountSame = (TestSetup.file1Lines.Count() == TestSetup.file2Lines.Count());

            if (isCountSame)
            {
               Trace.WriteLine("Template(s) File Path : " + TestSetup.TempFilePath);
                Trace.WriteLine("DB Environment : " + TestSetup.environment);
                Trace.WriteLine("DB Table : " + TestSetup.table);
                Trace.WriteLine("DB Query File : " + TestSetup.strSQLFile);
                Trace.WriteLine("------------------------------");
                Trace.WriteLine("Record Count Matched between File and DB: " + TestSetup.file1Lines.Count());
            }
            else
            {
                Trace.WriteLine("Template(s) File Path : " + TestSetup.TempFilePath);
                Trace.WriteLine("DB Environment : " + TestSetup.environment);
                Trace.WriteLine("DB Table : " + TestSetup.table);
                Trace.WriteLine("DB Query File : " + TestSetup.strSQLFile);
                Trace.WriteLine("------------------------------");
                Trace.WriteLine("Record Count in Template File: " + TestSetup.file1Lines.Count());
                Trace.WriteLine("Record Count in DB: " + TestSetup.file2Lines.Count());
                Trace.WriteLine("Difference: " + (TestSetup.file1Lines.Count() - TestSetup.file2Lines.Count()));
            }
            //Validation
            TestResult(TestSetup.ResultFilePath);

        }

    }
}