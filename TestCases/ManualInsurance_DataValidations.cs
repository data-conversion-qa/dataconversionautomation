﻿using System;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace DataConversionAutomation.ManualInsurance
{
    [TestClass]
    public class ManualInsurance_DataValidations
    {
        [TestMethod]
        public void DEV_DataValidation_InDestination()
        {
            TestSetup.module = "ManualInsurance";
            TestSetup.environment = "Dev";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\ManualInsurance\inDestination");
            String query = @"SELECT [TestId]
                                          ,[TestDescription]
                                          ,[ExpectedResult]
                                          ,[ActualResult]
                                          ,[TestStatus]
                                          ,[TestDateTime]
                                          ,[TestMessage]
                                          ,[TestStatusCode]
                                      FROM [dbo].[TestResults]
                                      where TestDateTime > '" + strTime + @"'
                                      order by TestId asc";
            try
            {

                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmss");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + TestSetup.module + "_" + TestSetup.environment + "_inDestination_" + strTime + ".xlsx");
                Methods.PrintRunReport();
            }
            catch (Exception)
            {

                throw;
            }
        }

        [TestMethod]
        public void DEV_DataValidation_PreDestination()
        {
            TestSetup.module = "ManualInsurance";
            TestSetup.environment = "Dev";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\ManualInsurance\preDestination");
            String query = @"SELECT [TestId]
                                          ,[TestDescription]
                                          ,[ExpectedResult]
                                          ,[ActualResult]
                                          ,[TestStatus]
                                          ,[TestDateTime]
                                          ,[TestMessage]
                                          ,[TestStatusCode]
                                      FROM [dbo].[TestResults]
                                      where TestDateTime > '" + strTime + @"'
                                      order by TestId asc";
            try
            {

                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmss");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + TestSetup.module + "_" + TestSetup.environment + "_PreDestination_" + strTime + ".xlsx");
                Methods.PrintRunReport();
            }
            catch (Exception)
            {

                throw;
            }
        }

        [TestMethod]
        public void DEV_DataValidation_Template()
        {
            TestSetup.module = "ManualInsurance";
            TestSetup.environment = "Dev";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\ManualInsurance\Template");
            String query = QueryManager.GetResults() + strTime + @"' order by TestId asc";
            try
            {
                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmss");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + strTime + ".xlsx");
                Methods.PrintRunReport();
            }
            catch (Exception)
            {

                throw;
            }
        }

        [TestMethod]
        public void QA_DataValidation_InDestination()
        {
            TestSetup.module = "ManualInsurance";
            TestSetup.environment = "QA";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\ManualInsurance\inDestination");
            String query = @"SELECT [TestId]
                                          ,[TestDescription]
                                          ,[ExpectedResult]
                                          ,[ActualResult]
                                          ,[TestStatus]
                                          ,[TestDateTime]
                                          ,[TestMessage]
                                          ,[TestStatusCode]
                                      FROM [dbo].[TestResults]
                                      where TestDateTime > '" + strTime + @"'
                                      order by TestId asc";
            try
            {

                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmss");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + TestSetup.module + "_" + TestSetup.environment + "_inDestination_" + strTime + ".xlsx");
                Methods.PrintRunReport();
            }
            catch (Exception)
            {

                throw;
            }
        }

        [TestMethod]
        public void QA_DataValidation_PreDestination()
        {
            TestSetup.module = "ManualInsurance";
            TestSetup.environment = "QA";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\ManualInsurance\preDestination");
            String query = @"SELECT [TestId]
                                          ,[TestDescription]
                                          ,[ExpectedResult]
                                          ,[ActualResult]
                                          ,[TestStatus]
                                          ,[TestDateTime]
                                          ,[TestMessage]
                                          ,[TestStatusCode]
                                      FROM [dbo].[TestResults]
                                      where TestDateTime > '" + strTime + @"'
                                      order by TestId asc";
            try
            {

                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmss");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + TestSetup.module + "_" + TestSetup.environment + "_PreDestination_" + strTime + ".xlsx");
                Methods.PrintRunReport();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [TestMethod]
        public void QA_DataValidation_Template()
        {
            TestSetup.module = "ManualInsurance";
            TestSetup.environment = "QA";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\ManualInsurance\Template");
            String query = QueryManager.GetResults() + strTime + @"' order by TestId asc"; 
            try
            {
                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmss");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + TestSetup.module + "_" + TestSetup.environment + "_Template_" + strTime + ".xlsx");
                Methods.PrintRunReport();
            }
            catch (Exception)
            {

                throw;
            }
        }


        [TestMethod]
        public void PreProd_DataValidation_Template()
        {
            TestSetup.module = "ManualInsurance";
            TestSetup.environment = "Pre-Prod";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\ManualInsurance\Template");
            String query = QueryManager.GetResults() + strTime + @"' order by TestId asc";
            try
            {
                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmss");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + TestSetup.module + "_" + TestSetup.environment + "_Template_" + strTime + ".xlsx");
                Methods.PrintRunReport();
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
