﻿using System;
using System.Data;
using ClosedXML.Excel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataConversionAutomation.DebugSamples
{
    [TestClass]
    public class QueryRunnerTest
    {
        [TestMethod]
        public void AIR_BusinessScenario_Tests()
        {
            TestSetup.module = "AIR";
            TestSetup.environment = "Dev";
            Database.RunDotSQLFile(@"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\SQL Test\AIR\");

            String query = @"SELECT [TestId]
                                          ,[TestDescription]
                                          ,[ExpectedResult]
                                          ,[ActualResult]
                                          ,[TestStatus]
                                          ,[TestDateTime]
                                          ,[TestMessage]
                                          ,[TestStatusCode]
                                      FROM[DataIntegrationStage].[dbo].[TestResults]
                                      where TestDateTime > '2020-05-05 12:50:00.000'
                                      order by TestId asc";

            XLWorkbook wb = new XLWorkbook(@"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\SQL Test\Result.xlsx");
            DataTable dt = Database.PullData(query, "");
            wb.Worksheets.Add(dt, "Result");
        }

        [TestMethod]
        public void AccountMaster_Test()
        {
            TestSetup.module = "AccountMaster";
            Database.RunDotSQLFile(@"C:\Users\kpriyank\Documents\SQL TESTS\");
        }
    }
}
