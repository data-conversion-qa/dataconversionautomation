﻿using System;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Globalization;

namespace DataConversionAutomation.AIR
{
    [TestClass]
    public class AIR_Database_Validations
    {

        [TestMethod]
        public void AIR_BusinessScenario_Tests()
        {
            TestSetup.module = "AIR";
            TestSetup.environment = "Pre-Prod";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\AIR\AIR Business Rule Tests\");
            String query = @"SELECT [TestId]
                                          ,[TestDescription]
                                          ,[ExpectedResult]
                                          ,[ActualResult]
                                          ,[TestStatus]
                                          ,[TestDateTime]
                                          ,[TestMessage]
                                          ,[TestStatusCode]
                                      FROM [DataIntegrationStage].[dbo].[TestResults]
                                      where TestDateTime > '" + strTime + @"'
                                      order by TestId asc";
            try
            {

                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmss");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + TestSetup.module + "_" + TestSetup.environment + "_Result_" + TestSetup.module + "_" + strTime + ".xlsx");
            }
            catch (Exception)
            {

                throw;
            }
        }
    }

}