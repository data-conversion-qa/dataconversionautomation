﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataConversionAutomation.Household
{

        [TestClass]

        public class HouseHold_Checksum_Tests
        {

            [TestMethod]
            public void Compare_Checksum_AccountToHousehold()
            {
            TestSetup.module = "HouseHold";
            TestSetup.environment = "Pre-Prod";
            TestSetup.section = "AccountToHousehold";
            TestSetup.table = TestSetup.section;
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\Household\Batch3\";
            TestSetup.query = QueryManager.GetQuery();
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

            [TestMethod]


            public void Compare_Checksum_MasterHousehold()
            {
            TestSetup.module = "HouseHold";
            TestSetup.environment = "Pre-Prod";
            TestSetup.section = "MasterHousehold";
            TestSetup.table = TestSetup.section;
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\Household\Batch3\";
            TestSetup.query = QueryManager.GetQuery();
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

            [TestMethod]
            public void Compare_Checksum_PolicyToHousehold()
            {
                TestSetup.module = "HouseHold";
                TestSetup.environment = "Pre-Prod";
                TestSetup.section = "PolicyToHousehold";
                TestSetup.table = TestSetup.section;
                TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\Household\18Nov2020\";
                TestSetup.query = QueryManager.GetQuery();
                Template.SetupChecksumComparisionTest();
                Template.ComputeTemplateChecksum();
                Template.ComputeDBChecksum();
                Template.ComputeChecksumDifference();
        }

            [TestMethod]
            public void Compare_Checksum_SecondaryRepToHousehold()
            {
                TestSetup.module = "HouseHold";
                TestSetup.environment = "Pre-Prod";
                TestSetup.section = "SecondaryRepToHousehold";
                TestSetup.table = TestSetup.section;
                TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\Household\Batch3\";
                TestSetup.query = QueryManager.GetQuery();
                Template.SetupChecksumComparisionTest();
                Template.ComputeTemplateChecksum();
                Template.ComputeDBChecksum();
                Template.ComputeChecksumDifference();
        }
        }
    }

