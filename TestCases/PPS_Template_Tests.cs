﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataConversionAutomation.PPS
{
    [TestClass]
    public class PPS_Template_Tests
    {
        [TestMethod]
        public void PPS_TemplateHeaderFooterCheck()
        {
            TestSetup.module = "PPS";
            String FileLocation = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\PPS\New\";
            String Actual = DataConversionAutomation.Template.GetHeaderFooterCount(TestSetup.module, FileLocation);
            if (!TestSetup.Expected.Equals(Actual))
            {
                Trace.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation); ;
                Debug.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation); ;
                Assert.Fail("Expected Header Footer Check Not Matched. FilePath : " + FileLocation);
            }
            else
            {
                Trace.WriteLine("Test Passed");
                Debug.WriteLine("Test Passed");
                //TestContext.WriteLine("Expected Header Footer Check Matched. FilePath : " + FileLocation);
            }
        }
    }
}
