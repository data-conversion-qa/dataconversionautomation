﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataConversionAutomation.AccountMaster
{

    [TestClass]
        public class AccountMaster_DatabaseValidations
    {
        [TestMethod]
        public void NonElectronic_DataValidation_TemplateTables_QA()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "QA";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\AccountMaster\NonElectronic\Template\");
            String query = @"SELECT [TestId]
                                          ,[TestDescription]
                                          ,[ExpectedResult]
                                          ,[ActualResult]
                                          ,[TestStatus]
                                          ,[TestDateTime]
                                          ,[TestMessage]
                                          ,[TestStatusCode]
                                      FROM [dbo].[TestResults]
                                      where TestDateTime > '" + strTime + @"'
                                      order by TestId asc";
            try
            {

                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmss");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + TestSetup.module + "_" + TestSetup.environment + "_Template_" + strTime + ".xlsx");
                Methods.PrintRunReport();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [TestMethod]
        public void NonElectronic_DataValidation_TemplateTables_PreProd()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "Pre-Prod";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\AccountMaster\NonElectronic\Template\");
            String query = @"SELECT [TestId]
                                          ,[TestDescription]
                                          ,[ExpectedResult]
                                          ,[ActualResult]
                                          ,[TestStatus]
                                          ,[TestDateTime]
                                          ,[TestMessage]
                                          ,[TestStatusCode]
                                      FROM [dbo].[TestResults]
                                      where TestDateTime > '" + strTime + @"'
                                      order by TestId asc";
            try
            {

                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmss");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + TestSetup.module + "_" + TestSetup.environment + "_Template_" + strTime + ".xlsx");
                Methods.PrintRunReport();
            }
            catch (Exception)
            {
                throw;
            }
        }

        
        [TestMethod]
        public void NonElectronic_DataValidation_TemplateTables_Dev()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "Dev";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\AccountMaster\NonElectronic\Template\");
            String query = @"SELECT [TestId]
                                          ,[TestDescription]
                                          ,[ExpectedResult]
                                          ,[ActualResult]
                                          ,[TestStatus]
                                          ,[TestDateTime]
                                          ,[TestMessage]
                                          ,[TestStatusCode]
                                      FROM [dbo].[TestResults]
                                      where TestDateTime > '" + strTime + @"'
                                      order by TestId asc";
            try
            {

                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmss");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + TestSetup.module + "_" + TestSetup.environment + "_Template_" + strTime + ".xlsx");
                Methods.PrintRunReport();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [TestMethod]
        public void NonElectronic_DataValidation_PreDestinationTables_PreProd()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "Pre-PROD";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA SQL Scripts Non-Automation\NonElectronicAccounts\Predestination\");
            String query = @"SELECT [TestId]
                                          ,[TestDescription]
                                          ,[ExpectedResult]
                                          ,[ActualResult]
                                          ,[TestStatus]
                                          ,[TestDateTime]
                                          ,[TestMessage]
                                          ,[TestStatusCode]
                                      FROM [dbo].[TestResults]
                                      where TestDateTime > '" + strTime + @"'
                                      order by TestId asc";
            try
            {

                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmss");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + TestSetup.module + "_" + TestSetup.environment + "_PreDestination_" + strTime + ".xlsx");
                Methods.PrintRunReport();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [TestMethod]
        public void NonElectronic_DataValidation_InDestinationTables_PreProd()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "Pre-PROD";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA SQL Scripts Non-Automation\NonElectronicAccounts\NonElectronicAccounts\inDestination\");
            String query = @"SELECT [TestId]
                                          ,[TestDescription]
                                          ,[ExpectedResult]
                                          ,[ActualResult]
                                          ,[TestStatus]
                                          ,[TestDateTime]
                                          ,[TestMessage]
                                          ,[TestStatusCode]
                                      FROM [dbo].[TestResults]
                                      where TestDateTime > '" + strTime + @"'
                                      order by TestId asc";
            try
            {

                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmss");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + TestSetup.module + "_" + TestSetup.environment + "_InDestination_" + strTime + ".xlsx");
                Methods.PrintRunReport();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [TestMethod]
        public void OpenDirect_DataValidation_TemplateTables()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "QA";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\AccountMaster\Open Direct\Template\");
            String query = @"SELECT [TestId]
                                          ,[TestDescription]
                                          ,[ExpectedResult]
                                          ,[ActualResult]
                                          ,[TestStatus]
                                          ,[TestDateTime]
                                          ,[TestMessage]
                                          ,[TestStatusCode]
                                      FROM [dbo].[TestResults]
                                      where TestDateTime > '" + strTime + @"'
                                      order by TestId asc";
            try
            {

                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmss");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + TestSetup.module + "_" + TestSetup.environment + "_Template_" + strTime + ".xlsx");
                Methods.PrintRunReport();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [TestMethod]
        public void QA_Closed_DataValidation_TemplateTables()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "QA";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\AccountMaster\Closed\Template\");
            String query = @"SELECT [TestId]
                                          ,[TestDescription]
                                          ,[ExpectedResult]
                                          ,[ActualResult]
                                          ,[TestStatus]
                                          ,[TestDateTime]
                                          ,[TestMessage]
                                          ,[TestStatusCode]
                                      FROM [dbo].[TestResults]
                                      where TestDateTime > '" + strTime + @"'
                                      order by TestId asc";
            try
            {

                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmss");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + TestSetup.module + "_" + TestSetup.environment + "_Template_" + strTime + ".xlsx"); ;
                Methods.PrintRunReport();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [TestMethod]
        public void PreProd_Closed_DataValidation_TemplateTables()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "Pre-Prod";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\AccountMaster\Closed\Template\");
            String query = @"SELECT [TestId]
                                          ,[TestDescription]
                                          ,[ExpectedResult]
                                          ,[ActualResult]
                                          ,[TestStatus]
                                          ,[TestDateTime]
                                          ,[TestMessage]
                                          ,[TestStatusCode]
                                      FROM [dbo].[TestResults]
                                      where TestDateTime > '" + strTime + @"'
                                      order by TestId asc";
            try
            {

                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmss");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + TestSetup.module + "_" + TestSetup.environment + "_Template_" + strTime + ".xlsx");
                Methods.PrintRunReport();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [TestMethod]
        public void Prod_Closed_DataValidation_TemplateTables()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "Prod";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\AccountMaster\Closed\Template\");
            String query = @"SELECT [TestId]
                                          ,[TestDescription]
                                          ,[ExpectedResult]
                                          ,[ActualResult]
                                          ,[TestStatus]
                                          ,[TestDateTime]
                                          ,[TestMessage]
                                          ,[TestStatusCode]
                                      FROM [dbo].[TestResults]
                                      where TestDateTime > '" + strTime + @"'
                                      order by TestId asc";
            try
            {

                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmss");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + TestSetup.module + "_" + TestSetup.environment + "_Template_" + strTime + ".xlsx");
                Methods.PrintRunReport();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [TestMethod]
        public void PreProd_Open_Accounts_DataValidation_PreDestinationTables()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "Pre-Prod";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\AccountMaster\Open Direct\PreDestination\");
            String query = @"SELECT [TestId]
                                          ,[TestDescription]
                                          ,[ExpectedResult]
                                          ,[ActualResult]
                                          ,[TestStatus]
                                          ,[TestDateTime]
                                          ,[TestMessage]
                                          ,[TestStatusCode]
                                      FROM [dbo].[TestResults]
                                      where TestDateTime > '" + strTime + @"'
                                      order by TestId asc";
            try
            {

                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmss");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + TestSetup.module + "_" + TestSetup.environment + "_PreDestination_" + strTime + ".xlsx");
                Methods.PrintRunReport();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [TestMethod]
        public void Prod_Closed_Accounts_DataValidation_PreDestinationTables()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "Prod";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\AccountMaster\Closed\PreDestination\");
            String query = @"SELECT [TestId]
                                          ,[TestDescription]
                                          ,[ExpectedResult]
                                          ,[ActualResult]
                                          ,[TestStatus]
                                          ,[TestDateTime]
                                          ,[TestMessage]
                                          ,[TestStatusCode]
                                      FROM [dbo].[TestResults]
                                      where TestDateTime > '" + strTime + @"'
                                      order by TestId asc";
            try
            {

                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmss");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + TestSetup.module + "_" + TestSetup.environment + "_PreDestination_" + strTime + ".xlsx");
                Methods.PrintRunReport();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [TestMethod]
        public void QA_Closed_DataValidation_PredestinationTables()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "QA";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\AccountMaster\Closed\PreDestination\");
            String query = @"SELECT [TestId]
                                          ,[TestDescription]
                                          ,[ExpectedResult]
                                          ,[ActualResult]
                                          ,[TestStatus]
                                          ,[TestDateTime]
                                          ,[TestMessage]
                                          ,[TestStatusCode]
                                      FROM [dbo].[TestResults]
                                      where TestDateTime > '" + strTime + @"'
                                      order by TestId asc";
            try
            {
                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmss");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + TestSetup.module + "_" + TestSetup.environment + "_PreDestination_" + strTime + ".xlsx");
                Methods.PrintRunReport();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [TestMethod]
        public void Compare_Schema_InDestinationClosedAccounts()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "PROD";
            DataTable dtTemp;
            String query = @"select DISTINCT Table_Name
                                            from INFORMATION_SCHEMA.COLUMNS
                                            Where Table_Name = 'inDestination_Close_AccountsProprietaryInForce'";
                                        /* Where Table_Name IN ('inDestination_Close_AccountDAZL',
                                        'inDestination_Close_AccountDST',
                                        'inDestination_Close_AccountETrade',
                                        'inDestination_Close_AccountGenworth',
                                        'inDestination_Close_AccountHolderDetails',
                                        'inDestination_Close_AccountIWS',
                                        'inDestination_Close_AccountNSCC',
                                        'inDestination_Close_AccountPershing',
                                        'inDestination_Close_AccountsDTCC',
                                        'inDestination_Close_AccountSEI',
                                        'inDestination_Close_AccountSEIT',
                                        'inDestination_Close_AccountsProprietaryInForce',
                                        'inDestination_Close_AccountsProprietaryMetLife',
                                        'inDestination_Close_AccountTDAmeritrade',
                                        'inDestination_Close_NFNameAddr_Books',
                                        'inDestination_Close_NFNameAddr_Cust',
                                        'inDestination_Close_SchwabAccountInformation')"; */
            DataTable dt = Database.RunQuery(query, "");
            foreach (DataRow row in dt.Rows)
            {
                Console.WriteLine("--- Executing ---");
                foreach (var item in row.ItemArray)
                {
                    Console.Write("Table: "); // Print label.
                    dtTemp = Database.GetTableSchema("AccountMaster." + item.ToString());
                    ExcelDataReader.SaveAsExcel(dtTemp, TestSetup.SQLTestResultPath + "Schema" + "_" + TestSetup.module + "_" + item.ToString() + "_" + TestSetup.environment + ".xlsx");
                }
            }
        }

        [TestMethod]
        public void Compare_Schema_AccountsDBOAccounts()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "PROD";
            TestSetup.strDB = "Accounts";
            DataTable dtTemp;
            String query = @"select DISTINCT Table_Name
                                            from INFORMATION_SCHEMA.COLUMNS
                                            Where Table_Name = 'AccountsProprietaryInForce'";
                                        /* Where Table_Name in (
	                                        'AccountETrade',
	                                        'AccountGENWORTH',
                                        'AccountACD',
                                        'AccountCURIAN',
                                        'AccountDAZL',
                                        'AccountDST',
                                        'AccountETrade',
                                        'AccountGENWORTH',
                                        'AccountIWS',
                                        'AccountNSCC',
                                        'AccountPERSHING',
                                        'AccountsDTCC',
                                        'AccountSEI',
                                        'AccountSEIT')";*/
            DataTable dt = Database.RunQuery(query, "");
            foreach (DataRow row in dt.Rows)
            {
                Console.WriteLine("--- Executing ---");
                foreach (var item in row.ItemArray)
                {
                    Console.Write("Table: "); // Print label.
                    dtTemp = Database.GetTableSchema("dbo." + item.ToString());
                    ExcelDataReader.SaveAsExcel(dtTemp, TestSetup.SQLTestResultPath + "Schema" + "_" + TestSetup.module + "_" + item.ToString() + "_" + TestSetup.environment + ".xlsx");
                }
            }
        }
        

            [TestMethod]
        public void OpenDirect_DataValidation_PreDestinationTables()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "QA";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\AccountMaster\Open Direct\PreDestination\");
            String query = @"SELECT [TestId]
                                          ,[TestDescription]
                                          ,[ExpectedResult]
                                          ,[ActualResult]
                                          ,[TestStatus]
                                          ,[TestDateTime]
                                          ,[TestMessage]
                                          ,[TestStatusCode]
                                      FROM [dbo].[TestResults]
                                      where TestDateTime > '" + strTime + @"'
                                      order by TestId asc";
            try
            {

                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmss");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + TestSetup.module + "_" + TestSetup.environment + "_PreDestination_" + strTime + ".xlsx");
                Methods.PrintRunReport();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
 }

