﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataConversionAutomation.Household
{
    [TestClass]
    public class Household_Template_Tests
    {
        [TestMethod]
        public void HouseHold_TemplateHeaderFooterCheck()
        {
            TestSetup.module = "HouseHold";
            String FileLocation = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\Household\18Nov2020\";
            String Actual = DataConversionAutomation.Template.GetHeaderFooterCount(TestSetup.module, FileLocation);
            if (!TestSetup.Expected.Equals(Actual))
            {
                Trace.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation); ;
                Debug.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation); ;
                Assert.Fail("Expected Header Footer Check Not Matched. FilePath : " + FileLocation);
            }
            else
            {
                Trace.WriteLine("Test Passed");
                Debug.WriteLine("Test Passed");
                //TestContext.WriteLine("Expected Header Footer Check Matched. FilePath : " + FileLocation);
            }
        }
    }
}
