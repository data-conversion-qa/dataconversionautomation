﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using System.Globalization;

namespace DataConversionAutomation.TestCases
{
    [TestClass]
    public class AccountMaster_TemplateTests
    {
        [TestMethod]
        public void AccountMaster_Template_Tests()
        {
            TestSetup.module = "AM";
            TestSetup.environment = "Dev";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Account Master\Open Direct\Template");
            String query = @"SELECT [TestId]
                                          ,[TestDescription]
                                          ,[ExpectedResult]
                                          ,[ActualResult]
                                          ,[TestStatus]
                                          ,[TestDateTime]
                                          ,[TestMessage]
                                          ,[TestStatusCode]
                                      FROM [dbo].[TestResults]
                                      where TestDateTime > '" + strTime + @"'
                                      order by TestId asc";
            try
            {
            
                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmssfff");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + TestSetup.module + "_" + TestSetup.environment + "_Template_" + strTime + ".xlsx");
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
