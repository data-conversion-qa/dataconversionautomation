﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace DataConversionAutomation.WorkBenchTests
{
    internal class WorkBench
    {
        private string v;

        public WorkBench(string v)
        {
            this.v = v;
        }

        [TestClass]
        public class WorkBenchTests
        {
            [TestMethod]
            public void ListComparer()
            {

                TestSetup.SourceChecksumFilePath = @"C:\DataConversionAutomation\ChecksumStore\Test\Test\WorkBench\sourceChecksum.txt";
                TestSetup.TargetChecksumFilePath = @"C:\DataConversionAutomation\ChecksumStore\Test\Test\WorkBench\targetChecksum.txt";
                Template.ComputeChecksumDifference();
                var strcomp = new MultiSetComparer<string>(StringComparer.OrdinalIgnoreCase);
                Console.WriteLine(strcomp.Equals(TestSetup.missingInDB, TestSetup.missingInRaw));
            }
        }
    }
}