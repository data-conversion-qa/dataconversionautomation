﻿using System;
using System.Data;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataConversionAutomation.Household
{
    [TestClass]
    public class Household_DatabaseValidations
    {
        [TestMethod]
        public void QA_Household_Database_Tests()
        {
            TestSetup.module = "HouseHold";
            TestSetup.environment = "QA";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\Household\");
            String query = @"SELECT [TestId]
                                          ,[TestDescription]
                                          ,[ExpectedResult]
                                          ,[ActualResult]
                                          ,[TestStatus]
                                          ,[TestDateTime]
                                          ,[TestMessage]
                                          ,[TestStatusCode]
                                      FROM [dbo].[TestResults]
                                      where TestDateTime > '" + strTime + @"'
                                      order by TestId asc";
            try
            {

                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmssfff");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + strTime + ".xlsx");
                Methods.PrintRunReport();
            }
            catch (Exception)
            {

                throw;
            }
        }

        [TestMethod]
            public void DEV_Household_Database_Tests()
            {
                TestSetup.module = "HouseHold";
                TestSetup.environment = "Dev";
                String strTime = Methods.GetCurrentTime();
                Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\Household\");
                String query = @"SELECT [TestId]
                                          ,[TestDescription]
                                          ,[ExpectedResult]
                                          ,[ActualResult]
                                          ,[TestStatus]
                                          ,[TestDateTime]
                                          ,[TestMessage]
                                          ,[TestStatusCode]
                                      FROM [dbo].[TestResults]
                                      where TestDateTime > '" + strTime + @"'
                                      order by TestId asc";
                try
                {

                    DataTable dt = Database.RunQuery(query, "");
                    strTime = Methods.GetCurrentTime("yyyyMMddHHmmssfff");
                    ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + strTime + ".xlsx");
                    Methods.PrintRunReport();
            }
                catch (Exception)
                {

                    throw;
                }
            }

        [TestMethod]
        public void PreProd_Household_Database_Tests()
        {
            TestSetup.module = "HouseHold";
            TestSetup.environment = "Pre-Prod";
            String strTime = Methods.GetCurrentTime();
            Database.RunDotSQLFile(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\Household\");
            String query = @"SELECT [TestId]
                                          ,[TestDescription]
                                          ,[ExpectedResult]
                                          ,[ActualResult]
                                          ,[TestStatus]
                                          ,[TestDateTime]
                                          ,[TestMessage]
                                          ,[TestStatusCode]
                                      FROM [dbo].[TestResults]
                                      where TestDateTime > '" + strTime + @"'
                                      order by TestId asc";
            try
            {

                DataTable dt = Database.RunQuery(query, "");
                strTime = Methods.GetCurrentTime("yyyyMMddHHmmssfff");
                ExcelDataReader.SaveAsExcel(dt, TestSetup.SQLTestResultPath + "Result" + "_" + strTime + ".xlsx");
                Methods.PrintRunReport();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
    }