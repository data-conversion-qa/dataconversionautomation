﻿using System;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Globalization;

namespace DataConversionAutomation.AIR
{
    [TestClass]
    public class AIR_TemplateVerification
    {
        private TestContext testContextInstance;
        /// <summary>
        ///  Gets or sets the test context which provides
        ///  information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }
        String Expected = "Pass";

        [TestMethod]
        public void AIRv6TemplateHeaderFooterCheck()
        {
            TestSetup.module = "AIR";
            String FileLocation = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AIR\MML_ADVISOR_DATA_V6\";
            String Actual = DataConversionAutomation.Template.GetHeaderFooterCount(TestSetup.module, FileLocation);
            if (!Expected.Equals(Actual))
            {
                Trace.WriteLine("Test Failed");
                Debug.WriteLine("Test Failed");
                TestContext.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation);
                Assert.Fail("Expected Header Footer Check Not Matched. FilePath : " + FileLocation);
            }
            else
            {
                Trace.WriteLine("Test Passed");
                Debug.WriteLine("Test Passed");
                TestContext.WriteLine("Expected Header Footer Check Matched. FilePath : " + FileLocation);
            }
        }

        [TestMethod]
        public void AIRv7TemplateHeaderFooterCheck()
        {
            TestSetup.module = "AIR";
            String FileLocation = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AIR\Batch7\";
            String Actual = DataConversionAutomation.Template.GetHeaderFooterCount(TestSetup.module, FileLocation);
            if (!Expected.Equals(Actual))
            {
                Trace.WriteLine("Test Failed");
                Debug.WriteLine("Test Failed");
                TestContext.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation);
                Assert.Fail("Expected Header Footer Check Not Matched. FilePath : " + FileLocation);
            }
            else
            {
                Trace.WriteLine("Test Passed");
                Debug.WriteLine("Test Passed");
                TestContext.WriteLine("Expected Header Footer Check Matched. FilePath : " + FileLocation);
            }
        }

        [TestMethod]
        public void AIRv3TemplateHeaderFooterCheck()
        {
            TestSetup.module = "AIR";
            String FileLocation = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\AIR\Batch5";
            String Actual = DataConversionAutomation.Template.GetHeaderFooterCount(TestSetup.module, FileLocation);
            if (!Expected.Equals(Actual))
            {
                Trace.WriteLine("Test Failed");
                Debug.WriteLine("Test Failed");
                TestContext.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation);
                Assert.Fail("Expected Header Footer Check Not Matched. FilePath : " + FileLocation);
            }
            else
            {
                Trace.WriteLine("Test Passed");
                Debug.WriteLine("Test Passed");
                TestContext.WriteLine("Expected Header Footer Check Matched. FilePath : " + FileLocation);
            }
        }

        [TestMethod]
        public void AIRv4TemplateHeaderFooterCheck()
        {
            TestSetup.module = "AIR";
            String FileLocation = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AIR\MML_ADVISOR_DATA_V5\";
            String Actual = DataConversionAutomation.Template.GetHeaderFooterCount(TestSetup.module, FileLocation);
            if (!Expected.Equals(Actual))
            {
                Trace.WriteLine("Test Failed");
                Debug.WriteLine("Test Failed");
                TestContext.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation);
                Assert.Fail("Expected Header Footer Check Not Matched. FilePath : " + FileLocation);
            }
            else
            {
                Trace.WriteLine("Test Passed");
                Debug.WriteLine("Test Passed");
                TestContext.WriteLine("Expected Header Footer Check Matched. FilePath : " + FileLocation);
            }
        }

        [TestMethod]
        public void AIRv4TemplateNameFormatCheck()
        {
            TestSetup.module = "AIR";
            String FileLocation = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AIR\MML_ADVISOR_DATA_V5";
            String Actual = Template.CheckFileNameFormat(TestSetup.module, FileLocation);
            if (!Expected.Equals(Actual))
            {
                Trace.WriteLine("Test Failed");
                Debug.WriteLine("Test Failed");
                TestContext.WriteLine("Expected Format Not Matched.FilePath : " + FileLocation);
                Assert.Fail("Expected Format Not Matched. FilePath : " + FileLocation);
            }
            else
            {
                Trace.WriteLine("Test Passed");
                Debug.WriteLine("Test Passed");
                TestContext.WriteLine("Expected Format Matched. FilePath : " + FileLocation);
                //Console.WriteLine("test console");
            }

        }

        [TestMethod]
        public void AIRv5TemplateNameFormatCheck()
        {
            TestSetup.module = "AIR";
            String FileLocation = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AIR\MML_ADVISOR_DATA_V5\";
            String Actual = Template.CheckFileNameFormat(TestSetup.module, FileLocation);
            if (!Expected.Equals(Actual))
            {
                Trace.WriteLine("Test Failed");
                Debug.WriteLine("Test Failed");
                TestContext.WriteLine("Expected Format Not Matched.FilePath : " + FileLocation);
                Assert.Fail("Expected Format Not Matched. FilePath : " + FileLocation);
            }
            else
            {
                Trace.WriteLine("Test Passed");
                Debug.WriteLine("Test Passed");
                TestContext.WriteLine("Expected Format Matched. FilePath : " + FileLocation);
                //Console.WriteLine("test console");
            }

        }

        [TestMethod]
        public void AIRv3TemplateNameFormatCheck()
        {
            TestSetup.module = "AIR";
            String FileLocation = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\AIR\Batch5";
            String Actual = Template.CheckFileNameFormat(TestSetup.module, FileLocation);
            if (!Expected.Equals(Actual))
            {
                Trace.WriteLine("Test Failed");
                Debug.WriteLine("Test Failed");
                TestContext.WriteLine("Expected Format Not Matched.FilePath : " + FileLocation);
                Assert.Fail("Expected Format Not Matched. FilePath : " + FileLocation);
            }
            else
            {
                Trace.WriteLine("Test Passed");
                Debug.WriteLine("Test Passed");
                TestContext.WriteLine("Expected Format Matched. FilePath : " + FileLocation);
                //Console.WriteLine("test console");
            }

        }

        public string WriteErrorToFile(System.IO.TextWriter textwriter, string errorMessage)
        {
            textwriter.WriteLine(errorMessage);
            return errorMessage;
        }

        public void PrintCollection(List<string> list)
        {
            foreach (string i in list)
            {
                TestContext.WriteLine(i.ToString());
            }
        }
    }

    [TestClass]
    public class ChecksumTest_AIR_Tables
    {
        [TestMethod]
        public void Compare_Checksum_Address()
        {
            TestSetup.module = "AIR";
            TestSetup.environment = "Dev";
            TestSetup.section = "Address";
            TestSetup.table = TestSetup.section;
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AIR\MML_ADVISOR_DATA_V5";
            TestSetup.query = QueryManager.GetQuery();
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Compare_Checksum_Contact()
        {
            TestSetup.module = "AIR";
            TestSetup.environment = "Dev";
            TestSetup.section = "Contact";
            TestSetup.table = TestSetup.section;
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AIR\MML_ADVISOR_DATA_V5\";
            TestSetup.query = QueryManager.GetQuery();
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Compare_Checksum_Contact_Relationship()
        {
            TestSetup.module = "AIR";
            TestSetup.environment = "Dev";
            TestSetup.section = "Contact_Relationship";
            TestSetup.table = TestSetup.section;
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AIR\MML_ADVISOR_DATA_V5\";
            TestSetup.query = QueryManager.GetQuery();
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Compare_Checksum_Plan()
        {
            TestSetup.module = "AIR";
            TestSetup.environment = "Dev";
            TestSetup.section = "Plan";
            TestSetup.table = TestSetup.section;
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AIR\MML_ADVISOR_DATA_V5\";
            TestSetup.query = QueryManager.GetQuery();
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Compare_Checksum_PlanIDTranslation()
        {
            TestSetup.module = "AIR";
            TestSetup.environment = "Dev";
            TestSetup.section = "Plan_ID_Translation";
            TestSetup.table = TestSetup.section;
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AIR\MML_ADVISOR_DATA_V5\";
            TestSetup.query = QueryManager.GetQuery();
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Compare_Checksum_Prefix()
        {
            TestSetup.module = "AIR";
            TestSetup.environment = "Dev";
            TestSetup.section = "Prefix";
            TestSetup.table = TestSetup.section;
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AIR\MML_ADVISOR_DATA_V5\";
            TestSetup.query = QueryManager.GetQuery();
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Compare_Checksum_DRP()
        {
            TestSetup.module = "AIR";
            TestSetup.environment = "Dev";
            TestSetup.section = "DRP";
            TestSetup.table = TestSetup.section;
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AIR\MML_ADVISOR_DATA_V5\";
            TestSetup.query = @"SELECT [ContactIdentifierReference]
                                                  ,[DRPQuestionCode]
                                                  ,[DRPStatus]
	                                            ,NULLIF(CONVERT(varchar,[DRPEventDate], 23),'1753-01-01') as[DRPEventDate]
	                                            ,NULLIF(CONVERT(varchar,[DRPReportedDate], 23),'1753-01-01') as[DRPReportedDate]
	                                            ,NULLIF(CONVERT(varchar,[DRPResolutionDate], 23),'1753-01-01') as[DRPResolutionDate]
                                                  ,[IsDRPActive]
                                              FROM [DataIntegrationStage].[Air].[Staging_DRP]";
            
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Compare_Checksum_FINRA_License()
        {
            TestSetup.module = "AIR";
            TestSetup.environment = "Dev";
            TestSetup.section = "FINRA_License";
            TestSetup.table = TestSetup.section;
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AIR\MML_ADVISOR_DATA_V5\";
            TestSetup.query = QueryManager.GetQuery();
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Compare_Checksum_Firm_Training()
        {
            TestSetup.module = "AIR";
            TestSetup.environment = "Dev";
            TestSetup.section = "Firm_Training";
            TestSetup.table = TestSetup.section;
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AIR\MML_ADVISOR_DATA_V5\";
            TestSetup.query = QueryManager.GetQuery();
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Compare_Checksum_InsuranceAppointments()
        {
            TestSetup.module = "AIR";
            TestSetup.environment = "Dev";
            TestSetup.section = "Insurance_Appointments";
            TestSetup.table = TestSetup.section;
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AIR\MML_ADVISOR_DATA_V5\";
            TestSetup.query = QueryManager.GetQuery();
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Compare_Checksum_Insurance_License()
        {
            TestSetup.module = "AIR";
            TestSetup.environment = "Dev";
            TestSetup.section = "Insurance_License";
            TestSetup.table = TestSetup.section;
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AIR\MML_ADVISOR_DATA_V5\";
            TestSetup.query = QueryManager.GetQuery();
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }
        [TestMethod]
        public void Compare_Checksum_OBA()
        {
            TestSetup.module = "AIR";
            TestSetup.environment = "Dev";
            TestSetup.section = "OTHER_BUSINESS";
            TestSetup.table = TestSetup.section;
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AIR\MML_ADVISOR_DATA_V5\";
            TestSetup.query = QueryManager.GetQuery();
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Compare_Checksum_Provisioning()
        {
            TestSetup.module = "AIR";
            TestSetup.environment = "Dev";
            TestSetup.section = "Provisioning";
            TestSetup.table = TestSetup.section;
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AIR\MML_ADVISOR_DATA_V5";
            TestSetup.query = @"SELECT 
                                          [ContactIdentifier]
                                          ,[LoginId]
                                          ,[WealthscapeId]
                                          ,[CRMPrimaryBusUnit]
                                          ,[CRMSecondaryBusUnit]
                                          ,[CasperRoles]
                                          ,[Status]
                                          ,[UPN]
                                      FROM [DataIntegrationStage].[air].[Staging_Provisioning]";
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Compare_Checksum_Regulatory_Training()
        {
            TestSetup.module = "AIR";
            TestSetup.environment = "Dev";
            TestSetup.section = "Regulatory_Training";
            TestSetup.table = TestSetup.section;
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AIR\MML_ADVISOR_DATA_V5";
            TestSetup.query = @"SELECT [ContactIdentifierReference]
                                        ,[CERegistrationStatus]
                                        ,NULLIF(CONVERT(varchar,[CECompletedDate], 23),'1753-01-01') as [CECompletedDate]
                                        ,NULLIF(CONVERT(varchar,[CEAnniversaryDate], 23),'1753-01-01') as [CEAnniversaryDate]
                                        ,NULLIF(CONVERT(varchar,[CECutoffDate], 23),'1753-01-01') as [CECutoffDate]
                                    FROM [DataIntegrationStage].[air].[Staging_Regulatory_Training]";
            //Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        /*  public void Compare_DataTable_Prefix()
          {
              TestSetup.module = "AIR";
              TestSetup.environment = "Dev";
              String filepath = TestSetup.strPrefixTemplatePath;
              String strHeader = "Prefix";
              DataTable dt1 = Template.ParseExtractToDataTable(filepath, strHeader);
              //dt1.Columns.RemoveAt(1);

              using (StreamWriter sw = new StreamWriter(@"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\ChecksumStore\AIR\Prefix\RawData.txt"))
              {
                  foreach (DataRow row in dt1.Rows)
                      sw.WriteLine(String.Join(",", row.ItemArray));
              }
              string query = @"SELECT 
                                            [PlanId]
                                            ,[AccountPrefix]
                                            ,[AccountPrefixDescription]
                                        FROM [DataIntegrationStage].[air].[Staging_Prefix]";


              String strConn = Database.GetConnectionString("AIR", TestSetup.environment);
              DataTable dt2 = Database.RunQuery(query, strConn);

              using (StreamWriter sw = new StreamWriter(@"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\ChecksumStore\AIR\Prefix\DBData.txt"))
              {
                  foreach (DataRow row in dt2.Rows)
                      sw.WriteLine(String.Join(",", row.ItemArray));
              }
              Methods.AreTablesTheSame(dt1, dt2);

          }

          [TestMethod]
          public void Compare_Checksum_Provisioning()
          {
              TestSetup.module = "AIR";
              TestSetup.environment = "Dev";
              TestSetup.section = "Provisioning";
              TestSetup.table = TestSetup.section;
              TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AIR\Batch6";
              TestSetup.query = @"SELECT 
                                            [ContactIdentifier]
                                            ,[LoginId]
                                            ,[WealthscapeId]
                                            ,[CRMPrimaryBusUnit]
                                            ,[CRMSecondaryBusUnit]
                                            ,[CasperRoles]
                                            ,[Status]
                                            ,[UPN]
                                        FROM [DataIntegrationStage].[air].[Staging_Provisioning]";
              //Template.SetupChecksumComparisionTest();
              Template.ComputeTemplateChecksum();
              Template.ComputeDBChecksum();
              Template.ComputeChecksumDifference();
          }

          [TestMethod]
          public void Compare_Checksum_Regulatory_Training()
          {
              TestSetup.module = "AIR";
              TestSetup.environment = "Dev";
              TestSetup.section = "Regulatory_Training";
              TestSetup.table = TestSetup.section;
              TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AIR\Batch6";
              TestSetup.query = @"SELECT [ContactIdentifierReference]
                                        ,[CERegistrationStatus]
                                        ,NULLIF(CONVERT(varchar,[CECompletedDate], 23),'1753-01-01') as [CECompletedDate]
                                        ,NULLIF(CONVERT(varchar,[CEAnniversaryDate], 23),'1753-01-01') as [CEAnniversaryDate]
                                        ,NULLIF(CONVERT(varchar,[CECutoffDate], 23),'1753-01-01') as [CECutoffDate]
                                    FROM [DataIntegrationStage].[air].[Staging_Regulatory_Training]";
              //Template.SetupChecksumComparisionTest();
              Template.ComputeTemplateChecksum();
              Template.ComputeDBChecksum();
              Template.ComputeChecksumDifference();
          }

          public void Compare_DataTable_Provisioning()
          {
              TestSetup.module = "AIR";
              String filepath = TestSetup.strProvisioningTemplatePath;
              String strHeader = "Provisioning";
              DataTable dt1 = Template.ParseExtractToDataTable(filepath, strHeader);
              //dt1.Columns.RemoveAt(1);

              using (StreamWriter sw = new StreamWriter(@"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\ChecksumStore\AIR\Provisioning\RawData.txt"))
              {
                  foreach (DataRow row in dt1.Rows)
                      sw.WriteLine(String.Join(",", row.ItemArray));
              }
              string query = @"SELECT 
                                            [ContactIdentifier]
                                            ,[LoginId]
                                            ,[WealthscapeId]
                                            ,[CRMPrimaryBusUnit]
                                            ,[CRMSecondaryBusUnit]
                                            ,[CasperRoles]
                                            ,[Status]
                                        FROM [DataIntegrationStage].[air].[Staging_Provisioning]";

              String strConn = Database.GetConnectionString("AIR", TestSetup.environment);
              DataTable dt2 = Database.RunQuery(query, strConn);

              using (StreamWriter sw = new StreamWriter(@"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\ChecksumStore\AIR\Provisioning\DBData.txt"))
              {
                  foreach (DataRow row in dt2.Rows)
                      sw.WriteLine(String.Join(",", row.ItemArray));
              }
              Methods.AreTablesTheSame(dt1, dt2);

          }

          public void Compare_DataTable_Plan()
          {
              TestSetup.module = "AIR";
              TestSetup.environment = "Dev";
              String filepath = TestSetup.strPlanTemplatePath;
              String strHeader = "Plan";
              DataTable dt1 = Template.ParseExtractToDataTable(filepath, strHeader);
              //dt1.Columns.RemoveAt(1);

              using (StreamWriter sw = new StreamWriter(@"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\ChecksumStore\AIR\Plan\RawData.txt"))
              {
                  foreach (DataRow row in dt1.Rows)
                      sw.WriteLine(String.Join(",", row.ItemArray));
              }
              string query = @"SELECT 
                                          [ContactIdentifierReference]
                                          ,[PlanIdTypecode]
                                          ,[Plan_Id]
                                          ,[MainAdvisorOnThePlan]
                                          ,(CASE [SplitPercentage] WHEN '0.00' THEN '.00' ELSE CONVERT(varchar,CONVERT(decimal(5,2),[SplitPercentage])) END ) as SplitPercentage
                                          ,[CommissionTypeCode]
                                          ,NULLIF(CONVERT(varchar,[PlanIdTerminationDate], 23),'1753-01-01') as PlanIdTerminationDate
                                          ,[ServiceModelsForThePlanId]
                                          ,[AUMDiscount]
                                          ,NULLIF(CONVERT(varchar,[MSAExclusionDate], 23),'1753-01-01') as MSAExclusionDate
                                          ,NULLIF(CONVERT(varchar,[ProgramElectionDate], 23),'1753-01-01') as ProgramElectionDate
                                          ,[12B1HousePaymentCode]
                                          ,[ExcludedFromMSA]
                                          FROM [DataIntegrationStage].[air].[Staging_Plan]";

              String strConn = Database.GetConnectionString("AIR", TestSetup.environment);
              DataTable dt2 = Database.RunQuery(query, strConn);

              using (StreamWriter sw = new StreamWriter(@"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\ChecksumStore\AIR\Plan\DBData.txt"))
              {
                  foreach (DataRow row in dt2.Rows)
                      sw.WriteLine(String.Join(",", row.ItemArray));
              }
              Methods.AreTablesTheSame(dt1, dt2);

          }

          public void Compare_DataTable_Contact()
          {
              TestSetup.module = "AIR";
              String filepath = TestSetup.strContactTemplatePath;
              String strHeader = "Contact";
              DataTable dt1 = Template.ParseExtractToDataTable(filepath, strHeader);
              //dt1.Columns.RemoveAt();

              using (StreamWriter sw = new StreamWriter(@"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\ChecksumStore\AIR\Contact\RawData.txt"))
              {
                  foreach (DataRow row in dt1.Rows)
                      sw.WriteLine(String.Join(",", row.ItemArray));
              }
              string query = @"SELECT 
                                          [ContactIdentifier]
                                          ,[ContactServiceModels]
                                          ,[FirstName]
                                          ,[LastName]
                                          ,[CompanyName]
                                          ,[ContactType]
                                          ,[ContactStatus]
                                          ,[EmailAddress]
                                          ,[NamePrefix]
                                          ,[NameSuffix]
                                          ,[PublicName]
                                          ,[SSNumber]
                                          ,[IsInternalEmployee]
                                          ,[FaxNumber]
                                          ,[PhoneNumber]
                                          ,[PhoneExtension]
                                          ,NULLIF(CONVERT(varchar,[BirthDate], 23),'1753-01-01') as BirthDate
                                          ,[CellPhoneNumber]
                                          ,[Designation]
                                          ,[AlternateEmailAddress]
                                          ,[FormerName]
                                          ,[GoesBy]
                                          ,[HomePhone]
                                          ,[MiddleName]
                                          ,[PlaceOfBirth]
                                          ,[WebAddress]
                                          ,[Agency]
                                          ,[Region]
                                          ,[AdvisorId]
                                          ,[PracticeAnalyticsPeerGroups]
                                          ,NULLIF(CONVERT(varchar,[ContractReceivedDate],23),'1753-01-01') as ContractReceivedDate
                                          ,NULLIF(CONVERT(varchar,[MSAExclusionDate], 23),'1753-01-01') as MSAExclusionDate
                                          ,NULLIF(CONVERT(varchar,[StartDate], 23),'1753-01-01') as StartDate
                                          ,NULLIF(CONVERT(varchar,[TerminationDate], 23),'1753-01-01') as TerminationDate
                                          ,[NationalFuturesAssocID]
                                          ,[BankRepFlag]
                                          ,[RepBankName]
                                          ,[CRDNumber]
                                          ,[NationalProducerNumber]

                                            FROM [DataIntegrationStage].[air].[Staging_Contact]";

              String strConn = Database.GetConnectionString("AIR", TestSetup.environment);
              DataTable dt2 = Database.RunQuery(query, strConn);

              using (StreamWriter sw = new StreamWriter(@"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\ChecksumStore\AIR\Contact\DBData.txt"))
              {
                  foreach (DataRow row in dt2.Rows)
                      sw.WriteLine(String.Join(",", row.ItemArray));
              }
              Methods.AreTablesTheSame(dt1, dt2);

          }

          public void Compare_DataTable_DRP()
          {
              TestSetup.module = "AIR";
              TestSetup.environment = "Dev";
              String section = "DRP";
              DirectoryInfo d = new DirectoryInfo(@"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\AIR\Batch4\");
              FileInfo[] Files = d.GetFiles("*_DRP_*.txt"); //Getting Text files
              String fullFilePath = string.Empty;
              DataSet dsDRP = new DataSet();

              foreach (var file in Files)
              {
                  fullFilePath = file.FullName;
                  dsDRP.Tables.Add(Template.ParseExtractToDataTable(fullFilePath, section));
              }
              DataTable dtFinal = Methods.MergeDataSet(dsDRP);
              dtFinal.Columns.RemoveAt(6);
              using (StreamWriter sw = new StreamWriter(@"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\ChecksumStore\AIR\DRP\RawData.txt"))
              {
                  foreach (DataRow row in dtFinal.Rows)
                      sw.WriteLine(String.Join(",", row.ItemArray));
              }
              //dtFinal.Clear();
              string query = @"SELECT 
                                           SS.[ContactIdentifierReference]
                                           ,STUFF((SELECT ',' + US.[DRPQuestionCode]
                                                FROM [DataIntegrationStage].[air].[Staging_DRP]  US
                                                WHERE [DRPQuestionCode] <> ''
                                                AND (US.[ContactIdentifierReference] = SS.[ContactIdentifierReference])
                                                AND ((US.[DRPEventDate] = SS.[DRPEventDate]) OR [DRPEventDate] is null)
                                                AND ((US.[DRPReportedDate] = SS.[DRPReportedDate]) OR [DRPReportedDate] is null)
                                                AND ((US.[DRPResolutionDate]= SS.[DRPResolutionDate]) OR [DRPResolutionDate] is null)
                                                AND ((US.[IsDRPActive] = SS.[IsDRPActive]) OR [IsDRPActive] is null)
                                                FOR XML PATH('')), 1, 1, '') [DRPQuestionCode]
                                          ,NULLIF(SS.[DRPStatus],'') as [DRPStatus]
                                          ,NULLIF(CONVERT(varchar,[DRPEventDate], 23),'1753-01-01') as[DRPEventDate]
                                          ,NULLIF(CONVERT(varchar,[DRPReportedDate], 23),'1753-01-01') as[DRPReportedDate]
                                          ,NULLIF(CONVERT(varchar,[DRPResolutionDate], 23),'1753-01-01') as[DRPResolutionDate]
                                         -- ,(CASE [IsDRPActive] WHEN 1 THEN '1' ELSE '0' END) as [IsDRPActive]
                                      FROM [DataIntegrationStage].[air].[Staging_DRP]  SS
                                      GROUP BY SS.[ContactIdentifierReference], SS.[DRPStatus], SS.[DRPEventDate], SS.[DRPReportedDate], [DRPResolutionDate], [IsDRPActive]
                                      ORDER BY 1 ";

              String strConn = Database.GetConnectionString(TestSetup.module, TestSetup.environment);
              DataTable dt2 = Database.RunQuery(query, strConn);

              using (StreamWriter sw = new StreamWriter(@"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\ChecksumStore\AIR\DRP\DBData.txt"))
              {
                  foreach (DataRow row in dt2.Rows)
                      sw.WriteLine(String.Join(",", row.ItemArray));
              }
              Methods.AreTablesTheSame(dtFinal, dt2);

          }

          public void Compare_DataTable_FirmTraining()
          {
              TestSetup.module = "AIR";
              String filepath = TestSetup.strFirmTrainingTemplatePath;
              String strHeader = "Firm_Training";
              DataTable dt1 = Template.ParseExtractToDataTable(filepath, strHeader);
              //dt1.Columns.RemoveAt(1);

              using (StreamWriter sw = new StreamWriter(@"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\ChecksumStore\AIR\Firm_Training\RawData.txt"))
              {
                  foreach (DataRow row in dt1.Rows)
                      sw.WriteLine(String.Join(",", row.ItemArray));
              }
              string query = @"SELECT 
                                            [ContactIdentifierReference]
                                            ,[YearCECourseApplied]
                                            ,[IsCECompleteForTheYear]
                                            ,[CECourse]
                                            ,[CESource]
                                            ,[CEType]
                                            ,NULLIF(Convert(varchar,[CECompletedDate], 120),'1753-01-01') as[CECompletedDate]
                                            ,NULLIF(Convert(varchar,[CEGoodUntilDate], 120),'1753-01-01') as[CEGoodUntilDate]
                                            ,[CECreditsEarned]
                                        FROM [DataIntegrationStage].[air].[Staging_Firm_Training]";

              String strConn = Database.GetConnectionString(TestSetup.module, TestSetup.environment);
              DataTable dt2 = Database.RunQuery(query, strConn);

              using (StreamWriter sw = new StreamWriter(@"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\ChecksumStore\AIR\Firm_Training\DBData.txt"))
              {
                  foreach (DataRow row in dt2.Rows)
                      sw.WriteLine(String.Join(",", row.ItemArray));
              }
              Methods.AreTablesTheSame(dt1, dt2);
          }

          public void Compare_DataTable_InsuranceAppointments()
          {
              TestSetup.module = "AIR";
              String filepath = TestSetup.strInsuranceAppointmentsTemplatePath;
              String strHeader = "Insurance_Appointments";
              DataTable dt1 = Template.ParseExtractToDataTable(filepath, strHeader);
              //dt1.Columns.RemoveAt(1);

              using (StreamWriter sw = new StreamWriter(@"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\ChecksumStore\AIR\Insurance_Appointments\RawData.txt"))
              {
                  foreach (DataRow row in dt1.Rows)
                      sw.WriteLine(String.Join(",", row.ItemArray));
              }
              string query = @"SELECT [ContactIdentifierReference]
                                            ,NULLIF(Convert(varchar,[AppointmentDate], 23),'1753-01-01') as[AppointmentDate]
                                            ,NULLIF(Convert(varchar,[AppointmentSubmittedDate], 23),'1753-01-01') as[AppointmentSubmittedDate]
                                            ,NULLIF(Convert(varchar,[AppointmentTerminationDate], 23),'1753-01-01') as[AppointmentTerminationDate]
                                            ,[AppointmentType]
                                            ,[AppointmentCompanyName]
                                            ,[InsuranceAppointmentLine]
                                            ,[AppointmentState]
                                        FROM [DataIntegrationStage].[air].[Staging_Insurance_Appointments]";

              String strConn = Database.GetConnectionString(TestSetup.module, TestSetup.environment);
              DataTable dt2 = Database.RunQuery(query, strConn);

              using (StreamWriter sw = new StreamWriter(@"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\ChecksumStore\AIR\Insurance_Appointments\DBData.txt"))
              {
                  foreach (DataRow row in dt2.Rows)
                      sw.WriteLine(String.Join(",", row.ItemArray));
              }

              dt1.DefaultView.Sort = "Column1";
              dt1 = dt1.DefaultView.ToTable();

              dt2.DefaultView.Sort = "ContactIdentifierReference";
              dt2 = dt2.DefaultView.ToTable();

              Methods.AreTablesTheSame(dt1, dt2);

          } */

    }
}
