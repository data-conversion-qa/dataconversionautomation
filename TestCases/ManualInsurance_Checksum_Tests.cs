﻿using System;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace DataConversionAutomation.ManualInsurance
{
    [TestClass]
    public class ManualInsurance_Checksum_Tests
    {
        [TestMethod]
        public void Compare_Checksum_ManualInsurancePolicy()
        {
            TestSetup.module = "ManualInsurance";
            TestSetup.environment = "Pre-Prod";
            TestSetup.section = "ManualInsurancePolicy";
            TestSetup.table = "ManualInsurancePolicy";
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\ManualInsurance\20201007\";
            TestSetup.query = QueryManager.GetQuery();
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

          [TestMethod]
        public void Compare_Checksum_ManualInsurancePolicyParty()
        {
            TestSetup.module = "ManualInsurance";
            TestSetup.environment = "Pre-Prod";
            TestSetup.section = "ManualInsurancePolicyParty";
            TestSetup.table = "ManualInsurancePolicyParty";
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\ManualInsurance\20201007\";
            TestSetup.query = QueryManager.GetQuery();
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void DB2DB_Checksum_ManualInsurancePolicy()
        {
            TestSetup.module = "ManualInsurance";
            TestSetup.environment = "Dev";
            TestSetup.section = "ManualInsurance";
            TestSetup.table = "ManualInsurancePolicy";
            Template.SetupChecksumComparisionTest();
            TestSetup.query = QueryManager.GetQuery(@"<Query1Path>");
            Template.ComputeDBChecksum();
            TestSetup.query = QueryManager.GetQuery(@"<Query1Path>");
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

    }
}
