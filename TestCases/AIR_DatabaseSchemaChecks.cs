﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
//using DataConversionAutomation;

namespace DataConversionAutomation.AIR_DatabaseSchemaChecks
{
    [TestClass]
    public class AIR_Address_ColumnChecks
    {

        [TestMethod]
        public void AIR_Address_NullCheck()
        { 
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Address", "AddressId"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Address", "ContactIdentifierReference"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Address", "AddressLine1"), "Pass");
            //Assert.AreNotEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Address", "addressLine2"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Address", "Country"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Address", "City"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Address", "StateOrProvince"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Address", "ZipCode"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Address", "OfficePhoneNumber"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Address", "FaxNumber"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Address", "BranchId"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Address", "FINRABranchNumber"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Address", "MainAdvisorForTheBranch"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Address", "AddressType"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Address", "ChangeType"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Address", "ProcessedDatetime"), "Pass");
        }
        [TestMethod]
        
        public void AIR_Address_DataTypeCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Address", "AddressId", typeof(int)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Address", "ContactIdentifierReference", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Address", "AddressLine1", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Address", "AddressLine2", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Address", "Country", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Address", "City", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Address", "StateOrProvince", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Address", "ZipCode", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Address", "OfficePhoneNumber", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Address", "FaxNumber", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Address", "BranchId", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Address", "FinraBranchNumber", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Address", "MainAdvisorForTheBranch", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Address", "AddressType", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Address", "ChangeType", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Address", "ProcessedDatetime", typeof(DateTime)), "Pass");
        }

        [TestMethod]

        public void AIR_Address_Column_MaxLength_Check()
        {
            //Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Address", "addressID", 10), "Pass");
            //A360 RefID :816
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Address", "ContactIdentifierReference", 10), "Pass");
            //A360 RefID :807
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Address", "AddressLine1", 200), "Pass");
            //A360 RefID :808
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Address", "AddressLine2", 200), "Pass");
            //A360 RefID :809
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Address", "Country", 40), "Pass");
            //A360 RefID :814
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Address", "City", 30), "Pass");
            //A360 RefID :817
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Address", "StateOrProvince", 40), "Pass");
            //A360 RefID :829
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Address", "ZipCode", 20), "Pass");
            //A360 RefID :826
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Address", "OfficePhoneNumber", 25), "Pass");
            //A360 RefID :828
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Address", "FaxNumber", 25), "Pass");
            //A360 RefID :823
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Address", "BranchId", 20), "Pass");
            //A360 RefID :825
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Address", "FinraBranchNumber", 20), "Pass");
            //A360 RefID :782
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Address", "MainAdvisorForTheBranch", 30), "Pass");
            //A360 RefID :New
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Address", "AddressType", 100), "Pass");
            //A360 RefID :Unknown
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Address", "ChangeType", 10), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Address", "processedDatetime", 23), "Pass");
        }
    }

    [TestClass]
    public class AIR_Contact_ColumnChecks
    {
        [TestMethod]
        public void AIR_Contact_NullCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "contactID"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "contactServiceModels"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "firstName"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "lastName"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "companyName"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "contactType"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "contactStatus"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "emailaddress"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "nameprefix"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "namesuffix"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "publicName"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "ssNumber"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "isInternalEmployee"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "faxnumber"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "phoneNumber"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "phoneextension"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "birthdate"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "cellPhoneNumber"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "designation"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "alternateemailaddress"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "formerName"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "goesBy"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "homePhone"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "middleName"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "placeofBirth"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "webaddress"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "agency"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "region"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "advisorID"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "practiceAnalyticspeergroups"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "contractReceivedDate"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "msaExclusionDate"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "startDate"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "terminationDate"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "nationalFuturesAssocID"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "bankRepFlag"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "repBankName"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "crdNumber"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "nationalProducerNumber"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact", "secorState"), "Pass");

        }

        [TestMethod]
        public void AIR_Contact_Column_MaxLength_Check()
        {
            //Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "contactID", 10), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "ContactIdentifier", 10), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "ContactServiceModels", 10), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "FirstName", 30), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "LastName", 80), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "CompanyName", 100), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "EmailAddress", 350), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "NamePrefix", 10), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "NameSuffix", 50), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "SSNumber", 50), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "FaxNumber", 25), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "PhoneNumber", 25), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "PhoneExtension", 20), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "CellPhoneNumber", 25), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "Designation", 40), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "AlternateEmailAddress", 350), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "FormerName", 100), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "GoesBy", 30), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "HomePhone", 25), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "MiddleName", 30), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "PlaceOfBirth", 40), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "WebAddress", 125), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "Agency", 80), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "Region", 80), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "AdvisorId", 30), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "PracticeAnalyticsPeerGroups", 100), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "NationalFuturesAssocID", 30), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "RepBankName", 50), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "CRDNumber", 18), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact", "NationalProducerNumber", 30), "Pass");
        }

        [TestMethod]

        public void AIR_Contact_DataTypeCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "ContactId", typeof(int)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "ContactIdentifier", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "ContactServiceModels", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "FirstName", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "LastName", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "CompanyName", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "ContactType", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "ContactStatus", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "EmailAddress", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "NamePrefix", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "NameSuffix", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "PublicName", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "SSNumber", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "IsInternalEmployee", typeof(byte)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "FaxNumber", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "PhoneNumber", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "PhoneExtension", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "BirthDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "CellPhoneNumber", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "Designation", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "AlternateEmailAddress", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "FormerName", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "GoesBy", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "HomePhone", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "MiddleName", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "PlaceOfBirth", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "WebAddress", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "Agency", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "Region", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "AdvisorId", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "PracticeAnalyticsPeerGroups", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "ContractReceivedDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "MSAExclusionDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "StartDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "TerminationDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "NationalFuturesAssocID", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "BankRepFlag", typeof(byte)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "RepBankName", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "CRDNumber", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "NationalProducerNumber", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact", "SecOrState", typeof(byte)), "Pass");
        }
    }

    [TestClass]
    public class AIR_Contact_Relationship_ColumnChecks
    {
        [TestMethod]
        public void AIR_Contact_Relationship_NullCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact_Relationship", "ContactRelationshipID"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact_Relationship", "Type"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact_Relationship", "EntityChild"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact_Relationship", "EntityChildName"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact_Relationship", "EntityChildRole"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact_Relationship", "EntityParent"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact_Relationship", "ChangeType"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Contact_Relationship", "ProcessedDatetime"), "Pass");

        }

        [TestMethod]
        public void AIR_Contact_Relationship_Column_MaxLength_Check()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact_Relationship", "Type", 100), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact_Relationship", "EntityChild", 100), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact_Relationship", "EntityChildName", 100), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact_Relationship", "EntityChildRole", 100), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact_Relationship", "EntityParent", 100), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Contact_Relationship", "ChangeType", 10), "Pass");

        }

        [TestMethod]

        public void AIR_Contact_Relationship_DataTypeCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact_Relationship", "ContactRelationshipId", typeof(int)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact_Relationship", "Type", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact_Relationship", "EntityChild", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact_Relationship", "EntityChildName", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact_Relationship", "EntityChildRole", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact_Relationship", "EntityParent", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact_Relationship", "ChangeType", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Contact_Relationship", "ProcessedDatetime", typeof(DateTime)), "Pass");

        }

    }

    [TestClass]

    public class AIR_DRP_ColumnChecks
    {
        [TestMethod]
        public void AIR_DRP_NullCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_DRP", "DRPId"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_DRP", "ContactIdentifierReference"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_DRP", "DRPQuestionCode"), "Pass");
            //Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_DRP", "DRPStatus"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_DRP", "DRPEventDate"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_DRP", "DRPReportedDate"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_DRP", "DRPResolutionDate"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_DRP", "IsDRPActive"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_DRP", "ChangeType"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_DRP", "ProcessedDatetime"), "Pass");

        }

        [TestMethod]
        public void AIR_DRP_Column_MaxLength_Check()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_DRP", "ContactIdentifierReference", 10), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_DRP", "DRPQuestionCode", 9), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_DRP", "DRPStatus", 50), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_DRP", "ChangeType", 10), "Pass");
        }

        [TestMethod]

        public void AIR_DRP_DataTypeCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_DRP", "DRPId", typeof(int)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_DRP", "ContactIdentifierReference", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_DRP", "DRPQuestionCode", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_DRP", "DRPStatus", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_DRP", "DRPEventDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_DRP", "DRPReportedDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_DRP", "DRPResolutionDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_DRP", "IsDRPActive", typeof(bool)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_DRP", "ChangeType", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_DRP", "ProcessedDatetime", typeof(DateTime)), "Pass");

        }
    }

    //FINRA_License
 [TestClass]

    public class AIR_FINRA_License_ColumnChecks
    {
        [TestMethod]
        public void AIR_FINRA_License_NullCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_FINRA_License", "FinraLicenseId"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_FINRA_License", "ContactIdentifierReference"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_FINRA_License", "FinraLicenseType"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_FINRA_License", "LicenseState"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_FINRA_License", "LicenseSeries"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_FINRA_License", "LicenseRequestDate"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_FINRA_License", "LicenseEffectivedate"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_FINRA_License", "LicenseTerminatedDate"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_FINRA_License", "LicenseRenewalDate"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_FINRA_License", "BeginningOfWindowDateRange"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_FINRA_License", "EndOfWindowDateRange"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_FINRA_License", "ChangeType"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_FINRA_License", "ProcessedDatetime"), "Pass");

        }

        [TestMethod]
        public void AIR_FINRA_License_Column_MaxLength_Check()
        {

            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_FINRA_License", "ContactIdentifierReference", 10), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_FINRA_License", "FinraLicenseType", 50), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_FINRA_License", "LicenseState", 6), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_FINRA_License", "LicenseSeries", 10), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_FINRA_License", "ChangeType", 10), "Pass");

        }

        [TestMethod]

        public void AIR_FINRA_License_DataTypeCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_FINRA_License", "finraLicenseId", typeof(int)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_FINRA_License", "ContactIdentifierReference", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_FINRA_License", "finraLicenseType", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_FINRA_License", "LicenseState", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_FINRA_License", "LicenseSeries", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_FINRA_License", "LicenseRequestDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_FINRA_License", "LicenseEffectivedate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_FINRA_License", "LicenseTerminatedDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_FINRA_License", "LicenseRenewalDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_FINRA_License", "beginningOfWindowDateRange", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_FINRA_License", "endOfWindowDateRange", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_FINRA_License", "changeType", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_FINRA_License", "processedDatetime", typeof(DateTime)), "Pass");

        }
    }

    // Staging_Firm_Training
    [TestClass]

    public class AIR_Firm_Training_ColumnChecks
    {
        [TestMethod]
        public void AIR_Firm_Training_NullCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Firm_Training", "firmTrainingId"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Firm_Training", "ContactIdentifierReference"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Firm_Training", "yearCECourseApplied"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Firm_Training", "isCECompleteForTheYear"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Firm_Training", "ceCourse"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Firm_Training", "ceSource"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Firm_Training", "ceType"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Firm_Training", "ceCompletedDate"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Firm_Training", "ceGoodUntilDate"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Firm_Training", "ceCreditsEarned"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Firm_Training", "changeType"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Firm_Training", "processedDatetime"), "Pass");

        }

        [TestMethod]
        public void AIR_Firm_Training_Column_MaxLength_Check()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Firm_Training", "ContactIdentifierReference", 10), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Firm_Training", "CECourse", 200), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Firm_Training", "CESource", 100), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Firm_Training", "CEType", 100), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Firm_Training", "ChangeType", 10), "Pass");

        }

        [TestMethod]

        public void AIR_Firm_Training_DataTypeCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Firm_Training", "FirmTrainingId", typeof(int)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Firm_Training", "ContactIdentifierReference", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Firm_Training", "YearCECourseApplied", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Firm_Training", "IsCECompleteForTheYear", typeof(byte)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Firm_Training", "CECourse", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Firm_Training", "CESource", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Firm_Training", "CEType", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Firm_Training", "CECompletedDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Firm_Training", "CEGoodUntilDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Firm_Training", "CECreditsEarned", typeof(byte)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Firm_Training", "ChangeType", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Firm_Training", "ProcessedDatetime", typeof(DateTime)), "Pass");

        }
    }

    // Staging_Insurance_Appointments
    [TestClass]

    public class AIR_Insurance_Appointments_ColumnChecks
    {
        [TestMethod]
        public void AIR_Insurance_Appointments_NullCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_Appointments", "appointmentsId"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_Appointments", "ContactIdentifierReference"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_Appointments", "appointmentDate"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_Appointments", "appointmentSubmittedDate"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_Appointments", "appointmentTerminationDate"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_Appointments", "appointmentType"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_Appointments", "appointmentCompanyName"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_Appointments", "insuranceAppointmentLine"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_Appointments", "appointmentState"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_Appointments", "changeType"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_Appointments", "processedDatetime"), "Pass");

        }

        [TestMethod]
        public void AIR_Insurance_Appointments_Column_MaxLength_Check()
        {

            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Insurance_Appointments", "ContactIdentifierReference", 10), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Insurance_Appointments", "appointmentType", 30), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Insurance_Appointments", "appointmentCompanyName", 200), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Insurance_Appointments", "insuranceAppointmentLine", 30), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Insurance_Appointments", "appointmentState", 6), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Insurance_Appointments", "changeType", 10), "Pass");

        }

        [TestMethod]

        public void AIR_Insurance_Appointments_DataTypeCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_Appointments", "appointmentsId", typeof(int)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_Appointments", "ContactIdentifierReference", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_Appointments", "appointmentDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_Appointments", "appointmentSubmittedDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_Appointments", "appointmentTerminationDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_Appointments", "appointmentType", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_Appointments", "appointmentCompanyName", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_Appointments", "insuranceAppointmentLine", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_Appointments", "appointmentState", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_Appointments", "changeType", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_Appointments", "processedDatetime", typeof(DateTime)), "Pass");

        }
    }

    // Staging_Insurance_License

    [TestClass]

    public class AIR_Insurance_License_ColumnChecks
    {
        [TestMethod]
        public void AIR_Insurance_License_NullCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_License", "insuranceLicenseId"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_License", "ContactIdentifierReference"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_License", "LicenseNumber"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_License", "LicenseState"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_License", "insuranceLicenseType"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_License", "insuranceLicenseLine"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_License", "individualOrCorporate"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_License", "isLicenseCEApplicable"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_License", "isLicenseRecurring"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_License", "LicenseSubmittedDate"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_License", "LicenseEffectiveDate"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_License", "LicenseTerminationDate"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_License", "LicenseContinuingEdTermDate"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_License", "changeType"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Insurance_License", "processedDatetime"), "Pass");

        }

        [TestMethod]
        public void AIR_Insurance_License_Column_MaxLength_Check()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Insurance_License", "ContactIdentifierReference", 10), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Insurance_License", "LicenseNumber", 30), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Insurance_License", "LicenseState", 6), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Insurance_License", "insuranceLicenseType", 30), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Insurance_License", "insuranceLicenseLine", 30), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Insurance_License", "individualOrCorporate", 30), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Insurance_License", "changeType", 10), "Pass");

        }

        [TestMethod]

        public void AIR_Insurance_License_DataTypeCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_License", "insuranceLicenseId", typeof(int)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_License", "ContactIdentifierReference", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_License", "LicenseNumber", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_License", "LicenseState", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_License", "insuranceLicenseType", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_License", "insuranceLicenseLine", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_License", "individualOrCorporate", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_License", "isLicenseCEApplicable", typeof(byte)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_License", "isLicenseRecurring", typeof(byte)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_License", "LicenseSubmittedDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_License", "LicenseEffectiveDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_License", "LicenseTerminationDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_License", "LicenseContinuingEdTermDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_License", "changeType", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Insurance_License", "processedDatetime", typeof(DateTime)), "Pass");

        }
    }

    // Staging_Other_Business
    [TestClass]

    public class AIR_Other_Business_ColumnChecks
    {
        [TestMethod]
        public void AIR_Other_Business_NullCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Other_Business", "OtherBusinessId"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Other_Business", "ContactIdentifierReference"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Other_Business", "businessDescription"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Other_Business", "changeType"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Other_Business", "processedDatetime"), "Pass");

        }

        [TestMethod]
        public void AIR_Other_Business_Column_MaxLength_Check()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Other_Business", "ContactIdentifierReference", 10), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Other_Business", "businessDescription", 255), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Other_Business", "changeType", 10), "Pass");

        }

        [TestMethod]

        public void AIR_Other_Business_DataTypeCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Other_Business", "OtherBusinessId", typeof(int)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Other_Business", "ContactIdentifierReference", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Other_Business", "businessDescription", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Other_Business", "changeType", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Other_Business", "processedDatetime", typeof(DateTime)), "Pass");

        }
    }

    // Staging_Plan
    [TestClass]

    public class AIR_Plan_ColumnChecks
    {
        [TestMethod]
        public void AIR_Plan_NullCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Plan", "PlanId"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Plan", "PlanIdTypecode"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Plan", "Plan_Id"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Plan", "ServiceModelsForThePlanId"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Plan", "AumDiscount"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Plan", "MSAExclusionDate"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Plan", "ProgramElectionDate"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Plan", "12B1HousePaymentCode"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Plan", "ExcludedFromMSA"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Plan", "ContactIdentifierReference"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Plan", "MainAdvisorOnThePlan"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Plan", "SplitPercentage"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Plan", "CommissionTypeCode"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Plan", "PlanIdTerminationDate"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Plan", "ChangeType"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Plan", "ProcessedDatetime"), "Pass");

        }

        [TestMethod]
        public void AIR_Plan_Column_MaxLength_Check()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Plan", "PlanIdTypecode", 30), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Plan", "Plan_Id", 30), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Plan", "ServiceModelsForThePlanId", 30), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Plan", "ContactIdentifierReference", 10), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Plan", "MainAdvisorOnThePlan", 30), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Plan", "CommissionTypeCode", 3), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Plan", "ChangeType", 10), "Pass");

        }

        [TestMethod]

        public void AIR_Plan_DataTypeCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Plan", "PlanId", typeof(int)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Plan", "PlanIdTypecode", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Plan", "Plan_Id", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Plan", "ServiceModelsForThePlanId", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Plan", "AUMDiscount", typeof(byte)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Plan", "MSAExclusionDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Plan", "ProgramElectionDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Plan", "12B1HousePaymentCode", typeof(byte)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Plan", "ExcludedFromMSA", typeof(byte)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Plan", "ContactIdentifierReference", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Plan", "MainAdvisorOnThePlan", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Plan", "SplitPercentage", typeof(decimal)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Plan", "CommissionTypeCode", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Plan", "PlanIdTerminationDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Plan", "ChangeType", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Plan", "ProcessedDatetime", typeof(DateTime)), "Pass");

        }
    }

    // Staging_Plan_ID_Translation
    [TestClass]

    public class AIR_Plan_ID_Translation_ColumnChecks
    {
        [TestMethod]
        public void AIR_Plan_ID_Translation_NullCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Plan_ID_Translation", "planIdTranslationId"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Plan_ID_Translation", "source"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Plan_ID_Translation", "sponsorCompanyCode"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Plan_ID_Translation", "sponsorPlanId"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Plan_ID_Translation", "mmlPlanId"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Plan_ID_Translation", "changeType"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Plan_ID_Translation", "processedDatetime"), "Pass");

        }

        [TestMethod]
        public void AIR_Plan_ID_Translation_Column_MaxLength_Check()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Plan_ID_Translation", "source", 10), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Plan_ID_Translation", "sponsorCompanyCode", 10), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Plan_ID_Translation", "sponsorPlanId", 30), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Plan_ID_Translation", "mmlPlanId", 30), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Plan_ID_Translation", "changeType", 10), "Pass");

        }

        [TestMethod]

        public void AIR_Plan_ID_Translation_DataTypeCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Plan_ID_Translation", "planIdTranslationId", typeof(int)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Plan_ID_Translation", "source", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Plan_ID_Translation", "sponsorCompanyCode", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Plan_ID_Translation", "sponsorPlanId", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Plan_ID_Translation", "mmlPlanId", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Plan_ID_Translation", "changeType", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Plan_ID_Translation", "processedDatetime", typeof(DateTime)), "Pass");

        }
    }

    // Staging_Prefix

    [TestClass]

    public class AIR_Prefix_ColumnChecks
    {
        [TestMethod]
        public void AIR_Prefix_NullCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Prefix", "prefixId"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Prefix", "planId"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Prefix", "accountPrefix"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Prefix", "accountPrefixDescription"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Prefix", "changeType"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Prefix", "processedDatetime"), "Pass");

        }

        [TestMethod]
        public void AIR_Prefix_Column_MaxLength_Check()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Prefix", "planId", 30), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Prefix", "accountPrefix", 50), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Prefix", "accountPrefixDescription", 250), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Prefix", "changeType", 10), "Pass");

        }

        [TestMethod]

        public void AIR_Prefix_DataTypeCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Prefix", "prefixId", typeof(int)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Prefix", "planId", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Prefix", "accountPrefix", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Prefix", "accountPrefixDescription", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Prefix", "changeType", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Prefix", "processedDatetime", typeof(DateTime)), "Pass");

        }
    }

    // Staging_Provisioning

    [TestClass]

    public class AIR_Provisioning_ColumnChecks
    {
        [TestMethod]
        public void AIR_Provisioning_NullCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Provisioning", "provisionId"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Provisioning", "contactIdentifier"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Provisioning", "loginId"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Provisioning", "wealthscapeId"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Provisioning", "crmPrimaryBusUnit"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Provisioning", "crmSecondaryBusUnit"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Provisioning", "casperRoles"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Provisioning", "status"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Provisioning", "changeType"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Provisioning", "processedDatetime"), "Pass");

        }

        [TestMethod]
        public void AIR_Provisioning_Column_MaxLength_Check()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Provisioning", "contactIdentifier", 10), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Provisioning", "loginId", 100), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Provisioning", "wealthscapeId", 20), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Provisioning", "crmPrimaryBusUnit", 100), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Provisioning", "crmSecondaryBusUnit", 1000), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Provisioning", "casperRoles", 1000), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Provisioning", "status", 10), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Provisioning", "changeType", 10), "Pass");
        }

        [TestMethod]

        public void AIR_Provisioning_DataTypeCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Provisioning", "provisionId", typeof(int)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Provisioning", "contactIdentifier", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Provisioning", "loginId", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Provisioning", "wealthscapeId", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Provisioning", "crmPrimaryBusUnit", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Provisioning", "crmSecondaryBusUnit", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Provisioning", "casperRoles", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Provisioning", "status", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Provisioning", "changeType", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Provisioning", "processedDatetime", typeof(DateTime)), "Pass");
        }
    }
    // Staging_Regulatory_Training
    [TestClass]
    public class AIR_Regulatory_Training_ColumnChecks
    {
        [TestMethod]
        public void AIR_Regulatory_Training_NullCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Regulatory_Training", "regulatoryTrainingId"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Regulatory_Training", "ContactIdentifierReference"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Regulatory_Training", "ceRegistrationStatus"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Regulatory_Training", "ceCompletedDate"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Regulatory_Training", "ceAnniversaryDate"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Regulatory_Training", "ceCutoffDate"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Regulatory_Training", "changeType"), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.DataConversionNullCheck("AIR", "air.Staging_Regulatory_Training", "processedDatetime"), "Pass");

        }

        [TestMethod]
        public void AIR_Regulatory_Training_Column_MaxLength_Check()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Regulatory_Training", "ContactIdentifierReference", 10), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Regulatory_Training", "ceRegistrationStatus", 10), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnMaxLength("AIR", "air.Staging_Regulatory_Training", "changeType", 10), "Pass");
        }

        [TestMethod]

        public void AIR_Regulatory_Training_DataTypeCheck()
        {
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Regulatory_Training", "regulatoryTrainingId", typeof(int)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Regulatory_Training", "ContactIdentifierReference", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Regulatory_Training", "ceRegistrationStatus", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Regulatory_Training", "ceCompletedDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Regulatory_Training", "ceAnniversaryDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Regulatory_Training", "ceCutoffDate", typeof(DateTime)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Regulatory_Training", "changeType", typeof(string)), "Pass");
            Assert.AreEqual(DataConversionAutomation.Methods.CheckColumnDatatype("AIR", "air.Staging_Regulatory_Training", "processedDatetime", typeof(DateTime)), "Pass");
        }
    }

}
