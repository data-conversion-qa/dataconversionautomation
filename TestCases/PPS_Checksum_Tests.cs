﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataConversionAutomation.PPS
{

    [TestClass]

    public class PPS_Checksum_Tests
    {
        [TestMethod]

        public void Compare_Checksum_Template_CrossReferenceMMLA360()
        {
            TestSetup.module = "PPS";
            TestSetup.environment = "Pre-Prod";
            TestSetup.section = "AccountxRef";
            TestSetup.table = TestSetup.section;
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\PPS\Batch3\";
            TestSetup.query = QueryManager.GetQuery();
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]

        public void Compare_Checksum_Template_CrossReferenceMHA()
        {
            TestSetup.module = "PPS";
            TestSetup.environment = "Prod";
            TestSetup.section = "MasterHouseholdAssignment";
            TestSetup.table = TestSetup.section;
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\Household\Cross Ref\";
            TestSetup.query = QueryManager.GetQuery();
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

    }
    }

