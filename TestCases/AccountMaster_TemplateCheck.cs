﻿using System;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework.Internal;

namespace DataConversionAutomation.AccountMaster
{

    [TestClass]
    public class AccountMaster_OPEN_NON_NFS_Account_ChecksumTest
    {
        [TestMethod]
        public void Compare_Checksum_OPEN_NON_NFS_DIRECTCLIENT()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "QA";
            TestSetup.section = "AccountMasterNonNFSOpenAccounts";
            TestSetup.table = "DirectClientInformation";
            TestSetup.templateFilePath = @"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\Extract\AccountMasterNonNFSOpenAccounts_07302020\";
            TestSetup.query = @"SELECT [LinkedTenantUniqueMATID]
                                                  ,[LinkedTenantNAFSequenceID]
                                                  ,[NAFStatus]
                                                  ,[RegistrationTypeCode]
                                                  ,[NAFRepIdentifier]
                                                  ,Trim([PrimaryAccountHolderSSNumber])[PrimaryAccountHolderSSNumber]
                                                  ,[SSNCode]
                                                  ,[WorkPhone]
                                                  ,[HomePhone]
                                                  ,NULLIF(CONVERT(varchar,[DateofBirth],23),'1753-01-01') as [DateofBirth]
                                                  ,[NAFRegistration1]
                                                  ,[RelationshiptoAdvisorCode]
                                                  ,[RelatedToCode]
	                                              ,CASE [Charitable] When '1' Then 'Y' When  '0' Then 'N' Else null END [Charitable]
                                                  ,CASE [Title1Erisa] When '1' Then 'Y' When  '0' Then 'N' Else null END [Title1Erisa]
                                                  ,[CorporationTypeCode]
                                                  ,[LegalAddress1]
                                                  ,[LegalAddress2]
                                                  ,[LegalAddress3]
                                                  ,[LegalCity]
                                                  ,[LegalState]
                                                  ,[LegalZip]
                                                  ,[LegalZipPlus4]
                                                  ,[LegalCountry]
                                                  ,[MailingAddress1]
                                                  ,[MailingAddress2]
                                                  ,[MailingAddress3]
                                                  ,[MailingCity]
                                                  ,[MailingState]
                                                  ,[MailingZip]
                                                  ,[MailingZipPlus4]
                                                  ,[MailingCountry]
                                              FROM [DataConversion].[AccountMaster].[Template_Open_DirectClient]";
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Compare_Checksum_OPEN_NON_NFS_CLIENT()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "QA";
            TestSetup.section = "AccountMasterNonNFSOpenAccounts";
            TestSetup.table = "Client";
            TestSetup.templateFilePath = @"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\Extract\AccountMasterNonNFSOpenAccounts_07302020\";
            TestSetup.query = @"SELECT [ClientIDENTIFIER]
      ,[LinkedTenantUniqueMATID]
      ,[LinkedTenantNAFSequenceID]
      ,[BusinessTrustName]
      ,[AccountOwnerFirstName]
      ,[AccountOwnerMiddleName]
      ,[AccountOwnerLastName]
      ,[AccountOwnerPrefix]
      ,[AccountOwnerSuffix]
      ,[SSNumber]
      ,[SSNCode]
      ,[AccountOwnerType]
      ,NULLIF(CONVERT(varchar,[AccountOwnerDateofBirth],23),'1753-01-01') as [AccountOwnerDateofBirth]
      ,[AccountOwnerHomePhone]
      ,[AccountOwnerWorkPhone]
      ,[AffiliatedFlag]
      ,[PrimaryIndicator]
      ,[NAFRelationshipCode]
      ,[AccountOwnerworkextension]
      ,[AccountOwnerCitizenshipCountry]
      ,[AccountOwnerCitizenshipLicense]
      ,[AccountOwnerCitizenshipPassport]
      ,[AccountOwnerState]
      ,[TrustDate]
      ,[ClientLegalAddress1]
      ,[ClientLegalAddress2]
      ,[ClientLegalAddress3]
      ,[ClientLegalCity]
      ,[ClientLegalState]
      ,[ClientLegalZip]
      ,[ClientLegalZipPlus4]
      ,[ClientLegalCountry]
      ,[ClientMailingAddress1]
      ,[ClientMailingAddress2]
      ,[ClientMailingAddress3]
      ,[ClientMailingCity]
      ,[ClientMailingState]
      ,[ClientMailingZip]
      ,[ClientMailingZipPlus4]
      ,[ClientMailingCountry]
      ,[ClientLegalAttention]
      ,[ClientMailingAttention]
      ,[ClientsOccupation]
      ,[ClientsEmploymentStatus]
      ,[ClientsIncomeSource]
      ,[EmployersAddress1]
      ,[EmployersAddress2]
      ,[EmployersAddress3]
      ,[EmployersCity]
      ,[EmployersState]
      ,[EmployersZip]
      ,[EmployersZipPlus4]
  FROM [DataConversion].[AccountMaster].[Template_Open_Client]";
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Compare_Checksum_OPEN_NON_NFS_CLIENTCONTROL()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "QA";
            TestSetup.section = "AccountMasterNonNFSOpenAccounts";
            TestSetup.table = "ClientControl";
            TestSetup.templateFilePath = @"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\Extract\AccountMasterNonNFSOpenAccounts_07302020\";
            TestSetup.query = @"SELECT [LinkedClientIDENTIFIER]
                                              ,[LinkedTenantUniqueMATID]
                                              ,[LinkedTenantNAFSequenceID]
                                              ,[FirmRelationship]
                                              ,[Companyname]
                                              ,[Cusip]
                                              ,[Relationship2]
                                              ,[Company2]
                                              ,[Cusip2]
                                              ,[Relationship3]
                                              ,[Company3]
                                              ,[Cusip3]
                                          FROM [DataConversion].[AccountMaster].[Template_Open_ClientControl]";
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Compare_Checksum_OPEN_NON_NFS_DIRECTSPONSOR()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "QA";
            TestSetup.section = "AccountMasterNonNFSOpenAccounts";
            TestSetup.table = "DirectSponsor";
            TestSetup.templateFilePath = @"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\Extract\AccountMasterNonNFSOpenAccounts_07302020\";
            TestSetup.query = @"SELECT [LinkedAccountNumber]
                                                  ,[SponsorIDENTIFIER]
                                                  ,[PlanName]
                                                  ,[Sponsor]
                                              FROM [DataConversion].[AccountMaster].[Template_Open_DirectSponsor]";
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Compare_Checksum_OPEN_NON_NFS_LINKTOCLIENTACCOUNT()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "QA";
            TestSetup.section = "AccountMasterNonNFSOpenAccounts";
            TestSetup.table = "LinkToClientAccount";
            TestSetup.templateFilePath = @"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\Extract\AccountMasterNonNFSOpenAccounts_07302020\";
            TestSetup.query = @"SELECT [LinkedClientIDENTIFIER]
                                                  ,[LinkedTenantUniqueMATID]
                                                  ,[LinkedTenantNAFSequenceID]
                                                  ,[TenantNAFStatus]
                                                  ,[LinkedAccountNumber]
                                              FROM [DataConversion].[AccountMaster].[Template_Open_LinkToClientAccount]";
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Compare_Checksum_OPEN_NON_NFS_TRUSTEDCONTACTS()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "QA";
            TestSetup.section = "AccountMasterNonNFSOpenAccounts";
            TestSetup.table = "TrustedContacts";
            TestSetup.templateFilePath = @"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\Extract\AccountMasterNonNFSOpenAccounts_07302020\";
            TestSetup.query = @"SELECT [LinkedAccountNumber]
                                                  ,[LinkedClientIdentifier]
                                                  ,[SourceType]
                                                  ,[CustomerType]
                                                  ,[OptOutIndicator]
                                                  ,[TrustedContactPrimary]
                                                  ,[FirstName]
                                                  ,[MiddleName]
                                                  ,[LastName]
                                                  ,[Suffix]
                                                  ,[AddressLine1]
                                                  ,[AddressLine2]
                                                  ,[City]
                                                  ,[State]
                                                  ,[Zip]
                                                  ,[ExtendedZip]
                                                  ,[Attention]
                                                  ,[Phone]
                                              FROM [DataConversion].[AccountMaster].[Template_Open_TrustedContacts]";
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Compare_Checksum_OPEN_NON_NFS_Account()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "QA";
            TestSetup.section = "AccountMasterNonNFSOpenAccounts";
            TestSetup.table = "Account";
            TestSetup.templateFilePath = @"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\Extract\AccountMasterNonNFSOpenAccounts_07302020\";
            TestSetup.query = @"SELECT 
                                                    [AccountDataSource]
                                                    ,[FundCode]
                                                    ,[FundCompanyCode]
                                                    ,[BranchIDENTIFIER]
                                                    ,[AccountNumber]
                                                    ,[TenantUniqueMATID]
                                                    ,[TenantNAFSequenceID]
                                                    ,[TenantNAFStatus]
                                                    ,[NAFRepIDENTIFIER]
                                                    ,[TINPrimaryAccountHolderSSNumber]
                                                    ,[RegistrationTypeCode]
                                                    ,[OpeningDeposit]
                                                    ,[ManagedAccountFlag]
                                                    ,[AccountStartDate]
                                                    ,[OwnershipPercent]
                                                    ,[LineCode]
                                                    ,[TaxIDCode]
                                                    ,[InvestmentObjective]
                                                    ,Trim([AnnualIncome]) [AnnualIncome]
                                                    ,Trim([EstimatedNetWorth])[EstimatedNetWorth]
                                                    ,Trim([EstimatedLiquidNetWorth])[EstimatedLiquidNetWorth]
                                                    ,Trim([MaritalStatus])[MaritalStatus]
                                                    ,[NumberOfDependents]
                                                    ,Trim([TaxBracketCode])[TaxBracketCode]
                                                    ,[TimeHorizonCode]
                                                    ,Trim([InvestmentKnowledge])[InvestmentKnowledge]
                                                    ,Trim([AnnualExpenses])[AnnualExpenses]
                                                    ,Trim([SpecialExpenses])[SpecialExpenses]
                                                    ,Trim([SpecialExpensesTimeFrame])[SpecialExpensesTimeFrame]
                                                    ,Trim([InvestmentPurpose])[InvestmentPurpose]
                                                    ,Trim([InvestmentPurposeText])[InvestmentPurposeText]
                                                    ,Trim([StocksKnowledge])[StocksKnowledge]
                                                    ,Trim([BondsKnowledge])[BondsKnowledge]
                                                    ,Trim([ShortTermKnowledge])[ShortTermKnowledge]
                                                    ,Trim([MutualFundsKnowledge])[MutualFundsKnowledge]
                                                    ,Trim([OptionsKnowledge])[OptionsKnowledge]
                                                    ,Trim([VariableContractKnowledge])[VariableContractKnowledge]
                                                    ,Trim([LimitedPartnershipsKnowledge])[LimitedPartnershipsKnowledge]
                                                    ,Trim([AlternativeInvestmentsKnowledge])[AlternativeInvestmentsKnowledge]
                                                    ,Trim([FuturesKnowledge])[FuturesKnowledge]
                                                    ,Trim([AnnuityKnowledge])[AnnuityKnowledge]
                                                    ,Trim([MarginKnowledge])[MarginKnowledge]
                                                    ,Trim([ForeignCurrencyKnowledge])[ForeignCurrencyKnowledge]
                                                    ,Trim([ForeignSecuritiesKnowledge])[ForeignSecuritiesKnowledge]
                                                    ,cast([AssetsHeldAwayAmount] as decimal(12,0))
                                                    ,REPLACE(LTRIM(REPLACE([StocksHeldAwayPercent],'0',' ')),' ','0') [StocksHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([BondsHeldAwayPercent],'0',' ')),' ','0')[BondsHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([ShortTermHeldAwayPercent],'0',' ')),' ','0')[ShortTermHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([MutualFundsHeldAwayPercent],'0',' ')),' ','0')[MutualFundsHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([OptionsHeldAwayPercent],'0',' ')),' ','0')[OptionsHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([VariableContractsHeldAwayPercent],'0',' ')),' ','0')[VariableContractsHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([LimitedPartnershipsHeldAwayPercent],'0',' ')),' ','0')[LimitedPartnershipsHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([FuturesHeldAwayPercent],'0',' ')),' ','0')[FuturesHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([AlternativeInvestmentsHeldAwayPercent],'0',' ')),' ','0')[AlternativeInvestmentsHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([AnnuitiesHeldAwayPercent],'0',' ')),' ','0')[AnnuitiesHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([ForeignCurrencyHeldAwayPercent],'0',' ')),' ','0')[ForeignCurrencyHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([ForeignSecuritiesHeldAwayPercent],'0',' ')),' ','0')[ForeignSecuritiesHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([OtherHeldAwayPercent],'0',' ')),' ','0')[OtherHeldAwayPercent]
                                                    ,[ProductKnowledgeAskedNotAnswered]
                                                    ,[AssetsHeldAwayAskedNotAnswered]
                                                    ,[FundingSource]
                                                    ,[FundingSourceOther]
                                                    ,NULLIF(CONVERT(varchar,[LastRAPDate],23),'1753-01-01') as [LastRAPDate]
                                                    FROM [DataConversion].[AccountMaster].[Template_Open_NonNFS_Account]";
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }
    }

    //Test For Account Master NFS OPEN Accounts//
    //Checksum Validation Tests//
    [TestClass]
    public class AccountMaster_OPEN_NFS_Account_ChecksumTest
    {
        [TestMethod]
        public void Compare_Checksum_OPEN_NFS_Account()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "Dev";
            TestSetup.section = "AccountMasterNFSOpenAccounts";
            TestSetup.table = TestSetup.section;
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AccountMaster\ACCOUNT_MASTER_NFS_OPEN_ACCOUNTS_07302020\";
            TestSetup.query = QueryManager.GetQuery();
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }
    }

    [TestClass]
    public class AccountMaster_CLOSED_Accounts_ChecksumTest
    {
        [TestMethod]
        public void Compare_Checksum_CLOSED_Accounts()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "Prod";
            TestSetup.section = "Account";
            TestSetup.table = TestSetup.section;
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AccountMaster\Set 1\ClosedAccounts\";
            TestSetup.query = QueryManager.GetQuery();
            Template.SetupChecksumComparisionTest();
            //TestSetup.PartialRun = true;
           //TestSetup.SourceChecksumFilePath = @"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\ChecksumStore\AccountMaster\Account\Account\sourceChecksum_20201009121021002.txt";
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }
    }

    [TestClass]
    public class AccountMaster_TemplateVerification
    {
        [TestMethod]
        public void NonNFS_Open_TemplateVerificationTest()
        {
            TestSetup.module = "AccountMaster";
            String FileLocation = @"C:\Users\kpriyank\Desktop\AccountMasterNonNFSOpenAccounts_10072020\";
            String Actual = Template.GetHeaderFooterCount(TestSetup.module, FileLocation);
            if (!TestSetup.Expected.Equals(Actual))
            {
                Trace.WriteLine("Test Failed");
                Debug.WriteLine("Test Failed");
                Assert.Fail("Expected Header Footer Check Not Matched. FilePath : " + FileLocation);
            }
            else
            {
                Trace.WriteLine("Test Passed");
                Debug.WriteLine("Test Passed");
            }
        }

        [TestMethod]
        public void OpenDirectTemplateVerification()
        {
            TestSetup.module = "AccountMaster";
            String FileLocation = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Account Master\Batch4\AccountMasterNonNFSOpenAccounts_06052020\";
            String Actual = Template.GetHeaderFooterCount(TestSetup.module, FileLocation);
            if (!TestSetup.Expected.Equals(Actual))
            {
                Trace.WriteLine("Test Failed");
                Debug.WriteLine("Test Failed");
                Assert.Fail("Expected Header Footer Check Not Matched. FilePath : " + FileLocation);
            }
            else
            {
                Trace.WriteLine("Test Passed");
                Debug.WriteLine("Test Passed");
            }
        }

        [TestMethod]
        public void Closed_Accounts_TemplateVerificationTest()
        {
            TestSetup.module = "AccountMaster";
            String FileLocation = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AccountMaster\Closed\";
            String Actual = Template.GetHeaderFooterCount(TestSetup.module, FileLocation);
            if (!TestSetup.Expected.Equals(Actual))
            {
                Trace.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation); ;
                Debug.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation); ;
                Assert.Fail("Expected Header Footer Check Not Matched. FilePath : " + FileLocation);
            }
            else
            {
                Trace.WriteLine("Test Passed");
                Debug.WriteLine("Test Passed");
                //TestContext.WriteLine("Expected Header Footer Check Matched. FilePath : " + FileLocation);
            }
        }
        [TestMethod]
        public void NFS_Open_Account_TemplateVerificationTest()
        {
            TestSetup.module = "AccountMaster";
            String FileLocation = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AccountMaster\Set 1\ACCOUNT_MASTER_NFS_OPEN_ACCOUNTS_10072020\";
            String Actual = Template.GetHeaderFooterCount(TestSetup.module, FileLocation);
            if (!TestSetup.Expected.Equals(Actual))
            {
                Trace.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation); ;
                Debug.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation); ;
                Assert.Fail("Expected Header Footer Check Not Matched. FilePath : " + FileLocation);
            }
            else
            {
                Trace.WriteLine("Test Passed");
                Debug.WriteLine("Test Passed");
                //TestContext.WriteLine("Expected Header Footer Check Matched. FilePath : " + FileLocation);
            }
        }

        [TestMethod]
        public void Compare_Checksum_NFSOpenAccount()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "Dev";
            TestSetup.section = "NFSOpenAccounts";
            TestSetup.table = TestSetup.section;
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Account Master\Batch3\";
            TestSetup.query = QueryManager.GetQuery();
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

       
        [TestMethod]
        public void AccountMasterTemplateHeaderFooterCheck()
        {
            String FileLocation = @"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\Extract\ACCOUNT_MASTER_NFS_OPEN_ACCOUNTS_07302020\";
            String Actual = DataConversionAutomation.Template.GetHeaderFooterCount("AccountMaster", FileLocation);
            if (!TestSetup.Expected.Equals(Actual))
            {
                Trace.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation); ;
                Debug.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation); ;
                Assert.Fail("Expected Header Footer Check Not Matched. FilePath : " + FileLocation);
            }
            else
            {
                Trace.WriteLine("Test Passed");
                Debug.WriteLine("Test Passed");
                //TestContext.WriteLine("Expected Header Footer Check Matched. FilePath : " + FileLocation);
            }
        }


        [TestMethod]
        public void NonElectronicsTemplateHeaderFooterCheck()
        {
            String FileLocation = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AccountMaster\Set 1\NonElectronicAccounts_10072020\";
            String Actual = DataConversionAutomation.Template.GetHeaderFooterCount("AccountMaster", FileLocation);
            if (!TestSetup.Expected.Equals(Actual))
            {
                Trace.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation); ;
                Debug.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation); ;
                Assert.Fail("Expected Header Footer Check Not Matched. FilePath : " + FileLocation);
            }
            else
            {
                Trace.WriteLine("Test Passed");
                Debug.WriteLine("Test Passed");
                //TestContext.WriteLine("Expected Header Footer Check Matched. FilePath : " + FileLocation);
            }
        }


        [TestMethod]
        public void AccountMasterTemplateNameFormat_NFSOpenAccountCheck()
        {
            String FileLocation = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Account Master\Batch3\";
            String Actual = Template.CheckFileNameFormat("AccountMaster", FileLocation);
            if (!TestSetup.Expected.Equals(Actual))
            {
                Trace.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation); ;
                Debug.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation); ;
                Assert.Fail("Expected Header Footer Check Not Matched. FilePath : " + FileLocation);
            }
            else
            {
                Trace.WriteLine("Test Passed");
                Debug.WriteLine("Test Passed");
                //TestContext.WriteLine("Expected Format Matched. FilePath : " + FileLocation);
            }
         }

        [TestMethod]
        public void AccountMasterTemplateNameFormat_NonNFSOpenAccountCheck()
        {
            String FileLocation = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Account Master\Batch3\";
            String Actual = Template.CheckFileNameFormat("AccountMaster", FileLocation);
            if (!TestSetup.Expected.Equals(Actual))
            {
                Trace.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation); ;
                Debug.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation); ;
                Assert.Fail("Expected Header Footer Check Not Matched. FilePath : " + FileLocation);
            }
            else
            {
                Trace.WriteLine("Test Passed");
                Debug.WriteLine("Test Passed");
                //TestContext.WriteLine("Expected Format Matched. FilePath : " + FileLocation);
            }
        }

        [TestMethod]
        public void AccountMasterTemplateNameFormat_HouseHoldCheck()
        {
            String FileLocation = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Account Master\Batch2";
            String Actual = Template.CheckFileNameFormat("HouseHold", FileLocation);
            if (!TestSetup.Expected.Equals(Actual))
            {
                Trace.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation); ;
                Debug.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation); ;
                Assert.Fail("Expected Header Footer Check Not Matched. FilePath : " + FileLocation);
            }
            else
            {
                Trace.WriteLine("Test Passed");
                Debug.WriteLine("Test Passed");
                //TestContext.WriteLine("Expected Format Matched. FilePath : " + FileLocation);
            }
        }
}
    [TestClass]
    public class AccountMaster_NFS_Account_ChecksumTest
    {
        [TestMethod]
        public void LinkToClientAccount()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "Dev";
            TestSetup.section = "LinkToClientAccount";
            TestSetup.table = "LinkToClientAccount";
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Account Master\Batch3\";
            TestSetup.query = @"SELECT [LinkedClientIDENTIFIER]
                                          ,[LinkedTenantUniqueMATID]
                                          ,[LinkedTenantNAFSequenceID]
                                          ,[TenantNAFStatus]
                                          ,[LinkedAccountNumber]
                                      FROM [DataConversion].[AccountMaster].[Template_LinkToClientAccount]";
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }


    }
    [TestClass]
    public class AccountMaster_NON_NFS_Account_ChecksumTest
    {
        [TestMethod]
        public void Get_Checksum_AccountMaster_LinkToClientAccount()

        {
            String sectionName = "Link To Client Account";
            String FilePath = @"\\DC01FS-P001FIL\ITShare\Development\DataConversions\ACCOUNT_MASTER_NON_NFS_OPEN_ACCOUNTS\ACCOUNT_MASTER_NON_NFS_OPEN_ACCOUNTS.txt";
            //Get Extract Row Cheksum List
            List<string> rawData = DataConversionAutomation.Methods.GetAllRowChecksumArray(DataConversionAutomation.Template.ParseExtractToDataTable(FilePath, sectionName));
            //rawData = rawData.OrderBy(q => q).ToList();
            using (StreamWriter tw = new StreamWriter(@"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\ChecksumStore\AccountMaster\NON NFS\LinkedToClientAccount\Temp1.txt"))
            {
                foreach (String s in rawData)
                    tw.WriteLine(s);
            }

        }

        [TestMethod]
        public void Compare_Checksum_LinkedToClientAccount()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "Dev";
            TestSetup.section = "LinkToClientAccount";
            TestSetup.table = TestSetup.section;
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AIR\Batch6";
            TestSetup.query = QueryManager.GetQuery();
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Get_BD_Checksum_LinkedToClientAccount()
        {
            TestSetup.environment = "Dev";
            string query =
        @"SELECT [LinkedClientIDENTIFIER]
                                  ,[LinkedTenantUniqueMATID]
                                  ,[LinkedTenantNAFSequenceID]
                                  ,[TenantNAFStatus]
                                  ,[LinkedAccountIDENTIFIER]
                              FROM [DataConversion].[AccountMaster].[Template_LinkToClientAccount]";

            string conString = DataConversionAutomation.Database.GetConnectionString("AccountMaster", TestSetup.environment);
            //Get Table in SQL Row Cheksum List
            List<string> TableData = DataConversionAutomation.Methods.GetAllRowChecksumArray(DataConversionAutomation.Database.RunQuery(query, conString));
            //TableData = TableData.OrderBy(q => q).ToList();
            using (StreamWriter tw = new StreamWriter(@"C:\DevKPriyank\Data Conversion Automation\DataConversionAutomation\ChecksumStore\AccountMaster\NON NFS\LinkedToClientAccount\Temp2.txt"))
            {
                foreach (String s in TableData)
                    tw.WriteLine(s);
            }

        }
    }
    [TestClass]
    public class AccountMaster_NonElectronic_Account_ChecksumTest
    {
        [TestMethod]
        public void NonElectronicTemplateValidation()
        {
            String FileLocation = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AccountMaster\NonElectronicAccounts_07302020\";
            String Actual = DataConversionAutomation.Template.GetHeaderFooterCount("AccountMaster", FileLocation);
            if (!TestSetup.Expected.Equals(Actual))
            {
                Trace.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation); ;
                Debug.WriteLine("Expected Header Footer Check Not Matched. FilePath : " + FileLocation); ;
                Assert.Fail("Expected Header Footer Check Not Matched. FilePath : " + FileLocation);
            }
            else
            {
                Trace.WriteLine("Test Passed");
                Debug.WriteLine("Test Passed");
                //TestContext.WriteLine("Expected Header Footer Check Matched. FilePath : " + FileLocation);
            }
        }

        [TestMethod]
        public void Compare_Checksum_NONELECTRONIC_ClientAffiliation()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "QA";
            TestSetup.section = "NonElectronicAccounts";
            TestSetup.table = "ClientAffiliation";
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AccountMaster\NonElectronicAccounts_07302020\";
            TestSetup.query = @"SELECT [LinkedClientIDENTIFIER]
                                                      ,[LinkedTenantUniqueMATID]
                                                      ,[LinkedTenantNAFSequenceID]
                                                      ,[AffiliationFirmName]
                                                      ,[FirmMailingAddress1]
                                                      ,[FirmMailingAddress2]
                                                      ,[FirmMailingAddress3]
                                                      ,[FirmMailingCity]
                                                      ,[FirmMailingState]
                                                      ,[FirmMailingZip]
                                                      ,[FirmMailingZipPlus4]
                                                      ,[FirmMailingCountry]
                                                      ,[FirmType]
                                                  FROM [DataConversion].[nonElectronic].[Template_ClientAffiliation]";
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Compare_Checksum_NONELECTRONIC_CLIENT()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "QA";
            TestSetup.section = "NonElectronicAccounts";
            TestSetup.table = "Client";
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AccountMaster\NonElectronicAccounts_07302020\";
            TestSetup.query = @"SELECT [ClientIDENTIFIER]
                                                      ,[LinkedTenantUniqueMATID]
                                                      ,[LinkedTenantNAFSequenceID]
                                                      ,[BusinessTrustName]
                                                      ,[AccountOwnerFirstName]
                                                      ,[AccountOwnerMiddleName]
                                                      ,[AccountOwnerLastName]
                                                      ,[AccountOwnerPrefix]
                                                      ,[AccountOwnerSuffix]
                                                      ,[SSNumber]
                                                      ,[SSNCode]
                                                      ,[AccountOwnerType]
                                                      ,NULLIF(CONVERT(varchar,[AccountOwnerDateofBirth],23),'1753-01-01') as [AccountOwnerDateofBirth]
                                                      ,[AccountOwnerHomePhone]
                                                      ,[AccountOwnerWorkPhone]
                                                      ,[AffiliatedFlag]
                                                      ,[PrimaryIndicator]
                                                      ,[NAFRelationshipCode]
                                                      ,[AccountOwnerworkextension]
                                                      ,[AccountOwnerCitizenshipCountry]
                                                      ,[AccountOwnerCitizenshipLicense]
                                                      ,[AccountOwnerCitizenshipPassport]
                                                      ,[AccountOwnerState]
                                                      ,[TrustDate]
                                                      ,[ClientLegalAddress1]
                                                      ,[ClientLegalAddress2]
                                                      ,[ClientLegalAddress3]
                                                      ,[ClientLegalCity]
                                                      ,[ClientLegalState]
                                                      ,[ClientLegalZip]
                                                      ,[ClientLegalZipPlus4]
                                                      ,[ClientLegalCountry]
                                                      ,[ClientMailingAddress1]
                                                      ,[ClientMailingAddress2]
                                                      ,[ClientMailingAddress3]
                                                      ,[ClientMailingCity]
                                                      ,[ClientMailingState]
                                                      ,[ClientMailingZip]
                                                      ,[ClientMailingZipPlus4]
                                                      ,[ClientMailingCountry]
                                                      ,[ClientLegalAttention]
                                                      ,[ClientMailingAttention]
                                                      ,[ClientsOccupation]
                                                      ,[ClientsEmploymentStatus]
                                                      ,[ClientsIncomeSource]
                                                      ,[EmployersAddress1]
                                                      ,[EmployersAddress2]
                                                      ,[EmployersAddress3]
                                                      ,[EmployersCity]
                                                      ,[EmployersState]
                                                      ,[EmployersZip]
                                                      ,[EmployersZipPlus4]
                                                  FROM [DataConversion].[nonElectronic].[Template_Client]";
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Compare_Checksum_NONELECTRONIC_CLIENTCONTROL()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "QA";
            TestSetup.section = "NonElectronicAccounts";
            TestSetup.table = "ClientControl";
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AccountMaster\NonElectronicAccounts_07302020\";
            TestSetup.query = @"SELECT [LinkedClientIDENTIFIER]
                                              ,[LinkedTenantUniqueMATID]
                                              ,[LinkedTenantNAFSequenceID]
                                              ,[FirmRelationship]
                                              ,[Companyname]
                                              ,[Cusip]
                                              ,[Relationship2]
                                              ,[Company2]
                                              ,[Cusip2]
                                              ,[Relationship3]
                                              ,[Company3]
                                              ,[Cusip3]
                                          FROM [DataConversion].[nonElectronic].[Template_ClientControl]";
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Compare_Checksum_NONELECTRONIC_SPONSOR()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "QA";
            TestSetup.section = "NonElectronicAccounts";
            TestSetup.table = "NewSponsor";
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AccountMaster\NonElectronicAccounts_07302020\";
            TestSetup.query = @"SELECT [LinkedAccountNumber]
                                                        ,[SponsorName]
                                                        ,[SponsorShortCode]
                                                        ,[SponsorStatus]
                                                        ,[SponsorAddressLine1]
                                                        ,[SponsorAddressLine2]
                                                        ,[SponsorCity]
                                                        ,[SponsorState]
                                                        ,[SponsorZipCode]
                                                        ,[SponsorWebsite]
                                                        ,[SponsorType]
                                                        ,[SponsorContact1Name]
                                                        ,[SponsorContact1Phone]
                                                        ,[SponsorContact1Fax]
                                                        ,[SponsorContact1email]
                                                        ,[SponsorContact1Note]
                                                        ,[SponsorContact2Name]
                                                        ,[SponsorContact2Phone]
                                                        ,[SponsorContact2Fax]
                                                        ,[SponsorContact2email]
                                                        ,[SponsorContact2Note]
                                                        ,[SponsorNote]
                                                    FROM [DataConversion].[nonElectronic].[Template_NewSponsor]";
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Compare_Checksum_NONELECTRONIC_NonElectronicClientNAFInformation()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "Dev";
            TestSetup.section = "NonElectronicAccounts";
            TestSetup.table = "NonElectronicClientNAFInfo";
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AccountMaster\NonElectronicAccounts_06292020\";
            TestSetup.query = @"SELECT [LinkedTenantUniqueMATID]
      ,[LinkedTenantNAFSequenceID]
      ,[NAFStatus]
      ,[RegistrationTypeCode]
      ,[NAFRepIdentifier]
      ,TRIM([PrimaryAccountHolderSSNumber])[PrimaryAccountHolderSSNumber]
      ,[SSNCode]
      ,[WorkPhone]
      ,[HomePhone]
      ,NULLIF(CONVERT(varchar,[DateofBirth],23),'1753-01-01') as [DateofBirth]
      ,[NAFRegistration1]
      ,[RelationshipToAdvisorCode]
      ,[RelatedToCode]
	  ,CASE  WHEN [Charitable] = '0' THEN 'N' WHEN [Charitable] = '1' THEN 'Y' ELSE '' END as [Charitable]
      ,CASE  WHEN [Title1Erisa] = '0' THEN 'N' WHEN [Title1Erisa] = '1' THEN 'Y' ELSE '' END as [Title1Erisa]
      ,[CorporationTypeCode]
      ,[LegalAddress1]
      ,[LegalAddress2]
      ,[LegalAddress3]
      ,[LegalCity]
      ,[LegalState]
      ,[LegalZip]
      ,[LegalZipPlus4]
      ,[LegalCountry]
      ,[MailingAddress1]
      ,[MailingAddress2]
      ,[MailingAddress3]
      ,[MailingCity]
      ,[MailingState]
      ,[MailingZip]
      ,[MailingZipPlus4]
      ,[MailingCountry]
  FROM [DataConversion].[nonElectronic].[Template_NonElectronicClientNAFInfo]";
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }

        [TestMethod]
        public void Compare_Checksum_NONELECTRONIC_TRUSTEDCONTACTS()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "QA";
            TestSetup.section = "NonElectronicAccounts";
            TestSetup.table = "TrustedContacts";
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AccountMaster\NonElectronicAccounts_07302020\";
            TestSetup.query = @"SELECT [LinkedAccountNumber]
                                                  ,[LinkedClientIdentifier]
                                                  ,[SourceType]
                                                  ,[CustomerType]
                                                  ,[OptOutIndicator]
                                                  ,[TrustedContactPrimary]
                                                  ,[FirstName]
                                                  ,[MiddleName]
                                                  ,[LastName]
                                                  ,[Suffix]
                                                  ,[AddressLine1]
                                                  ,[AddressLine2]
                                                  ,[City]
                                                  ,[State]
                                                  ,[Zip]
                                                  ,[ExtendedZip]
                                                  ,[Attention]
                                                  ,[Phone]
                                              FROM [DataConversion].[nonElectronic].[Template_TrustedContacts]";
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }
            
        [TestMethod]
        public void Compare_Checksum_NONELECTRONIC_Account()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "QA";
            TestSetup.section = "NonElectronicAccounts";
            TestSetup.table = "Account";
            TestSetup.templateFilePath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Extracts\AccountMaster\NonElectronicAccounts_07302020\";
            TestSetup.query = @"SELECT [AccountDataSource]
                                                      ,[FundCompanyName]
                                                      ,[FundCompanyCode]
                                                      ,[BranchIDENTIFIER]
                                                      ,[AccountNumber]
                                                      ,[TenantUniqueMATID]
                                                      ,[TenantNAFSequenceID]
                                                      ,[TenantNAFStatus]
                                                      ,[NAFRepIdentifier]
                                                      ,[AccountName]
                                                      ,[RegistrationTypeCode]
                                                      ,[TINPrimaryAccountHolderSS]
                                                      ,[TaxIDCodeIndicator]
                                                      ,[AccountOpenDate]
                                                      ,[BusinessLine]
                                                      ,[InvestmentObjective]
                                                      ,Trim([AnnualIncome]) [AnnualIncome]
                                                      ,Trim([EstimatedNetWorth])[EstimatedNetWorth]
                                                      ,Trim([EstimatedLiquidNetWorth])[EstimatedLiquidNetWorth]
                                                      ,Trim([MaritalStatus])[MaritalStatus]
                                                      ,[NumberOfDependents]
                                                      ,Trim([TaxBracketCode])[TaxBracketCode]
                                                      ,[TimeHorizonCode]
                                                    ,Trim([InvestmentKnowledge])[InvestmentKnowledge]
                                                    ,Trim([AnnualExpenses])[AnnualExpenses]
                                                    ,Trim([SpecialExpenses])[SpecialExpenses]
                                                    ,Trim([SpecialExpensesTimeFrame])[SpecialExpensesTimeFrame]
                                                    ,Trim([InvestmentPurpose])[InvestmentPurpose]
                                                    ,Trim([InvestmentPurposeText])[InvestmentPurposeText]
                                                    ,Trim([StocksKnowledge])[StocksKnowledge]
                                                    ,Trim([BondsKnowledge])[BondsKnowledge]
                                                    ,Trim([ShortTermKnowledge])[ShortTermKnowledge]
                                                    ,Trim([MutualFundsKnowledge])[MutualFundsKnowledge]
                                                    ,Trim([OptionsKnowledge])[OptionsKnowledge]
                                                    ,Trim([VariableContractKnowledge])[VariableContractKnowledge]
                                                    ,Trim([LimitedPartnershipsKnowledge])[LimitedPartnershipsKnowledge]
                                                    ,Trim([AlternativeInvestmentsKnowledge])[AlternativeInvestmentsKnowledge]
                                                    ,Trim([FuturesKnowledge])[FuturesKnowledge]
                                                    ,Trim([AnnuityKnowledge])[AnnuityKnowledge]
                                                    ,Trim([MarginKnowledge])[MarginKnowledge]
                                                    ,Trim([ForeignCurrencyKnowledge])[ForeignCurrencyKnowledge]
                                                    ,Trim([ForeignSecuritiesKnowledge])[ForeignSecuritiesKnowledge]
                                                    ,Cast([AssetsHeldAwayAmount] as decimal(12,2))[AssetsHeldAwayAmount]
                                                    ,REPLACE(LTRIM(REPLACE([StocksHeldAwayPercent],'0',' ')),' ','0') [StocksHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([BondsHeldAwayPercent],'0',' ')),' ','0')[BondsHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([ShortTermHeldAwayPercent],'0',' ')),' ','0')[ShortTermHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([MutualFundsHeldAwayPercent],'0',' ')),' ','0')[MutualFundsHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([OptionsHeldAwayPercent],'0',' ')),' ','0')[OptionsHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([VariableContractsHeldAwayPercent],'0',' ')),' ','0')[VariableContractsHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([LimitedPartnershipsHeldAwayPercent],'0',' ')),' ','0')[LimitedPartnershipsHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([FuturesHeldAwayPercent],'0',' ')),' ','0')[FuturesHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([AlternativeInvestmentsHeldAwayPercent],'0',' ')),' ','0')[AlternativeInvestmentsHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([AnnuitiesHeldAwayPercent],'0',' ')),' ','0')[AnnuitiesHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([ForeignCurrencyHeldAwayPercent],'0',' ')),' ','0')[ForeignCurrencyHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([ForeignSecuritiesHeldAwayPercent],'0',' ')),' ','0')[ForeignSecuritiesHeldAwayPercent]
                                                    ,REPLACE(LTRIM(REPLACE([OtherHeldAwayPercent],'0',' ')),' ','0')[OtherHeldAwayPercent]
                                                      ,[ProductKnowledgeAskedNotAnswered]
                                                      ,[AssetsHeldAwayAskedNotAnswered]
                                                      ,[FundingSource]
                                                      ,[FundingSourceOther]
                                                    ,NULLIF(CONVERT(varchar,[LastRAPDate],23),'1753-01-01') as [LastRAPDate]
                                                    FROM [DataConversion].[nonElectronic].[Template_Account]";
            Template.SetupChecksumComparisionTest();
            Template.ComputeTemplateChecksum();
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }


        [TestMethod]
        public void Compare_Checksum_TemplateToPredestination()
        {
            TestSetup.module = "AccountMaster";
            TestSetup.environment = "QA";
            TestSetup.section = "NonElectronicAccounts";
            TestSetup.table = "TrustedContacts_Template";
            TestSetup.query = @"SELECT [LinkedAccountNumber]
                                                  ,[LinkedClientIdentifier]
                                                  ,[SourceType]
                                                  ,[CustomerType]
                                                  ,[OptOutIndicator]
                                                  ,[TrustedContactPrimary]
                                                  ,[FirstName]
                                                  ,[MiddleName]
                                                  ,[LastName]
                                                  ,[Suffix]
                                                  ,[AddressLine1]
                                                  ,[AddressLine2]
                                                  ,[City]
                                                  ,[State]
                                                  ,[Zip]
                                                  ,[ExtendedZip]
                                                  ,[Attention]
                                                  ,[Phone]
                                              FROM [DataConversion].[nonElectronic].[Template_TrustedContacts]";
            Template.SetupChecksumComparisionTest();
            Template.ComputeDBChecksum();
            TestSetup.table = "TrustedContacts_Predestination";
            TestSetup.query = @"SELECT [LinkedAccountNumber]
                                                  ,[LinkedClientIdentifier]
                                                  ,[SourceType]
                                                  ,[CustomerType]
                                                  ,[OptOutIndicator]
                                                  ,[TrustedContactPrimary]
                                                  ,[FirstName]
                                                  ,[MiddleName]
                                                  ,[LastName]
                                                  ,[Suffix]
                                                  ,[AddressLine1]
                                                  ,[AddressLine2]
                                                  ,[City]
                                                  ,[State]
                                                  ,[Zip]
                                                  ,[ExtendedZip]
                                                  ,[Attention]
                                                  ,[Phone]
                                              FROM [DataConversion].[nonElectronic].[Template_TrustedContacts]";
            Template.ComputeDBChecksum();
            Template.ComputeChecksumDifference();
        }
    }
    }
