Use DataConversion

declare @MaxTestID int; --Variable to caputure Last Test ID in TestResult Table
declare @TestAreaBeginning varchar(30) = 'AccountMaster' + ' '
declare @MaxTestLogID int;
-- Test Name Declaration Starts Here --
declare @TableName varchar(100) = 'PreDestination_close_AccountPERSHING';
-- Test Name Declaration Ends Here --

-- Table Column Declaration Starts Here --

declare @cTaxIDType varchar(10) = 'cTaxIDType';
declare @dtAccountClosed varchar(15) = 'dtAccountClosed';
declare @dtAccountOpened varchar(15) = 'dtAccountOpened';
declare @vchAccountNumber varchar(16) = 'vchAccountNumber';
declare @vchInvestmentProfessional varchar(25) = 'vchInvestmentProfessional';
declare @vchplanStatuscode varchar(17) = 'vchplanStatuscode';
declare @vchRegLine1 varchar(11) = 'vchRegLine1';
declare @vchRegLine2 varchar(11) = 'vchRegLine2';
declare @vchRegLine3 varchar(11) = 'vchRegLine3';
declare @vchRegLine4 varchar(11) = 'vchRegLine4';
declare @vchRegLine5 varchar(11) = 'vchRegLine5';
declare @vchRegLine6 varchar(11) = 'vchRegLine6';
declare @vchTaxIDNumber varchar(14) = 'vchTaxIDNumber';
declare @vchCountryofCitizenship varchar(23) = 'vchCountryofCitizenship';
declare @dtBirthDate varchar(11) = 'dtBirthDate';
declare @loadDate varchar(8) = 'loadDate';
declare @AccountDataSource varchar(17) = 'AccountDataSource';
declare @AccountNumber varchar(13) = 'AccountNumber';
declare @AdvisorRepIDENTIFIER varchar(20) = 'AdvisorRepIDENTIFIER';
declare @ClientIDENTIFIER varchar(16) = 'ClientIDENTIFIER';
declare @LinkedAccountNumber varchar(19) = 'LinkedAccountNumber';
declare @vchAcctKey varchar(10) = 'vchAcctKey';
declare @vchFileName varchar(11) = 'vchFileName';


-- Table Column Declaration Ends Here --

--Fixed -- Do Not Change -- 
declare @CDNE varchar(75) = ' column does not exist.';
declare @CE varchar(25) = ' column exists.';
declare @TC varchar(25) = ' Test Case #';
declare @E varchar(25) = ' exists.'
declare @DNE varchar(25) = ' does not exist.';
declare @Table varchar(10) = 'Table';
Declare @Column varchar(25) = ' column ';
Declare @HTCT varchar(30) = 'has the correct type. ';
Declare @DNHTCT varchar(40) = 'does not have the correct type.';
Declare @CPIC varchar(50) = ' column position is correct.';
Declare @CPII varchar(50) = ' column position is incorrect.';
declare @CLIC varchar(40) = ' column length is correct.';
declare @CLII varchar(40) = ' column length is incorrect.';
declare @INPWDT varchar(40) = ' is not populated with date time.';
declare @IPWDT varchar(40) = ' is populated with date time.';
declare @TCDNED varchar(40) = 'does not exist, damn it!';
declare @somearenull varchar(25) = ' some columns are null.';
declare @tasbpwvda varchar(255) = ' should be populated with valid data. ';
declare @sbpwagcir varchar(255) = ' should be populated with a good contact identifier reference.'
declare @somecolumnsblank varchar(50) = ' some columns are blank.';
declare @somerhbd varchar(50) = ' some rows have bad dates.';
declare @sciraor varchar(175) = ' some contact identifier reference are oprhaned records';
declare @cchav varchar(50) = 'changeType column has appropriate values.';
declare @ccdnhav varchar(50) = 'changeType column does not have appropriate values.'
DECLARE @MyTestDescription varchar(500)
Declare @RC int;
DECLARE @TestStatusCode int
declare @NullCount int;
declare @Position int;
declare @TypeVarChar int = 167;
declare @TypeDateTime int = 61;
declare @TypeDate int = 40;
declare @TypeInt int = 56;
declare @TypeBit int = 104;
declare @TypeBigInt int = 127;
declare @TypeNVarChar int = 231;
declare @TypeChar int = 175;
declare @TypeMoney int = 60;
declare @TypeDecimal int = 106;
declare @TypeTinyInt int = 48;
declare @TypeTUserId int = 167;
declare @TypeNumeric int = 108;
declare @Passed varchar(10) = 'Passed.';
declare @Failed varchar(10) = 'Failed!';
declare @Warning varchar(10) = 'Warning!';
declare @PrimaryKey varchar(100) = 'PrimaryKey';
declare @SystemTypeId int;
declare @LengthDateTime int = 8;
declare @LengthInt int = 4;
declare @Zero int = 0;
declare @One int = 1;
declare @Two int = 2;
declare @Three int = 3;
declare @Four int = 4;
declare @Five int = 5;
declare @Seven int = 7;
declare @Six int = 6;
declare @Eight int = 8;
declare @Nine int = 9;
declare @Ten int = 10;
declare @eleven int = 11;
declare @Twelve int = 12;
declare @Thirteen int = 13;
declare @Fourteen int = 14;
declare @Fifteen int = 15;
declare @Sixteen int = 16;
declare @Seventeen int = 17;
declare @Eighteen int = 18;
declare @Nineteen int = 19;
declare @Twenty int = 20;
declare @TwentyOne int = 21;
declare @TwentyTwo int = 22;
declare @TwentyThree int = 23;
declare @TwentyFour int = 24;
declare @TwentyFive int = 25;
declare @TwentySix int = 26;
declare @TwentySeven int = 27;
declare @TwentyEight int = 28;
declare @TwentyNine int = 29;
declare @Thirty int = 30;
declare @ThirtyOne int = 31;
declare @ThirtyTwo int = 32;
declare @ThirtyThree int = 33;
declare @ThirtyFour int = 34;
declare @ThirtyFive int = 35;
declare @ThirtySix int = 36;
declare @ThirtySeven int = 37;
declare @ThirtyEight int = 38;
declare @ThirtyNine int = 39;
declare @Forty int = 40;
declare @FortyOne int = 41;
declare @FortyTwo int = 42;
declare @FortyThree int = 43;
declare @FortyFour int = 44;
declare @FortyFive int = 45;
declare @FortySix int = 46;
declare @FortySeven int = 47;
declare @FortyEight int = 48;
declare @FortyNine int = 49;
declare @Fifty int = 50;
declare @FiftyOne int = 51;
declare @FiftyTwo int = 52;
declare @FiftyThree int = 53;
declare @FiftyFour int = 54;
declare @FiftyFive int = 55;
declare @FiftySix int = 56;
declare @FiftySeven int = 57;
declare @FiftyEight int = 58;
declare @FiftyNine int = 59;
declare @Sixty int = 60;
declare @SixtyOne int = 61;
declare @SixtyTwo int = 62;
declare @SixtyThree int = 63;
declare @SixtyFour int = 64;
declare @SixtyFive int = 65;
declare @SixtySix int = 66;
declare @SixtySeven int = 67;
declare @SixtyEight int = 68;
declare @SixtyNine int = 69;
declare @Seventy int = 70;
declare @SeventyOne int = 71;
declare @SeventyTwo int = 72;
declare @SeventyThree int = 73;
declare @SeventyFour int = 74;
declare @SeventyFive int = 75;
declare @SeventySix int = 76;
declare @SeventySeven int = 77;
declare @SeventyEight int = 78;
declare @SeventyNine int = 79;
declare @Eighty int = 80;
declare @EightyOne int = 81;
declare @EightyTwo int = 82;
declare @EightyThree int = 83;
declare @EightyFour int = 84;
declare @EightyFive int = 85;
declare @EightySix int = 86;
declare @EightySeven int = 87;
declare @EightyEight int = 88;
declare @EightyNine int = 89;
declare @Ninety int = 90;
declare @NinetyOne int = 91;
declare @NinetyTwo int = 92;
declare @NinetyThree int = 93;
declare @NinetyFour int = 94;
declare @NinetyFive int = 95;
declare @NinetySix int = 96;
declare @NinetySeven int = 97;
declare @NinetyEight int = 98;
declare @NinetyNine int = 99;
declare @Hundred int = 100;
Declare @OneHundred int = 100;
Declare @OneHundredOne int = 101;
Declare @OneHundredTwo int = 102;
Declare @OneHundredThree int = 103;
Declare @OneHundredFour int = 104;
Declare @OneHundredFive int = 105;
Declare @OneHundredSix int = 106;
Declare @OneHundredSeven int = 107;
Declare @OneHundredEight int = 108;
Declare @OneHundredNine int = 109;
Declare @OneHundredTen int = 110;
Declare @OneHundredEleven int = 111;
Declare @OneHundredTwelve int = 112;
Declare @OneHundredThirteen int = 113;
Declare @OneHundredFourteen int = 114;
Declare @OneHundredFifteen int = 115;
Declare @OneHundredSixteen int = 116;
Declare @OneHundredSeventeen int = 117;
Declare @OneHundredEighteen int = 118;
Declare @OneHundredNineteen int = 119;
Declare @OneHundredTwenty int = 120;
Declare @OneHundredTwentyOne int = 121;
Declare @OneHundredTwentyTwo int = 122;
Declare @OneHundredTwentyThree int = 123;
Declare @OneHundredTwentyfour int = 124;
declare @OneHundredTwentyFive int = 125;
declare @OneHundredTwentySix int = 126;
declare @OneHundredTwentySeven int = 127;
declare @OneHundredTwentyEight int = 128;
Declare @OneHundredTwentyNine int = 129;
Declare @OneHundredThirty int = 130;
Declare @OneHundredThirtyOne int = 131;
Declare @OneHundredThirtyTwo int = 132;
declare @OneHundredThirtyThree int = 133;
Declare @OneHundredThirtyFour int = 134;
Declare @OneHundredThirtyFive int = 135;
Declare @OneHundredThirtySix int = 136;
Declare @OneHundredThirtySeven int = 137;
Declare @OneHundredThirtyEight int = 138;
Declare @OneHundredThirtyNine int = 139;
Declare @OneHundredForty int = 140;
Declare @OneHundredFortyOne int = 141;
Declare @OneHundredFortyTwo int = 142;
Declare @OneHundredFortyThree int = 143;
Declare @OneHundredFortyFour int = 144;
declare @OneHundredFortyFive int = 145;
declare @OneHundredFortySix int = 146;
declare @OneHundredFortySeven int = 147;
Declare @OneHundredFortyEight int = 148;
Declare @OneHundredFortyNine int = 149;
Declare @OneHundredFifty int = 150;
Declare @OneHundredFiftyOne int = 151;
Declare @OneHundredFiftyTwo int = 152;
Declare @OneHundredFiftyThree int = 153;
Declare @OneHundredFiftyFour int = 154;
Declare @OneHundredFiftyFive int = 155;
Declare @OneHundredFiftySix int = 156;
Declare @OneHundredFiftySeven int = 157;
Declare @OneHundredFiftyEight int = 158;
Declare @OneHundredFiftyNine int = 159;
Declare @OneHundredSixty int = 160;
Declare @OneHundredSixtyOne int = 161;
Declare @OneHundredSixtyTwo int = 162;
Declare @OneHundredSixtyThree int = 163;
Declare @OneHundredSixtyFour int = 164;
Declare @OneHundredSixtyFive int = 165;
Declare @OneHundredSixtySix int = 166;
Declare @OneHundredSixtySeven int = 167;
Declare @OneHundredSixtyEight int = 168;
Declare @OneHundredSixtyNine int = 169;
Declare @OneHundredSeventy int = 170;
Declare @OneHundredSeventyOne int = 171;
Declare @OneHundredSeventyTwo int = 172;
declare @OneHundredSeventyThree int = 173;
Declare @OneHundredSeventyFour int = 174;
Declare @OneHundredSeventyFive int = 175;
Declare @OneHundredSeventySix int = 176;
Declare @OneHundredSeventySeven int = 177;
Declare @OneHundredSeventyEight int = 178;
Declare @OneHundredSeventyNine int = 179;
Declare @OneHundredEighty int = 180;
Declare @OneHundredEightyOne int = 181;
Declare @OneHundredEightyTwo int = 182;
Declare @OneHundredEightyThree int = 183;
Declare @OneHundredEightyFour int = 184;
Declare @OneHundredEightyFive int = 185;
Declare @OneHundredEightySix int = 186;
Declare @OneHundredEightySeven int = 187;
Declare @OneHundredEightyEight int = 188;
Declare @OneHundredEightyNine int = 189;
declare @OneHundredNinety int = 190;
Declare @OneHundredNinetyOne int = 191;
Declare @OneHundredNinetyTwo int = 192;
Declare @OneHundredNinetyThree int = 193;
Declare @OneHundredNinetyFour int = 194;
Declare @OneHundredNinetyFive int = 195;
Declare @OneHundredNinetySix int = 196;
Declare @OneHundredNinetySeven int = 197;
Declare @OneHundredNinetyEight int = 198;
Declare @OneHundredNinetyNine int = 199;
declare @TwoHundred int = 200;
Declare @TwoHundredOne int = 201;
Declare @TwoHundredTwo int = 202;
Declare @TwoHundredThree int = 203;
Declare @TwoHundredFour int = 204;
Declare @TwoHundredFive int = 205;
Declare @TwoHundredSix int = 206;
Declare @TwoHundredSeven int = 207;
Declare @TwoHundredEight int = 208;
Declare @TwoHundredNine int = 209;
Declare @TwoHundredTen int = 210;
Declare @TwoHundredEleven int = 211;
Declare @TwoHundredTwelve int = 212;
Declare @TwoHundredThirteen int = 213;
Declare @TwoHundredFourteen int = 214;
Declare @TwoHundredFifteen int = 215;
Declare @TwoHundredSixteen int = 216;
Declare @TwoHundredSeventeen int = 217;
Declare @TwoHundredEighteen int = 218;
Declare @TwoHundredNineteen int = 219;
Declare @TwoHundredTwenty int = 220;
Declare @TwoHundredTwentyOne int = 221;
Declare @TwoHundredTwentyTwo int = 222;
Declare @TwoHundredTwentyThree int = 223;
Declare @TwoHundredTwentyFour int = 224;
Declare @TwoHundredTwentyFive int = 225;
Declare @TwoHundredTwentySix int = 226;
Declare @TwoHundredTwentySeven int = 227;
Declare @TwoHundredTwentyEight int = 228;
Declare @TwoHundredTwentyNine int = 229;
Declare @TwoHundredThirty int = 230;
Declare @TwoHundredThirtyOne int = 231;
Declare @TwoHundredThirtyTwo int = 232;
Declare @TwoHundredThirtyThree int = 233;
Declare @TwoHundredThirtyFour int = 234;
Declare @TwoHundredThirtyFive int = 235;
Declare @TwoHundredThirtySix int = 236;
Declare @TwoHundredThirtySeven int = 237;
Declare @TwoHundredThirtyEight int = 238;
Declare @TwoHundredThirtyNine int = 239;
Declare @TwoHundredForty int = 240;
Declare @TwoHundredFortyOne int = 241;
Declare @TwoHundredFortyTwo int = 242;
Declare @TwoHundredFortyThree int = 243;
Declare @TwoHundredFortyFour int = 244;
Declare @TwoHundredFortyFive int = 245;
Declare @TwoHundredFortySix int = 246;
Declare @TwoHundredFortySeven int = 247;
Declare @TwoHundredFortyEight int = 248;
declare @TwoHundredFortyNine int = 249;
declare @TwoHundredFifty int = 250;
Declare @TwoHundredFiftyOne int = 251;
Declare @TwoHundredFiftyTwo int = 252;
Declare @TwoHundredFiftyThree int = 253;
Declare @TwoHundredFiftyfour int = 254;
declare @TwoHundredFiftyFive int = 255;
declare @TwoHundredFiftySix int = 256;
declare @ThreeHundredFifty int = 350;
declare @FiveHundred int = 500;
declare @OneThousand int = 1000;
declare @ThirteenHundred int = 1300;
declare @FiveThousand int = 5000;
declare @onehundreddecimal decimal = 100.00;
declare @GreaterThan100Percent int;
declare @LessThanZeroPercent int;
declare @hapgt100 varchar(100) = ' held away percent greater than 100 percent. ';
declare @haplt100 varchar(100) = ' held away percent less than 100 percent. ';
declare @thapt varchar(100) = ' The held away percent test. ';
declare @hapltz varchar(100) = ' held away percent less than zero percent. ';
declare @hapnltz varchar(100) = ' held away percent not less than zero percent. '
declare @jvt varchar(50) = ' Junk values test. ';
declare @JVRCount int;
--declare @TestDescription varchar(200);
declare @ExpectedResult varchar(100);
declare @ActualResult varchar(100);
declare @TestStatus varchar(15);
declare @TestDateTime datetime;
declare @TestMessage varchar(200);
--declare @TableNameOne varchar(30) = 'staging_plan';
declare @Schema int = 17;
declare @Insert varchar(30) = 'I-insert';
Declare @Update varchar(30) = 'U-update';
Declare @Delete varchar(30) = 'D-delete';
declare @TableObject Int;
declare @ColumnObject Int;
declare @ColumnIsNullable Int;
declare @ICN varchar(30) = ' is column nullable.';
declare @TCIN varchar(40) = ' the column is nullable. ';
declare @TCINN varchar(50) = ' the column is not nullable. ';
declare @MaxLength Int;
declare @Return int;
declare @AllowsNull int;
declare @HasNull int;
declare @ColHasNull varchar(20) = ' column has null';
declare @ColHasNoNull varchar(20)= ' column has no null';
declare @HasBlank Bit;
declare @ColHasBlank varchar(25) = ' column has blank value '
declare @ColHasNoBlank varchar(25) = ' column has no blank value '
declare @HasBlankCount int;
declare @NumBlank int;
Declare @SchemaName varchar(50) = 'AccountMaster';
--Declare @TypeDecimal int = 106;
declare @LengthTinyInt int = 1;
declare @LengthDecimal int = 17;
declare @Eigthteen int = 18;
declare @BadValueCount int;
declare @TempTableName varchar(25) = '#testResults';
Declare @TestScenario varchar(80) = 'AccountMaster PreDestination_close_AccountPERSHING'
declare @BeginningStatus varchar(25) = 'Beginning';
Declare @EndingStatus varchar(25) = 'Ending';
Declare @TestDescription varchar(800) = 'Test the PreDestination_close_AccountPERSHING table and columns confirming the properties of the columns and some data verification.';

--Get the Schema ID for the table retrieval
select @Schema = dbo.fnGetSchemaId('AccountMaster');
--Get the TableObjectId for the table using the Table Name and the SchemaId
select @TableObject = dbo.fnGetTableObject(@Schema,@TableName);
-- Test the table exists.
select @MaxTestID = (select Max(Testid) from TestResults);

select @MaxTestLogId = (select Max(TestLogId) from TestLog)

insert testLog values (@TestScenario,@BeginningStatus,@TestDescription,SYSDATETIME(),System_User)

If @TableObject is not null
Begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table + ' exists test.';
Set @ExpectedResult =  'Table Exists.';
Set @ActualResult = 'Table Exists.';
Set @TestStatus =  @Passed;          
Set @TestDateTime = SYSDATETIME();
Set @TestMessage =  @TC + trim(str(@@IDENTITY)) +' '+@Table+' '+@TableName + @E;
Set @TestStatusCode =  3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
End
	else
Begin

Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' exists test.';
Set @ExpectedResult = 'Table Exists.';
Set @ActualResult = 'Table object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Table+' '+@TableName + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
		
End
--------------------------------------------------

set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@cTaxIDType,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @cTaxIDType + ' exists test.';
Set @ExpectedResult =  @cTaxIDType + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @cTaxIDType + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @cTaxIDType + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @cTaxIDType + ' exists test.';
Set @ExpectedResult = @cTaxIDType + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@cTaxIDType + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@cTaxIDType + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@dtAccountClosed,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @dtAccountClosed + ' exists test.';
Set @ExpectedResult =  @dtAccountClosed + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @dtAccountClosed + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @dtAccountClosed + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @dtAccountClosed + ' exists test.';
Set @ExpectedResult = @dtAccountClosed + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@dtAccountClosed + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@dtAccountClosed + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@dtAccountOpened,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @dtAccountOpened + ' exists test.';
Set @ExpectedResult =  @dtAccountOpened + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @dtAccountOpened + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @dtAccountOpened + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @dtAccountOpened + ' exists test.';
Set @ExpectedResult = @dtAccountOpened + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@dtAccountOpened + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@dtAccountOpened + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchAccountNumber,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchAccountNumber + ' exists test.';
Set @ExpectedResult =  @vchAccountNumber + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchAccountNumber + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchAccountNumber + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchAccountNumber + ' exists test.';
Set @ExpectedResult = @vchAccountNumber + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchAccountNumber + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchAccountNumber + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


--set @ColumnObject = null
--select
--		@ColumnObject = dbo.fnGetColumnObjectId(@vchInvestmentProfessional,@TableObject)

--if @ColumnObject is not null
--begin		
--Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchInvestmentProfessional + ' exists test.';
--Set @ExpectedResult =  @vchInvestmentProfessional + ' ' + @Column + ' ' +@E;
--Set @ActualResult =   @vchInvestmentProfessional + ' ' + @Column + ' ' +@E;
--Set @TestStatus =   @Passed;
--Set @TestDateTime =  SysDateTime();
--Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchInvestmentProfessional + @E;
--Set @TestStatusCode = 3;

--EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end
--		else
--begin
--Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchInvestmentProfessional + ' exists test.';
--Set @ExpectedResult = @vchInvestmentProfessional + ' ' + @Column + ' ' +@E;
--Set @ActualResult =	@vchInvestmentProfessional + ' ' + @Column + ' ' +@DNE;
--Set @TestStatus = @Failed;
--Set @TestDateTime =	SysDatetime();
--Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchInvestmentProfessional + @DNE;
--Set @TestStatusCode =   1;

--EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchplanStatuscode,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchplanStatuscode + ' exists test.';
Set @ExpectedResult =  @vchplanStatuscode + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchplanStatuscode + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchplanStatuscode + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchplanStatuscode + ' exists test.';
Set @ExpectedResult = @vchplanStatuscode + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchplanStatuscode + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchplanStatuscode + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchRegLine1,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchRegLine1 + ' exists test.';
Set @ExpectedResult =  @vchRegLine1 + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchRegLine1 + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchRegLine1 + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchRegLine1 + ' exists test.';
Set @ExpectedResult = @vchRegLine1 + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchRegLine1 + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchRegLine1 + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchRegLine2,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchRegLine2 + ' exists test.';
Set @ExpectedResult =  @vchRegLine2 + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchRegLine2 + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchRegLine2 + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchRegLine2 + ' exists test.';
Set @ExpectedResult = @vchRegLine2 + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchRegLine2 + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchRegLine2 + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchRegLine3,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchRegLine3 + ' exists test.';
Set @ExpectedResult =  @vchRegLine3 + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchRegLine3 + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchRegLine3 + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchRegLine3 + ' exists test.';
Set @ExpectedResult = @vchRegLine3 + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchRegLine3 + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchRegLine3 + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchRegLine4,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchRegLine4 + ' exists test.';
Set @ExpectedResult =  @vchRegLine4 + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchRegLine4 + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchRegLine4 + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchRegLine4 + ' exists test.';
Set @ExpectedResult = @vchRegLine4 + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchRegLine4 + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchRegLine4 + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchRegLine5,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchRegLine5 + ' exists test.';
Set @ExpectedResult =  @vchRegLine5 + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchRegLine5 + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchRegLine5 + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchRegLine5 + ' exists test.';
Set @ExpectedResult = @vchRegLine5 + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchRegLine5 + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchRegLine5 + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchRegLine6,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchRegLine6 + ' exists test.';
Set @ExpectedResult =  @vchRegLine6 + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchRegLine6 + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchRegLine6 + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchRegLine6 + ' exists test.';
Set @ExpectedResult = @vchRegLine6 + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchRegLine6 + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchRegLine6 + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchTaxIDNumber,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchTaxIDNumber + ' exists test.';
Set @ExpectedResult =  @vchTaxIDNumber + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchTaxIDNumber + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchTaxIDNumber + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchTaxIDNumber + ' exists test.';
Set @ExpectedResult = @vchTaxIDNumber + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchTaxIDNumber + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchTaxIDNumber + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchCountryofCitizenship,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchCountryofCitizenship + ' exists test.';
Set @ExpectedResult =  @vchCountryofCitizenship + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchCountryofCitizenship + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchCountryofCitizenship + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchCountryofCitizenship + ' exists test.';
Set @ExpectedResult = @vchCountryofCitizenship + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchCountryofCitizenship + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchCountryofCitizenship + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@dtBirthDate,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @dtBirthDate + ' exists test.';
Set @ExpectedResult =  @dtBirthDate + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @dtBirthDate + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @dtBirthDate + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @dtBirthDate + ' exists test.';
Set @ExpectedResult = @dtBirthDate + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@dtBirthDate + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@dtBirthDate + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@loadDate,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @loadDate + ' exists test.';
Set @ExpectedResult =  @loadDate + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @loadDate + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @loadDate + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @loadDate + ' exists test.';
Set @ExpectedResult = @loadDate + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@loadDate + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@loadDate + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@AccountDataSource,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @AccountDataSource + ' exists test.';
Set @ExpectedResult =  @AccountDataSource + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @AccountDataSource + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @AccountDataSource + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @AccountDataSource + ' exists test.';
Set @ExpectedResult = @AccountDataSource + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@AccountDataSource + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@AccountDataSource + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@AccountNumber,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @AccountNumber + ' exists test.';
Set @ExpectedResult =  @AccountNumber + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @AccountNumber + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @AccountNumber + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @AccountNumber + ' exists test.';
Set @ExpectedResult = @AccountNumber + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@AccountNumber + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@AccountNumber + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@AdvisorRepIDENTIFIER,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @AdvisorRepIDENTIFIER + ' exists test.';
Set @ExpectedResult =  @AdvisorRepIDENTIFIER + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @AdvisorRepIDENTIFIER + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @AdvisorRepIDENTIFIER + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @AdvisorRepIDENTIFIER + ' exists test.';
Set @ExpectedResult = @AdvisorRepIDENTIFIER + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@AdvisorRepIDENTIFIER + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@AdvisorRepIDENTIFIER + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@ClientIDENTIFIER,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @ClientIDENTIFIER + ' exists test.';
Set @ExpectedResult =  @ClientIDENTIFIER + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @ClientIDENTIFIER + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @ClientIDENTIFIER + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @ClientIDENTIFIER + ' exists test.';
Set @ExpectedResult = @ClientIDENTIFIER + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@ClientIDENTIFIER + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@ClientIDENTIFIER + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@LinkedAccountNumber,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @LinkedAccountNumber + ' exists test.';
Set @ExpectedResult =  @LinkedAccountNumber + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @LinkedAccountNumber + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @LinkedAccountNumber + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @LinkedAccountNumber + ' exists test.';
Set @ExpectedResult = @LinkedAccountNumber + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@LinkedAccountNumber + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@LinkedAccountNumber + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchAcctKey,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchAcctKey + ' exists test.';
Set @ExpectedResult =  @vchAcctKey + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchAcctKey + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchAcctKey + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchAcctKey + ' exists test.';
Set @ExpectedResult = @vchAcctKey + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchAcctKey + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchAcctKey + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchFileName,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchFileName + ' exists test.';
Set @ExpectedResult =  @vchFileName + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchFileName + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchFileName + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchFileName + ' exists test.';
Set @ExpectedResult = @vchFileName + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchFileName + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchFileName + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@cTaxIDType)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @cTaxIDType ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @cTaxIDType + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @cTaxIDType;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @cTaxIDType + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @cTaxIDType + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @cTaxIDType + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@dtAccountClosed)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @dtAccountClosed ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @dtAccountClosed + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeDateTime =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @dtAccountClosed;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeDateTime);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @dtAccountClosed + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @dtAccountClosed + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeDateTime);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @dtAccountClosed + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@dtAccountOpened)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @dtAccountOpened ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @dtAccountOpened + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeDateTime =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @dtAccountOpened;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeDateTime);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @dtAccountOpened + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @dtAccountOpened + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeDateTime);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @dtAccountOpened + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchAccountNumber)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchAccountNumber ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchAccountNumber + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchAccountNumber;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchAccountNumber + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchAccountNumber + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchAccountNumber + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


--set @SystemTypeId = null
--select
--@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchInvestmentProfessional)
--if @SystemTypeId is null
--begin
--Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchInvestmentProfessional ;
--Set @ExpectedResult =   'Column type matched.';
--Set @ActualResult =   'Column object is null.';
--Set @TestStatus =  @Failed;
--Set @TestDateTime =   SysDatetime();
--Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchInvestmentProfessional + @Column + @DNE;
--Set @TestStatusCode =   1;
--EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;	
--	end
--if @SystemTypeId is not null

--if @TypeVarChar =  @SystemTypeId
--begin	
--	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchInvestmentProfessional;
--	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
--	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
--	Set @TestStatus =   @Passed; 
--	Set @TestDateTime =  Sysdatetime();
--	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchInvestmentProfessional + @Column + @HTCT;
--	Set @TestStatusCode =  3;

--   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;	
--end
--	else
--	begin
--	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchInvestmentProfessional + ' system type id test.';
--	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
--	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
--	Set @TestStatus = 	@Failed;
--	Set @TestDateTime =	 sysdatetime();
--	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchInvestmentProfessional + @Column + @DNHTCT;
--    Set @TestStatusCode = 1;

--   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;	
--end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchplanStatuscode)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchplanStatuscode ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchplanStatuscode + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchplanStatuscode;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchplanStatuscode + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchplanStatuscode + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchplanStatuscode + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchRegLine1)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchRegLine1 ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchRegLine1 + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchRegLine1;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchRegLine1 + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchRegLine1 + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchRegLine1 + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchRegLine2)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchRegLine2 ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchRegLine2 + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchRegLine2;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchRegLine2 + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchRegLine2 + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchRegLine2 + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchRegLine3)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchRegLine3 ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchRegLine3 + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchRegLine3;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchRegLine3 + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchRegLine3 + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchRegLine3 + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchRegLine4)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchRegLine4 ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchRegLine4 + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchRegLine4;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchRegLine4 + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchRegLine4 + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchRegLine4 + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchRegLine5)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchRegLine5 ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchRegLine5 + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchRegLine5;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchRegLine5 + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchRegLine5 + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchRegLine5 + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchRegLine6)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchRegLine6 ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchRegLine6 + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchRegLine6;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchRegLine6 + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchRegLine6 + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchRegLine6 + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchTaxIDNumber)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchTaxIDNumber ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchTaxIDNumber + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchTaxIDNumber;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchTaxIDNumber + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchTaxIDNumber + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchTaxIDNumber + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchCountryofCitizenship)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchCountryofCitizenship ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchCountryofCitizenship + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchCountryofCitizenship;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchCountryofCitizenship + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchCountryofCitizenship + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchCountryofCitizenship + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@dtBirthDate)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @dtBirthDate ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @dtBirthDate + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeDateTime =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @dtBirthDate;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeDateTime);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @dtBirthDate + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @dtBirthDate + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeDateTime);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @dtBirthDate + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@loadDate)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @loadDate ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @loadDate + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeDateTime =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @loadDate;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeDateTime);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @loadDate + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @loadDate + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeDateTime);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @loadDate + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@AccountDataSource)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @AccountDataSource ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @AccountDataSource + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @AccountDataSource;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @AccountDataSource + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @AccountDataSource + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @AccountDataSource + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@AccountNumber)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @AccountNumber ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @AccountNumber + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @AccountNumber;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @AccountNumber + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @AccountNumber + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @AccountNumber + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@AdvisorRepIDENTIFIER)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @AdvisorRepIDENTIFIER ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @AdvisorRepIDENTIFIER + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @AdvisorRepIDENTIFIER;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @AdvisorRepIDENTIFIER + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @AdvisorRepIDENTIFIER + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @AdvisorRepIDENTIFIER + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@ClientIDENTIFIER)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @ClientIDENTIFIER ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @ClientIDENTIFIER + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ClientIDENTIFIER;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ClientIDENTIFIER + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ClientIDENTIFIER + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ClientIDENTIFIER + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@LinkedAccountNumber)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @LinkedAccountNumber ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @LinkedAccountNumber + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @LinkedAccountNumber;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @LinkedAccountNumber + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @LinkedAccountNumber + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @LinkedAccountNumber + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchAcctKey)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchAcctKey ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchAcctKey + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchAcctKey;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchAcctKey + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchAcctKey + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchAcctKey + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchFileName)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchFileName ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchFileName + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchFileName;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchFileName + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchFileName + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchFileName + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@cTaxIDType,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @cTaxIDType + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @cTaxIDType + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @One
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@cTaxIDType +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @cTaxIDType + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@cTaxIDType +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @cTaxIDType + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@dtAccountClosed,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @dtAccountClosed + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @dtAccountClosed + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@dtAccountClosed +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @dtAccountClosed + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@dtAccountClosed +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @dtAccountClosed + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@dtAccountOpened,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @dtAccountOpened + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @dtAccountOpened + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@dtAccountOpened +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @dtAccountOpened + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@dtAccountOpened +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @dtAccountOpened + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchAccountNumber,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchAccountNumber + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchAccountNumber + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Twenty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAccountNumber +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAccountNumber + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAccountNumber +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchAccountNumber + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


--select @MaxLength = COLUMNPROPERTY(@TableObject,@vchInvestmentProfessional,'Precision')
--if @MaxLength is null
--begin
--Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchInvestmentProfessional + ' '+ @Column + ' length test';
--Set @ExpectedResult =    'MaxLength does NOT match!';
--Set @ActualResult =    'MaxLength set to null, column does not exist.';
--Set @TestStatus =    @Failed;
--Set @TestDateTime =    Sysdatetime();
--Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchInvestmentProfessional + @CLII;
--Set @TestStatusCode =    1; 

--EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;	

--end
--If @MaxLength is not null
--If @MaxLength = @TwoHundredFiftyFive
--begin
--Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchInvestmentProfessional +  ' length test.';
--Set @ExpectedResult = 'MaxLength matches!';
--Set @ActualResult = 'MaxLength matches!';  
--Set @TestStatus = @Passed;
--Set @TestDateTime = Sysdatetime();
--Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchInvestmentProfessional + @CLIC;
--Set @TestStatusCode = 3;

--EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end
--	else
--begin
--Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchInvestmentProfessional +  ' length test.';
--Set @ExpectedResult =    'MaxLength does NOT match!'; 
--Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
--Set @TestStatus =    @Failed; 
--Set @TestDateTime =    Sysdatetime();
--Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchInvestmentProfessional + @CLII;
--Set @TestStatusCode =   1;

--EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchplanStatuscode,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchplanStatuscode + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchplanStatuscode + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @One
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchplanStatuscode +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchplanStatuscode + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchplanStatuscode +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchplanStatuscode + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchRegLine1,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchRegLine1 + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchRegLine1 + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @ThirtyTwo
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine1 +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine1 + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine1 +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchRegLine1 + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchRegLine2,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchRegLine2 + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchRegLine2 + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @ThirtyTwo
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine2 +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine2 + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine2 +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchRegLine2 + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchRegLine3,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchRegLine3 + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchRegLine3 + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @ThirtyTwo
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine3 +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine3 + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine3 +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchRegLine3 + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchRegLine4,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchRegLine4 + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchRegLine4 + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @ThirtyTwo
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine4 +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine4 + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine4 +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchRegLine4 + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchRegLine5,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchRegLine5 + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchRegLine5 + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @ThirtyTwo
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine5 +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine5 + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine5 +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchRegLine5 + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchRegLine6,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchRegLine6 + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchRegLine6 + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @ThirtyTwo
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine6 +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine6 + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine6 +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchRegLine6 + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchTaxIDNumber,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchTaxIDNumber + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchTaxIDNumber + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchTaxIDNumber +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchTaxIDNumber + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchTaxIDNumber +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchTaxIDNumber + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchCountryofCitizenship,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchCountryofCitizenship + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchCountryofCitizenship + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchCountryofCitizenship +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchCountryofCitizenship + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchCountryofCitizenship +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchCountryofCitizenship + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@dtBirthDate,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @dtBirthDate + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @dtBirthDate + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@dtBirthDate +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @dtBirthDate + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@dtBirthDate +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @dtBirthDate + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@loadDate,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @loadDate + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @loadDate + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @loadDate + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@AccountDataSource,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @AccountDataSource + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @AccountDataSource + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@AccountDataSource +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AccountDataSource + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@AccountDataSource +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @AccountDataSource + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@AccountNumber,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @AccountNumber + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @AccountNumber + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Twenty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@AccountNumber +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AccountNumber + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@AccountNumber +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @AccountNumber + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@AdvisorRepIDENTIFIER,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @AdvisorRepIDENTIFIER + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @AdvisorRepIDENTIFIER + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @TwoHundredFiftyFive
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@AdvisorRepIDENTIFIER +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AdvisorRepIDENTIFIER + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@AdvisorRepIDENTIFIER +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @AdvisorRepIDENTIFIER + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@ClientIDENTIFIER,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @ClientIDENTIFIER + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @ClientIDENTIFIER + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @TwoHundredFiftyFive
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ClientIDENTIFIER +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ClientIDENTIFIER + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ClientIDENTIFIER +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @ClientIDENTIFIER + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@LinkedAccountNumber,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @LinkedAccountNumber + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @LinkedAccountNumber + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Fifty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LinkedAccountNumber +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LinkedAccountNumber + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LinkedAccountNumber +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @LinkedAccountNumber + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchAcctKey,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchAcctKey + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchAcctKey + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Sixty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAcctKey +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAcctKey + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAcctKey +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchAcctKey + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchFileName,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchFileName + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchFileName + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Fifty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchFileName +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchFileName + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchFileName +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchFileName + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @cTaxIDType;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @cTaxIDType + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @cTaxIDType + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Two
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@cTaxIDType +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @cTaxIDType + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@cTaxIDType +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @cTaxIDType + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @dtAccountClosed;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @dtAccountClosed + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @dtAccountClosed + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Three
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@dtAccountClosed +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @dtAccountClosed + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@dtAccountClosed +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @dtAccountClosed + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @dtAccountOpened;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @dtAccountOpened + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @dtAccountOpened + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Four
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@dtAccountOpened +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @dtAccountOpened + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@dtAccountOpened +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @dtAccountOpened + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchAccountNumber;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchAccountNumber + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchAccountNumber + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Five
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAccountNumber +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAccountNumber + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAccountNumber +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchAccountNumber + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
--select
--@Position = column_id
--from
--sys.all_columns
--where
--object_id = @TableObject 
--and
--name = @vchInvestmentProfessional;


--if @Position  is null
--begin
--Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchInvestmentProfessional + ' '+ @Column + ' position test';
--Set @ExpectedResult =    'Position matches!';
--Set @ActualResult =    'Position set to null, column does not exist.';
--Set @TestStatus =    @Failed;
--Set @TestDateTime =    Sysdatetime();
--Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchInvestmentProfessional + @CPII;
--Set @TestStatusCode =    1; 

--EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;	

--end
--If @Position  is not null
--If @Position = @Five
--begin
--Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchInvestmentProfessional +  ' position test.';
--Set @ExpectedResult = 'Position matches!';
--Set @ActualResult = 'Position matches!';  
--Set @TestStatus = @Passed;
--Set @TestDateTime = Sysdatetime();
--Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchInvestmentProfessional + @CPIC;
--Set @TestStatusCode = 3;

--EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end
--	else
--begin
--Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchInvestmentProfessional +  ' position test.';
--Set @ExpectedResult =    'Position matches!'; 
--Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
--Set @TestStatus =    @Failed; 
--Set @TestDateTime =    Sysdatetime();
--Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchInvestmentProfessional + @CPII;
--Set @TestStatusCode =   1;

--EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchplanStatuscode;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchplanStatuscode + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchplanStatuscode + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Eight
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchplanStatuscode +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchplanStatuscode + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchplanStatuscode +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchplanStatuscode + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchRegLine1;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchRegLine1 + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchRegLine1 + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Nine
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine1 +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine1 + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine1 +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchRegLine1 + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchRegLine2;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchRegLine2 + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchRegLine2 + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine2 +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine2 + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine2 +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchRegLine2 + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchRegLine3;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchRegLine3 + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchRegLine3 + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Eleven
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine3 +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine3 + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine3 +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchRegLine3 + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchRegLine4;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchRegLine4 + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchRegLine4 + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Twelve
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine4 +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine4 + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine4 +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchRegLine4 + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchRegLine5;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchRegLine5 + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchRegLine5 + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Thirteen
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine5 +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine5 + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine5 +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchRegLine5 + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchRegLine6;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchRegLine6 + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchRegLine6 + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Fourteen
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine6 +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine6 + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine6 +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchRegLine6 + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchTaxIDNumber;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchTaxIDNumber + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchTaxIDNumber + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Fifteen
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchTaxIDNumber +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchTaxIDNumber + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchTaxIDNumber +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchTaxIDNumber + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchCountryofCitizenship;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchCountryofCitizenship + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchCountryofCitizenship + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Sixteen
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchCountryofCitizenship +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchCountryofCitizenship + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchCountryofCitizenship +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchCountryofCitizenship + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @dtBirthDate;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @dtBirthDate + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @dtBirthDate + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Seventeen
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@dtBirthDate +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @dtBirthDate + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@dtBirthDate +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @dtBirthDate + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @loadDate;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @loadDate + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @loadDate + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @TwentyTwo
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @loadDate + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @AccountDataSource;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @AccountDataSource + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @AccountDataSource + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@AccountDataSource +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AccountDataSource + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@AccountDataSource +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @AccountDataSource + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @AccountNumber;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @AccountNumber + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @AccountNumber + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @TwentyFour
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@AccountNumber +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AccountNumber + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@AccountNumber +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @AccountNumber + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @AdvisorRepIDENTIFIER;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @AdvisorRepIDENTIFIER + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @AdvisorRepIDENTIFIER + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @TwentyFive
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@AdvisorRepIDENTIFIER +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AdvisorRepIDENTIFIER + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@AdvisorRepIDENTIFIER +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @AdvisorRepIDENTIFIER + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @ClientIDENTIFIER;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @ClientIDENTIFIER + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @ClientIDENTIFIER + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @TwentySix
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ClientIDENTIFIER +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ClientIDENTIFIER + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ClientIDENTIFIER +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @ClientIDENTIFIER + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @LinkedAccountNumber;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @LinkedAccountNumber + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @LinkedAccountNumber + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @TwentySeven
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LinkedAccountNumber +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LinkedAccountNumber + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LinkedAccountNumber +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @LinkedAccountNumber + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchAcctKey;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchAcctKey + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchAcctKey + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Nineteen
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAcctKey +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAcctKey + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAcctKey +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchAcctKey + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchFileName;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchFileName + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchFileName + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Eighteen
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchFileName +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchFileName + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchFileName +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchFileName + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@cTaxIDType,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_close_AccountPERSHING WHERE @cTaxIDType IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@cTaxIDType +  ' Has nullable test.';
Set @ExpectedResult = @cTaxIDType+ @ColHasNull+trim(str(@One));
Set @ActualResult = @cTaxIDType+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @cTaxIDType + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@cTaxIDType +  ' Has nullable test.'; 
Set @ExpectedResult = @cTaxIDType+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@cTaxIDType+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @cTaxIDType + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@cTaxIDType +  ' Has nullable test.';
Set @ExpectedResult =    @cTaxIDType+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @cTaxIDType+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @cTaxIDType + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@cTaxIDType +  ' Has nullable test.';
Set @ExpectedResult = @cTaxIDType+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @cTaxIDType+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @cTaxIDType + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@dtAccountClosed,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_close_AccountPERSHING WHERE @dtAccountClosed IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@dtAccountClosed +  ' Has nullable test.';
Set @ExpectedResult = @dtAccountClosed+ @ColHasNull+trim(str(@One));
Set @ActualResult = @dtAccountClosed+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @dtAccountClosed + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@dtAccountClosed +  ' Has nullable test.'; 
Set @ExpectedResult = @dtAccountClosed+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@dtAccountClosed+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @dtAccountClosed + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@dtAccountClosed +  ' Has nullable test.';
Set @ExpectedResult =    @dtAccountClosed+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @dtAccountClosed+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @dtAccountClosed + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@dtAccountClosed +  ' Has nullable test.';
Set @ExpectedResult = @dtAccountClosed+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @dtAccountClosed+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @dtAccountClosed + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@dtAccountOpened,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_close_AccountPERSHING WHERE @dtAccountOpened IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@dtAccountOpened +  ' Has nullable test.';
Set @ExpectedResult = @dtAccountOpened+ @ColHasNull+trim(str(@One));
Set @ActualResult = @dtAccountOpened+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @dtAccountOpened + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@dtAccountOpened +  ' Has nullable test.'; 
Set @ExpectedResult = @dtAccountOpened+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@dtAccountOpened+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @dtAccountOpened + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@dtAccountOpened +  ' Has nullable test.';
Set @ExpectedResult =    @dtAccountOpened+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @dtAccountOpened+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @dtAccountOpened + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@dtAccountOpened +  ' Has nullable test.';
Set @ExpectedResult = @dtAccountOpened+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @dtAccountOpened+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @dtAccountOpened + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchAccountNumber,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_close_AccountPERSHING WHERE @vchAccountNumber IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAccountNumber +  ' Has nullable test.';
Set @ExpectedResult = @vchAccountNumber+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchAccountNumber+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAccountNumber + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchAccountNumber +  ' Has nullable test.'; 
Set @ExpectedResult = @vchAccountNumber+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchAccountNumber+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAccountNumber + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchAccountNumber +  ' Has nullable test.';
Set @ExpectedResult =    @vchAccountNumber+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchAccountNumber+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAccountNumber + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchAccountNumber +  ' Has nullable test.';
Set @ExpectedResult = @vchAccountNumber+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchAccountNumber+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAccountNumber + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
--Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchInvestmentProfessional,'AllowsNull')
	
--Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_close_AccountPERSHING WHERE @vchInvestmentProfessional IS NULL)
--			THEN CAST(1 AS BIT)
--			ELSE CAST(0 AS BIT) END)

--if @AllowsNull = 1 AND @HasNull = 1
--begin
--Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchInvestmentProfessional +  ' Has nullable test.';
--Set @ExpectedResult = @vchInvestmentProfessional+ @ColHasNull+trim(str(@One));
--Set @ActualResult = @vchInvestmentProfessional+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
--Set @TestStatus = @Passed;
--Set @TestDateTime = Sysdatetime();
--Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchInvestmentProfessional + @ColHasNull;
--Set @TestStatusCode = 3;

--EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;	
--end
--if @AllowsNull = 1 AND @HasNull = 0
--begin	
--Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchInvestmentProfessional +  ' Has nullable test.'; 
--Set @ExpectedResult = @vchInvestmentProfessional+ @ColHasNoNull+trim(str(@One));
--Set @ActualResult =@vchInvestmentProfessional+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
--Set @TestStatus = @Passed;
--Set @TestDateTime = Sysdatetime();
--Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchInvestmentProfessional + @ColHasNoNull;
--Set @TestStatusCode = 3;

--EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;	

--end
--if @AllowsNull = 0 AND @HasNull = 1
--begin
--Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchInvestmentProfessional +  ' Has nullable test.';
--Set @ExpectedResult =    @vchInvestmentProfessional+ @ColHasNull+trim(str(@One));
--Set @ActualResult =    @vchInvestmentProfessional+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
--Set @TestStatus =    @Failed;
--Set @TestDateTime =    Sysdatetime();
--Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchInvestmentProfessional + @ColHasNull;
--Set @TestStatusCode =    1;

--EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;	
--end
--if @AllowsNull = 0 AND @HasNull = 0
--begin
--Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchInvestmentProfessional +  ' Has nullable test.';
--Set @ExpectedResult = @vchInvestmentProfessional+ @ColHasNoNull+trim(str(@One));
--Set @ActualResult = @vchInvestmentProfessional+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
--Set @TestStatus = @Passed;
--Set @TestDateTime = Sysdatetime();
--Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchInvestmentProfessional + @ColHasNoNull;
--Set @TestStatusCode = 3;

--EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchplanStatuscode,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_close_AccountPERSHING WHERE @vchplanStatuscode IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchplanStatuscode +  ' Has nullable test.';
Set @ExpectedResult = @vchplanStatuscode+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchplanStatuscode+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchplanStatuscode + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchplanStatuscode +  ' Has nullable test.'; 
Set @ExpectedResult = @vchplanStatuscode+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchplanStatuscode+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchplanStatuscode + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchplanStatuscode +  ' Has nullable test.';
Set @ExpectedResult =    @vchplanStatuscode+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchplanStatuscode+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchplanStatuscode + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchplanStatuscode +  ' Has nullable test.';
Set @ExpectedResult = @vchplanStatuscode+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchplanStatuscode+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchplanStatuscode + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchRegLine1,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_close_AccountPERSHING WHERE @vchRegLine1 IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine1 +  ' Has nullable test.';
Set @ExpectedResult = @vchRegLine1+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchRegLine1+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine1 + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchRegLine1 +  ' Has nullable test.'; 
Set @ExpectedResult = @vchRegLine1+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchRegLine1+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine1 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine1 +  ' Has nullable test.';
Set @ExpectedResult =    @vchRegLine1+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchRegLine1+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine1 + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine1 +  ' Has nullable test.';
Set @ExpectedResult = @vchRegLine1+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchRegLine1+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine1 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchRegLine2,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_close_AccountPERSHING WHERE @vchRegLine2 IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine2 +  ' Has nullable test.';
Set @ExpectedResult = @vchRegLine2+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchRegLine2+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine2 + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchRegLine2 +  ' Has nullable test.'; 
Set @ExpectedResult = @vchRegLine2+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchRegLine2+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine2 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine2 +  ' Has nullable test.';
Set @ExpectedResult =    @vchRegLine2+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchRegLine2+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine2 + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine2 +  ' Has nullable test.';
Set @ExpectedResult = @vchRegLine2+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchRegLine2+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine2 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchRegLine3,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_close_AccountPERSHING WHERE @vchRegLine3 IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine3 +  ' Has nullable test.';
Set @ExpectedResult = @vchRegLine3+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchRegLine3+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine3 + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchRegLine3 +  ' Has nullable test.'; 
Set @ExpectedResult = @vchRegLine3+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchRegLine3+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine3 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine3 +  ' Has nullable test.';
Set @ExpectedResult =    @vchRegLine3+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchRegLine3+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine3 + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine3 +  ' Has nullable test.';
Set @ExpectedResult = @vchRegLine3+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchRegLine3+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine3 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchRegLine4,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_close_AccountPERSHING WHERE @vchRegLine4 IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine4 +  ' Has nullable test.';
Set @ExpectedResult = @vchRegLine4+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchRegLine4+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine4 + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchRegLine4 +  ' Has nullable test.'; 
Set @ExpectedResult = @vchRegLine4+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchRegLine4+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine4 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine4 +  ' Has nullable test.';
Set @ExpectedResult =    @vchRegLine4+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchRegLine4+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine4 + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine4 +  ' Has nullable test.';
Set @ExpectedResult = @vchRegLine4+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchRegLine4+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine4 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchRegLine5,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_close_AccountPERSHING WHERE @vchRegLine5 IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine5 +  ' Has nullable test.';
Set @ExpectedResult = @vchRegLine5+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchRegLine5+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine5 + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchRegLine5 +  ' Has nullable test.'; 
Set @ExpectedResult = @vchRegLine5+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchRegLine5+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine5 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine5 +  ' Has nullable test.';
Set @ExpectedResult =    @vchRegLine5+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchRegLine5+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine5 + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine5 +  ' Has nullable test.';
Set @ExpectedResult = @vchRegLine5+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchRegLine5+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine5 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchRegLine6,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_close_AccountPERSHING WHERE @vchRegLine6 IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine6 +  ' Has nullable test.';
Set @ExpectedResult = @vchRegLine6+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchRegLine6+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine6 + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchRegLine6 +  ' Has nullable test.'; 
Set @ExpectedResult = @vchRegLine6+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchRegLine6+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine6 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine6 +  ' Has nullable test.';
Set @ExpectedResult =    @vchRegLine6+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchRegLine6+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine6 + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchRegLine6 +  ' Has nullable test.';
Set @ExpectedResult = @vchRegLine6+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchRegLine6+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchRegLine6 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchTaxIDNumber,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_close_AccountPERSHING WHERE @vchTaxIDNumber IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchTaxIDNumber +  ' Has nullable test.';
Set @ExpectedResult = @vchTaxIDNumber+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchTaxIDNumber+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchTaxIDNumber + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchTaxIDNumber +  ' Has nullable test.'; 
Set @ExpectedResult = @vchTaxIDNumber+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchTaxIDNumber+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchTaxIDNumber + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchTaxIDNumber +  ' Has nullable test.';
Set @ExpectedResult =    @vchTaxIDNumber+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchTaxIDNumber+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchTaxIDNumber + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchTaxIDNumber +  ' Has nullable test.';
Set @ExpectedResult = @vchTaxIDNumber+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchTaxIDNumber+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchTaxIDNumber + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchCountryofCitizenship,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_close_AccountPERSHING WHERE @vchCountryofCitizenship IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchCountryofCitizenship +  ' Has nullable test.';
Set @ExpectedResult = @vchCountryofCitizenship+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchCountryofCitizenship+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchCountryofCitizenship + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchCountryofCitizenship +  ' Has nullable test.'; 
Set @ExpectedResult = @vchCountryofCitizenship+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchCountryofCitizenship+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchCountryofCitizenship + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchCountryofCitizenship +  ' Has nullable test.';
Set @ExpectedResult =    @vchCountryofCitizenship+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchCountryofCitizenship+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchCountryofCitizenship + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchCountryofCitizenship +  ' Has nullable test.';
Set @ExpectedResult = @vchCountryofCitizenship+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchCountryofCitizenship+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchCountryofCitizenship + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@dtBirthDate,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_close_AccountPERSHING WHERE @dtBirthDate IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@dtBirthDate +  ' Has nullable test.';
Set @ExpectedResult = @dtBirthDate+ @ColHasNull+trim(str(@One));
Set @ActualResult = @dtBirthDate+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @dtBirthDate + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@dtBirthDate +  ' Has nullable test.'; 
Set @ExpectedResult = @dtBirthDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@dtBirthDate+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @dtBirthDate + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@dtBirthDate +  ' Has nullable test.';
Set @ExpectedResult =    @dtBirthDate+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @dtBirthDate+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @dtBirthDate + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@dtBirthDate +  ' Has nullable test.';
Set @ExpectedResult = @dtBirthDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @dtBirthDate+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @dtBirthDate + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@loadDate,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_close_AccountPERSHING WHERE @loadDate IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' Has nullable test.';
Set @ExpectedResult = @loadDate+ @ColHasNull+trim(str(@One));
Set @ActualResult = @loadDate+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' Has nullable test.'; 
Set @ExpectedResult = @loadDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@loadDate+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' Has nullable test.';
Set @ExpectedResult =    @loadDate+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @loadDate+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' Has nullable test.';
Set @ExpectedResult = @loadDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @loadDate+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@AccountDataSource,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_close_AccountPERSHING WHERE @AccountDataSource IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@AccountDataSource +  ' Has nullable test.';
Set @ExpectedResult = @AccountDataSource+ @ColHasNull+trim(str(@One));
Set @ActualResult = @AccountDataSource+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AccountDataSource + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@AccountDataSource +  ' Has nullable test.'; 
Set @ExpectedResult = @AccountDataSource+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@AccountDataSource+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AccountDataSource + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@AccountDataSource +  ' Has nullable test.';
Set @ExpectedResult =    @AccountDataSource+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @AccountDataSource+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @AccountDataSource + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@AccountDataSource +  ' Has nullable test.';
Set @ExpectedResult = @AccountDataSource+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @AccountDataSource+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AccountDataSource + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@AccountNumber,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_close_AccountPERSHING WHERE @AccountNumber IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@AccountNumber +  ' Has nullable test.';
Set @ExpectedResult = @AccountNumber+ @ColHasNull+trim(str(@One));
Set @ActualResult = @AccountNumber+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AccountNumber + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@AccountNumber +  ' Has nullable test.'; 
Set @ExpectedResult = @AccountNumber+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@AccountNumber+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AccountNumber + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@AccountNumber +  ' Has nullable test.';
Set @ExpectedResult =    @AccountNumber+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @AccountNumber+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @AccountNumber + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@AccountNumber +  ' Has nullable test.';
Set @ExpectedResult = @AccountNumber+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @AccountNumber+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AccountNumber + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@AdvisorRepIDENTIFIER,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_close_AccountPERSHING WHERE @AdvisorRepIDENTIFIER IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@AdvisorRepIDENTIFIER +  ' Has nullable test.';
Set @ExpectedResult = @AdvisorRepIDENTIFIER+ @ColHasNull+trim(str(@One));
Set @ActualResult = @AdvisorRepIDENTIFIER+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AdvisorRepIDENTIFIER + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@AdvisorRepIDENTIFIER +  ' Has nullable test.'; 
Set @ExpectedResult = @AdvisorRepIDENTIFIER+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@AdvisorRepIDENTIFIER+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AdvisorRepIDENTIFIER + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@AdvisorRepIDENTIFIER +  ' Has nullable test.';
Set @ExpectedResult =    @AdvisorRepIDENTIFIER+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @AdvisorRepIDENTIFIER+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @AdvisorRepIDENTIFIER + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@AdvisorRepIDENTIFIER +  ' Has nullable test.';
Set @ExpectedResult = @AdvisorRepIDENTIFIER+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @AdvisorRepIDENTIFIER+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AdvisorRepIDENTIFIER + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@ClientIDENTIFIER,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_close_AccountPERSHING WHERE @ClientIDENTIFIER IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ClientIDENTIFIER +  ' Has nullable test.';
Set @ExpectedResult = @ClientIDENTIFIER+ @ColHasNull+trim(str(@One));
Set @ActualResult = @ClientIDENTIFIER+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ClientIDENTIFIER + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@ClientIDENTIFIER +  ' Has nullable test.'; 
Set @ExpectedResult = @ClientIDENTIFIER+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@ClientIDENTIFIER+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ClientIDENTIFIER + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@ClientIDENTIFIER +  ' Has nullable test.';
Set @ExpectedResult =    @ClientIDENTIFIER+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @ClientIDENTIFIER+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @ClientIDENTIFIER + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@ClientIDENTIFIER +  ' Has nullable test.';
Set @ExpectedResult = @ClientIDENTIFIER+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @ClientIDENTIFIER+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ClientIDENTIFIER + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@LinkedAccountNumber,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_close_AccountPERSHING WHERE @LinkedAccountNumber IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LinkedAccountNumber +  ' Has nullable test.';
Set @ExpectedResult = @LinkedAccountNumber+ @ColHasNull+trim(str(@One));
Set @ActualResult = @LinkedAccountNumber+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LinkedAccountNumber + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@LinkedAccountNumber +  ' Has nullable test.'; 
Set @ExpectedResult = @LinkedAccountNumber+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@LinkedAccountNumber+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LinkedAccountNumber + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@LinkedAccountNumber +  ' Has nullable test.';
Set @ExpectedResult =    @LinkedAccountNumber+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @LinkedAccountNumber+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @LinkedAccountNumber + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@LinkedAccountNumber +  ' Has nullable test.';
Set @ExpectedResult = @LinkedAccountNumber+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @LinkedAccountNumber+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LinkedAccountNumber + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchAcctKey,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_close_AccountPERSHING WHERE @vchAcctKey IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAcctKey +  ' Has nullable test.';
Set @ExpectedResult = @vchAcctKey+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchAcctKey+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAcctKey + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchAcctKey +  ' Has nullable test.'; 
Set @ExpectedResult = @vchAcctKey+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchAcctKey+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAcctKey + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchAcctKey +  ' Has nullable test.';
Set @ExpectedResult =    @vchAcctKey+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchAcctKey+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAcctKey + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchAcctKey +  ' Has nullable test.';
Set @ExpectedResult = @vchAcctKey+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchAcctKey+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAcctKey + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchFileName,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_close_AccountPERSHING WHERE @vchFileName IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchFileName +  ' Has nullable test.';
Set @ExpectedResult = @vchFileName+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchFileName+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchFileName + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchFileName +  ' Has nullable test.'; 
Set @ExpectedResult = @vchFileName+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchFileName+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchFileName + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchFileName +  ' Has nullable test.';
Set @ExpectedResult =    @vchFileName+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchFileName+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchFileName + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchFileName +  ' Has nullable test.';
Set @ExpectedResult = @vchFileName+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchFileName+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchFileName + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where cTaxIDType = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'cTaxIDType' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'cTaxIDType' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='cTaxIDType' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'cTaxIDType' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'cTaxIDType' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'cTaxIDType' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'cTaxIDType' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'cTaxIDType' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where dtAccountClosed = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'dtAccountClosed' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'dtAccountClosed' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='dtAccountClosed' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtAccountClosed' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'dtAccountClosed' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'dtAccountClosed' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'dtAccountClosed' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtAccountClosed' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where dtAccountOpened = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'dtAccountOpened' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'dtAccountOpened' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='dtAccountOpened' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtAccountOpened' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'dtAccountOpened' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'dtAccountOpened' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'dtAccountOpened' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtAccountOpened' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchAccountNumber = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAccountNumber' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchAccountNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchAccountNumber' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAccountNumber' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchAccountNumber' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchAccountNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchAccountNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAccountNumber' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


--select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchInvestmentProfessional = '';

--If @NumBlank = 0
--begin
--Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchInvestmentProfessional' + @Column+ @somecolumnsblank + 'test.';
--Set @ExpectedResult = 'vchInvestmentProfessional' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
--Set @ActualResult ='vchInvestmentProfessional' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
--Set @TestStatus = @Passed;
--Set @TestDateTime = Sysdatetime();
--Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchInvestmentProfessional' + @Column + @somecolumnsblank + @tasbpwvda;
--Set @TestStatusCode = 3;

--EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end
--else
--begin
--Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchInvestmentProfessional' + @Column+ @somecolumnsblank + 'test.';
--Set @ExpectedResult = 'vchInvestmentProfessional' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
--Set @ActualResult = 'vchInvestmentProfessional' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
--Set @TestStatus = @Warning;
--Set @TestDateTime = Sysdatetime();
--Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchInvestmentProfessional' + @Column + @somecolumnsblank + @tasbpwvda;
--Set @TestStatusCode = 2;
--EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end
--set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchplanStatuscode = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchplanStatuscode' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchplanStatuscode' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchplanStatuscode' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchplanStatuscode' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchplanStatuscode' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchplanStatuscode' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchplanStatuscode' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchplanStatuscode' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchRegLine1 = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine1' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchRegLine1' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchRegLine1' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine1' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchRegLine1' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchRegLine1' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchRegLine1' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine1' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchRegLine2 = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine2' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchRegLine2' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchRegLine2' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine2' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchRegLine2' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchRegLine2' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchRegLine2' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine2' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchRegLine3 = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine3' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchRegLine3' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchRegLine3' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine3' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchRegLine3' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchRegLine3' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchRegLine3' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine3' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchRegLine4 = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine4' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchRegLine4' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchRegLine4' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine4' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchRegLine4' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchRegLine4' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchRegLine4' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine4' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchRegLine5 = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine5' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchRegLine5' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchRegLine5' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine5' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchRegLine5' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchRegLine5' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchRegLine5' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine5' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchRegLine6 = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine6' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchRegLine6' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchRegLine6' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine6' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchRegLine6' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchRegLine6' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchRegLine6' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine6' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchTaxIDNumber = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchTaxIDNumber' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchTaxIDNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchTaxIDNumber' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchTaxIDNumber' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchTaxIDNumber' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchTaxIDNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchTaxIDNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchTaxIDNumber' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchCountryofCitizenship = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchCountryofCitizenship' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchCountryofCitizenship' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchCountryofCitizenship' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchCountryofCitizenship' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchCountryofCitizenship' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchCountryofCitizenship' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchCountryofCitizenship' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchCountryofCitizenship' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where dtBirthDate = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'dtBirthDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'dtBirthDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='dtBirthDate' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtBirthDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'dtBirthDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'dtBirthDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'dtBirthDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtBirthDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where loadDate = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'loadDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'loadDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='loadDate' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'loadDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'loadDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'loadDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where AccountDataSource = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'AccountDataSource' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'AccountDataSource' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='AccountDataSource' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AccountDataSource' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'AccountDataSource' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'AccountDataSource' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'AccountDataSource' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AccountDataSource' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where AccountNumber = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'AccountNumber' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'AccountNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='AccountNumber' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AccountNumber' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'AccountNumber' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'AccountNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'AccountNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AccountNumber' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where AdvisorRepIDENTIFIER = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'AdvisorRepIDENTIFIER' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'AdvisorRepIDENTIFIER' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='AdvisorRepIDENTIFIER' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AdvisorRepIDENTIFIER' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'AdvisorRepIDENTIFIER' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'AdvisorRepIDENTIFIER' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'AdvisorRepIDENTIFIER' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AdvisorRepIDENTIFIER' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where ClientIDENTIFIER = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ClientIDENTIFIER' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ClientIDENTIFIER' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='ClientIDENTIFIER' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ClientIDENTIFIER' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'ClientIDENTIFIER' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ClientIDENTIFIER' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ClientIDENTIFIER' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ClientIDENTIFIER' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where LinkedAccountNumber = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LinkedAccountNumber' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'LinkedAccountNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='LinkedAccountNumber' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LinkedAccountNumber' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'LinkedAccountNumber' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'LinkedAccountNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LinkedAccountNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LinkedAccountNumber' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchAcctKey = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAcctKey' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchAcctKey' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchAcctKey' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAcctKey' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchAcctKey' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchAcctKey' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchAcctKey' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAcctKey' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchFileName = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchFileName' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchFileName' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchFileName' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchFileName' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchFileName' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchFileName' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchFileName' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchFileName' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where cTaxIDType is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'cTaxIDType' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'cTaxIDType' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'cTaxIDType' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'cTaxIDType' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'cTaxIDType' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'cTaxIDType' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'cTaxIDType' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'cTaxIDType' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where dtAccountClosed is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'dtAccountClosed' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'dtAccountClosed' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'dtAccountClosed' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtAccountClosed' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'dtAccountClosed' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'dtAccountClosed' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'dtAccountClosed' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtAccountClosed' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where dtAccountOpened is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'dtAccountOpened' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'dtAccountOpened' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'dtAccountOpened' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtAccountOpened' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'dtAccountOpened' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'dtAccountOpened' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'dtAccountOpened' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtAccountOpened' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchAccountNumber is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAccountNumber' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchAccountNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchAccountNumber' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAccountNumber' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAccountNumber' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchAccountNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchAccountNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAccountNumber' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


--select
--@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchInvestmentProfessional is null

--If @NullCount = 0

--Begin
--Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchInvestmentProfessional' + @Column+ @somearenull + 'test.';
--Set @ExpectedResult = 'vchInvestmentProfessional' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
--Set @ActualResult = 'vchInvestmentProfessional' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
--Set @TestStatus = @Passed;
--Set @TestDateTime = Sysdatetime();
--Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchInvestmentProfessional' + @Column + @somearenull + @tasbpwvda;
--Set @TestStatusCode = 3;
--EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end
--else
--begin

--Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchInvestmentProfessional' + @Column+ @somearenull + 'test.';
--Set @ExpectedResult = 'vchInvestmentProfessional' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
--Set @ActualResult = 'vchInvestmentProfessional' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
--Set @TestStatus = @Warning;
--Set @TestDateTime = Sysdatetime();
--Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchInvestmentProfessional' + @Column + @somearenull + @tasbpwvda;
--Set @TestStatusCode = 2;
--EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end
--set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchplanStatuscode is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchplanStatuscode' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchplanStatuscode' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchplanStatuscode' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchplanStatuscode' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchplanStatuscode' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchplanStatuscode' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchplanStatuscode' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchplanStatuscode' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchRegLine1 is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine1' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchRegLine1' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchRegLine1' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine1' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine1' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchRegLine1' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchRegLine1' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine1' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchRegLine2 is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine2' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchRegLine2' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchRegLine2' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine2' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine2' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchRegLine2' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchRegLine2' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine2' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchRegLine3 is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine3' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchRegLine3' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchRegLine3' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine3' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine3' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchRegLine3' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchRegLine3' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine3' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchRegLine4 is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine4' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchRegLine4' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchRegLine4' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine4' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine4' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchRegLine4' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchRegLine4' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine4' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchRegLine5 is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine5' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchRegLine5' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchRegLine5' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine5' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine5' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchRegLine5' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchRegLine5' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine5' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchRegLine6 is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine6' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchRegLine6' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchRegLine6' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine6' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine6' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchRegLine6' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchRegLine6' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine6' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchTaxIDNumber is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchTaxIDNumber' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchTaxIDNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchTaxIDNumber' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchTaxIDNumber' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchTaxIDNumber' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchTaxIDNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchTaxIDNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchTaxIDNumber' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchCountryofCitizenship is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchCountryofCitizenship' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchCountryofCitizenship' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchCountryofCitizenship' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchCountryofCitizenship' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchCountryofCitizenship' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchCountryofCitizenship' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchCountryofCitizenship' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchCountryofCitizenship' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where dtBirthDate is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'dtBirthDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'dtBirthDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'dtBirthDate' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtBirthDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'dtBirthDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'dtBirthDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'dtBirthDate' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtBirthDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where loadDate is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'loadDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'loadDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'loadDate' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'loadDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'loadDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'loadDate' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where AccountDataSource is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'AccountDataSource' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'AccountDataSource' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'AccountDataSource' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AccountDataSource' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'AccountDataSource' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'AccountDataSource' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'AccountDataSource' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AccountDataSource' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where AccountNumber is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'AccountNumber' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'AccountNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'AccountNumber' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AccountNumber' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'AccountNumber' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'AccountNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'AccountNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AccountNumber' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where AdvisorRepIDENTIFIER is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'AdvisorRepIDENTIFIER' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'AdvisorRepIDENTIFIER' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'AdvisorRepIDENTIFIER' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AdvisorRepIDENTIFIER' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'AdvisorRepIDENTIFIER' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'AdvisorRepIDENTIFIER' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'AdvisorRepIDENTIFIER' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AdvisorRepIDENTIFIER' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where ClientIDENTIFIER is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ClientIDENTIFIER' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ClientIDENTIFIER' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ClientIDENTIFIER' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ClientIDENTIFIER' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ClientIDENTIFIER' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ClientIDENTIFIER' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ClientIDENTIFIER' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ClientIDENTIFIER' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where LinkedAccountNumber is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LinkedAccountNumber' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'LinkedAccountNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LinkedAccountNumber' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LinkedAccountNumber' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LinkedAccountNumber' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'LinkedAccountNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LinkedAccountNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LinkedAccountNumber' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchAcctKey is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAcctKey' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchAcctKey' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchAcctKey' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAcctKey' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAcctKey' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchAcctKey' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchAcctKey' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAcctKey' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_close_AccountPERSHING where vchFileName is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchFileName' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchFileName' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchFileName' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchFileName' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchFileName' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchFileName' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchFileName' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchFileName' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
if not exists (select cTaxIDType 
from
AccountMaster.PreDestination_close_AccountPERSHING
where 
cTaxIDType like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'cTaxIDType'  + @Column+ @jvt ;
Set @ExpectedResult = 'cTaxIDType'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'cTaxIDType' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'cTaxIDType'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'cTaxIDType'  + @Column+ @jvt;
Set @ExpectedResult = 'cTaxIDType'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'cTaxIDType'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'cTaxIDType'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

if not exists (select cTaxIDType 
from
AccountMaster.PreDestination_close_AccountPERSHING
where 
dtAccountClosed like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'dtAccountClosed'  + @Column+ @jvt ;
Set @ExpectedResult = 'dtAccountClosed'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'dtAccountClosed' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtAccountClosed'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'dtAccountClosed'  + @Column+ @jvt;
Set @ExpectedResult = 'dtAccountClosed'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'dtAccountClosed'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtAccountClosed'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

if not exists (select cTaxIDType 
from
AccountMaster.PreDestination_close_AccountPERSHING
where 
dtAccountOpened like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'dtAccountOpened'  + @Column+ @jvt ;
Set @ExpectedResult = 'dtAccountOpened'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'dtAccountOpened' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtAccountOpened'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'dtAccountOpened'  + @Column+ @jvt;
Set @ExpectedResult = 'dtAccountOpened'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'dtAccountOpened'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtAccountOpened'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

if not exists (select cTaxIDType 
from
AccountMaster.PreDestination_close_AccountPERSHING
where 
vchAccountNumber like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAccountNumber'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchAccountNumber'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchAccountNumber' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAccountNumber'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAccountNumber'  + @Column+ @jvt;
Set @ExpectedResult = 'vchAccountNumber'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchAccountNumber'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAccountNumber'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

--if not exists (select cTaxIDType 
--from
--AccountMaster.PreDestination_close_AccountPERSHING
--where 
--vchInvestmentProfessional like '%|%')
--begin
----Select 'The data in the field is clean.' Pass
--Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchInvestmentProfessional'  + @Column+ @jvt ;
--Set @ExpectedResult = 'vchInvestmentProfessional'  + @Column + ' The data in the field is clean.';
--Set @ActualResult = 'vchInvestmentProfessional' + @Column + ' The data in the field is clean.';
--Set @TestStatus = @Passed;
--Set @TestDateTime = Sysdatetime();
--Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchInvestmentProfessional'  + @Column + @jvt;
--Set @TestStatusCode = 3;

--EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end
--else
--begin
----Select 'There is an invalid character in the field.' Fail
--Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchInvestmentProfessional'  + @Column+ @jvt;
--Set @ExpectedResult = 'vchInvestmentProfessional'  +  @Column + ' The data in the field is clean.';
--Set @ActualResult = 'vchInvestmentProfessional'  + @Column + ' There is an invalid character in the field.';
--Set @TestStatus = @Failed;
--Set @TestDateTime = Sysdatetime();
--Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchInvestmentProfessional'  + @Column + @jvt;
--Set @TestStatusCode = 1;


--EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

if not exists (select cTaxIDType 
from
AccountMaster.PreDestination_close_AccountPERSHING
where 
vchplanStatuscode like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchplanStatuscode'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchplanStatuscode'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchplanStatuscode' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchplanStatuscode'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchplanStatuscode'  + @Column+ @jvt;
Set @ExpectedResult = 'vchplanStatuscode'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchplanStatuscode'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchplanStatuscode'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

if not exists (select cTaxIDType 
from
AccountMaster.PreDestination_close_AccountPERSHING
where 
vchRegLine1 like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine1'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchRegLine1'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchRegLine1' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine1'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine1'  + @Column+ @jvt;
Set @ExpectedResult = 'vchRegLine1'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchRegLine1'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine1'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

if not exists (select cTaxIDType 
from
AccountMaster.PreDestination_close_AccountPERSHING
where 
vchRegLine2 like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine2'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchRegLine2'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchRegLine2' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine2'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine2'  + @Column+ @jvt;
Set @ExpectedResult = 'vchRegLine2'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchRegLine2'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine2'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

if not exists (select cTaxIDType 
from
AccountMaster.PreDestination_close_AccountPERSHING
where 
vchRegLine3 like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine3'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchRegLine3'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchRegLine3' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine3'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine3'  + @Column+ @jvt;
Set @ExpectedResult = 'vchRegLine3'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchRegLine3'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine3'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

if not exists (select cTaxIDType 
from
AccountMaster.PreDestination_close_AccountPERSHING
where 
vchRegLine4 like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine4'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchRegLine4'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchRegLine4' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine4'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine4'  + @Column+ @jvt;
Set @ExpectedResult = 'vchRegLine4'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchRegLine4'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine4'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

if not exists (select cTaxIDType 
from
AccountMaster.PreDestination_close_AccountPERSHING
where 
vchRegLine5 like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine5'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchRegLine5'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchRegLine5' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine5'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine5'  + @Column+ @jvt;
Set @ExpectedResult = 'vchRegLine5'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchRegLine5'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine5'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

if not exists (select cTaxIDType 
from
AccountMaster.PreDestination_close_AccountPERSHING
where 
vchRegLine6 like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine6'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchRegLine6'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchRegLine6' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine6'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchRegLine6'  + @Column+ @jvt;
Set @ExpectedResult = 'vchRegLine6'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchRegLine6'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchRegLine6'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

if not exists (select cTaxIDType 
from
AccountMaster.PreDestination_close_AccountPERSHING
where 
vchTaxIDNumber like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchTaxIDNumber'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchTaxIDNumber'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchTaxIDNumber' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchTaxIDNumber'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchTaxIDNumber'  + @Column+ @jvt;
Set @ExpectedResult = 'vchTaxIDNumber'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchTaxIDNumber'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchTaxIDNumber'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

if not exists (select cTaxIDType 
from
AccountMaster.PreDestination_close_AccountPERSHING
where 
vchCountryofCitizenship like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchCountryofCitizenship'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchCountryofCitizenship'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchCountryofCitizenship' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchCountryofCitizenship'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchCountryofCitizenship'  + @Column+ @jvt;
Set @ExpectedResult = 'vchCountryofCitizenship'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchCountryofCitizenship'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchCountryofCitizenship'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

if not exists (select cTaxIDType 
from
AccountMaster.PreDestination_close_AccountPERSHING
where 
dtBirthDate like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'dtBirthDate'  + @Column+ @jvt ;
Set @ExpectedResult = 'dtBirthDate'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'dtBirthDate' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtBirthDate'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'dtBirthDate'  + @Column+ @jvt;
Set @ExpectedResult = 'dtBirthDate'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'dtBirthDate'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtBirthDate'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

if not exists (select cTaxIDType 
from
AccountMaster.PreDestination_close_AccountPERSHING
where 
loadDate like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'loadDate'  + @Column+ @jvt ;
Set @ExpectedResult = 'loadDate'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'loadDate' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'loadDate'  + @Column+ @jvt;
Set @ExpectedResult = 'loadDate'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'loadDate'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

if not exists (select cTaxIDType 
from
AccountMaster.PreDestination_close_AccountPERSHING
where 
AccountDataSource like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'AccountDataSource'  + @Column+ @jvt ;
Set @ExpectedResult = 'AccountDataSource'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'AccountDataSource' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AccountDataSource'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'AccountDataSource'  + @Column+ @jvt;
Set @ExpectedResult = 'AccountDataSource'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'AccountDataSource'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AccountDataSource'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

if not exists (select cTaxIDType 
from
AccountMaster.PreDestination_close_AccountPERSHING
where 
AccountNumber like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'AccountNumber'  + @Column+ @jvt ;
Set @ExpectedResult = 'AccountNumber'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'AccountNumber' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AccountNumber'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'AccountNumber'  + @Column+ @jvt;
Set @ExpectedResult = 'AccountNumber'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'AccountNumber'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AccountNumber'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

if not exists (select cTaxIDType 
from
AccountMaster.PreDestination_close_AccountPERSHING
where 
AdvisorRepIDENTIFIER like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'AdvisorRepIDENTIFIER'  + @Column+ @jvt ;
Set @ExpectedResult = 'AdvisorRepIDENTIFIER'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'AdvisorRepIDENTIFIER' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AdvisorRepIDENTIFIER'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'AdvisorRepIDENTIFIER'  + @Column+ @jvt;
Set @ExpectedResult = 'AdvisorRepIDENTIFIER'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'AdvisorRepIDENTIFIER'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AdvisorRepIDENTIFIER'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

if not exists (select cTaxIDType 
from
AccountMaster.PreDestination_close_AccountPERSHING
where 
ClientIDENTIFIER like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ClientIDENTIFIER'  + @Column+ @jvt ;
Set @ExpectedResult = 'ClientIDENTIFIER'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'ClientIDENTIFIER' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ClientIDENTIFIER'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ClientIDENTIFIER'  + @Column+ @jvt;
Set @ExpectedResult = 'ClientIDENTIFIER'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'ClientIDENTIFIER'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ClientIDENTIFIER'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

if not exists (select cTaxIDType 
from
AccountMaster.PreDestination_close_AccountPERSHING
where 
LinkedAccountNumber like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LinkedAccountNumber'  + @Column+ @jvt ;
Set @ExpectedResult = 'LinkedAccountNumber'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'LinkedAccountNumber' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LinkedAccountNumber'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LinkedAccountNumber'  + @Column+ @jvt;
Set @ExpectedResult = 'LinkedAccountNumber'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'LinkedAccountNumber'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LinkedAccountNumber'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

if not exists (select cTaxIDType 
from
AccountMaster.PreDestination_close_AccountPERSHING
where 
vchAcctKey like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAcctKey'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchAcctKey'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchAcctKey' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAcctKey'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAcctKey'  + @Column+ @jvt;
Set @ExpectedResult = 'vchAcctKey'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchAcctKey'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAcctKey'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

if not exists (select cTaxIDType 
from
AccountMaster.PreDestination_close_AccountPERSHING
where 
vchFileName like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchFileName'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchFileName'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchFileName' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchFileName'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchFileName'  + @Column+ @jvt;
Set @ExpectedResult = 'vchFileName'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchFileName'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchFileName'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
select @BadValueCount = count(*)
from
AccountMaster.PreDestination_close_AccountPERSHING
where
cTaxIDType not in ('S','T')
if @BadValueCount = @Zero
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'TaxIDType values test.'   ;
Set @ExpectedResult = ' The TaxIDType values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The TaxIDType values are all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @cTaxIDType + ' ' +  'TaxIDType values test.';
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'TaxIDType values test.'   ;
Set @ExpectedResult = ' The TaxIDType values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The TaxIDType values are not all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @cTaxIDType + ' ' + 'TaxIDType values test.';
Set @TestStatusCode = 1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------------
---------------------------------------------------------
---------------------------------------------------------
---------------------------------------------------------
---------------------------------------------------
---------------------------------------------------

if exists(select
[vchAcctKey]
,count([vchAccountNumber])Occurrences
from
[AccountMaster].[PreDestination_close_AccountPershing]
group by vchAcctKey
having count(*)>1)
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Duplicate records test.'   ;
Set @ExpectedResult = 'We do not have duplicate records in the PreDestination_close_AccountPershing table';
Set @ActualResult = 'We have duplicate records in the PreDestination_close_AccountPershing table';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' +  'SELECT [AcctKey],count([AccountNumber])Occurrences FROM [AccountMaster].[PreDestination_close_AccountPershing] group by [vchAcctKey] having count(vchAccoutNumber) > 1';
Set @TestStatusCode = 1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Duplicate records test.'   ;
Set @ExpectedResult = 'We do not have duplicate records in the PreDestination_close_AccountPershing table';
Set @ActualResult = 'We have duplicate records in the PreDestination_close_AccountPershing table';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'SELECT [vchAcctKey],count([vchAccountNumber])Occurrences FROM [AccountMaster].[PreDestination_close_AccountPershing] group by [vchAcctKey] having count(vchAccoutNumber) > 1';
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
select @BadValueCount = count(*)
from
AccountMaster.PreDestination_close_AccountPERSHING
where
vchTaxIDNumber not like '%[^\d{9}]'
if @BadValueCount = @Zero
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'SocialSecurityIdentifier values test.'   ;
Set @ExpectedResult = ' The SocialSecurityIdentifier values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The SocialSecurityIdentifier values are all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchTaxIDNumber + ' ' +  'SocialSecurityIdentifier values test.';
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'SocialSecurityIdentifier values test.'   ;
Set @ExpectedResult = ' The SocialSecurityIdentifier values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The SocialSecurityIdentifier values are not all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchTaxIDNumber + ' ' + 'SocialSecurityIdentifier values test.';
Set @TestStatusCode = 1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------------
---------------------------------------------------------
select @BadValueCount = count(*)
from
AccountMaster.PreDestination_close_AccountPERSHING
where
left(vchTaxIDNumber,3) in ('000','666', '900')
and substring(vchTaxIDNumber, 4,2) <> '00'
and right(vchTaxIDNumber,4) <> '0000'
if @BadValueCount = @Zero
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'SocialSecurityIdentifier values test.'   ;
Set @ExpectedResult = ' The SocialSecurityIdentifier values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The SocialSecurityIdentifier values are all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchTaxIDNumber + ' ' +  'SocialSecurityIdentifier values test.';
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'SocialSecurityIdentifier values test.'   ;
Set @ExpectedResult = ' The SocialSecurityIdentifier values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The SocialSecurityIdentifier values are not all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchTaxIDNumber + ' ' + 'SocialSecurityIdentifier values test.';
Set @TestStatusCode = 1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------------
---------------------------------------------------------

select @BadValueCount = count(*)
from
AccountMaster.PreDestination_close_AccountPERSHING
where
vchCountryofCitizenship not in (select vchCodeValue from Accounts.dbo.codeValues where vchCodeType = 'Country.Citizenship')
if @BadValueCount = @Zero
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Country.Citizenship values test.'   ;
Set @ExpectedResult = ' The Country.Citizenship values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The Country.Citizenship values are all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchCountryofCitizenship + ' ' +  'Country.Citizenship values test.';
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Country.Citizenship values test.'   ;
Set @ExpectedResult = ' The Country.Citizenship values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The Country.Citizenship values are not all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchCountryofCitizenship + ' ' + 'Country.Citizenship values test.';
Set @TestStatusCode = 1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------------
---------------------------------------------------------
---------------------------------------------------------
---------------------------------------------------------

select @BadValueCount = count(*)
from
AccountMaster.PreDestination_close_AccountPERSHING
where
AccountDataSource not in ('PERSHING')
if @BadValueCount = @Zero
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Source values test.'   ;
Set @ExpectedResult = ' The Source values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The Source values are all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AccountDataSource + ' ' +  'Source values test.';
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Source values test.'   ;
Set @ExpectedResult = ' The Source values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The Source values are not all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AccountDataSource + ' ' + 'Source values test.';
Set @TestStatusCode = 1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------------
---------------------------------------------------------
---------------------------------------------------------
---------------------------------------------------------

select @BadValueCount = count(*)
from
AccountMaster.PreDestination_close_AccountPERSHING
where
AdvisorRepIDENTIFIER not in (select x_vchplanid from Rep.API.vTablePlanID)
if @BadValueCount = @Zero
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'AdvisorRepIDENTIFIER values test.'   ;
Set @ExpectedResult = ' The AdvisorRepIDENTIFIER values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The AdvisorRepIDENTIFIER values are all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AdvisorRepIDENTIFIER + ' ' +  'AdvisorRepIDENTIFIER values test.';
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'AdvisorRepIDENTIFIER values test.'   ;
Set @ExpectedResult = ' The AdvisorRepIDENTIFIER values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The AdvisorRepIDENTIFIER values are not all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AdvisorRepIDENTIFIER + ' ' + 'AdvisorRepIDENTIFIER values test.';
Set @TestStatusCode = 1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
---------------------------------------------------
select @BadValueCount = count(*)
from
AccountMaster.PreDestination_close_AccountPERSHING Account
where
vchAcctKey <> concat(Account.vchAccountNumber,'-',Account.AccountDataSource)
if @BadValueCount = @Zero
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAcctKey values test.'   ;
Set @ExpectedResult = ' The vchAcctKey values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The vchAcctKey values are all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAcctKey + ' ' +  'vchAcctKey values test.';
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAcctKey values test.'   ;
Set @ExpectedResult = ' The vchAcctKey values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The vchAcctKey values are not all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAcctKey + ' ' + 'vchAcctKey values test.';
Set @TestStatusCode = 1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end




---------------------------------------------------
---------------------------------------------------
select @TestDescription=(Select case WHEN
(select count(TestStatus) 
from [dbo].[TestResults] 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%PreDestination_close_AccountPERSHING%'
and TestStatus = @Passed
and TestId> @MaxTestID) = (select count(TestStatus) 
from [dbo].[TestResults] 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%PreDestination_close_AccountPERSHING%'
and TestId> @MaxTestID)
THEN 'ALL TESTS PASSED!'
WHEN
(select count(TestStatus) 
from [dbo].[TestResults] 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%PreDestination_close_AccountPERSHING%'
and TestStatus in(@Passed,@Warning)
and TestId> @MaxTestID) = (select count(TestStatus) 
from [dbo].[TestResults] 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%PreDestination_close_AccountPERSHING%'
and TestId> @MaxTestID)
THEN 'ALL TESTS PASSED. SOME PASS WITH WARNING!'
ELSE 'ALL TESTS DID NOT PASS!' END)

insert testLog values (@TestScenario,@EndingStatus,@TestDescription,SYSDATETIME(),System_User)

select
testStatus
,count(TestId)"NumberOfTests"
from
[dbo].[TestResults]
where
TestDateTime > convert(Date,GetDate())
and
TestDescription LIKE '%PreDestination_close_AccountPERSHING%'
and 
TestId> @MaxTestID
group by TestStatus

select 
TestLogId
,TestScenario
,TestDescription
,TestStatus
,TestExecutionTime
,ExecutedBy
 from 
 testLog 
 Where TestScenario LIKE '%PreDestination_close_AccountPERSHING%' 
--And TestExecutionTime > convert(Date,GetDate())
and TestLogId > @MaxTestLogId
Order by TestExecutionTime Desc

select
[TestId]
,[TestMessage]
,[TestDescription]
,[TestStatus]
,[ExpectedResult]
,[ActualResult]
,[TestDateTime]
,[TestStatusCode]
from
[dbo].[TestResults]
where
TestDateTime > convert(Date,GetDate())
and
TestDescription LIKE '%PreDestination_close_AccountPERSHING%'
and 
TestId> @MaxTestID
order by TestStatusCode asc

---------------------------------------------------
