Use DataConversion

declare @MaxTestID int; --Varaible to caputure Last Test ID in TestResult Table
declare @TestAreaBeginning varchar(30) = 'AccountMaster' + ' '
declare @MaxTestLogID int;
-- Test Name Declaration Starts Here --
declare @TableName varchar(40) = 'inDestination_Open_ClientAddress';
-- Test Name Declaration Ends Here --

-- Table Column Declaration Starts Here --

declare @iClientAddID varchar(12) = 'iClientAddID';
declare @iClientID varchar(9) = 'iClientID';
declare @iNAFID varchar(6) = 'iNAFID';
declare @vchLegalAddress1 varchar(16) = 'vchLegalAddress1';
declare @vchLegalAddress2 varchar(16) = 'vchLegalAddress2';
declare @vchLegalAddress3 varchar(16) = 'vchLegalAddress3';
declare @vchLegalCity varchar(12) = 'vchLegalCity';
declare @vchLegalState varchar(13) = 'vchLegalState';
declare @vchLegalZip varchar(11) = 'vchLegalZip';
declare @vchLegalZipPlus4 varchar(16) = 'vchLegalZipPlus4';
declare @vchLegalCountry varchar(15) = 'vchLegalCountry';
declare @vchMailingAddress1 varchar(18) = 'vchMailingAddress1';
declare @vchMailingAddress2 varchar(18) = 'vchMailingAddress2';
declare @vchMailingAddress3 varchar(18) = 'vchMailingAddress3';
declare @vchMailingCity varchar(14) = 'vchMailingCity';
declare @vchMailingState varchar(15) = 'vchMailingState';
declare @vchMailingZip varchar(13) = 'vchMailingZip';
declare @vchMailingZipPlus4 varchar(18) = 'vchMailingZipPlus4';
declare @vchMailingCountry varchar(17) = 'vchMailingCountry';
declare @dtLastUpdate varchar(12) = 'dtLastUpdate';
declare @vchNFSAccount varchar(13) = 'vchNFSAccount';
declare @vchLegalAttention varchar(17) = 'vchLegalAttention';
declare @vchMailingAttention varchar(19) = 'vchMailingAttention';


-- Table Column Declaration Ends Here --

--Fixed -- Do Not Change -- 
declare @CDNE varchar(75) = ' column does not exist.';
declare @CE varchar(25) = ' column exists.';
declare @TC varchar(25) = ' Test Case #';
declare @E varchar(25) = ' exists.'
declare @DNE varchar(25) = ' does not exist.';
declare @Table varchar(10) = 'Table';
Declare @Column varchar(25) = ' column ';
Declare @HTCT varchar(30) = 'has the correct type. ';
Declare @DNHTCT varchar(40) = 'does not have the correct type.';
Declare @CPIC varchar(50) = ' column position is correct.';
Declare @CPII varchar(50) = ' column position is incorrect.';
declare @CLIC varchar(40) = ' column length is correct.';
declare @CLII varchar(40) = ' column length is incorrect.';
declare @INPWDT varchar(40) = ' is not populated with date time.';
declare @IPWDT varchar(40) = ' is populated with date time.';
declare @TCDNED varchar(40) = 'does not exist, damn it!';
declare @somearenull varchar(25) = ' some columns are null.';
declare @tasbpwvda varchar(255) = ' should be populated with valid data. ';
declare @sbpwagcir varchar(255) = ' should be populated with a good contact identifier reference.'
declare @somecolumnsblank varchar(50) = ' some columns are blank.';
declare @somerhbd varchar(50) = ' some rows have bad dates.';
declare @sciraor varchar(175) = ' some contact identifier reference are oprhaned records';
declare @cchav varchar(50) = 'changeType column has appropriate values.';
declare @ccdnhav varchar(50) = 'changeType column does not have appropriate values.'
DECLARE @MyTestDescription varchar(500)
Declare @RC int;
DECLARE @TestStatusCode int
declare @NullCount int;
declare @Position int;
declare @TypeVarChar int = 167;
declare @TypeDateTime int = 61;
declare @TypeDate int = 40;
declare @TypeInt int = 56;
declare @TypeBit int = 104;
declare @TypeBigInt int = 127;
declare @TypeNVarChar int = 231;
declare @TypeChar int = 175;
declare @TypeMoney int = 60;
declare @TypeDecimal int = 106;
declare @TypeTinyInt int = 48;
declare @TypeTUserId int = 167;
declare @Passed varchar(10) = 'Passed.';
declare @Failed varchar(10) = 'Failed!';
declare @Warning varchar(10) = 'Warning!';
declare @PrimaryKey varchar(100) = 'PrimaryKey';
declare @SystemTypeId int;
declare @LengthDateTime int = 8;
declare @LengthInt int = 4;
Declare @BadValueCount int = 0;
declare @Zero int = 0;
declare @One int = 1;
declare @Two int = 2;
declare @Three int = 3;
declare @Four int = 4;
declare @Five int = 5;
declare @Seven int = 7;
declare @Six int = 6;
declare @Eight int = 8;
declare @Nine int = 9;
declare @Ten int = 10;
declare @eleven int = 11;
declare @Twelve int = 12;
declare @Thirteen int = 13;
declare @Fourteen int = 14;
declare @Fifteen int = 15;
declare @Sixteen int = 16;
declare @Seventeen int = 17;
declare @Eighteen int = 18;
declare @Nineteen int = 19;
declare @Twenty int = 20;
declare @TwentyOne int = 21;
declare @TwentyTwo int = 22;
declare @TwentyThree int = 23;
declare @TwentyFour int = 24;
declare @TwentyFive int = 25;
declare @TwentySix int = 26;
declare @TwentySeven int = 27;
declare @TwentyEight int = 28;
declare @TwentyNine int = 29;
declare @Thirty int = 30;
declare @ThirtyOne int = 31;
declare @ThirtyTwo int = 32;
declare @ThirtyThree int = 33;
declare @ThirtyFour int = 34;
declare @ThirtyFive int = 35;
declare @ThirtySix int = 36;
declare @ThirtySeven int = 37;
declare @ThirtyEight int = 38;
declare @ThirtyNine int = 39;
declare @Forty int = 40;
declare @FortyOne int = 41;
declare @FortyTwo int = 42;
declare @FortyThree int = 43;
declare @FortyFour int = 44;
declare @FortyFive int = 45;
declare @FortySix int = 46;
declare @FortySeven int = 47;
declare @FortyEight int = 48;
declare @FortyNine int = 49;
declare @Fifty int = 50;
declare @FiftyOne int = 51;
declare @FiftyTwo int = 52;
declare @FiftyThree int = 53;
declare @FiftyFour int = 54;
declare @FiftyFive int = 55;
declare @FiftySix int = 56;
declare @FiftySeven int = 57;
declare @FiftyEight int = 58;
declare @FiftyNine int = 59;
declare @Sixty int = 60;
declare @SixtyOne int = 61;
declare @SixtyTwo int = 62;
declare @SixtyThree int = 63;
declare @SixtyFour int = 64;
declare @SixtyFive int = 65;
declare @SixtySix int = 66;
declare @SixtySeven int = 67;
declare @SixtyEight int = 68;
declare @SixtyNine int = 69;
declare @Seventy int = 70;
declare @SeventyOne int = 71;
declare @SeventyTwo int = 72;
declare @SeventyThree int = 73;
declare @SeventyFour int = 74;
declare @SeventyFive int = 75;
declare @SeventySix int = 76;
declare @SeventySeven int = 77;
declare @SeventyEight int = 78;
declare @SeventyNine int = 79;
declare @Eighty int = 80;
declare @EightyOne int = 81;
declare @EightyTwo int = 82;
declare @EightyThree int = 83;
declare @EightyFour int = 84;
declare @EightyFive int = 85;
declare @EightySix int = 86;
declare @EightySeven int = 87;
declare @EightyEight int = 88;
declare @EightyNine int = 89;
declare @Ninety int = 90;
declare @NinetyOne int = 91;
declare @NinetyTwo int = 92;
declare @NinetyThree int = 93;
declare @NinetyFour int = 94;
declare @NinetyFive int = 95;
declare @NinetySix int = 96;
declare @NinetySeven int = 97;
declare @NinetyEight int = 98;
declare @NinetyNine int = 99;
declare @Hundred int = 100;
Declare @OneHundred int = 100;
declare @OneHundredTwentyFive int = 125;
declare @OneHundredTwentyEight int = 128;
declare @OneHundredThirtyThree int = 133;
declare @TwoHundred int = 200;
declare @TwoHundredFifty int = 250;
declare @TwoHundredFiftyFive int = 255;
declare @TwoHundredFiftySix int = 256;
declare @ThreeHundredFifty int = 350;
declare @FiveHundred int = 500;
declare @OneThousand int = 1000;
declare @ThirteenHundred int = 1300;
declare @FiveThousand int = 5000;
declare @onehundreddecimal decimal = 100.00;
declare @GreaterThan100Percent int;
declare @LessThanZeroPercent int;
declare @hapgt100 varchar(100) = ' held away percent greater than 100 percent. ';
declare @haplt100 varchar(100) = ' held away percent less than 100 percent. ';
declare @thapt varchar(100) = ' The held away percent test. ';
declare @hapltz varchar(100) = ' held away percent less than zero percent. ';
declare @hapnltz varchar(100) = ' held away percent not less than zero percent. '
declare @jvt varchar(50) = ' Junk values test. ';
declare @JVRCount int;
--declare @TestDescription varchar(200);
declare @ExpectedResult varchar(100);
declare @ActualResult varchar(100);
declare @TestStatus varchar(15);
declare @TestDateTime datetime;
declare @TestMessage varchar(200);
--declare @TableNameOne varchar(30) = 'staging_plan';
declare @Schema int = 17;
declare @Insert varchar(30) = 'I-insert';
Declare @Update varchar(30) = 'U-update';
Declare @Delete varchar(30) = 'D-delete';
declare @TableObject Int;
declare @ColumnObject Int;
declare @ColumnIsNullable Int;
declare @ICN varchar(30) = ' is column nullable.';
declare @TCIN varchar(40) = ' the column is nullable. ';
declare @TCINN varchar(50) = ' the column is not nullable. ';
declare @MaxLength Int;
declare @Return int;
declare @AllowsNull int;
declare @HasNull int;
declare @ColHasNull varchar(20) = ' column has null';
declare @ColHasNoNull varchar(20)= ' column has no null';
declare @HasBlank Bit;
declare @ColHasBlank varchar(25) = ' column has blank value '
declare @ColHasNoBlank varchar(25) = ' column has no blank value '
declare @HasBlankCount int;
declare @NumBlank int;
Declare @SchemaName varchar(50) = 'AccountMaster';
--Declare @TypeDecimal int = 106;
declare @LengthTinyInt int = 1;
declare @LengthDecimal int = 17;
declare @Eigthteen int = 18;
declare @TempTableName varchar(25) = '#testResults';
Declare @TestScenario varchar(80) = 'AccountMaster inDestination_Open_ClientAddress'
declare @BeginningStatus varchar(25) = 'Beginning';
Declare @EndingStatus varchar(25) = 'Ending';
Declare @TestDescription varchar(800) = 'Test the inDestination_Open_ClientAddress table and columns confirming the properties of the columns and some data verification.';

--Get the Schema ID for the table retrieval
select @Schema = dbo.fnGetSchemaId('AccountMaster');
--Get the TableObjectId for the table using the Table Name and the SchemaId
select @TableObject = dbo.fnGetTableObject(@Schema,@TableName);
-- Test the table exists.
select @MaxTestID = (select Max(Testid) from TestResults);

select @MaxTestLogId = (select Max(TestLogId) from TestLog)
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON


CREATE TABLE #StatesTerritoriesPostalCodes(
	[StateTerritoryId] [int] IDENTITY(1,1) NOT NULL,
	[StateTerritoryName] [varchar](50) NOT NULL,
	[Abbreviation] [varchar](25) NOT NULL,
	[PostalCode] [varchar](5) NOT NULL	
) ON [PRIMARY]



insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Alabama','Ala.','AL');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Alaska','Alaska','AK');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Arizona','Ariz.','AZ');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Arkansas','Ark.','AR');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('California','Calif.','CA');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Colorado','Colo.','CO');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Connecticut','Conn.','CT');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Delaware','Del.','DE');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('District of Columbia','D.C.','DC');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Florida','Fla.','FL');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Georgia','Ga.','GA');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Hawaii','Hawaii','HI');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Idaho','Idaho','ID');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Illinois','Ill.','IL');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Indiana','Ind.','IN');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Iowa','Iowa','IA');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Kansas','Kans.','KS');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Kentucky','Ky.','KY');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Louisiana','La.','LA');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Maine','Maine','ME');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Maryland','Md.','MD');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Massachusetts','Mass.','MA');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Michigan','Mich.','MI');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Minnesota','Minn.','MN');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Mississippi','Miss.','MS');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Missouri','Mo.','MO');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Montana','Mont.','MT');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Nebraska','Nebr.','NE');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Nevada','Nev.','NV');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('New Hampshire','N.H.','NH');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('New Jersey','N.J.','NJ');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('New Mexico','N.M.','NM');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('New York','N.Y.','NY');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('North Carolina','N.C.','NC');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('North Dakota','N.D.','ND');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Ohio','Ohio','OH');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Oklahoma','Okla.','OK');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Oregon','Ore.','OR');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Pennsylvania','Pa.','PA');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Rhode Island','R.I.','RI');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('South Carolina','S.C.','SC');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('South Dakota','S.D.','SD');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Tennessee','Tenn.','TN');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Texas','Tex.','TX');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Utah','Utah','UT');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Vermont','Vt.','VT');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Virginia','Va.','VA');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Washington','Wash.','WA');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('West Virginia','W.V.','WV');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Wisconsin','Wis.','WI');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Wyoming','Wyo.','WY');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('American Samoa','','AS');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Guam','Guam','GU');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Marshall Islands','','MH');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Micronesia','','FM');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Northern Marianas','','MP');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Palau','','PW');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Puerto Rico','P.R.','PR');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Virgin Islands','','VI');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('US Armed Forces - Americas','','AA');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('US Armed Forces - Europe','','AE');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('US Armed Forces - Pacific','','AP');
insert testLog values (@TestScenario,@BeginningStatus,@TestDescription,SYSDATETIME(),System_User)

If @TableObject is not null
Begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table + ' exists test.';
Set @ExpectedResult =  'Table Exists.';
Set @ActualResult = 'Table Exists.';
Set @TestStatus =  @Passed;          
Set @TestDateTime = SYSDATETIME();
Set @TestMessage =  @TC + trim(str(@@IDENTITY)) +' '+@Table+' '+@TableName + @E;
Set @TestStatusCode =  3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
End
	else
Begin

Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' exists test.';
Set @ExpectedResult = 'Table Exists.';
Set @ActualResult = 'Table object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Table+' '+@TableName + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
		
End
--------------------------------------------------

set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@iClientAddID,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @iClientAddID + ' exists test.';
Set @ExpectedResult =  @iClientAddID + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @iClientAddID + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @iClientAddID + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @iClientAddID + ' exists test.';
Set @ExpectedResult = @iClientAddID + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@iClientAddID + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@iClientAddID + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@iClientID,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @iClientID + ' exists test.';
Set @ExpectedResult =  @iClientID + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @iClientID + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @iClientID + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @iClientID + ' exists test.';
Set @ExpectedResult = @iClientID + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@iClientID + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@iClientID + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@iNAFID,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @iNAFID + ' exists test.';
Set @ExpectedResult =  @iNAFID + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @iNAFID + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @iNAFID + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @iNAFID + ' exists test.';
Set @ExpectedResult = @iNAFID + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@iNAFID + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@iNAFID + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchLegalAddress1,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchLegalAddress1 + ' exists test.';
Set @ExpectedResult =  @vchLegalAddress1 + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchLegalAddress1 + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchLegalAddress1 + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchLegalAddress1 + ' exists test.';
Set @ExpectedResult = @vchLegalAddress1 + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchLegalAddress1 + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchLegalAddress1 + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchLegalAddress2,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchLegalAddress2 + ' exists test.';
Set @ExpectedResult =  @vchLegalAddress2 + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchLegalAddress2 + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchLegalAddress2 + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchLegalAddress2 + ' exists test.';
Set @ExpectedResult = @vchLegalAddress2 + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchLegalAddress2 + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchLegalAddress2 + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchLegalAddress3,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchLegalAddress3 + ' exists test.';
Set @ExpectedResult =  @vchLegalAddress3 + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchLegalAddress3 + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchLegalAddress3 + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchLegalAddress3 + ' exists test.';
Set @ExpectedResult = @vchLegalAddress3 + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchLegalAddress3 + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchLegalAddress3 + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchLegalCity,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchLegalCity + ' exists test.';
Set @ExpectedResult =  @vchLegalCity + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchLegalCity + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchLegalCity + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchLegalCity + ' exists test.';
Set @ExpectedResult = @vchLegalCity + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchLegalCity + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchLegalCity + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchLegalState,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchLegalState + ' exists test.';
Set @ExpectedResult =  @vchLegalState + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchLegalState + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchLegalState + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchLegalState + ' exists test.';
Set @ExpectedResult = @vchLegalState + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchLegalState + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchLegalState + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchLegalZip,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchLegalZip + ' exists test.';
Set @ExpectedResult =  @vchLegalZip + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchLegalZip + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchLegalZip + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchLegalZip + ' exists test.';
Set @ExpectedResult = @vchLegalZip + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchLegalZip + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchLegalZip + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchLegalZipPlus4,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchLegalZipPlus4 + ' exists test.';
Set @ExpectedResult =  @vchLegalZipPlus4 + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchLegalZipPlus4 + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchLegalZipPlus4 + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchLegalZipPlus4 + ' exists test.';
Set @ExpectedResult = @vchLegalZipPlus4 + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchLegalZipPlus4 + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchLegalZipPlus4 + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchLegalCountry,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchLegalCountry + ' exists test.';
Set @ExpectedResult =  @vchLegalCountry + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchLegalCountry + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchLegalCountry + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchLegalCountry + ' exists test.';
Set @ExpectedResult = @vchLegalCountry + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchLegalCountry + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchLegalCountry + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchMailingAddress1,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchMailingAddress1 + ' exists test.';
Set @ExpectedResult =  @vchMailingAddress1 + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchMailingAddress1 + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchMailingAddress1 + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchMailingAddress1 + ' exists test.';
Set @ExpectedResult = @vchMailingAddress1 + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchMailingAddress1 + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchMailingAddress1 + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchMailingAddress2,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchMailingAddress2 + ' exists test.';
Set @ExpectedResult =  @vchMailingAddress2 + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchMailingAddress2 + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchMailingAddress2 + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchMailingAddress2 + ' exists test.';
Set @ExpectedResult = @vchMailingAddress2 + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchMailingAddress2 + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchMailingAddress2 + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchMailingAddress3,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchMailingAddress3 + ' exists test.';
Set @ExpectedResult =  @vchMailingAddress3 + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchMailingAddress3 + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchMailingAddress3 + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchMailingAddress3 + ' exists test.';
Set @ExpectedResult = @vchMailingAddress3 + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchMailingAddress3 + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchMailingAddress3 + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchMailingCity,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchMailingCity + ' exists test.';
Set @ExpectedResult =  @vchMailingCity + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchMailingCity + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchMailingCity + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchMailingCity + ' exists test.';
Set @ExpectedResult = @vchMailingCity + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchMailingCity + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchMailingCity + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchMailingState,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchMailingState + ' exists test.';
Set @ExpectedResult =  @vchMailingState + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchMailingState + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchMailingState + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchMailingState + ' exists test.';
Set @ExpectedResult = @vchMailingState + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchMailingState + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchMailingState + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchMailingZip,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchMailingZip + ' exists test.';
Set @ExpectedResult =  @vchMailingZip + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchMailingZip + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchMailingZip + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchMailingZip + ' exists test.';
Set @ExpectedResult = @vchMailingZip + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchMailingZip + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchMailingZip + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchMailingZipPlus4,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchMailingZipPlus4 + ' exists test.';
Set @ExpectedResult =  @vchMailingZipPlus4 + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchMailingZipPlus4 + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchMailingZipPlus4 + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchMailingZipPlus4 + ' exists test.';
Set @ExpectedResult = @vchMailingZipPlus4 + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchMailingZipPlus4 + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchMailingZipPlus4 + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchMailingCountry,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchMailingCountry + ' exists test.';
Set @ExpectedResult =  @vchMailingCountry + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchMailingCountry + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchMailingCountry + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchMailingCountry + ' exists test.';
Set @ExpectedResult = @vchMailingCountry + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchMailingCountry + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchMailingCountry + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@dtLastUpdate,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @dtLastUpdate + ' exists test.';
Set @ExpectedResult =  @dtLastUpdate + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @dtLastUpdate + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @dtLastUpdate + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @dtLastUpdate + ' exists test.';
Set @ExpectedResult = @dtLastUpdate + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@dtLastUpdate + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@dtLastUpdate + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchNFSAccount,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchNFSAccount + ' exists test.';
Set @ExpectedResult =  @vchNFSAccount + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchNFSAccount + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchNFSAccount + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchNFSAccount + ' exists test.';
Set @ExpectedResult = @vchNFSAccount + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchNFSAccount + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchNFSAccount + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchLegalAttention,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchLegalAttention + ' exists test.';
Set @ExpectedResult =  @vchLegalAttention + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchLegalAttention + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchLegalAttention + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchLegalAttention + ' exists test.';
Set @ExpectedResult = @vchLegalAttention + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchLegalAttention + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchLegalAttention + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchMailingAttention,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchMailingAttention + ' exists test.';
Set @ExpectedResult =  @vchMailingAttention + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchMailingAttention + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchMailingAttention + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchMailingAttention + ' exists test.';
Set @ExpectedResult = @vchMailingAttention + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchMailingAttention + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchMailingAttention + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@iClientAddID)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @iClientAddID ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @iClientAddID + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeInt =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @iClientAddID;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeInt);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @iClientAddID + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @iClientAddID + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeInt);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @iClientAddID + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@iClientID)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @iClientID ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @iClientID + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeInt =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @iClientID;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeInt);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @iClientID + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @iClientID + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeInt);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @iClientID + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@iNAFID)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @iNAFID ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @iNAFID + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeInt =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @iNAFID;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeInt);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @iNAFID + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @iNAFID + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeInt);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @iNAFID + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchLegalAddress1)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchLegalAddress1 ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchLegalAddress1 + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchLegalAddress1;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchLegalAddress1 + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchLegalAddress1 + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchLegalAddress1 + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchLegalAddress2)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchLegalAddress2 ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchLegalAddress2 + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchLegalAddress2;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchLegalAddress2 + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchLegalAddress2 + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchLegalAddress2 + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchLegalAddress3)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchLegalAddress3 ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchLegalAddress3 + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchLegalAddress3;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchLegalAddress3 + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchLegalAddress3 + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchLegalAddress3 + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchLegalCity)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchLegalCity ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchLegalCity + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchLegalCity;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchLegalCity + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchLegalCity + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchLegalCity + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchLegalState)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchLegalState ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchLegalState + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchLegalState;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchLegalState + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchLegalState + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchLegalState + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchLegalZip)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchLegalZip ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchLegalZip + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchLegalZip;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchLegalZip + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchLegalZip + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchLegalZip + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchLegalZipPlus4)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchLegalZipPlus4 ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchLegalZipPlus4 + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchLegalZipPlus4;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchLegalZipPlus4 + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchLegalZipPlus4 + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchLegalZipPlus4 + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchLegalCountry)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchLegalCountry ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchLegalCountry + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchLegalCountry;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchLegalCountry + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchLegalCountry + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchLegalCountry + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchMailingAddress1)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchMailingAddress1 ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchMailingAddress1 + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchMailingAddress1;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchMailingAddress1 + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchMailingAddress1 + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchMailingAddress1 + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchMailingAddress2)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchMailingAddress2 ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchMailingAddress2 + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchMailingAddress2;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchMailingAddress2 + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchMailingAddress2 + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchMailingAddress2 + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchMailingAddress3)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchMailingAddress3 ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchMailingAddress3 + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchMailingAddress3;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchMailingAddress3 + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchMailingAddress3 + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchMailingAddress3 + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchMailingCity)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchMailingCity ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchMailingCity + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchMailingCity;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchMailingCity + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchMailingCity + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchMailingCity + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchMailingState)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchMailingState ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchMailingState + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchMailingState;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchMailingState + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchMailingState + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchMailingState + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchMailingZip)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchMailingZip ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchMailingZip + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchMailingZip;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchMailingZip + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchMailingZip + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchMailingZip + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchMailingZipPlus4)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchMailingZipPlus4 ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchMailingZipPlus4 + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchMailingZipPlus4;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchMailingZipPlus4 + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchMailingZipPlus4 + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchMailingZipPlus4 + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchMailingCountry)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchMailingCountry ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchMailingCountry + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchMailingCountry;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchMailingCountry + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchMailingCountry + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchMailingCountry + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@dtLastUpdate)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @dtLastUpdate ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @dtLastUpdate + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeDateTime =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @dtLastUpdate;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeDateTime);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @dtLastUpdate + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @dtLastUpdate + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeDateTime);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @dtLastUpdate + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchNFSAccount)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchNFSAccount ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchNFSAccount + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchNFSAccount;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchNFSAccount + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchNFSAccount + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchNFSAccount + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchLegalAttention)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchLegalAttention ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchLegalAttention + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchLegalAttention;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchLegalAttention + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchLegalAttention + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchLegalAttention + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchMailingAttention)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchMailingAttention ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchMailingAttention + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchMailingAttention;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchMailingAttention + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchMailingAttention + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchMailingAttention + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@iClientAddID,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @iClientAddID + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @iClientAddID + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@iClientAddID +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @iClientAddID + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@iClientAddID +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @iClientAddID + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@iClientID,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @iClientID + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @iClientID + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@iClientID +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @iClientID + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@iClientID +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @iClientID + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@iNAFID,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @iNAFID + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @iNAFID + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@iNAFID +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @iNAFID + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@iNAFID +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @iNAFID + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchLegalAddress1,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchLegalAddress1 + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress1 + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Forty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress1 +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress1 + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress1 +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress1 + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchLegalAddress2,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchLegalAddress2 + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress2 + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Forty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress2 +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress2 + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress2 +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress2 + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchLegalAddress3,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchLegalAddress3 + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress3 + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Forty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress3 +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress3 + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress3 +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress3 + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchLegalCity,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchLegalCity + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchLegalCity + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Thirty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalCity +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalCity + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalCity +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchLegalCity + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchLegalState,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchLegalState + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchLegalState + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Five
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalState +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalState + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalState +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchLegalState + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchLegalZip,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchLegalZip + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchLegalZip + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Fifteen
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalZip +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalZip + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalZip +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchLegalZip + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchLegalZipPlus4,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchLegalZipPlus4 + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchLegalZipPlus4 + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalZipPlus4 +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalZipPlus4 + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalZipPlus4 +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchLegalZipPlus4 + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchLegalCountry,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchLegalCountry + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchLegalCountry + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Twenty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalCountry +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalCountry + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalCountry +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchLegalCountry + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchMailingAddress1,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchMailingAddress1 + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress1 + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Forty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress1 +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress1 + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress1 +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress1 + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchMailingAddress2,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchMailingAddress2 + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress2 + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Forty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress2 +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress2 + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress2 +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress2 + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchMailingAddress3,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchMailingAddress3 + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress3 + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Forty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress3 +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress3 + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress3 +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress3 + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchMailingCity,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchMailingCity + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchMailingCity + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Thirty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingCity +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingCity + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingCity +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchMailingCity + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchMailingState,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchMailingState + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchMailingState + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Five
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingState +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingState + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingState +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchMailingState + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchMailingZip,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchMailingZip + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchMailingZip + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Fifteen
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingZip +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingZip + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingZip +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchMailingZip + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchMailingZipPlus4,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchMailingZipPlus4 + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchMailingZipPlus4 + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingZipPlus4 +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingZipPlus4 + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingZipPlus4 +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchMailingZipPlus4 + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchMailingCountry,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchMailingCountry + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchMailingCountry + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Five
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingCountry +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingCountry + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingCountry +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchMailingCountry + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@dtLastUpdate,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @dtLastUpdate + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @dtLastUpdate + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@dtLastUpdate +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @dtLastUpdate + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@dtLastUpdate +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @dtLastUpdate + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchNFSAccount,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchNFSAccount + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchNFSAccount + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchNFSAccount +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchNFSAccount + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchNFSAccount +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchNFSAccount + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchLegalAttention,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchLegalAttention + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchLegalAttention + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Forty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAttention +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAttention + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAttention +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchLegalAttention + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchMailingAttention,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchMailingAttention + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchMailingAttention + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Forty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAttention +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAttention + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAttention +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchMailingAttention + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @iClientAddID;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @iClientAddID + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @iClientAddID + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @One
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@iClientAddID +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @iClientAddID + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@iClientAddID +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @iClientAddID + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @iClientID;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @iClientID + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @iClientID + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Two
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@iClientID +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @iClientID + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@iClientID +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @iClientID + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @iNAFID;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @iNAFID + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @iNAFID + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Three
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@iNAFID +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @iNAFID + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@iNAFID +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @iNAFID + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchLegalAddress1;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchLegalAddress1 + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress1 + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Four
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress1 +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress1 + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress1 +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress1 + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchLegalAddress2;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchLegalAddress2 + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress2 + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Five
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress2 +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress2 + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress2 +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress2 + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchLegalAddress3;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchLegalAddress3 + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress3 + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Six
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress3 +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress3 + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress3 +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress3 + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchLegalCity;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchLegalCity + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchLegalCity + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Seven
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalCity +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalCity + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalCity +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchLegalCity + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchLegalState;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchLegalState + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchLegalState + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Eight
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalState +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalState + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalState +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchLegalState + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchLegalZip;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchLegalZip + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchLegalZip + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Nine
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalZip +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalZip + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalZip +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchLegalZip + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchLegalZipPlus4;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchLegalZipPlus4 + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchLegalZipPlus4 + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalZipPlus4 +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalZipPlus4 + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalZipPlus4 +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchLegalZipPlus4 + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchLegalCountry;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchLegalCountry + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchLegalCountry + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Eleven
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalCountry +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalCountry + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalCountry +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchLegalCountry + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchMailingAddress1;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchMailingAddress1 + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress1 + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Twelve
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress1 +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress1 + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress1 +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress1 + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchMailingAddress2;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchMailingAddress2 + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress2 + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Thirteen
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress2 +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress2 + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress2 +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress2 + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchMailingAddress3;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchMailingAddress3 + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress3 + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Fourteen
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress3 +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress3 + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress3 +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress3 + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchMailingCity;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchMailingCity + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchMailingCity + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Fifteen
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingCity +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingCity + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingCity +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchMailingCity + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchMailingState;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchMailingState + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchMailingState + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Sixteen
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingState +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingState + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingState +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchMailingState + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchMailingZip;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchMailingZip + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchMailingZip + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Seventeen
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingZip +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingZip + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingZip +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchMailingZip + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchMailingZipPlus4;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchMailingZipPlus4 + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchMailingZipPlus4 + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Eighteen
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingZipPlus4 +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingZipPlus4 + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingZipPlus4 +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchMailingZipPlus4 + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchMailingCountry;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchMailingCountry + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchMailingCountry + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Nineteen
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingCountry +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingCountry + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingCountry +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchMailingCountry + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @dtLastUpdate;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @dtLastUpdate + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @dtLastUpdate + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Twenty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@dtLastUpdate +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @dtLastUpdate + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@dtLastUpdate +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @dtLastUpdate + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchNFSAccount;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchNFSAccount + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchNFSAccount + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @TwentyOne
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchNFSAccount +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchNFSAccount + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchNFSAccount +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchNFSAccount + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchLegalAttention;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchLegalAttention + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchLegalAttention + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @TwentyTwo
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAttention +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAttention + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAttention +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchLegalAttention + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchMailingAttention;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchMailingAttention + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchMailingAttention + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAttention +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAttention + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAttention +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchMailingAttention + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@iClientAddID,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress WHERE @iClientAddID IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@iClientAddID +  ' Has nullable test.';
Set @ExpectedResult = @iClientAddID+ @ColHasNull+trim(str(@One));
Set @ActualResult = @iClientAddID+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @iClientAddID + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@iClientAddID +  ' Has nullable test.'; 
Set @ExpectedResult = @iClientAddID+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@iClientAddID+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @iClientAddID + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@iClientAddID +  ' Has nullable test.';
Set @ExpectedResult =    @iClientAddID+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @iClientAddID+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @iClientAddID + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@iClientAddID +  ' Has nullable test.';
Set @ExpectedResult = @iClientAddID+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @iClientAddID+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @iClientAddID + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@iClientID,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress WHERE @iClientID IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@iClientID +  ' Has nullable test.';
Set @ExpectedResult = @iClientID+ @ColHasNull+trim(str(@One));
Set @ActualResult = @iClientID+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @iClientID + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@iClientID +  ' Has nullable test.'; 
Set @ExpectedResult = @iClientID+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@iClientID+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @iClientID + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@iClientID +  ' Has nullable test.';
Set @ExpectedResult =    @iClientID+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @iClientID+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @iClientID + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@iClientID +  ' Has nullable test.';
Set @ExpectedResult = @iClientID+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @iClientID+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @iClientID + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@iNAFID,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress WHERE @iNAFID IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@iNAFID +  ' Has nullable test.';
Set @ExpectedResult = @iNAFID+ @ColHasNull+trim(str(@One));
Set @ActualResult = @iNAFID+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @iNAFID + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@iNAFID +  ' Has nullable test.'; 
Set @ExpectedResult = @iNAFID+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@iNAFID+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @iNAFID + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@iNAFID +  ' Has nullable test.';
Set @ExpectedResult =    @iNAFID+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @iNAFID+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @iNAFID + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@iNAFID +  ' Has nullable test.';
Set @ExpectedResult = @iNAFID+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @iNAFID+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @iNAFID + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchLegalAddress1,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress WHERE @vchLegalAddress1 IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress1 +  ' Has nullable test.';
Set @ExpectedResult = @vchLegalAddress1+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchLegalAddress1+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress1 + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress1 +  ' Has nullable test.'; 
Set @ExpectedResult = @vchLegalAddress1+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchLegalAddress1+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress1 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress1 +  ' Has nullable test.';
Set @ExpectedResult =    @vchLegalAddress1+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchLegalAddress1+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress1 + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress1 +  ' Has nullable test.';
Set @ExpectedResult = @vchLegalAddress1+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchLegalAddress1+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress1 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchLegalAddress2,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress WHERE @vchLegalAddress2 IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress2 +  ' Has nullable test.';
Set @ExpectedResult = @vchLegalAddress2+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchLegalAddress2+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress2 + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress2 +  ' Has nullable test.'; 
Set @ExpectedResult = @vchLegalAddress2+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchLegalAddress2+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress2 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress2 +  ' Has nullable test.';
Set @ExpectedResult =    @vchLegalAddress2+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchLegalAddress2+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress2 + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress2 +  ' Has nullable test.';
Set @ExpectedResult = @vchLegalAddress2+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchLegalAddress2+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress2 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchLegalAddress3,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress WHERE @vchLegalAddress3 IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress3 +  ' Has nullable test.';
Set @ExpectedResult = @vchLegalAddress3+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchLegalAddress3+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress3 + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress3 +  ' Has nullable test.'; 
Set @ExpectedResult = @vchLegalAddress3+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchLegalAddress3+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress3 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress3 +  ' Has nullable test.';
Set @ExpectedResult =    @vchLegalAddress3+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchLegalAddress3+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress3 + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress3 +  ' Has nullable test.';
Set @ExpectedResult = @vchLegalAddress3+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchLegalAddress3+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress3 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchLegalCity,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress WHERE @vchLegalCity IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalCity +  ' Has nullable test.';
Set @ExpectedResult = @vchLegalCity+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchLegalCity+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalCity + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchLegalCity +  ' Has nullable test.'; 
Set @ExpectedResult = @vchLegalCity+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchLegalCity+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalCity + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchLegalCity +  ' Has nullable test.';
Set @ExpectedResult =    @vchLegalCity+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchLegalCity+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalCity + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchLegalCity +  ' Has nullable test.';
Set @ExpectedResult = @vchLegalCity+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchLegalCity+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalCity + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchLegalState,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress WHERE @vchLegalState IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalState +  ' Has nullable test.';
Set @ExpectedResult = @vchLegalState+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchLegalState+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalState + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchLegalState +  ' Has nullable test.'; 
Set @ExpectedResult = @vchLegalState+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchLegalState+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalState + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchLegalState +  ' Has nullable test.';
Set @ExpectedResult =    @vchLegalState+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchLegalState+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalState + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchLegalState +  ' Has nullable test.';
Set @ExpectedResult = @vchLegalState+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchLegalState+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalState + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchLegalZip,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress WHERE @vchLegalZip IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalZip +  ' Has nullable test.';
Set @ExpectedResult = @vchLegalZip+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchLegalZip+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalZip + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchLegalZip +  ' Has nullable test.'; 
Set @ExpectedResult = @vchLegalZip+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchLegalZip+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalZip + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchLegalZip +  ' Has nullable test.';
Set @ExpectedResult =    @vchLegalZip+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchLegalZip+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalZip + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchLegalZip +  ' Has nullable test.';
Set @ExpectedResult = @vchLegalZip+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchLegalZip+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalZip + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchLegalZipPlus4,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress WHERE @vchLegalZipPlus4 IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalZipPlus4 +  ' Has nullable test.';
Set @ExpectedResult = @vchLegalZipPlus4+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchLegalZipPlus4+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalZipPlus4 + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchLegalZipPlus4 +  ' Has nullable test.'; 
Set @ExpectedResult = @vchLegalZipPlus4+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchLegalZipPlus4+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalZipPlus4 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchLegalZipPlus4 +  ' Has nullable test.';
Set @ExpectedResult =    @vchLegalZipPlus4+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchLegalZipPlus4+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalZipPlus4 + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchLegalZipPlus4 +  ' Has nullable test.';
Set @ExpectedResult = @vchLegalZipPlus4+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchLegalZipPlus4+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalZipPlus4 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchLegalCountry,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress WHERE @vchLegalCountry IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalCountry +  ' Has nullable test.';
Set @ExpectedResult = @vchLegalCountry+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchLegalCountry+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalCountry + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchLegalCountry +  ' Has nullable test.'; 
Set @ExpectedResult = @vchLegalCountry+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchLegalCountry+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalCountry + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchLegalCountry +  ' Has nullable test.';
Set @ExpectedResult =    @vchLegalCountry+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchLegalCountry+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalCountry + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchLegalCountry +  ' Has nullable test.';
Set @ExpectedResult = @vchLegalCountry+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchLegalCountry+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalCountry + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchMailingAddress1,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress WHERE @vchMailingAddress1 IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress1 +  ' Has nullable test.';
Set @ExpectedResult = @vchMailingAddress1+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchMailingAddress1+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress1 + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress1 +  ' Has nullable test.'; 
Set @ExpectedResult = @vchMailingAddress1+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchMailingAddress1+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress1 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress1 +  ' Has nullable test.';
Set @ExpectedResult =    @vchMailingAddress1+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchMailingAddress1+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress1 + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress1 +  ' Has nullable test.';
Set @ExpectedResult = @vchMailingAddress1+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchMailingAddress1+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress1 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchMailingAddress2,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress WHERE @vchMailingAddress2 IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress2 +  ' Has nullable test.';
Set @ExpectedResult = @vchMailingAddress2+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchMailingAddress2+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress2 + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress2 +  ' Has nullable test.'; 
Set @ExpectedResult = @vchMailingAddress2+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchMailingAddress2+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress2 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress2 +  ' Has nullable test.';
Set @ExpectedResult =    @vchMailingAddress2+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchMailingAddress2+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress2 + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress2 +  ' Has nullable test.';
Set @ExpectedResult = @vchMailingAddress2+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchMailingAddress2+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress2 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchMailingAddress3,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress WHERE @vchMailingAddress3 IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress3 +  ' Has nullable test.';
Set @ExpectedResult = @vchMailingAddress3+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchMailingAddress3+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress3 + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress3 +  ' Has nullable test.'; 
Set @ExpectedResult = @vchMailingAddress3+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchMailingAddress3+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress3 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress3 +  ' Has nullable test.';
Set @ExpectedResult =    @vchMailingAddress3+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchMailingAddress3+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress3 + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress3 +  ' Has nullable test.';
Set @ExpectedResult = @vchMailingAddress3+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchMailingAddress3+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress3 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchMailingCity,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress WHERE @vchMailingCity IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingCity +  ' Has nullable test.';
Set @ExpectedResult = @vchMailingCity+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchMailingCity+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingCity + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchMailingCity +  ' Has nullable test.'; 
Set @ExpectedResult = @vchMailingCity+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchMailingCity+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingCity + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchMailingCity +  ' Has nullable test.';
Set @ExpectedResult =    @vchMailingCity+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchMailingCity+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingCity + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchMailingCity +  ' Has nullable test.';
Set @ExpectedResult = @vchMailingCity+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchMailingCity+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingCity + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchMailingState,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress WHERE @vchMailingState IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingState +  ' Has nullable test.';
Set @ExpectedResult = @vchMailingState+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchMailingState+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingState + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchMailingState +  ' Has nullable test.'; 
Set @ExpectedResult = @vchMailingState+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchMailingState+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingState + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchMailingState +  ' Has nullable test.';
Set @ExpectedResult =    @vchMailingState+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchMailingState+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingState + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchMailingState +  ' Has nullable test.';
Set @ExpectedResult = @vchMailingState+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchMailingState+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingState + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchMailingZip,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress WHERE @vchMailingZip IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingZip +  ' Has nullable test.';
Set @ExpectedResult = @vchMailingZip+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchMailingZip+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingZip + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchMailingZip +  ' Has nullable test.'; 
Set @ExpectedResult = @vchMailingZip+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchMailingZip+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingZip + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchMailingZip +  ' Has nullable test.';
Set @ExpectedResult =    @vchMailingZip+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchMailingZip+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingZip + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchMailingZip +  ' Has nullable test.';
Set @ExpectedResult = @vchMailingZip+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchMailingZip+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingZip + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchMailingZipPlus4,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress WHERE @vchMailingZipPlus4 IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingZipPlus4 +  ' Has nullable test.';
Set @ExpectedResult = @vchMailingZipPlus4+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchMailingZipPlus4+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingZipPlus4 + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchMailingZipPlus4 +  ' Has nullable test.'; 
Set @ExpectedResult = @vchMailingZipPlus4+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchMailingZipPlus4+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingZipPlus4 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchMailingZipPlus4 +  ' Has nullable test.';
Set @ExpectedResult =    @vchMailingZipPlus4+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchMailingZipPlus4+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingZipPlus4 + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchMailingZipPlus4 +  ' Has nullable test.';
Set @ExpectedResult = @vchMailingZipPlus4+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchMailingZipPlus4+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingZipPlus4 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchMailingCountry,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress WHERE @vchMailingCountry IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingCountry +  ' Has nullable test.';
Set @ExpectedResult = @vchMailingCountry+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchMailingCountry+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingCountry + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchMailingCountry +  ' Has nullable test.'; 
Set @ExpectedResult = @vchMailingCountry+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchMailingCountry+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingCountry + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchMailingCountry +  ' Has nullable test.';
Set @ExpectedResult =    @vchMailingCountry+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchMailingCountry+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingCountry + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchMailingCountry +  ' Has nullable test.';
Set @ExpectedResult = @vchMailingCountry+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchMailingCountry+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingCountry + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@dtLastUpdate,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress WHERE @dtLastUpdate IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@dtLastUpdate +  ' Has nullable test.';
Set @ExpectedResult = @dtLastUpdate+ @ColHasNull+trim(str(@One));
Set @ActualResult = @dtLastUpdate+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @dtLastUpdate + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@dtLastUpdate +  ' Has nullable test.'; 
Set @ExpectedResult = @dtLastUpdate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@dtLastUpdate+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @dtLastUpdate + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@dtLastUpdate +  ' Has nullable test.';
Set @ExpectedResult =    @dtLastUpdate+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @dtLastUpdate+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @dtLastUpdate + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@dtLastUpdate +  ' Has nullable test.';
Set @ExpectedResult = @dtLastUpdate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @dtLastUpdate+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @dtLastUpdate + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchNFSAccount,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress WHERE @vchNFSAccount IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchNFSAccount +  ' Has nullable test.';
Set @ExpectedResult = @vchNFSAccount+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchNFSAccount+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchNFSAccount + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchNFSAccount +  ' Has nullable test.'; 
Set @ExpectedResult = @vchNFSAccount+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchNFSAccount+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchNFSAccount + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchNFSAccount +  ' Has nullable test.';
Set @ExpectedResult =    @vchNFSAccount+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchNFSAccount+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchNFSAccount + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchNFSAccount +  ' Has nullable test.';
Set @ExpectedResult = @vchNFSAccount+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchNFSAccount+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchNFSAccount + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchLegalAttention,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress WHERE @vchLegalAttention IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAttention +  ' Has nullable test.';
Set @ExpectedResult = @vchLegalAttention+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchLegalAttention+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAttention + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchLegalAttention +  ' Has nullable test.'; 
Set @ExpectedResult = @vchLegalAttention+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchLegalAttention+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAttention + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAttention +  ' Has nullable test.';
Set @ExpectedResult =    @vchLegalAttention+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchLegalAttention+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAttention + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchLegalAttention +  ' Has nullable test.';
Set @ExpectedResult = @vchLegalAttention+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchLegalAttention+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAttention + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchMailingAttention,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress WHERE @vchMailingAttention IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAttention +  ' Has nullable test.';
Set @ExpectedResult = @vchMailingAttention+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchMailingAttention+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAttention + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchMailingAttention +  ' Has nullable test.'; 
Set @ExpectedResult = @vchMailingAttention+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchMailingAttention+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAttention + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAttention +  ' Has nullable test.';
Set @ExpectedResult =    @vchMailingAttention+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchMailingAttention+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAttention + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchMailingAttention +  ' Has nullable test.';
Set @ExpectedResult = @vchMailingAttention+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchMailingAttention+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAttention + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress where @iClientAddID = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@iClientAddID +  ' Has blank value test.';
Set @ExpectedResult = @iClientAddID+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @iClientAddID+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @iClientAddID + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@iClientAddID +  ' Has blank value test.';
Set @ExpectedResult = @iClientAddID+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @iClientAddID+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @iClientAddID + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress where @iClientID = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@iClientID +  ' Has blank value test.';
Set @ExpectedResult = @iClientID+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @iClientID+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @iClientID + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@iClientID +  ' Has blank value test.';
Set @ExpectedResult = @iClientID+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @iClientID+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @iClientID + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress where @iNAFID = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@iNAFID +  ' Has blank value test.';
Set @ExpectedResult = @iNAFID+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @iNAFID+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @iNAFID + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@iNAFID +  ' Has blank value test.';
Set @ExpectedResult = @iNAFID+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @iNAFID+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @iNAFID + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress where @vchLegalAddress1 = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress1 +  ' Has blank value test.';
Set @ExpectedResult = @vchLegalAddress1+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchLegalAddress1+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress1 + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress1 +  ' Has blank value test.';
Set @ExpectedResult = @vchLegalAddress1+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchLegalAddress1+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress1 + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress where @vchLegalAddress2 = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress2 +  ' Has blank value test.';
Set @ExpectedResult = @vchLegalAddress2+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchLegalAddress2+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress2 + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress2 +  ' Has blank value test.';
Set @ExpectedResult = @vchLegalAddress2+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchLegalAddress2+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress2 + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress where @vchLegalAddress3 = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress3 +  ' Has blank value test.';
Set @ExpectedResult = @vchLegalAddress3+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchLegalAddress3+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress3 + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchLegalAddress3 +  ' Has blank value test.';
Set @ExpectedResult = @vchLegalAddress3+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchLegalAddress3+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAddress3 + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress where @vchLegalCity = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchLegalCity +  ' Has blank value test.';
Set @ExpectedResult = @vchLegalCity+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchLegalCity+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalCity + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchLegalCity +  ' Has blank value test.';
Set @ExpectedResult = @vchLegalCity+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchLegalCity+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalCity + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress where @vchLegalState = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchLegalState +  ' Has blank value test.';
Set @ExpectedResult = @vchLegalState+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchLegalState+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalState + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchLegalState +  ' Has blank value test.';
Set @ExpectedResult = @vchLegalState+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchLegalState+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalState + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress where @vchLegalZip = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchLegalZip +  ' Has blank value test.';
Set @ExpectedResult = @vchLegalZip+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchLegalZip+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalZip + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchLegalZip +  ' Has blank value test.';
Set @ExpectedResult = @vchLegalZip+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchLegalZip+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalZip + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress where @vchLegalZipPlus4 = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchLegalZipPlus4 +  ' Has blank value test.';
Set @ExpectedResult = @vchLegalZipPlus4+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchLegalZipPlus4+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalZipPlus4 + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchLegalZipPlus4 +  ' Has blank value test.';
Set @ExpectedResult = @vchLegalZipPlus4+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchLegalZipPlus4+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalZipPlus4 + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress where @vchLegalCountry = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchLegalCountry +  ' Has blank value test.';
Set @ExpectedResult = @vchLegalCountry+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchLegalCountry+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalCountry + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchLegalCountry +  ' Has blank value test.';
Set @ExpectedResult = @vchLegalCountry+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchLegalCountry+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalCountry + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress where @vchMailingAddress1 = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress1 +  ' Has blank value test.';
Set @ExpectedResult = @vchMailingAddress1+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchMailingAddress1+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress1 + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress1 +  ' Has blank value test.';
Set @ExpectedResult = @vchMailingAddress1+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchMailingAddress1+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress1 + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress where @vchMailingAddress2 = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress2 +  ' Has blank value test.';
Set @ExpectedResult = @vchMailingAddress2+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchMailingAddress2+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress2 + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress2 +  ' Has blank value test.';
Set @ExpectedResult = @vchMailingAddress2+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchMailingAddress2+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress2 + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress where @vchMailingAddress3 = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress3 +  ' Has blank value test.';
Set @ExpectedResult = @vchMailingAddress3+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchMailingAddress3+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress3 + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchMailingAddress3 +  ' Has blank value test.';
Set @ExpectedResult = @vchMailingAddress3+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchMailingAddress3+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAddress3 + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress where @vchMailingCity = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchMailingCity +  ' Has blank value test.';
Set @ExpectedResult = @vchMailingCity+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchMailingCity+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingCity + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchMailingCity +  ' Has blank value test.';
Set @ExpectedResult = @vchMailingCity+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchMailingCity+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingCity + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress where @vchMailingState = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchMailingState +  ' Has blank value test.';
Set @ExpectedResult = @vchMailingState+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchMailingState+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingState + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchMailingState +  ' Has blank value test.';
Set @ExpectedResult = @vchMailingState+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchMailingState+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingState + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress where @vchMailingZip = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchMailingZip +  ' Has blank value test.';
Set @ExpectedResult = @vchMailingZip+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchMailingZip+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingZip + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchMailingZip +  ' Has blank value test.';
Set @ExpectedResult = @vchMailingZip+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchMailingZip+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingZip + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress where @vchMailingZipPlus4 = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchMailingZipPlus4 +  ' Has blank value test.';
Set @ExpectedResult = @vchMailingZipPlus4+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchMailingZipPlus4+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingZipPlus4 + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchMailingZipPlus4 +  ' Has blank value test.';
Set @ExpectedResult = @vchMailingZipPlus4+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchMailingZipPlus4+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingZipPlus4 + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress where @vchMailingCountry = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchMailingCountry +  ' Has blank value test.';
Set @ExpectedResult = @vchMailingCountry+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchMailingCountry+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingCountry + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchMailingCountry +  ' Has blank value test.';
Set @ExpectedResult = @vchMailingCountry+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchMailingCountry+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingCountry + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress where @dtLastUpdate = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@dtLastUpdate +  ' Has blank value test.';
Set @ExpectedResult = @dtLastUpdate+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @dtLastUpdate+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @dtLastUpdate + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@dtLastUpdate +  ' Has blank value test.';
Set @ExpectedResult = @dtLastUpdate+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @dtLastUpdate+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @dtLastUpdate + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress where @vchNFSAccount = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchNFSAccount +  ' Has blank value test.';
Set @ExpectedResult = @vchNFSAccount+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchNFSAccount+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchNFSAccount + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchNFSAccount +  ' Has blank value test.';
Set @ExpectedResult = @vchNFSAccount+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchNFSAccount+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchNFSAccount + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress where @vchLegalAttention = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchLegalAttention +  ' Has blank value test.';
Set @ExpectedResult = @vchLegalAttention+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchLegalAttention+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAttention + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchLegalAttention +  ' Has blank value test.';
Set @ExpectedResult = @vchLegalAttention+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchLegalAttention+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalAttention + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.inDestination_Open_ClientAddress where @vchMailingAttention = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchMailingAttention +  ' Has blank value test.';
Set @ExpectedResult = @vchMailingAttention+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchMailingAttention+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAttention + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchMailingAttention +  ' Has blank value test.';
Set @ExpectedResult = @vchMailingAttention+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchMailingAttention+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingAttention + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where iClientAddID = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'iClientAddID' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'iClientAddID' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='iClientAddID' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'iClientAddID' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'iClientAddID' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'iClientAddID' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'iClientAddID' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'iClientAddID' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where iClientID = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'iClientID' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'iClientID' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='iClientID' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'iClientID' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'iClientID' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'iClientID' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'iClientID' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'iClientID' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where iNAFID = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'iNAFID' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'iNAFID' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='iNAFID' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'iNAFID' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'iNAFID' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'iNAFID' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'iNAFID' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'iNAFID' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchLegalAddress1 = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalAddress1' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchLegalAddress1' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchLegalAddress1' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAddress1' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchLegalAddress1' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchLegalAddress1' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalAddress1' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAddress1' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchLegalAddress2 = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalAddress2' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchLegalAddress2' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchLegalAddress2' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAddress2' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchLegalAddress2' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchLegalAddress2' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalAddress2' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAddress2' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchLegalAddress3 = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalAddress3' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchLegalAddress3' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchLegalAddress3' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAddress3' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchLegalAddress3' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchLegalAddress3' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalAddress3' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAddress3' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchLegalCity = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalCity' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchLegalCity' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchLegalCity' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalCity' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchLegalCity' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchLegalCity' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalCity' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalCity' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchLegalState = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalState' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchLegalState' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchLegalState' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalState' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchLegalState' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchLegalState' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalState' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalState' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchLegalZip = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalZip' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchLegalZip' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchLegalZip' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalZip' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchLegalZip' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchLegalZip' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalZip' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalZip' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchLegalZipPlus4 = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalZipPlus4' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchLegalZipPlus4' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchLegalZipPlus4' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalZipPlus4' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchLegalZipPlus4' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchLegalZipPlus4' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalZipPlus4' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalZipPlus4' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchLegalCountry = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalCountry' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchLegalCountry' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchLegalCountry' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalCountry' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchLegalCountry' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchLegalCountry' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalCountry' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalCountry' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchMailingAddress1 = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingAddress1' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchMailingAddress1' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchMailingAddress1' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAddress1' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchMailingAddress1' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchMailingAddress1' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingAddress1' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAddress1' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchMailingAddress2 = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingAddress2' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchMailingAddress2' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchMailingAddress2' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAddress2' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchMailingAddress2' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchMailingAddress2' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingAddress2' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAddress2' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchMailingAddress3 = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingAddress3' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchMailingAddress3' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchMailingAddress3' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAddress3' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchMailingAddress3' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchMailingAddress3' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingAddress3' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAddress3' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchMailingCity = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingCity' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchMailingCity' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchMailingCity' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingCity' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchMailingCity' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchMailingCity' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingCity' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingCity' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchMailingState = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingState' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchMailingState' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchMailingState' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingState' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchMailingState' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchMailingState' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingState' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingState' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchMailingZip = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingZip' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchMailingZip' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchMailingZip' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingZip' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchMailingZip' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchMailingZip' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingZip' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingZip' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchMailingZipPlus4 = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingZipPlus4' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchMailingZipPlus4' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchMailingZipPlus4' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingZipPlus4' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchMailingZipPlus4' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchMailingZipPlus4' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingZipPlus4' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingZipPlus4' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchMailingCountry = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingCountry' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchMailingCountry' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchMailingCountry' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingCountry' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchMailingCountry' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchMailingCountry' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingCountry' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingCountry' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where dtLastUpdate = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'dtLastUpdate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'dtLastUpdate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='dtLastUpdate' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtLastUpdate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'dtLastUpdate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'dtLastUpdate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'dtLastUpdate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtLastUpdate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchNFSAccount = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchNFSAccount' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchNFSAccount' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchNFSAccount' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchNFSAccount' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchNFSAccount' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchNFSAccount' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchNFSAccount' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchNFSAccount' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchLegalAttention = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalAttention' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchLegalAttention' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchLegalAttention' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAttention' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchLegalAttention' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchLegalAttention' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalAttention' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAttention' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchMailingAttention = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingAttention' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchMailingAttention' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchMailingAttention' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAttention' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchMailingAttention' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchMailingAttention' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingAttention' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAttention' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where iClientAddID is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'iClientAddID' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'iClientAddID' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'iClientAddID' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'iClientAddID' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'iClientAddID' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'iClientAddID' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'iClientAddID' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'iClientAddID' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where iClientID is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'iClientID' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'iClientID' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'iClientID' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'iClientID' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'iClientID' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'iClientID' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'iClientID' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'iClientID' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where iNAFID is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'iNAFID' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'iNAFID' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'iNAFID' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'iNAFID' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'iNAFID' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'iNAFID' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'iNAFID' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'iNAFID' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchLegalAddress1 is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalAddress1' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchLegalAddress1' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalAddress1' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAddress1' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalAddress1' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchLegalAddress1' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalAddress1' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAddress1' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchLegalAddress2 is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalAddress2' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchLegalAddress2' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalAddress2' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAddress2' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalAddress2' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchLegalAddress2' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalAddress2' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAddress2' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchLegalAddress3 is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalAddress3' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchLegalAddress3' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalAddress3' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAddress3' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalAddress3' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchLegalAddress3' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalAddress3' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAddress3' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchLegalCity is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalCity' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchLegalCity' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalCity' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalCity' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalCity' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchLegalCity' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalCity' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalCity' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchLegalState is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalState' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchLegalState' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalState' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalState' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalState' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchLegalState' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalState' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalState' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchLegalZip is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalZip' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchLegalZip' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalZip' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalZip' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalZip' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchLegalZip' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalZip' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalZip' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchLegalZipPlus4 is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalZipPlus4' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchLegalZipPlus4' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalZipPlus4' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalZipPlus4' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalZipPlus4' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchLegalZipPlus4' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalZipPlus4' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalZipPlus4' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchLegalCountry is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalCountry' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchLegalCountry' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalCountry' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalCountry' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalCountry' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchLegalCountry' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalCountry' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalCountry' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchMailingAddress1 is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingAddress1' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchMailingAddress1' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingAddress1' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAddress1' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingAddress1' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchMailingAddress1' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingAddress1' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAddress1' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchMailingAddress2 is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingAddress2' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchMailingAddress2' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingAddress2' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAddress2' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingAddress2' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchMailingAddress2' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingAddress2' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAddress2' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchMailingAddress3 is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingAddress3' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchMailingAddress3' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingAddress3' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAddress3' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingAddress3' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchMailingAddress3' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingAddress3' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAddress3' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchMailingCity is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingCity' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchMailingCity' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingCity' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingCity' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingCity' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchMailingCity' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingCity' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingCity' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchMailingState is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingState' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchMailingState' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingState' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingState' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingState' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchMailingState' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingState' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingState' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchMailingZip is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingZip' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchMailingZip' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingZip' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingZip' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingZip' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchMailingZip' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingZip' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingZip' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchMailingZipPlus4 is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingZipPlus4' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchMailingZipPlus4' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingZipPlus4' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingZipPlus4' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingZipPlus4' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchMailingZipPlus4' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingZipPlus4' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingZipPlus4' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchMailingCountry is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingCountry' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchMailingCountry' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingCountry' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingCountry' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingCountry' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchMailingCountry' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingCountry' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingCountry' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where dtLastUpdate is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'dtLastUpdate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'dtLastUpdate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'dtLastUpdate' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtLastUpdate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'dtLastUpdate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'dtLastUpdate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'dtLastUpdate' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtLastUpdate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchNFSAccount is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchNFSAccount' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchNFSAccount' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchNFSAccount' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchNFSAccount' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchNFSAccount' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchNFSAccount' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchNFSAccount' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchNFSAccount' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchLegalAttention is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalAttention' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchLegalAttention' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalAttention' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAttention' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalAttention' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchLegalAttention' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchLegalAttention' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAttention' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.inDestination_Open_ClientAddress where vchMailingAttention is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingAttention' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchMailingAttention' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingAttention' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAttention' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingAttention' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchMailingAttention' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchMailingAttention' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAttention' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
--------------------------------------------------
if not exists (select iClientAddID 
from
AccountMaster.inDestination_Open_ClientAddress
where 
iClientAddID like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'iClientAddID'  + @Column+ @jvt ;
Set @ExpectedResult = 'iClientAddID'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'iClientAddID' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'iClientAddID'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'iClientAddID'  + @Column+ @jvt;
Set @ExpectedResult = 'iClientAddID'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'iClientAddID'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'iClientAddID'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select iClientAddID 
from
AccountMaster.inDestination_Open_ClientAddress
where 
iClientID like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'iClientID'  + @Column+ @jvt ;
Set @ExpectedResult = 'iClientID'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'iClientID' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'iClientID'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'iClientID'  + @Column+ @jvt;
Set @ExpectedResult = 'iClientID'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'iClientID'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'iClientID'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select iClientAddID 
from
AccountMaster.inDestination_Open_ClientAddress
where 
iNAFID like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'iNAFID'  + @Column+ @jvt ;
Set @ExpectedResult = 'iNAFID'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'iNAFID' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'iNAFID'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'iNAFID'  + @Column+ @jvt;
Set @ExpectedResult = 'iNAFID'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'iNAFID'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'iNAFID'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select iClientAddID 
from
AccountMaster.inDestination_Open_ClientAddress
where 
vchLegalAddress1 like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalAddress1'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchLegalAddress1'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchLegalAddress1' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAddress1'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalAddress1'  + @Column+ @jvt;
Set @ExpectedResult = 'vchLegalAddress1'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchLegalAddress1'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAddress1'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select iClientAddID 
from
AccountMaster.inDestination_Open_ClientAddress
where 
vchLegalAddress2 like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalAddress2'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchLegalAddress2'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchLegalAddress2' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAddress2'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalAddress2'  + @Column+ @jvt;
Set @ExpectedResult = 'vchLegalAddress2'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchLegalAddress2'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAddress2'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select iClientAddID 
from
AccountMaster.inDestination_Open_ClientAddress
where 
vchLegalAddress3 like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalAddress3'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchLegalAddress3'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchLegalAddress3' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAddress3'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalAddress3'  + @Column+ @jvt;
Set @ExpectedResult = 'vchLegalAddress3'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchLegalAddress3'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAddress3'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select iClientAddID 
from
AccountMaster.inDestination_Open_ClientAddress
where 
vchLegalCity like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalCity'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchLegalCity'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchLegalCity' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalCity'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalCity'  + @Column+ @jvt;
Set @ExpectedResult = 'vchLegalCity'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchLegalCity'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalCity'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select iClientAddID 
from
AccountMaster.inDestination_Open_ClientAddress
where 
vchLegalState like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalState'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchLegalState'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchLegalState' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalState'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalState'  + @Column+ @jvt;
Set @ExpectedResult = 'vchLegalState'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchLegalState'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalState'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select iClientAddID 
from
AccountMaster.inDestination_Open_ClientAddress
where 
vchLegalZip like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalZip'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchLegalZip'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchLegalZip' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalZip'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalZip'  + @Column+ @jvt;
Set @ExpectedResult = 'vchLegalZip'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchLegalZip'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalZip'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select iClientAddID 
from
AccountMaster.inDestination_Open_ClientAddress
where 
vchLegalZipPlus4 like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalZipPlus4'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchLegalZipPlus4'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchLegalZipPlus4' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalZipPlus4'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalZipPlus4'  + @Column+ @jvt;
Set @ExpectedResult = 'vchLegalZipPlus4'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchLegalZipPlus4'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalZipPlus4'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select iClientAddID 
from
AccountMaster.inDestination_Open_ClientAddress
where 
vchLegalCountry like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalCountry'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchLegalCountry'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchLegalCountry' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalCountry'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalCountry'  + @Column+ @jvt;
Set @ExpectedResult = 'vchLegalCountry'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchLegalCountry'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalCountry'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select iClientAddID 
from
AccountMaster.inDestination_Open_ClientAddress
where 
vchMailingAddress1 like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingAddress1'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchMailingAddress1'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchMailingAddress1' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAddress1'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingAddress1'  + @Column+ @jvt;
Set @ExpectedResult = 'vchMailingAddress1'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchMailingAddress1'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAddress1'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select iClientAddID 
from
AccountMaster.inDestination_Open_ClientAddress
where 
vchMailingAddress2 like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingAddress2'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchMailingAddress2'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchMailingAddress2' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAddress2'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingAddress2'  + @Column+ @jvt;
Set @ExpectedResult = 'vchMailingAddress2'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchMailingAddress2'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAddress2'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select iClientAddID 
from
AccountMaster.inDestination_Open_ClientAddress
where 
vchMailingAddress3 like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingAddress3'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchMailingAddress3'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchMailingAddress3' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAddress3'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingAddress3'  + @Column+ @jvt;
Set @ExpectedResult = 'vchMailingAddress3'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchMailingAddress3'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAddress3'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select iClientAddID 
from
AccountMaster.inDestination_Open_ClientAddress
where 
vchMailingCity like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingCity'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchMailingCity'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchMailingCity' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingCity'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingCity'  + @Column+ @jvt;
Set @ExpectedResult = 'vchMailingCity'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchMailingCity'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingCity'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select iClientAddID 
from
AccountMaster.inDestination_Open_ClientAddress
where 
vchMailingState like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingState'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchMailingState'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchMailingState' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingState'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingState'  + @Column+ @jvt;
Set @ExpectedResult = 'vchMailingState'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchMailingState'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingState'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select iClientAddID 
from
AccountMaster.inDestination_Open_ClientAddress
where 
vchMailingZip like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingZip'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchMailingZip'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchMailingZip' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingZip'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingZip'  + @Column+ @jvt;
Set @ExpectedResult = 'vchMailingZip'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchMailingZip'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingZip'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select iClientAddID 
from
AccountMaster.inDestination_Open_ClientAddress
where 
vchMailingZipPlus4 like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingZipPlus4'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchMailingZipPlus4'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchMailingZipPlus4' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingZipPlus4'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingZipPlus4'  + @Column+ @jvt;
Set @ExpectedResult = 'vchMailingZipPlus4'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchMailingZipPlus4'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingZipPlus4'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select iClientAddID 
from
AccountMaster.inDestination_Open_ClientAddress
where 
vchMailingCountry like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingCountry'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchMailingCountry'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchMailingCountry' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingCountry'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingCountry'  + @Column+ @jvt;
Set @ExpectedResult = 'vchMailingCountry'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchMailingCountry'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingCountry'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select iClientAddID 
from
AccountMaster.inDestination_Open_ClientAddress
where 
dtLastUpdate like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'dtLastUpdate'  + @Column+ @jvt ;
Set @ExpectedResult = 'dtLastUpdate'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'dtLastUpdate' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtLastUpdate'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'dtLastUpdate'  + @Column+ @jvt;
Set @ExpectedResult = 'dtLastUpdate'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'dtLastUpdate'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'dtLastUpdate'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select iClientAddID 
from
AccountMaster.inDestination_Open_ClientAddress
where 
vchNFSAccount like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchNFSAccount'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchNFSAccount'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchNFSAccount' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchNFSAccount'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchNFSAccount'  + @Column+ @jvt;
Set @ExpectedResult = 'vchNFSAccount'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchNFSAccount'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchNFSAccount'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select iClientAddID 
from
AccountMaster.inDestination_Open_ClientAddress
where 
vchLegalAttention like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalAttention'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchLegalAttention'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchLegalAttention' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAttention'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchLegalAttention'  + @Column+ @jvt;
Set @ExpectedResult = 'vchLegalAttention'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchLegalAttention'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchLegalAttention'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select iClientAddID 
from
AccountMaster.inDestination_Open_ClientAddress
where 
vchMailingAttention like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingAttention'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchMailingAttention'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchMailingAttention' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAttention'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchMailingAttention'  + @Column+ @jvt;
Set @ExpectedResult = 'vchMailingAttention'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchMailingAttention'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchMailingAttention'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------------
---------------------------------------------------------

select @BadValueCount = count(*)
from
AccountMaster.inDestination_Open_ClientAddress
where
vchLegalCountry not in (select vchCodeValue from Accounts.dbo.codeValues where vchCodeType = 'Country.Code')
if @BadValueCount = @Zero
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Country.Code values test.'   ;
Set @ExpectedResult = ' The Country.Code values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The Country.Code values are all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalCountry + ' ' +  'Country.Code values test.';
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Country.Code values test.'   ;
Set @ExpectedResult = ' The Country.Code values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The Country.Code values are not all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalCountry + ' ' + 'Country.Code values test.';
Set @TestStatusCode = 1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------------
---------------------------------------------------------
---------------------------------------------------------
---------------------------------------------------------

select @BadValueCount = count(*)
from
AccountMaster.inDestination_Open_ClientAddress
where
vchLegalState not in (select PostalCode from #StatesTerritoriesPostalCodes)
if @BadValueCount = @Zero
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'State values test.'   ;
Set @ExpectedResult = ' The State values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The State values are all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalState + ' ' +  'State values test.';
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'State values test.'   ;
Set @ExpectedResult = ' The State values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The State values are not all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchLegalState + ' ' + 'State values test.';
Set @TestStatusCode = 1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------------
---------------------------------------------------------
---------------------------------------------------------
---------------------------------------------------------

select @BadValueCount = count(*)
from
AccountMaster.inDestination_Open_ClientAddress
where
vchMailingState not in (select PostalCode from #StatesTerritoriesPostalCodes)
if @BadValueCount = @Zero
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'State values test.'   ;
Set @ExpectedResult = ' The State values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The State values are all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingState + ' ' +  'State values test.';
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'State values test.'   ;
Set @ExpectedResult = ' The State values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The State values are not all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingState + ' ' + 'State values test.';
Set @TestStatusCode = 1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------------
---------------------------------------------------------
---------------------------------------------------------
---------------------------------------------------------

select @BadValueCount = count(*)
from
AccountMaster.inDestination_Open_ClientAddress
where
vchMailingCountry not in (select vchCodeValue from Accounts.dbo.codeValues where vchCodeType = 'Country.Code')
if @BadValueCount = @Zero
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Country.Code values test.'   ;
Set @ExpectedResult = ' The Country.Code values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The Country.Code values are all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingCountry + ' ' +  'Country.Code values test.';
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Country.Code values test.'   ;
Set @ExpectedResult = ' The Country.Code values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The Country.Code values are not all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchMailingCountry + ' ' + 'Country.Code values test.';
Set @TestStatusCode = 1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------------
---------------------------------------------------------

select @TestDescription=(Select case WHEN
(select count(TestStatus) 
from [dbo].[TestResults] 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%inDestination_Open_ClientAddress%'
and TestStatus = @Passed
and TestId> @MaxTestID) = (select count(TestStatus) 
from [dbo].[TestResults] 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%inDestination_Open_ClientAddress%'
and TestId> @MaxTestID)
THEN 'ALL TESTS PASSED!'
WHEN
(select count(TestStatus) 
from [dbo].[TestResults] 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%inDestination_Open_ClientAddress%'
and TestStatus in(@Passed,@Warning)
and TestId> @MaxTestID) = (select count(TestStatus) 
from [dbo].[TestResults] 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%inDestination_Open_ClientAddress%'
and TestId> @MaxTestID)
THEN 'ALL TESTS PASSED. SOME PASS WITH WARNING!'
ELSE 'ALL TESTS DID NOT PASS!' END)

insert testLog values (@TestScenario,@EndingStatus,@TestDescription,SYSDATETIME(),System_User)

select
testStatus
,count(TestId)"NumberOfTests"
from
[dbo].[TestResults]
where
TestDateTime > convert(Date,GetDate())
and
TestDescription LIKE '%inDestination_Open_ClientAddress%'
and 
TestId> @MaxTestID
group by TestStatus

select 
TestLogId
,TestScenario
,TestDescription
,TestStatus
,TestExecutionTime
,ExecutedBy
 from 
 testLog 
 Where TestScenario LIKE '%inDestination_Open_ClientAddress%' 
--And TestExecutionTime > convert(Date,GetDate())
and TestLogId > @MaxTestLogId
Order by TestExecutionTime Desc

select
[TestId]
,[TestMessage]
,[TestDescription]
,[TestStatus]
,[ExpectedResult]
,[ActualResult]
,[TestDateTime]
,[TestStatusCode]
from
[dbo].[TestResults]
where
TestDateTime > convert(Date,GetDate())
and
TestDescription LIKE '%inDestination_Open_ClientAddress%'
and 
TestId> @MaxTestID
order by TestStatusCode asc

---------------------------------------------------
drop table #StatesTerritoriesPostalCodes
---------------------------------------------------