Select 'AccountMaster.inDestination_Open_NAFSuitabilityOther' as src, * from (select
iNAFSuitID,
iNAFID,
vchAnnualIncOther,
vchEstNetWorthOther,
vchInvestableAssetsOther,
vchPreCapitalObj,
vchIncomeObj,
vchCapitalAppObj,
vchSpeculationObj,
vchTradingProfitsObj,
vchObjectiveOther,
vchOtherObj,
vchRiskTolerance,
vchRiskComb,
vchInvestTimeHorizon,
vchTimeComb,
vchKnowStocksLevel,
vchKnowStocksYear,
vchKnowBondsLevel,
vchKnowBondsYear,
vchKnowMFundsLevel,
vchKnowMFundsYear,
vchKnowOptionsLevel,
vchKnowOptionsYear,
vchKnowVContractsLevel,
vchKnowVContractsYear,
vchKnowLtdPartLevel,
vchKnowLtdPartYear,
dtLastUpdate,
dtInsertdate,
cAnnualExpenses,
cSpecialExpenses,
cSpecialExpensesTimeFrame,
cInvestmentPurpose,
vchInvestmentPurposetext,
cStockTranCount,
cBondTranCount,
cShortTermKnowledge,
cShortTermTranCount,
cMutualFundTranCount,
cOptionsTranCount,
cVariableContractTranCount,
cLPTranCount,
cFuturesKnowledge,
cFuturesTranCount,
cAltInvKnowledge,
cAltInvTranCount,
cAnnuityKnowledge,
cAnnuityTranCount,
cMarginKnowledge,
cMarginTranCount,
cForeignCurrencyKnowledge,
cForeignCurrencyTranCount,
cForeignSecurityKnowledge,
cForeignSecurityTranCount,
cConsultingBroker,
cConsultingOwnDecisions,
cConsultingFamilyFriends,
dStockHeldAwayPercent,
dBondHeldAwayPercent,
dShortTermHeldAwayPercent,
dMutualFundsAwayPercent,
dOptionsAwayPercent,
dVariableContractsAwayPercent,
dLPAwayPercent,
dFuturesAwayPercent,
dAltInvAwayPercent,
dAnnuityAwayPercent,
dForeignCurrencyAwayPercent,
dForeignSecurityAwayPercent,
dOtherAwayPercent,
cControlPerson,
vchOtherAwayFreeForm,
iUID,
ProductKnowledgeANA,
AssetsHeldAwayANA,
FundingSource,
FundingSourceOther,
FinancialInformationShow
from
AccountMaster.inDestination_Open_NAFSuitabilityOther  where
  dtInsertDate = '2020-06-23 15:32:13.660'
except
select
iNAFSuitID,
iNAFID,
vchAnnualIncOther,
vchEstNetWorthOther,
vchInvestableAssetsOther,
vchPreCapitalObj,
vchIncomeObj,
vchCapitalAppObj,
vchSpeculationObj,
vchTradingProfitsObj,
vchObjectiveOther,
vchOtherObj,
vchRiskTolerance,
vchRiskComb,
vchInvestTimeHorizon,
vchTimeComb,
vchKnowStocksLevel,
vchKnowStocksYear,
vchKnowBondsLevel,
vchKnowBondsYear,
vchKnowMFundsLevel,
vchKnowMFundsYear,
vchKnowOptionsLevel,
vchKnowOptionsYear,
vchKnowVContractsLevel,
vchKnowVContractsYear,
vchKnowLtdPartLevel,
vchKnowLtdPartYear,
dtLastUpdate,
dtInsertdate,
cAnnualExpenses,
cSpecialExpenses,
cSpecialExpensesTimeFrame,
cInvestmentPurpose,
vchInvestmentPurposetext,
cStockTranCount,
cBondTranCount,
cShortTermKnowledge,
cShortTermTranCount,
cMutualFundTranCount,
cOptionsTranCount,
cVariableContractTranCount,
cLPTranCount,
cFuturesKnowledge,
cFuturesTranCount,
cAltInvKnowledge,
cAltInvTranCount,
cAnnuityKnowledge,
cAnnuityTranCount,
cMarginKnowledge,
cMarginTranCount,
cForeignCurrencyKnowledge,
cForeignCurrencyTranCount,
cForeignSecurityKnowledge,
cForeignSecurityTranCount,
cConsultingBroker,
cConsultingOwnDecisions,
cConsultingFamilyFriends,
dStockHeldAwayPercent,
dBondHeldAwayPercent,
dShortTermHeldAwayPercent,
dMutualFundsAwayPercent,
dOptionsAwayPercent,
dVariableContractsAwayPercent,
dLPAwayPercent,
dFuturesAwayPercent,
dAltInvAwayPercent,
dAnnuityAwayPercent,
dForeignCurrencyAwayPercent,
dForeignSecurityAwayPercent,
dOtherAwayPercent,
cControlPerson,
vchOtherAwayFreeForm,
iUID,
ProductKnowledgeANA,
AssetsHeldAwayANA,
FundingSource,
FundingSourceOther,
FinancialInformationShow
from
Accounts.dbo.NAFSuitabilityOther  where
  dtInsertDate = '2020-06-23 15:32:13.660') Left_Diffs
Union
Select 'Accounts.dbo.NAFSuitabilityOther' as src,* from(select
iNAFSuitID,
iNAFID,
vchAnnualIncOther,
vchEstNetWorthOther,
vchInvestableAssetsOther,
vchPreCapitalObj,
vchIncomeObj,
vchCapitalAppObj,
vchSpeculationObj,
vchTradingProfitsObj,
vchObjectiveOther,
vchOtherObj,
vchRiskTolerance,
vchRiskComb,
vchInvestTimeHorizon,
vchTimeComb,
vchKnowStocksLevel,
vchKnowStocksYear,
vchKnowBondsLevel,
vchKnowBondsYear,
vchKnowMFundsLevel,
vchKnowMFundsYear,
vchKnowOptionsLevel,
vchKnowOptionsYear,
vchKnowVContractsLevel,
vchKnowVContractsYear,
vchKnowLtdPartLevel,
vchKnowLtdPartYear,
dtLastUpdate,
dtInsertdate,
cAnnualExpenses,
cSpecialExpenses,
cSpecialExpensesTimeFrame,
cInvestmentPurpose,
vchInvestmentPurposetext,
cStockTranCount,
cBondTranCount,
cShortTermKnowledge,
cShortTermTranCount,
cMutualFundTranCount,
cOptionsTranCount,
cVariableContractTranCount,
cLPTranCount,
cFuturesKnowledge,
cFuturesTranCount,
cAltInvKnowledge,
cAltInvTranCount,
cAnnuityKnowledge,
cAnnuityTranCount,
cMarginKnowledge,
cMarginTranCount,
cForeignCurrencyKnowledge,
cForeignCurrencyTranCount,
cForeignSecurityKnowledge,
cForeignSecurityTranCount,
cConsultingBroker,
cConsultingOwnDecisions,
cConsultingFamilyFriends,
dStockHeldAwayPercent,
dBondHeldAwayPercent,
dShortTermHeldAwayPercent,
dMutualFundsAwayPercent,
dOptionsAwayPercent,
dVariableContractsAwayPercent,
dLPAwayPercent,
dFuturesAwayPercent,
dAltInvAwayPercent,
dAnnuityAwayPercent,
dForeignCurrencyAwayPercent,
dForeignSecurityAwayPercent,
dOtherAwayPercent,
cControlPerson,
vchOtherAwayFreeForm,
iUID,
ProductKnowledgeANA,
AssetsHeldAwayANA,
FundingSource,
FundingSourceOther,
FinancialInformationShow
from
Accounts.dbo.NAFSuitabilityOther  where
  dtInsertDate = '2020-06-23 15:32:13.660'
except
select
iNAFSuitID,
iNAFID,
vchAnnualIncOther,
vchEstNetWorthOther,
vchInvestableAssetsOther,
vchPreCapitalObj,
vchIncomeObj,
vchCapitalAppObj,
vchSpeculationObj,
vchTradingProfitsObj,
vchObjectiveOther,
vchOtherObj,
vchRiskTolerance,
vchRiskComb,
vchInvestTimeHorizon,
vchTimeComb,
vchKnowStocksLevel,
vchKnowStocksYear,
vchKnowBondsLevel,
vchKnowBondsYear,
vchKnowMFundsLevel,
vchKnowMFundsYear,
vchKnowOptionsLevel,
vchKnowOptionsYear,
vchKnowVContractsLevel,
vchKnowVContractsYear,
vchKnowLtdPartLevel,
vchKnowLtdPartYear,
dtLastUpdate,
dtInsertdate,
cAnnualExpenses,
cSpecialExpenses,
cSpecialExpensesTimeFrame,
cInvestmentPurpose,
vchInvestmentPurposetext,
cStockTranCount,
cBondTranCount,
cShortTermKnowledge,
cShortTermTranCount,
cMutualFundTranCount,
cOptionsTranCount,
cVariableContractTranCount,
cLPTranCount,
cFuturesKnowledge,
cFuturesTranCount,
cAltInvKnowledge,
cAltInvTranCount,
cAnnuityKnowledge,
cAnnuityTranCount,
cMarginKnowledge,
cMarginTranCount,
cForeignCurrencyKnowledge,
cForeignCurrencyTranCount,
cForeignSecurityKnowledge,
cForeignSecurityTranCount,
cConsultingBroker,
cConsultingOwnDecisions,
cConsultingFamilyFriends,
dStockHeldAwayPercent,
dBondHeldAwayPercent,
dShortTermHeldAwayPercent,
dMutualFundsAwayPercent,
dOptionsAwayPercent,
dVariableContractsAwayPercent,
dLPAwayPercent,
dFuturesAwayPercent,
dAltInvAwayPercent,
dAnnuityAwayPercent,
dForeignCurrencyAwayPercent,
dForeignSecurityAwayPercent,
dOtherAwayPercent,
cControlPerson,
vchOtherAwayFreeForm,
iUID,
ProductKnowledgeANA,
AssetsHeldAwayANA,
FundingSource,
FundingSourceOther,
FinancialInformationShow
from
AccountMaster.inDestination_Open_NAFSuitabilityOther  where
  dtInsertDate = '2020-06-23 15:32:13.660') Right_Diffs
