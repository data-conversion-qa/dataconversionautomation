Select 'AccountMaster.inDestination_Open_ClientControl' as src, * from (select
iControlID,
iNAFID,
iClientID,
vchRelationship,
vchCompany,
vchCusip,
dtLastUpdate,
dtInsert,
Relationship2,
Company2,
Cusip2,
Relationship3,
Company3,
Cusip3
from
AccountMaster.inDestination_Open_ClientControl
except
select
iControlID,
iNAFID,
iClientID,
vchRelationship,
vchCompany,
vchCusip,
dtLastUpdate,
dtInsert,
Relationship2,
Company2,
Cusip2,
Relationship3,
Company3,
Cusip3
from
Accounts.dbo.ClientControl) Left_Diffs
Union
Select 'Accounts.dbo.ClientControl' as src,* from(select
iControlID,
iNAFID,
iClientID,
vchRelationship,
vchCompany,
vchCusip,
dtLastUpdate,
dtInsert,
Relationship2,
Company2,
Cusip2,
Relationship3,
Company3,
Cusip3
from
Accounts.dbo.ClientControl
except
select
iControlID,
iNAFID,
iClientID,
vchRelationship,
vchCompany,
vchCusip,
dtLastUpdate,
dtInsert,
Relationship2,
Company2,
Cusip2,
Relationship3,
Company3,
Cusip3
from
AccountMaster.inDestination_Open_ClientControl) Right_Diffs
