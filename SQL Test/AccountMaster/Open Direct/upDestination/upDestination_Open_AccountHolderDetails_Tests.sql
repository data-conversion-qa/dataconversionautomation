Use DataConversion

declare @MaxTestID int; --Varaible to caputure Last Test ID in TestResult Table
declare @TestAreaBeginning varchar(30) = 'AccountMaster' + ' '
declare @MaxTestLogID int;
-- Test Name Declaration Starts Here --
declare @TableName varchar(100) = 'upDestination_Open_AccountHolderDetails';
-- Test Name Declaration Ends Here --

-- Table Column Declaration Starts Here --

declare @vchAccountNumber varchar(16) = 'vchAccountNumber';
declare @OLD_OwnershipPercent varchar(20) = 'OLD_OwnershipPercent';
declare @New_OwnershipPercent varchar(20) = 'New_OwnershipPercent';
declare @OLD_Passport varchar(12) = 'OLD_Passport';
declare @New_Passport varchar(12) = 'New_Passport';
declare @OLD_vchaccountNumber varchar(20) = 'OLD_vchaccountNumber';
declare @New_vchaccountNumber varchar(20) = 'New_vchaccountNumber';
declare @loadDate varchar(8) = 'loadDate';


-- Table Column Declaration Ends Here --

--Fixed -- Do Not Change -- 
declare @CDNE varchar(75) = ' column does not exist.';
declare @CE varchar(25) = ' column exists.';
declare @TC varchar(25) = ' Test Case #';
declare @E varchar(25) = ' exists.'
declare @DNE varchar(25) = ' does not exist.';
declare @Table varchar(10) = 'Table';
Declare @Column varchar(25) = ' column ';
Declare @HTCT varchar(30) = 'has the correct type. ';
Declare @DNHTCT varchar(40) = 'does not have the correct type.';
Declare @CPIC varchar(50) = ' column position is correct.';
Declare @CPII varchar(50) = ' column position is incorrect.';
declare @CLIC varchar(40) = ' column length is correct.';
declare @CLII varchar(40) = ' column length is incorrect.';
declare @INPWDT varchar(40) = ' is not populated with date time.';
declare @IPWDT varchar(40) = ' is populated with date time.';
declare @TCDNED varchar(40) = 'does not exist, damn it!';
declare @somearenull varchar(25) = ' some columns are null.';
declare @tasbpwvda varchar(255) = ' should be populated with valid data. ';
declare @sbpwagcir varchar(255) = ' should be populated with a good contact identifier reference.'
declare @somecolumnsblank varchar(50) = ' some columns are blank.';
declare @somerhbd varchar(50) = ' some rows have bad dates.';
declare @sciraor varchar(175) = ' some contact identifier reference are oprhaned records';
declare @cchav varchar(50) = 'changeType column has appropriate values.';
declare @ccdnhav varchar(50) = 'changeType column does not have appropriate values.'
DECLARE @MyTestDescription varchar(500)
Declare @RC int;
DECLARE @TestStatusCode int
declare @NullCount int;
declare @Position int;
declare @TypeVarChar int = 167;
declare @TypeDateTime int = 61;
declare @TypeDate int = 40;
declare @TypeInt int = 56;
declare @TypeBit int = 104;
declare @TypeBigInt int = 127;
declare @TypeNVarChar int = 231;
declare @TypeChar int = 175;
declare @TypeMoney int = 60;
declare @TypeDecimal int = 106;
declare @TypeTinyInt int = 48;
declare @TypeTUserId int = 167;
declare @Passed varchar(10) = 'Passed.';
declare @Failed varchar(10) = 'Failed!';
declare @Warning varchar(10) = 'Warning!';
declare @PrimaryKey varchar(100) = 'PrimaryKey';
declare @SystemTypeId int;
declare @LengthDateTime int = 8;
declare @LengthInt int = 4;
Declare @BadValueCount int = 0;
declare @Zero int = 0;
declare @One int = 1;
declare @Two int = 2;
declare @Three int = 3;
declare @Four int = 4;
declare @Five int = 5;
declare @Seven int = 7;
declare @Six int = 6;
declare @Eight int = 8;
declare @Nine int = 9;
declare @Ten int = 10;
declare @eleven int = 11;
declare @Twelve int = 12;
declare @Thirteen int = 13;
declare @Fourteen int = 14;
declare @Fifteen int = 15;
declare @Sixteen int = 16;
declare @Seventeen int = 17;
declare @Eighteen int = 18;
declare @Nineteen int = 19;
declare @Twenty int = 20;
declare @TwentyOne int = 21;
declare @TwentyTwo int = 22;
declare @TwentyThree int = 23;
declare @TwentyFour int = 24;
declare @TwentyFive int = 25;
declare @TwentySix int = 26;
declare @TwentySeven int = 27;
declare @TwentyEight int = 28;
declare @TwentyNine int = 29;
declare @Thirty int = 30;
declare @ThirtyOne int = 31;
declare @ThirtyTwo int = 32;
declare @ThirtyThree int = 33;
declare @ThirtyFour int = 34;
declare @ThirtyFive int = 35;
declare @ThirtySix int = 36;
declare @ThirtySeven int = 37;
declare @ThirtyEight int = 38;
declare @ThirtyNine int = 39;
declare @Forty int = 40;
declare @FortyOne int = 41;
declare @FortyTwo int = 42;
declare @FortyThree int = 43;
declare @FortyFour int = 44;
declare @FortyFive int = 45;
declare @FortySix int = 46;
declare @FortySeven int = 47;
declare @FortyEight int = 48;
declare @FortyNine int = 49;
declare @Fifty int = 50;
declare @FiftyOne int = 51;
declare @FiftyTwo int = 52;
declare @FiftyThree int = 53;
declare @FiftyFour int = 54;
declare @FiftyFive int = 55;
declare @FiftySix int = 56;
declare @FiftySeven int = 57;
declare @FiftyEight int = 58;
declare @FiftyNine int = 59;
declare @Sixty int = 60;
declare @SixtyOne int = 61;
declare @SixtyTwo int = 62;
declare @SixtyThree int = 63;
declare @SixtyFour int = 64;
declare @SixtyFive int = 65;
declare @SixtySix int = 66;
declare @SixtySeven int = 67;
declare @SixtyEight int = 68;
declare @SixtyNine int = 69;
declare @Seventy int = 70;
declare @SeventyOne int = 71;
declare @SeventyTwo int = 72;
declare @SeventyThree int = 73;
declare @SeventyFour int = 74;
declare @SeventyFive int = 75;
declare @SeventySix int = 76;
declare @SeventySeven int = 77;
declare @SeventyEight int = 78;
declare @SeventyNine int = 79;
declare @Eighty int = 80;
declare @EightyOne int = 81;
declare @EightyTwo int = 82;
declare @EightyThree int = 83;
declare @EightyFour int = 84;
declare @EightyFive int = 85;
declare @EightySix int = 86;
declare @EightySeven int = 87;
declare @EightyEight int = 88;
declare @EightyNine int = 89;
declare @Ninety int = 90;
declare @NinetyOne int = 91;
declare @NinetyTwo int = 92;
declare @NinetyThree int = 93;
declare @NinetyFour int = 94;
declare @NinetyFive int = 95;
declare @NinetySix int = 96;
declare @NinetySeven int = 97;
declare @NinetyEight int = 98;
declare @NinetyNine int = 99;
declare @Hundred int = 100;
Declare @OneHundred int = 100;
declare @OneHundredTwentyFive int = 125;
declare @OneHundredTwentyEight int = 128;
declare @OneHundredThirtyThree int = 133;
declare @TwoHundred int = 200;
declare @TwoHundredFifty int = 250;
declare @TwoHundredFiftyFive int = 255;
declare @TwoHundredFiftySix int = 256;
declare @ThreeHundredFifty int = 350;
declare @FiveHundred int = 500;
declare @OneThousand int = 1000;
declare @ThirteenHundred int = 1300;
declare @FiveThousand int = 5000;
declare @onehundreddecimal decimal = 100.00;
declare @GreaterThan100Percent int;
declare @LessThanZeroPercent int;
declare @hapgt100 varchar(100) = ' held away percent greater than 100 percent. ';
declare @haplt100 varchar(100) = ' held away percent less than 100 percent. ';
declare @thapt varchar(100) = ' The held away percent test. ';
declare @hapltz varchar(100) = ' held away percent less than zero percent. ';
declare @hapnltz varchar(100) = ' held away percent not less than zero percent. '
declare @jvt varchar(50) = ' Junk values test. ';
declare @JVRCount int;
--declare @TestDescription varchar(200);
declare @ExpectedResult varchar(100);
declare @ActualResult varchar(100);
declare @TestStatus varchar(15);
declare @TestDateTime datetime;
declare @TestMessage varchar(200);
--declare @TableNameOne varchar(30) = 'staging_plan';
declare @Schema int = 17;
declare @Insert varchar(30) = 'I-insert';
Declare @Update varchar(30) = 'U-update';
Declare @Delete varchar(30) = 'D-delete';
declare @TableObject Int;
declare @ColumnObject Int;
declare @ColumnIsNullable Int;
declare @ICN varchar(30) = ' is column nullable.';
declare @TCIN varchar(40) = ' the column is nullable. ';
declare @TCINN varchar(50) = ' the column is not nullable. ';
declare @MaxLength Int;
declare @Return int;
declare @AllowsNull int;
declare @HasNull int;
declare @ColHasNull varchar(20) = ' column has null';
declare @ColHasNoNull varchar(20)= ' column has no null';
declare @HasBlank Bit;
declare @ColHasBlank varchar(25) = ' column has blank value '
declare @ColHasNoBlank varchar(25) = ' column has no blank value '
declare @HasBlankCount int;
declare @NumBlank int;
Declare @SchemaName varchar(50) = 'AccountMaster';
--Declare @TypeDecimal int = 106;
declare @LengthTinyInt int = 1;
declare @LengthDecimal int = 17;
declare @Eigthteen int = 18;
declare @TempTableName varchar(25) = '#testResults';
Declare @TestScenario varchar(80) = 'AccountMaster upDestination_Open_AccountHolderDetails'
declare @BeginningStatus varchar(25) = 'Beginning';
Declare @EndingStatus varchar(25) = 'Ending';
Declare @TestDescription varchar(800) = 'Test the upDestination_Open_AccountHolderDetails table and columns confirming the properties of the columns and some data verification.';

--Get the Schema ID for the table retrieval
select @Schema = dbo.fnGetSchemaId('AccountMaster');
--Get the TableObjectId for the table using the Table Name and the SchemaId
select @TableObject = dbo.fnGetTableObject(@Schema,@TableName);
-- Test the table exists.
select @MaxTestID = (select Max(Testid) from TestResults);

select @MaxTestLogId = (select Max(TestLogId) from TestLog)

insert testLog values (@TestScenario,@BeginningStatus,@TestDescription,SYSDATETIME(),System_User)

If @TableObject is not null
Begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table + ' exists test.';
Set @ExpectedResult =  'Table Exists.';
Set @ActualResult = 'Table Exists.';
Set @TestStatus =  @Passed;          
Set @TestDateTime = SYSDATETIME();
Set @TestMessage =  @TC + trim(str(@@IDENTITY)) +' '+@Table+' '+@TableName + @E;
Set @TestStatusCode =  3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
End
	else
Begin

Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' exists test.';
Set @ExpectedResult = 'Table Exists.';
Set @ActualResult = 'Table object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Table+' '+@TableName + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
		
End
--------------------------------------------------

set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchAccountNumber,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchAccountNumber + ' exists test.';
Set @ExpectedResult =  @vchAccountNumber + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchAccountNumber + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchAccountNumber + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchAccountNumber + ' exists test.';
Set @ExpectedResult = @vchAccountNumber + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchAccountNumber + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchAccountNumber + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@OLD_OwnershipPercent,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @OLD_OwnershipPercent + ' exists test.';
Set @ExpectedResult =  @OLD_OwnershipPercent + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @OLD_OwnershipPercent + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @OLD_OwnershipPercent + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @OLD_OwnershipPercent + ' exists test.';
Set @ExpectedResult = @OLD_OwnershipPercent + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@OLD_OwnershipPercent + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@OLD_OwnershipPercent + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@New_OwnershipPercent,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @New_OwnershipPercent + ' exists test.';
Set @ExpectedResult =  @New_OwnershipPercent + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @New_OwnershipPercent + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @New_OwnershipPercent + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @New_OwnershipPercent + ' exists test.';
Set @ExpectedResult = @New_OwnershipPercent + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@New_OwnershipPercent + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@New_OwnershipPercent + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@OLD_Passport,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @OLD_Passport + ' exists test.';
Set @ExpectedResult =  @OLD_Passport + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @OLD_Passport + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @OLD_Passport + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @OLD_Passport + ' exists test.';
Set @ExpectedResult = @OLD_Passport + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@OLD_Passport + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@OLD_Passport + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@New_Passport,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @New_Passport + ' exists test.';
Set @ExpectedResult =  @New_Passport + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @New_Passport + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @New_Passport + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @New_Passport + ' exists test.';
Set @ExpectedResult = @New_Passport + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@New_Passport + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@New_Passport + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@OLD_vchaccountNumber,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @OLD_vchaccountNumber + ' exists test.';
Set @ExpectedResult =  @OLD_vchaccountNumber + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @OLD_vchaccountNumber + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @OLD_vchaccountNumber + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @OLD_vchaccountNumber + ' exists test.';
Set @ExpectedResult = @OLD_vchaccountNumber + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@OLD_vchaccountNumber + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@OLD_vchaccountNumber + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@New_vchaccountNumber,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @New_vchaccountNumber + ' exists test.';
Set @ExpectedResult =  @New_vchaccountNumber + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @New_vchaccountNumber + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @New_vchaccountNumber + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @New_vchaccountNumber + ' exists test.';
Set @ExpectedResult = @New_vchaccountNumber + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@New_vchaccountNumber + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@New_vchaccountNumber + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@loadDate,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @loadDate + ' exists test.';
Set @ExpectedResult =  @loadDate + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @loadDate + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @loadDate + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @loadDate + ' exists test.';
Set @ExpectedResult = @loadDate + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@loadDate + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@loadDate + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchAccountNumber)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchAccountNumber ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchAccountNumber + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchAccountNumber;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchAccountNumber + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchAccountNumber + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchAccountNumber + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@OLD_OwnershipPercent)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @OLD_OwnershipPercent ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @OLD_OwnershipPercent + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeDecimal =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @OLD_OwnershipPercent;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeDecimal);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @OLD_OwnershipPercent + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @OLD_OwnershipPercent + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeDecimal);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @OLD_OwnershipPercent + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@New_OwnershipPercent)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @New_OwnershipPercent ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @New_OwnershipPercent + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeDecimal =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @New_OwnershipPercent;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeDecimal);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @New_OwnershipPercent + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @New_OwnershipPercent + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeDecimal);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @New_OwnershipPercent + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@OLD_Passport)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @OLD_Passport ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @OLD_Passport + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @OLD_Passport;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @OLD_Passport + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @OLD_Passport + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @OLD_Passport + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@New_Passport)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @New_Passport ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @New_Passport + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @New_Passport;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @New_Passport + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @New_Passport + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @New_Passport + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@OLD_vchaccountNumber)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @OLD_vchaccountNumber ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @OLD_vchaccountNumber + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @OLD_vchaccountNumber;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @OLD_vchaccountNumber + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @OLD_vchaccountNumber + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @OLD_vchaccountNumber + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@New_vchaccountNumber)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @New_vchaccountNumber ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @New_vchaccountNumber + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @New_vchaccountNumber;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @New_vchaccountNumber + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @New_vchaccountNumber + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @New_vchaccountNumber + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@loadDate)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @loadDate ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @loadDate + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeDateTime =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @loadDate;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeDateTime);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @loadDate + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @loadDate + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeDateTime);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @loadDate + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchAccountNumber,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchAccountNumber + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchAccountNumber + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Fifteen
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAccountNumber +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAccountNumber + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAccountNumber +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchAccountNumber + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@OLD_OwnershipPercent,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @OLD_OwnershipPercent + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @OLD_OwnershipPercent + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Five
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@OLD_OwnershipPercent +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_OwnershipPercent + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@OLD_OwnershipPercent +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @OLD_OwnershipPercent + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@New_OwnershipPercent,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @New_OwnershipPercent + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @New_OwnershipPercent + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Five
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@New_OwnershipPercent +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @New_OwnershipPercent + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@New_OwnershipPercent +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @New_OwnershipPercent + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@OLD_Passport,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @OLD_Passport + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @OLD_Passport + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Thirty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@OLD_Passport +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_Passport + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@OLD_Passport +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @OLD_Passport + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@New_Passport,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @New_Passport + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @New_Passport + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Thirty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@New_Passport +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @New_Passport + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@New_Passport +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @New_Passport + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@OLD_vchaccountNumber,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @OLD_vchaccountNumber + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @OLD_vchaccountNumber + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Fifteen
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@OLD_vchaccountNumber +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_vchaccountNumber + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@OLD_vchaccountNumber +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @OLD_vchaccountNumber + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@New_vchaccountNumber,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @New_vchaccountNumber + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @New_vchaccountNumber + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Fifteen
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@New_vchaccountNumber +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @New_vchaccountNumber + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@New_vchaccountNumber +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @New_vchaccountNumber + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@loadDate,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @loadDate + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @loadDate + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @loadDate + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchAccountNumber;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchAccountNumber + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchAccountNumber + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @One
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAccountNumber +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAccountNumber + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAccountNumber +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchAccountNumber + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @OLD_OwnershipPercent;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @OLD_OwnershipPercent + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @OLD_OwnershipPercent + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Two
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@OLD_OwnershipPercent +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_OwnershipPercent + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@OLD_OwnershipPercent +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @OLD_OwnershipPercent + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @New_OwnershipPercent;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @New_OwnershipPercent + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @New_OwnershipPercent + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Three
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@New_OwnershipPercent +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @New_OwnershipPercent + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@New_OwnershipPercent +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @New_OwnershipPercent + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @OLD_Passport;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @OLD_Passport + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @OLD_Passport + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Four
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@OLD_Passport +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_Passport + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@OLD_Passport +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @OLD_Passport + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @New_Passport;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @New_Passport + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @New_Passport + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Five
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@New_Passport +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @New_Passport + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@New_Passport +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @New_Passport + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @OLD_vchaccountNumber;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @OLD_vchaccountNumber + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @OLD_vchaccountNumber + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Six
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@OLD_vchaccountNumber +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_vchaccountNumber + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@OLD_vchaccountNumber +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @OLD_vchaccountNumber + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @New_vchaccountNumber;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @New_vchaccountNumber + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @New_vchaccountNumber + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Seven
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@New_vchaccountNumber +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @New_vchaccountNumber + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@New_vchaccountNumber +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @New_vchaccountNumber + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @loadDate;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @loadDate + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @loadDate + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Eight
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @loadDate + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchAccountNumber,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.upDestination_Open_AccountHolderDetails WHERE @vchAccountNumber IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAccountNumber +  ' Has nullable test.';
Set @ExpectedResult = @vchAccountNumber+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchAccountNumber+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAccountNumber + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchAccountNumber +  ' Has nullable test.'; 
Set @ExpectedResult = @vchAccountNumber+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchAccountNumber+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAccountNumber + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchAccountNumber +  ' Has nullable test.';
Set @ExpectedResult =    @vchAccountNumber+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchAccountNumber+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAccountNumber + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchAccountNumber +  ' Has nullable test.';
Set @ExpectedResult = @vchAccountNumber+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchAccountNumber+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAccountNumber + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@OLD_OwnershipPercent,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.upDestination_Open_AccountHolderDetails WHERE @OLD_OwnershipPercent IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@OLD_OwnershipPercent +  ' Has nullable test.';
Set @ExpectedResult = @OLD_OwnershipPercent+ @ColHasNull+trim(str(@One));
Set @ActualResult = @OLD_OwnershipPercent+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_OwnershipPercent + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@OLD_OwnershipPercent +  ' Has nullable test.'; 
Set @ExpectedResult = @OLD_OwnershipPercent+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@OLD_OwnershipPercent+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_OwnershipPercent + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@OLD_OwnershipPercent +  ' Has nullable test.';
Set @ExpectedResult =    @OLD_OwnershipPercent+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @OLD_OwnershipPercent+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_OwnershipPercent + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@OLD_OwnershipPercent +  ' Has nullable test.';
Set @ExpectedResult = @OLD_OwnershipPercent+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @OLD_OwnershipPercent+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_OwnershipPercent + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@New_OwnershipPercent,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.upDestination_Open_AccountHolderDetails WHERE @New_OwnershipPercent IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@New_OwnershipPercent +  ' Has nullable test.';
Set @ExpectedResult = @New_OwnershipPercent+ @ColHasNull+trim(str(@One));
Set @ActualResult = @New_OwnershipPercent+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @New_OwnershipPercent + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@New_OwnershipPercent +  ' Has nullable test.'; 
Set @ExpectedResult = @New_OwnershipPercent+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@New_OwnershipPercent+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @New_OwnershipPercent + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@New_OwnershipPercent +  ' Has nullable test.';
Set @ExpectedResult =    @New_OwnershipPercent+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @New_OwnershipPercent+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @New_OwnershipPercent + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@New_OwnershipPercent +  ' Has nullable test.';
Set @ExpectedResult = @New_OwnershipPercent+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @New_OwnershipPercent+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @New_OwnershipPercent + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@OLD_Passport,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.upDestination_Open_AccountHolderDetails WHERE @OLD_Passport IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@OLD_Passport +  ' Has nullable test.';
Set @ExpectedResult = @OLD_Passport+ @ColHasNull+trim(str(@One));
Set @ActualResult = @OLD_Passport+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_Passport + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@OLD_Passport +  ' Has nullable test.'; 
Set @ExpectedResult = @OLD_Passport+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@OLD_Passport+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_Passport + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@OLD_Passport +  ' Has nullable test.';
Set @ExpectedResult =    @OLD_Passport+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @OLD_Passport+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_Passport + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@OLD_Passport +  ' Has nullable test.';
Set @ExpectedResult = @OLD_Passport+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @OLD_Passport+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_Passport + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@New_Passport,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.upDestination_Open_AccountHolderDetails WHERE @New_Passport IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@New_Passport +  ' Has nullable test.';
Set @ExpectedResult = @New_Passport+ @ColHasNull+trim(str(@One));
Set @ActualResult = @New_Passport+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @New_Passport + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@New_Passport +  ' Has nullable test.'; 
Set @ExpectedResult = @New_Passport+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@New_Passport+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @New_Passport + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@New_Passport +  ' Has nullable test.';
Set @ExpectedResult =    @New_Passport+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @New_Passport+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @New_Passport + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@New_Passport +  ' Has nullable test.';
Set @ExpectedResult = @New_Passport+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @New_Passport+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @New_Passport + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@OLD_vchaccountNumber,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.upDestination_Open_AccountHolderDetails WHERE @OLD_vchaccountNumber IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@OLD_vchaccountNumber +  ' Has nullable test.';
Set @ExpectedResult = @OLD_vchaccountNumber+ @ColHasNull+trim(str(@One));
Set @ActualResult = @OLD_vchaccountNumber+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_vchaccountNumber + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@OLD_vchaccountNumber +  ' Has nullable test.'; 
Set @ExpectedResult = @OLD_vchaccountNumber+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@OLD_vchaccountNumber+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_vchaccountNumber + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@OLD_vchaccountNumber +  ' Has nullable test.';
Set @ExpectedResult =    @OLD_vchaccountNumber+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @OLD_vchaccountNumber+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_vchaccountNumber + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@OLD_vchaccountNumber +  ' Has nullable test.';
Set @ExpectedResult = @OLD_vchaccountNumber+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @OLD_vchaccountNumber+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_vchaccountNumber + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@New_vchaccountNumber,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.upDestination_Open_AccountHolderDetails WHERE @New_vchaccountNumber IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@New_vchaccountNumber +  ' Has nullable test.';
Set @ExpectedResult = @New_vchaccountNumber+ @ColHasNull+trim(str(@One));
Set @ActualResult = @New_vchaccountNumber+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @New_vchaccountNumber + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@New_vchaccountNumber +  ' Has nullable test.'; 
Set @ExpectedResult = @New_vchaccountNumber+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@New_vchaccountNumber+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @New_vchaccountNumber + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@New_vchaccountNumber +  ' Has nullable test.';
Set @ExpectedResult =    @New_vchaccountNumber+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @New_vchaccountNumber+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @New_vchaccountNumber + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@New_vchaccountNumber +  ' Has nullable test.';
Set @ExpectedResult = @New_vchaccountNumber+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @New_vchaccountNumber+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @New_vchaccountNumber + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@loadDate,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.upDestination_Open_AccountHolderDetails WHERE @loadDate IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' Has nullable test.';
Set @ExpectedResult = @loadDate+ @ColHasNull+trim(str(@One));
Set @ActualResult = @loadDate+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' Has nullable test.'; 
Set @ExpectedResult = @loadDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@loadDate+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' Has nullable test.';
Set @ExpectedResult =    @loadDate+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @loadDate+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' Has nullable test.';
Set @ExpectedResult = @loadDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @loadDate+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.upDestination_Open_AccountHolderDetails where @vchAccountNumber = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchAccountNumber +  ' Has blank value test.';
Set @ExpectedResult = @vchAccountNumber+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchAccountNumber+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAccountNumber + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchAccountNumber +  ' Has blank value test.';
Set @ExpectedResult = @vchAccountNumber+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchAccountNumber+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAccountNumber + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.upDestination_Open_AccountHolderDetails where @OLD_OwnershipPercent = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@OLD_OwnershipPercent +  ' Has blank value test.';
Set @ExpectedResult = @OLD_OwnershipPercent+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @OLD_OwnershipPercent+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_OwnershipPercent + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@OLD_OwnershipPercent +  ' Has blank value test.';
Set @ExpectedResult = @OLD_OwnershipPercent+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @OLD_OwnershipPercent+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_OwnershipPercent + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.upDestination_Open_AccountHolderDetails where @New_OwnershipPercent = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@New_OwnershipPercent +  ' Has blank value test.';
Set @ExpectedResult = @New_OwnershipPercent+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @New_OwnershipPercent+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @New_OwnershipPercent + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@New_OwnershipPercent +  ' Has blank value test.';
Set @ExpectedResult = @New_OwnershipPercent+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @New_OwnershipPercent+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @New_OwnershipPercent + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.upDestination_Open_AccountHolderDetails where @OLD_Passport = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@OLD_Passport +  ' Has blank value test.';
Set @ExpectedResult = @OLD_Passport+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @OLD_Passport+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_Passport + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@OLD_Passport +  ' Has blank value test.';
Set @ExpectedResult = @OLD_Passport+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @OLD_Passport+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_Passport + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.upDestination_Open_AccountHolderDetails where @New_Passport = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@New_Passport +  ' Has blank value test.';
Set @ExpectedResult = @New_Passport+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @New_Passport+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @New_Passport + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@New_Passport +  ' Has blank value test.';
Set @ExpectedResult = @New_Passport+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @New_Passport+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @New_Passport + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.upDestination_Open_AccountHolderDetails where @OLD_vchaccountNumber = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@OLD_vchaccountNumber +  ' Has blank value test.';
Set @ExpectedResult = @OLD_vchaccountNumber+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @OLD_vchaccountNumber+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_vchaccountNumber + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@OLD_vchaccountNumber +  ' Has blank value test.';
Set @ExpectedResult = @OLD_vchaccountNumber+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @OLD_vchaccountNumber+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OLD_vchaccountNumber + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.upDestination_Open_AccountHolderDetails where @New_vchaccountNumber = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@New_vchaccountNumber +  ' Has blank value test.';
Set @ExpectedResult = @New_vchaccountNumber+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @New_vchaccountNumber+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @New_vchaccountNumber + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@New_vchaccountNumber +  ' Has blank value test.';
Set @ExpectedResult = @New_vchaccountNumber+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @New_vchaccountNumber+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @New_vchaccountNumber + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.upDestination_Open_AccountHolderDetails where @loadDate = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' Has blank value test.';
Set @ExpectedResult = @loadDate+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @loadDate+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' Has blank value test.';
Set @ExpectedResult = @loadDate+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @loadDate+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.upDestination_Open_AccountHolderDetails where vchAccountNumber = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAccountNumber' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchAccountNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchAccountNumber' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAccountNumber' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchAccountNumber' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchAccountNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchAccountNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAccountNumber' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


--select @NumBlank = count(@PrimaryKey) from AccountMaster.upDestination_Open_AccountHolderDetails where OLD_OwnershipPercent = '';

--If @NumBlank = 0
--begin
--Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OLD_OwnershipPercent' + @Column+ @somecolumnsblank + 'test.';
--Set @ExpectedResult = 'OLD_OwnershipPercent' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
--Set @ActualResult ='OLD_OwnershipPercent' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
--Set @TestStatus = @Passed;
--Set @TestDateTime = Sysdatetime();
--Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OLD_OwnershipPercent' + @Column + @somecolumnsblank + @tasbpwvda;
--Set @TestStatusCode = 3;

--EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end
--else
--begin
--Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'OLD_OwnershipPercent' + @Column+ @somecolumnsblank + 'test.';
--Set @ExpectedResult = 'OLD_OwnershipPercent' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
--Set @ActualResult = 'OLD_OwnershipPercent' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
--Set @TestStatus = @Warning;
--Set @TestDateTime = Sysdatetime();
--Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OLD_OwnershipPercent' + @Column + @somecolumnsblank + @tasbpwvda;
--Set @TestStatusCode = 2;
--EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end
--set @NumBlank = null


--------------------------------------------------------
--------------------------------------------------------


--select @NumBlank = count(@PrimaryKey) from AccountMaster.upDestination_Open_AccountHolderDetails where New_OwnershipPercent = '';

--If @NumBlank = 0
--begin
--Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'New_OwnershipPercent' + @Column+ @somecolumnsblank + 'test.';
--Set @ExpectedResult = 'New_OwnershipPercent' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
--Set @ActualResult ='New_OwnershipPercent' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
--Set @TestStatus = @Passed;
--Set @TestDateTime = Sysdatetime();
--Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'New_OwnershipPercent' + @Column + @somecolumnsblank + @tasbpwvda;
--Set @TestStatusCode = 3;

--EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end
--else
--begin
--Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'New_OwnershipPercent' + @Column+ @somecolumnsblank + 'test.';
--Set @ExpectedResult = 'New_OwnershipPercent' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
--Set @ActualResult = 'New_OwnershipPercent' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
--Set @TestStatus = @Warning;
--Set @TestDateTime = Sysdatetime();
--Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'New_OwnershipPercent' + @Column + @somecolumnsblank + @tasbpwvda;
--Set @TestStatusCode = 2;
--EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
--   @MyTestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end
--set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.upDestination_Open_AccountHolderDetails where OLD_Passport = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OLD_Passport' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'OLD_Passport' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='OLD_Passport' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OLD_Passport' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'OLD_Passport' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'OLD_Passport' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OLD_Passport' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OLD_Passport' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.upDestination_Open_AccountHolderDetails where New_Passport = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'New_Passport' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'New_Passport' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='New_Passport' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'New_Passport' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'New_Passport' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'New_Passport' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'New_Passport' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'New_Passport' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.upDestination_Open_AccountHolderDetails where OLD_vchaccountNumber = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OLD_vchaccountNumber' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'OLD_vchaccountNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='OLD_vchaccountNumber' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OLD_vchaccountNumber' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'OLD_vchaccountNumber' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'OLD_vchaccountNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OLD_vchaccountNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OLD_vchaccountNumber' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.upDestination_Open_AccountHolderDetails where New_vchaccountNumber = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'New_vchaccountNumber' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'New_vchaccountNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='New_vchaccountNumber' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'New_vchaccountNumber' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'New_vchaccountNumber' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'New_vchaccountNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'New_vchaccountNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'New_vchaccountNumber' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.upDestination_Open_AccountHolderDetails where loadDate = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'loadDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'loadDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='loadDate' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'loadDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'loadDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'loadDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.upDestination_Open_AccountHolderDetails where vchAccountNumber is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAccountNumber' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchAccountNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchAccountNumber' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAccountNumber' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAccountNumber' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchAccountNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchAccountNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAccountNumber' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.upDestination_Open_AccountHolderDetails where OLD_OwnershipPercent is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OLD_OwnershipPercent' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'OLD_OwnershipPercent' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OLD_OwnershipPercent' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OLD_OwnershipPercent' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OLD_OwnershipPercent' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'OLD_OwnershipPercent' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OLD_OwnershipPercent' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OLD_OwnershipPercent' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.upDestination_Open_AccountHolderDetails where New_OwnershipPercent is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'New_OwnershipPercent' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'New_OwnershipPercent' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'New_OwnershipPercent' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'New_OwnershipPercent' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'New_OwnershipPercent' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'New_OwnershipPercent' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'New_OwnershipPercent' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'New_OwnershipPercent' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.upDestination_Open_AccountHolderDetails where OLD_Passport is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OLD_Passport' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'OLD_Passport' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OLD_Passport' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OLD_Passport' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OLD_Passport' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'OLD_Passport' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OLD_Passport' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OLD_Passport' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.upDestination_Open_AccountHolderDetails where New_Passport is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'New_Passport' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'New_Passport' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'New_Passport' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'New_Passport' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'New_Passport' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'New_Passport' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'New_Passport' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'New_Passport' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.upDestination_Open_AccountHolderDetails where OLD_vchaccountNumber is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OLD_vchaccountNumber' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'OLD_vchaccountNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OLD_vchaccountNumber' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OLD_vchaccountNumber' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OLD_vchaccountNumber' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'OLD_vchaccountNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OLD_vchaccountNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OLD_vchaccountNumber' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.upDestination_Open_AccountHolderDetails where New_vchaccountNumber is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'New_vchaccountNumber' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'New_vchaccountNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'New_vchaccountNumber' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'New_vchaccountNumber' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'New_vchaccountNumber' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'New_vchaccountNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'New_vchaccountNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'New_vchaccountNumber' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.upDestination_Open_AccountHolderDetails where loadDate is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'loadDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'loadDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'loadDate' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'loadDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'loadDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'loadDate' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
--------------------------------------------------
if not exists (select vchAccountNumber 
from
AccountMaster.upDestination_Open_AccountHolderDetails
where 
vchAccountNumber like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAccountNumber'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchAccountNumber'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchAccountNumber' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAccountNumber'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAccountNumber'  + @Column+ @jvt;
Set @ExpectedResult = 'vchAccountNumber'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchAccountNumber'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAccountNumber'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select vchAccountNumber 
from
AccountMaster.upDestination_Open_AccountHolderDetails
where 
OLD_OwnershipPercent like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OLD_OwnershipPercent'  + @Column+ @jvt ;
Set @ExpectedResult = 'OLD_OwnershipPercent'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'OLD_OwnershipPercent' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OLD_OwnershipPercent'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OLD_OwnershipPercent'  + @Column+ @jvt;
Set @ExpectedResult = 'OLD_OwnershipPercent'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'OLD_OwnershipPercent'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OLD_OwnershipPercent'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select vchAccountNumber 
from
AccountMaster.upDestination_Open_AccountHolderDetails
where 
New_OwnershipPercent like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'New_OwnershipPercent'  + @Column+ @jvt ;
Set @ExpectedResult = 'New_OwnershipPercent'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'New_OwnershipPercent' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'New_OwnershipPercent'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'New_OwnershipPercent'  + @Column+ @jvt;
Set @ExpectedResult = 'New_OwnershipPercent'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'New_OwnershipPercent'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'New_OwnershipPercent'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select vchAccountNumber 
from
AccountMaster.upDestination_Open_AccountHolderDetails
where 
OLD_Passport like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OLD_Passport'  + @Column+ @jvt ;
Set @ExpectedResult = 'OLD_Passport'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'OLD_Passport' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OLD_Passport'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OLD_Passport'  + @Column+ @jvt;
Set @ExpectedResult = 'OLD_Passport'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'OLD_Passport'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OLD_Passport'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select vchAccountNumber 
from
AccountMaster.upDestination_Open_AccountHolderDetails
where 
New_Passport like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'New_Passport'  + @Column+ @jvt ;
Set @ExpectedResult = 'New_Passport'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'New_Passport' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'New_Passport'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'New_Passport'  + @Column+ @jvt;
Set @ExpectedResult = 'New_Passport'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'New_Passport'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'New_Passport'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select vchAccountNumber 
from
AccountMaster.upDestination_Open_AccountHolderDetails
where 
OLD_vchaccountNumber like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OLD_vchaccountNumber'  + @Column+ @jvt ;
Set @ExpectedResult = 'OLD_vchaccountNumber'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'OLD_vchaccountNumber' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OLD_vchaccountNumber'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OLD_vchaccountNumber'  + @Column+ @jvt;
Set @ExpectedResult = 'OLD_vchaccountNumber'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'OLD_vchaccountNumber'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OLD_vchaccountNumber'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select vchAccountNumber 
from
AccountMaster.upDestination_Open_AccountHolderDetails
where 
New_vchaccountNumber like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'New_vchaccountNumber'  + @Column+ @jvt ;
Set @ExpectedResult = 'New_vchaccountNumber'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'New_vchaccountNumber' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'New_vchaccountNumber'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'New_vchaccountNumber'  + @Column+ @jvt;
Set @ExpectedResult = 'New_vchaccountNumber'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'New_vchaccountNumber'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'New_vchaccountNumber'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select vchAccountNumber 
from
AccountMaster.upDestination_Open_AccountHolderDetails
where 
loadDate like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'loadDate'  + @Column+ @jvt ;
Set @ExpectedResult = 'loadDate'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'loadDate' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'loadDate'  + @Column+ @jvt;
Set @ExpectedResult = 'loadDate'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'loadDate'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

select @TestDescription=(Select case WHEN
(select count(TestStatus) 
from [dbo].[TestResults] 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%upDestination_Open_AccountHolderDetails%'
and TestStatus = @Passed
and TestId> @MaxTestID) = (select count(TestStatus) 
from [dbo].[TestResults] 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%upDestination_Open_AccountHolderDetails%'
and TestId> @MaxTestID)
THEN 'ALL TESTS PASSED!'
WHEN
(select count(TestStatus) 
from [dbo].[TestResults] 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%upDestination_Open_AccountHolderDetails%'
and TestStatus in(@Passed,@Warning)
and TestId> @MaxTestID) = (select count(TestStatus) 
from [dbo].[TestResults] 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%upDestination_Open_AccountHolderDetails%'
and TestId> @MaxTestID)
THEN 'ALL TESTS PASSED. SOME PASS WITH WARNING!'
ELSE 'ALL TESTS DID NOT PASS!' END)

insert testLog values (@TestScenario,@EndingStatus,@TestDescription,SYSDATETIME(),System_User)

select
testStatus
,count(TestId)"NumberOfTests"
from
[dbo].[TestResults]
where
TestDateTime > convert(Date,GetDate())
and
TestDescription LIKE '%upDestination_Open_AccountHolderDetails%'
and 
TestId> @MaxTestID
group by TestStatus

select 
TestLogId
,TestScenario
,TestDescription
,TestStatus
,TestExecutionTime
,ExecutedBy
 from 
 testLog 
 Where TestScenario LIKE '%upDestination_Open_AccountHolderDetails%' 
--And TestExecutionTime > convert(Date,GetDate())
and TestLogId > @MaxTestLogId
Order by TestExecutionTime Desc

select
[TestId]
,[TestMessage]
,[TestDescription]
,[TestStatus]
,[ExpectedResult]
,[ActualResult]
,[TestDateTime]
,[TestStatusCode]
from
[dbo].[TestResults]
where
TestDateTime > convert(Date,GetDate())
and
TestDescription LIKE '%upDestination_Open_AccountHolderDetails%'
and 
TestId> @MaxTestID
order by TestStatusCode asc

---------------------------------------------------
