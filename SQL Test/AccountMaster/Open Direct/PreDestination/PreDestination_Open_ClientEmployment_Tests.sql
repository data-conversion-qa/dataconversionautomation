Use DataConversion

declare @MaxTestID int; --Varaible to caputure Last Test ID in TestResult Table
declare @TestAreaBeginning varchar(30) = 'AccountMaster' + ' '
declare @MaxTestLogID int;
-- Test Name Declaration Starts Here --
declare @TableName varchar(40) = 'PreDestination_Open_ClientEmployment';
-- Test Name Declaration Ends Here --

-- Table Column Declaration Starts Here --

declare @vchAddress1 varchar(11) = 'vchAddress1';
declare @vchAddress2 varchar(11) = 'vchAddress2';
declare @vchCity varchar(7) = 'vchCity';
declare @vchEmploymentStatus varchar(19) = 'vchEmploymentStatus';
declare @vchIncomeSource varchar(15) = 'vchIncomeSource';
declare @vchOccupation varchar(13) = 'vchOccupation';
declare @vchState varchar(8) = 'vchState';
declare @vchZip1 varchar(7) = 'vchZip1';
declare @vchZip2 varchar(7) = 'vchZip2';
declare @loadDate varchar(8) = 'loadDate';
declare @ClientIDENTIFIER varchar(16) = 'ClientIDENTIFIER';
declare @LinkedTenantNAFSequenceID varchar(25) = 'LinkedTenantNAFSequenceID';
declare @LinkedTenantUniqueMATID varchar(23) = 'LinkedTenantUniqueMATID';


-- Table Column Declaration Ends Here --

--Fixed -- Do Not Change -- 
declare @CDNE varchar(75) = ' column does not exist.';
declare @CE varchar(25) = ' column exists.';
declare @TC varchar(25) = ' Test Case #';
declare @E varchar(25) = ' exists.'
declare @DNE varchar(25) = ' does not exist.';
declare @Table varchar(10) = 'Table';
Declare @Column varchar(25) = ' column ';
Declare @HTCT varchar(30) = 'has the correct type. ';
Declare @DNHTCT varchar(40) = 'does not have the correct type.';
Declare @CPIC varchar(50) = ' column position is correct.';
Declare @CPII varchar(50) = ' column position is incorrect.';
declare @CLIC varchar(40) = ' column length is correct.';
declare @CLII varchar(40) = ' column length is incorrect.';
declare @INPWDT varchar(40) = ' is not populated with date time.';
declare @IPWDT varchar(40) = ' is populated with date time.';
declare @TCDNED varchar(40) = 'does not exist, damn it!';
declare @somearenull varchar(25) = ' some columns are null.';
declare @tasbpwvda varchar(255) = ' should be populated with valid data. ';
declare @sbpwagcir varchar(255) = ' should be populated with a good contact identifier reference.'
declare @somecolumnsblank varchar(50) = ' some columns are blank.';
declare @somerhbd varchar(50) = ' some rows have bad dates.';
declare @sciraor varchar(175) = ' some contact identifier reference are oprhaned records';
declare @cchav varchar(50) = 'changeType column has appropriate values.';
declare @ccdnhav varchar(50) = 'changeType column does not have appropriate values.'
DECLARE @MyTestDescription varchar(500)
Declare @RC int;
DECLARE @TestStatusCode int
declare @NullCount int;
declare @Position int;
declare @TypeVarChar int = 167;
declare @TypeDateTime int = 61;
declare @TypeDate int = 40;
declare @TypeInt int = 56;
declare @TypeBit int = 104;
declare @TypeBigInt int = 127;
declare @TypeNVarChar int = 231;
declare @TypeChar int = 175;
declare @TypeMoney int = 60;
declare @TypeDecimal int = 106;
declare @TypeTinyInt int = 48;
declare @TypeTUserId int = 167;
declare @Passed varchar(10) = 'Passed.';
declare @Failed varchar(10) = 'Failed!';
declare @Warning varchar(10) = 'Warning!';
declare @PrimaryKey varchar(100) = 'PrimaryKey';
declare @SystemTypeId int;
declare @LengthDateTime int = 8;
declare @LengthInt int = 4;
Declare @BadValueCount int = 0;
declare @Zero int = 0;
declare @One int = 1;
declare @Two int = 2;
declare @Three int = 3;
declare @Four int = 4;
declare @Five int = 5;
declare @Seven int = 7;
declare @Six int = 6;
declare @Eight int = 8;
declare @Nine int = 9;
declare @Ten int = 10;
declare @eleven int = 11;
declare @Twelve int = 12;
declare @Thirteen int = 13;
declare @Fourteen int = 14;
declare @Fifteen int = 15;
declare @Sixteen int = 16;
declare @Seventeen int = 17;
declare @Eighteen int = 18;
declare @Nineteen int = 19;
declare @Twenty int = 20;
declare @TwentyOne int = 21;
declare @TwentyTwo int = 22;
declare @TwentyThree int = 23;
declare @TwentyFour int = 24;
declare @TwentyFive int = 25;
declare @TwentySix int = 26;
declare @TwentySeven int = 27;
declare @TwentyEight int = 28;
declare @TwentyNine int = 29;
declare @Thirty int = 30;
declare @ThirtyOne int = 31;
declare @ThirtyTwo int = 32;
declare @ThirtyThree int = 33;
declare @ThirtyFour int = 34;
declare @ThirtyFive int = 35;
declare @ThirtySix int = 36;
declare @ThirtySeven int = 37;
declare @ThirtyEight int = 38;
declare @ThirtyNine int = 39;
declare @Forty int = 40;
declare @FortyOne int = 41;
declare @FortyTwo int = 42;
declare @FortyThree int = 43;
declare @FortyFour int = 44;
declare @FortyFive int = 45;
declare @FortySix int = 46;
declare @FortySeven int = 47;
declare @FortyEight int = 48;
declare @FortyNine int = 49;
declare @Fifty int = 50;
declare @FiftyOne int = 51;
declare @FiftyTwo int = 52;
declare @FiftyThree int = 53;
declare @FiftyFour int = 54;
declare @FiftyFive int = 55;
declare @FiftySix int = 56;
declare @FiftySeven int = 57;
declare @FiftyEight int = 58;
declare @FiftyNine int = 59;
declare @Sixty int = 60;
declare @SixtyOne int = 61;
declare @SixtyTwo int = 62;
declare @SixtyThree int = 63;
declare @SixtyFour int = 64;
declare @SixtyFive int = 65;
declare @SixtySix int = 66;
declare @SixtySeven int = 67;
declare @SixtyEight int = 68;
declare @SixtyNine int = 69;
declare @Seventy int = 70;
declare @SeventyOne int = 71;
declare @SeventyTwo int = 72;
declare @SeventyThree int = 73;
declare @SeventyFour int = 74;
declare @SeventyFive int = 75;
declare @SeventySix int = 76;
declare @SeventySeven int = 77;
declare @SeventyEight int = 78;
declare @SeventyNine int = 79;
declare @Eighty int = 80;
declare @EightyOne int = 81;
declare @EightyTwo int = 82;
declare @EightyThree int = 83;
declare @EightyFour int = 84;
declare @EightyFive int = 85;
declare @EightySix int = 86;
declare @EightySeven int = 87;
declare @EightyEight int = 88;
declare @EightyNine int = 89;
declare @Ninety int = 90;
declare @NinetyOne int = 91;
declare @NinetyTwo int = 92;
declare @NinetyThree int = 93;
declare @NinetyFour int = 94;
declare @NinetyFive int = 95;
declare @NinetySix int = 96;
declare @NinetySeven int = 97;
declare @NinetyEight int = 98;
declare @NinetyNine int = 99;
declare @Hundred int = 100;
Declare @OneHundred int = 100;
declare @OneHundredTwentyFive int = 125;
declare @OneHundredTwentyEight int = 128;
declare @OneHundredThirtyThree int = 133;
declare @TwoHundred int = 200;
declare @TwoHundredFifty int = 250;
declare @TwoHundredFiftyFive int = 255;
declare @TwoHundredFiftySix int = 256;
declare @ThreeHundredFifty int = 350;
declare @FiveHundred int = 500;
declare @OneThousand int = 1000;
declare @ThirteenHundred int = 1300;
declare @FiveThousand int = 5000;
declare @onehundreddecimal decimal = 100.00;
declare @GreaterThan100Percent int;
declare @LessThanZeroPercent int;
declare @hapgt100 varchar(100) = ' held away percent greater than 100 percent. ';
declare @haplt100 varchar(100) = ' held away percent less than 100 percent. ';
declare @thapt varchar(100) = ' The held away percent test. ';
declare @hapltz varchar(100) = ' held away percent less than zero percent. ';
declare @hapnltz varchar(100) = ' held away percent not less than zero percent. '
declare @jvt varchar(50) = ' Junk values test. ';
declare @JVRCount int;
--declare @TestDescription varchar(200);
declare @ExpectedResult varchar(100);
declare @ActualResult varchar(100);
declare @TestStatus varchar(15);
declare @TestDateTime datetime;
declare @TestMessage varchar(200);
--declare @TableNameOne varchar(30) = 'staging_plan';
declare @Schema int = 17;
declare @Insert varchar(30) = 'I-insert';
Declare @Update varchar(30) = 'U-update';
Declare @Delete varchar(30) = 'D-delete';
declare @TableObject Int;
declare @ColumnObject Int;
declare @ColumnIsNullable Int;
declare @ICN varchar(30) = ' is column nullable.';
declare @TCIN varchar(40) = ' the column is nullable. ';
declare @TCINN varchar(50) = ' the column is not nullable. ';
declare @MaxLength Int;
declare @Return int;
declare @AllowsNull int;
declare @HasNull int;
declare @ColHasNull varchar(20) = ' column has null';
declare @ColHasNoNull varchar(20)= ' column has no null';
declare @HasBlank Bit;
declare @ColHasBlank varchar(25) = ' column has blank value '
declare @ColHasNoBlank varchar(25) = ' column has no blank value '
declare @HasBlankCount int;
declare @NumBlank int;
Declare @SchemaName varchar(50) = 'AccountMaster';
--Declare @TypeDecimal int = 106;
declare @LengthTinyInt int = 1;
declare @LengthDecimal int = 17;
declare @Eigthteen int = 18;
declare @TempTableName varchar(25) = '#testResults';
Declare @TestScenario varchar(80) = 'AccountMaster PreDestination_Open_ClientEmployment'
declare @BeginningStatus varchar(25) = 'Beginning';
Declare @EndingStatus varchar(25) = 'Ending';
Declare @TestDescription varchar(800) = 'Test the PreDestination_Open_ClientEmployment table and columns confirming the properties of the columns and some data verification.';

--Get the Schema ID for the table retrieval
select @Schema = dbo.fnGetSchemaId('AccountMaster');
--Get the TableObjectId for the table using the Table Name and the SchemaId
select @TableObject = dbo.fnGetTableObject(@Schema,@TableName);
-- Test the table exists.
select @MaxTestID = (select Max(Testid) from TestResults);

select @MaxTestLogId = (select Max(TestLogId) from TestLog)

/*Create temporary table of state and territory codes*/

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE #StatesTerritoriesPostalCodes(
	[StateTerritoryId] [int] IDENTITY(1,1) NOT NULL,
	[StateTerritoryName] [varchar](50) NOT NULL,
	[Abbreviation] [varchar](25) NOT NULL,
	[PostalCode] [varchar](5) NOT NULL	
) ON [PRIMARY]

insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Alabama','Ala.','AL');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Alaska','Alaska','AK');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Arizona','Ariz.','AZ');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Arkansas','Ark.','AR');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('California','Calif.','CA');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Colorado','Colo.','CO');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Connecticut','Conn.','CT');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Delaware','Del.','DE');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('District of Columbia','D.C.','DC');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Florida','Fla.','FL');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Georgia','Ga.','GA');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Hawaii','Hawaii','HI');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Idaho','Idaho','ID');
insert into  #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Illinois','Ill.','IL');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Indiana','Ind.','IN');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Iowa','Iowa','IA');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Kansas','Kans.','KS');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Kentucky','Ky.','KY');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Louisiana','La.','LA');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Maine','Maine','ME');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Maryland','Md.','MD');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Massachusetts','Mass.','MA');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Michigan','Mich.','MI');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Minnesota','Minn.','MN');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Mississippi','Miss.','MS');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Missouri','Mo.','MO');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Montana','Mont.','MT');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Nebraska','Nebr.','NE');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Nevada','Nev.','NV');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('New Hampshire','N.H.','NH');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('New Jersey','N.J.','NJ');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('New Mexico','N.M.','NM');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('New York','N.Y.','NY');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('North Carolina','N.C.','NC');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('North Dakota','N.D.','ND');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Ohio','Ohio','OH');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Oklahoma','Okla.','OK');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Oregon','Ore.','OR');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Pennsylvania','Pa.','PA');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Rhode Island','R.I.','RI');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('South Carolina','S.C.','SC');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('South Dakota','S.D.','SD');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Tennessee','Tenn.','TN');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Texas','Tex.','TX');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Utah','Utah','UT');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Vermont','Vt.','VT');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Virginia','Va.','VA');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Washington','Wash.','WA');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('West Virginia','W.V.','WV');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Wisconsin','Wis.','WI');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Wyoming','Wyo.','WY');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('American Samoa','','AS');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Guam','Guam','GU');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Marshall Islands','','MH');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Micronesia','','FM');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Northern Marianas','','MP');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Palau','','PW');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Puerto Rico','P.R.','PR');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('Virgin Islands','','VI');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('US Armed Forces - Americas','','AA');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('US Armed Forces - Europe','','AE');
insert into #StatesTerritoriesPostalCodes([StateTerritoryName],[Abbreviation],[PostalCode])Values
('US Armed Forces - Pacific','','AP');
insert testLog values (@TestScenario,@BeginningStatus,@TestDescription,SYSDATETIME(),System_User)

If @TableObject is not null
Begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table + ' exists test.';
Set @ExpectedResult =  'Table Exists.';
Set @ActualResult = 'Table Exists.';
Set @TestStatus =  @Passed;          
Set @TestDateTime = SYSDATETIME();
Set @TestMessage =  @TC + trim(str(@@IDENTITY)) +' '+@Table+' '+@TableName + @E;
Set @TestStatusCode =  3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
End
	else
Begin

Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' exists test.';
Set @ExpectedResult = 'Table Exists.';
Set @ActualResult = 'Table object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Table+' '+@TableName + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
		
End
--------------------------------------------------

set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchAddress1,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchAddress1 + ' exists test.';
Set @ExpectedResult =  @vchAddress1 + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchAddress1 + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchAddress1 + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchAddress1 + ' exists test.';
Set @ExpectedResult = @vchAddress1 + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchAddress1 + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchAddress1 + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchAddress2,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchAddress2 + ' exists test.';
Set @ExpectedResult =  @vchAddress2 + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchAddress2 + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchAddress2 + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchAddress2 + ' exists test.';
Set @ExpectedResult = @vchAddress2 + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchAddress2 + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchAddress2 + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchCity,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchCity + ' exists test.';
Set @ExpectedResult =  @vchCity + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchCity + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchCity + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchCity + ' exists test.';
Set @ExpectedResult = @vchCity + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchCity + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchCity + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchEmploymentStatus,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchEmploymentStatus + ' exists test.';
Set @ExpectedResult =  @vchEmploymentStatus + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchEmploymentStatus + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchEmploymentStatus + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchEmploymentStatus + ' exists test.';
Set @ExpectedResult = @vchEmploymentStatus + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchEmploymentStatus + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchEmploymentStatus + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchIncomeSource,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchIncomeSource + ' exists test.';
Set @ExpectedResult =  @vchIncomeSource + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchIncomeSource + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchIncomeSource + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchIncomeSource + ' exists test.';
Set @ExpectedResult = @vchIncomeSource + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchIncomeSource + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchIncomeSource + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchOccupation,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchOccupation + ' exists test.';
Set @ExpectedResult =  @vchOccupation + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchOccupation + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchOccupation + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchOccupation + ' exists test.';
Set @ExpectedResult = @vchOccupation + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchOccupation + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchOccupation + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchState,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchState + ' exists test.';
Set @ExpectedResult =  @vchState + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchState + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchState + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchState + ' exists test.';
Set @ExpectedResult = @vchState + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchState + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchState + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchZip1,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchZip1 + ' exists test.';
Set @ExpectedResult =  @vchZip1 + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchZip1 + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchZip1 + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchZip1 + ' exists test.';
Set @ExpectedResult = @vchZip1 + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchZip1 + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchZip1 + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@vchZip2,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @vchZip2 + ' exists test.';
Set @ExpectedResult =  @vchZip2 + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @vchZip2 + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @vchZip2 + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @vchZip2 + ' exists test.';
Set @ExpectedResult = @vchZip2 + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@vchZip2 + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@vchZip2 + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@loadDate,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @loadDate + ' exists test.';
Set @ExpectedResult =  @loadDate + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @loadDate + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @loadDate + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @loadDate + ' exists test.';
Set @ExpectedResult = @loadDate + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@loadDate + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@loadDate + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@ClientIDENTIFIER,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @ClientIDENTIFIER + ' exists test.';
Set @ExpectedResult =  @ClientIDENTIFIER + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @ClientIDENTIFIER + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @ClientIDENTIFIER + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @ClientIDENTIFIER + ' exists test.';
Set @ExpectedResult = @ClientIDENTIFIER + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@ClientIDENTIFIER + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@ClientIDENTIFIER + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@LinkedTenantNAFSequenceID,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @LinkedTenantNAFSequenceID + ' exists test.';
Set @ExpectedResult =  @LinkedTenantNAFSequenceID + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @LinkedTenantNAFSequenceID + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @LinkedTenantNAFSequenceID + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @LinkedTenantNAFSequenceID + ' exists test.';
Set @ExpectedResult = @LinkedTenantNAFSequenceID + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@LinkedTenantNAFSequenceID + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@LinkedTenantNAFSequenceID + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@LinkedTenantUniqueMATID,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @LinkedTenantUniqueMATID + ' exists test.';
Set @ExpectedResult =  @LinkedTenantUniqueMATID + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @LinkedTenantUniqueMATID + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @LinkedTenantUniqueMATID + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @LinkedTenantUniqueMATID + ' exists test.';
Set @ExpectedResult = @LinkedTenantUniqueMATID + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@LinkedTenantUniqueMATID + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@LinkedTenantUniqueMATID + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchAddress1)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchAddress1 ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchAddress1 + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchAddress1;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchAddress1 + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchAddress1 + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchAddress1 + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchAddress2)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchAddress2 ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchAddress2 + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchAddress2;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchAddress2 + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchAddress2 + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchAddress2 + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchCity)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchCity ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchCity + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchCity;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchCity + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchCity + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchCity + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchEmploymentStatus)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchEmploymentStatus ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchEmploymentStatus + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchEmploymentStatus;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchEmploymentStatus + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchEmploymentStatus + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchEmploymentStatus + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchIncomeSource)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchIncomeSource ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchIncomeSource + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchIncomeSource;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchIncomeSource + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchIncomeSource + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchIncomeSource + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchOccupation)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchOccupation ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchOccupation + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchOccupation;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchOccupation + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchOccupation + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchOccupation + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchState)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchState ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchState + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchState;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchState + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchState + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchState + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchZip1)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchZip1 ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchZip1 + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchZip1;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchZip1 + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchZip1 + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchZip1 + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@vchZip2)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @vchZip2 ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @vchZip2 + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchZip2;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchZip2 + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @vchZip2 + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @vchZip2 + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@loadDate)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @loadDate ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @loadDate + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeDateTime =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @loadDate;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeDateTime);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @loadDate + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @loadDate + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeDateTime);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @loadDate + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@ClientIDENTIFIER)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @ClientIDENTIFIER ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @ClientIDENTIFIER + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ClientIDENTIFIER;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ClientIDENTIFIER + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ClientIDENTIFIER + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ClientIDENTIFIER + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@LinkedTenantNAFSequenceID)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @LinkedTenantNAFSequenceID ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @LinkedTenantNAFSequenceID + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @LinkedTenantNAFSequenceID;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @LinkedTenantNAFSequenceID + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @LinkedTenantNAFSequenceID + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @LinkedTenantNAFSequenceID + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@LinkedTenantUniqueMATID)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @LinkedTenantUniqueMATID ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @LinkedTenantUniqueMATID + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @LinkedTenantUniqueMATID;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @LinkedTenantUniqueMATID + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @LinkedTenantUniqueMATID + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @LinkedTenantUniqueMATID + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchAddress1,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchAddress1 + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchAddress1 + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Forty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAddress1 +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAddress1 + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAddress1 +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchAddress1 + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchAddress2,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchAddress2 + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchAddress2 + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Forty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAddress2 +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAddress2 + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAddress2 +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchAddress2 + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchCity,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchCity + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchCity + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Thirty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchCity +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchCity + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchCity +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchCity + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchEmploymentStatus,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchEmploymentStatus + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchEmploymentStatus + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Two
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchEmploymentStatus +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchEmploymentStatus + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchEmploymentStatus +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchEmploymentStatus + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchIncomeSource,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchIncomeSource + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchIncomeSource + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Fifty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchIncomeSource +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchIncomeSource + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchIncomeSource +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchIncomeSource + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchOccupation,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchOccupation + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchOccupation + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Fifty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchOccupation +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchOccupation + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchOccupation +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchOccupation + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchState,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchState + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchState + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Five
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchState +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchState + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchState +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchState + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchZip1,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchZip1 + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchZip1 + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Fifteen
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchZip1 +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchZip1 + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchZip1 +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchZip1 + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@vchZip2,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchZip2 + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchZip2 + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchZip2 +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchZip2 + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchZip2 +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchZip2 + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@loadDate,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @loadDate + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @loadDate + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @loadDate + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@ClientIDENTIFIER,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @ClientIDENTIFIER + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @ClientIDENTIFIER + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @TwoHundredFiftyFive
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ClientIDENTIFIER +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ClientIDENTIFIER + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ClientIDENTIFIER +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @ClientIDENTIFIER + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@LinkedTenantNAFSequenceID,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @LinkedTenantNAFSequenceID + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @LinkedTenantNAFSequenceID + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @TwoHundredFiftyFive
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LinkedTenantNAFSequenceID +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LinkedTenantNAFSequenceID + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LinkedTenantNAFSequenceID +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @LinkedTenantNAFSequenceID + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@LinkedTenantUniqueMATID,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @LinkedTenantUniqueMATID + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @LinkedTenantUniqueMATID + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @TwoHundredFiftyFive
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LinkedTenantUniqueMATID +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LinkedTenantUniqueMATID + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LinkedTenantUniqueMATID +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @LinkedTenantUniqueMATID + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchAddress1;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchAddress1 + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchAddress1 + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @One
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAddress1 +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAddress1 + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAddress1 +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchAddress1 + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchAddress2;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchAddress2 + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchAddress2 + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Two
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAddress2 +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAddress2 + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAddress2 +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchAddress2 + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchCity;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchCity + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchCity + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Three
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchCity +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchCity + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchCity +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchCity + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchEmploymentStatus;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchEmploymentStatus + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchEmploymentStatus + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Four
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchEmploymentStatus +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchEmploymentStatus + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchEmploymentStatus +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchEmploymentStatus + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchIncomeSource;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchIncomeSource + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchIncomeSource + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Five
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchIncomeSource +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchIncomeSource + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchIncomeSource +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchIncomeSource + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchOccupation;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchOccupation + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchOccupation + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Six
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchOccupation +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchOccupation + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchOccupation +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchOccupation + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchState;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchState + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchState + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Seven
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchState +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchState + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchState +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchState + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchZip1;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchZip1 + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchZip1 + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Eight
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchZip1 +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchZip1 + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchZip1 +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchZip1 + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @vchZip2;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @vchZip2 + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @vchZip2 + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Nine
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchZip2 +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchZip2 + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchZip2 +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @vchZip2 + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @loadDate;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @loadDate + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @loadDate + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @loadDate + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @ClientIDENTIFIER;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @ClientIDENTIFIER + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @ClientIDENTIFIER + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Eleven
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ClientIDENTIFIER +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ClientIDENTIFIER + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ClientIDENTIFIER +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @ClientIDENTIFIER + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @LinkedTenantNAFSequenceID;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @LinkedTenantNAFSequenceID + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @LinkedTenantNAFSequenceID + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Twelve
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LinkedTenantNAFSequenceID +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LinkedTenantNAFSequenceID + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LinkedTenantNAFSequenceID +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @LinkedTenantNAFSequenceID + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @LinkedTenantUniqueMATID;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @LinkedTenantUniqueMATID + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @LinkedTenantUniqueMATID + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Thirteen
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LinkedTenantUniqueMATID +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LinkedTenantUniqueMATID + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LinkedTenantUniqueMATID +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @LinkedTenantUniqueMATID + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchAddress1,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment WHERE @vchAddress1 IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAddress1 +  ' Has nullable test.';
Set @ExpectedResult = @vchAddress1+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchAddress1+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAddress1 + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchAddress1 +  ' Has nullable test.'; 
Set @ExpectedResult = @vchAddress1+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchAddress1+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAddress1 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchAddress1 +  ' Has nullable test.';
Set @ExpectedResult =    @vchAddress1+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchAddress1+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAddress1 + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchAddress1 +  ' Has nullable test.';
Set @ExpectedResult = @vchAddress1+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchAddress1+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAddress1 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchAddress2,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment WHERE @vchAddress2 IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchAddress2 +  ' Has nullable test.';
Set @ExpectedResult = @vchAddress2+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchAddress2+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAddress2 + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchAddress2 +  ' Has nullable test.'; 
Set @ExpectedResult = @vchAddress2+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchAddress2+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAddress2 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchAddress2 +  ' Has nullable test.';
Set @ExpectedResult =    @vchAddress2+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchAddress2+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAddress2 + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchAddress2 +  ' Has nullable test.';
Set @ExpectedResult = @vchAddress2+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchAddress2+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAddress2 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchCity,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment WHERE @vchCity IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchCity +  ' Has nullable test.';
Set @ExpectedResult = @vchCity+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchCity+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchCity + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchCity +  ' Has nullable test.'; 
Set @ExpectedResult = @vchCity+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchCity+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchCity + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchCity +  ' Has nullable test.';
Set @ExpectedResult =    @vchCity+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchCity+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchCity + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchCity +  ' Has nullable test.';
Set @ExpectedResult = @vchCity+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchCity+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchCity + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchEmploymentStatus,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment WHERE @vchEmploymentStatus IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchEmploymentStatus +  ' Has nullable test.';
Set @ExpectedResult = @vchEmploymentStatus+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchEmploymentStatus+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchEmploymentStatus + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchEmploymentStatus +  ' Has nullable test.'; 
Set @ExpectedResult = @vchEmploymentStatus+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchEmploymentStatus+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchEmploymentStatus + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchEmploymentStatus +  ' Has nullable test.';
Set @ExpectedResult =    @vchEmploymentStatus+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchEmploymentStatus+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchEmploymentStatus + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchEmploymentStatus +  ' Has nullable test.';
Set @ExpectedResult = @vchEmploymentStatus+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchEmploymentStatus+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchEmploymentStatus + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchIncomeSource,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment WHERE @vchIncomeSource IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchIncomeSource +  ' Has nullable test.';
Set @ExpectedResult = @vchIncomeSource+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchIncomeSource+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchIncomeSource + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchIncomeSource +  ' Has nullable test.'; 
Set @ExpectedResult = @vchIncomeSource+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchIncomeSource+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchIncomeSource + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchIncomeSource +  ' Has nullable test.';
Set @ExpectedResult =    @vchIncomeSource+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchIncomeSource+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchIncomeSource + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchIncomeSource +  ' Has nullable test.';
Set @ExpectedResult = @vchIncomeSource+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchIncomeSource+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchIncomeSource + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchOccupation,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment WHERE @vchOccupation IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchOccupation +  ' Has nullable test.';
Set @ExpectedResult = @vchOccupation+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchOccupation+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchOccupation + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchOccupation +  ' Has nullable test.'; 
Set @ExpectedResult = @vchOccupation+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchOccupation+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchOccupation + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchOccupation +  ' Has nullable test.';
Set @ExpectedResult =    @vchOccupation+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchOccupation+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchOccupation + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchOccupation +  ' Has nullable test.';
Set @ExpectedResult = @vchOccupation+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchOccupation+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchOccupation + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchState,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment WHERE @vchState IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchState +  ' Has nullable test.';
Set @ExpectedResult = @vchState+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchState+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchState + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchState +  ' Has nullable test.'; 
Set @ExpectedResult = @vchState+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchState+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchState + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchState +  ' Has nullable test.';
Set @ExpectedResult =    @vchState+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchState+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchState + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchState +  ' Has nullable test.';
Set @ExpectedResult = @vchState+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchState+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchState + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchZip1,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment WHERE @vchZip1 IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchZip1 +  ' Has nullable test.';
Set @ExpectedResult = @vchZip1+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchZip1+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchZip1 + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchZip1 +  ' Has nullable test.'; 
Set @ExpectedResult = @vchZip1+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchZip1+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchZip1 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchZip1 +  ' Has nullable test.';
Set @ExpectedResult =    @vchZip1+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchZip1+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchZip1 + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchZip1 +  ' Has nullable test.';
Set @ExpectedResult = @vchZip1+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchZip1+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchZip1 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@vchZip2,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment WHERE @vchZip2 IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@vchZip2 +  ' Has nullable test.';
Set @ExpectedResult = @vchZip2+ @ColHasNull+trim(str(@One));
Set @ActualResult = @vchZip2+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchZip2 + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@vchZip2 +  ' Has nullable test.'; 
Set @ExpectedResult = @vchZip2+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@vchZip2+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchZip2 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchZip2 +  ' Has nullable test.';
Set @ExpectedResult =    @vchZip2+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @vchZip2+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @vchZip2 + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@vchZip2 +  ' Has nullable test.';
Set @ExpectedResult = @vchZip2+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @vchZip2+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchZip2 + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@loadDate,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment WHERE @loadDate IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' Has nullable test.';
Set @ExpectedResult = @loadDate+ @ColHasNull+trim(str(@One));
Set @ActualResult = @loadDate+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' Has nullable test.'; 
Set @ExpectedResult = @loadDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@loadDate+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' Has nullable test.';
Set @ExpectedResult =    @loadDate+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @loadDate+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' Has nullable test.';
Set @ExpectedResult = @loadDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @loadDate+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@ClientIDENTIFIER,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment WHERE @ClientIDENTIFIER IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ClientIDENTIFIER +  ' Has nullable test.';
Set @ExpectedResult = @ClientIDENTIFIER+ @ColHasNull+trim(str(@One));
Set @ActualResult = @ClientIDENTIFIER+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ClientIDENTIFIER + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@ClientIDENTIFIER +  ' Has nullable test.'; 
Set @ExpectedResult = @ClientIDENTIFIER+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@ClientIDENTIFIER+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ClientIDENTIFIER + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@ClientIDENTIFIER +  ' Has nullable test.';
Set @ExpectedResult =    @ClientIDENTIFIER+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @ClientIDENTIFIER+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @ClientIDENTIFIER + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@ClientIDENTIFIER +  ' Has nullable test.';
Set @ExpectedResult = @ClientIDENTIFIER+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @ClientIDENTIFIER+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ClientIDENTIFIER + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@LinkedTenantNAFSequenceID,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment WHERE @LinkedTenantNAFSequenceID IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LinkedTenantNAFSequenceID +  ' Has nullable test.';
Set @ExpectedResult = @LinkedTenantNAFSequenceID+ @ColHasNull+trim(str(@One));
Set @ActualResult = @LinkedTenantNAFSequenceID+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LinkedTenantNAFSequenceID + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@LinkedTenantNAFSequenceID +  ' Has nullable test.'; 
Set @ExpectedResult = @LinkedTenantNAFSequenceID+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@LinkedTenantNAFSequenceID+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LinkedTenantNAFSequenceID + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@LinkedTenantNAFSequenceID +  ' Has nullable test.';
Set @ExpectedResult =    @LinkedTenantNAFSequenceID+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @LinkedTenantNAFSequenceID+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @LinkedTenantNAFSequenceID + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@LinkedTenantNAFSequenceID +  ' Has nullable test.';
Set @ExpectedResult = @LinkedTenantNAFSequenceID+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @LinkedTenantNAFSequenceID+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LinkedTenantNAFSequenceID + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@LinkedTenantUniqueMATID,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment WHERE @LinkedTenantUniqueMATID IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LinkedTenantUniqueMATID +  ' Has nullable test.';
Set @ExpectedResult = @LinkedTenantUniqueMATID+ @ColHasNull+trim(str(@One));
Set @ActualResult = @LinkedTenantUniqueMATID+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LinkedTenantUniqueMATID + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@LinkedTenantUniqueMATID +  ' Has nullable test.'; 
Set @ExpectedResult = @LinkedTenantUniqueMATID+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@LinkedTenantUniqueMATID+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LinkedTenantUniqueMATID + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@LinkedTenantUniqueMATID +  ' Has nullable test.';
Set @ExpectedResult =    @LinkedTenantUniqueMATID+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @LinkedTenantUniqueMATID+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @LinkedTenantUniqueMATID + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@LinkedTenantUniqueMATID +  ' Has nullable test.';
Set @ExpectedResult = @LinkedTenantUniqueMATID+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @LinkedTenantUniqueMATID+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LinkedTenantUniqueMATID + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment where @vchAddress1 = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchAddress1 +  ' Has blank value test.';
Set @ExpectedResult = @vchAddress1+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchAddress1+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAddress1 + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchAddress1 +  ' Has blank value test.';
Set @ExpectedResult = @vchAddress1+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchAddress1+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAddress1 + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment where @vchAddress2 = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchAddress2 +  ' Has blank value test.';
Set @ExpectedResult = @vchAddress2+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchAddress2+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAddress2 + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchAddress2 +  ' Has blank value test.';
Set @ExpectedResult = @vchAddress2+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchAddress2+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchAddress2 + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment where @vchCity = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchCity +  ' Has blank value test.';
Set @ExpectedResult = @vchCity+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchCity+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchCity + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchCity +  ' Has blank value test.';
Set @ExpectedResult = @vchCity+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchCity+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchCity + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment where @vchEmploymentStatus = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchEmploymentStatus +  ' Has blank value test.';
Set @ExpectedResult = @vchEmploymentStatus+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchEmploymentStatus+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchEmploymentStatus + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchEmploymentStatus +  ' Has blank value test.';
Set @ExpectedResult = @vchEmploymentStatus+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchEmploymentStatus+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchEmploymentStatus + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment where @vchIncomeSource = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchIncomeSource +  ' Has blank value test.';
Set @ExpectedResult = @vchIncomeSource+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchIncomeSource+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchIncomeSource + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchIncomeSource +  ' Has blank value test.';
Set @ExpectedResult = @vchIncomeSource+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchIncomeSource+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchIncomeSource + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment where @vchOccupation = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchOccupation +  ' Has blank value test.';
Set @ExpectedResult = @vchOccupation+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchOccupation+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchOccupation + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchOccupation +  ' Has blank value test.';
Set @ExpectedResult = @vchOccupation+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchOccupation+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchOccupation + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment where @vchState = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchState +  ' Has blank value test.';
Set @ExpectedResult = @vchState+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchState+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchState + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchState +  ' Has blank value test.';
Set @ExpectedResult = @vchState+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchState+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchState + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment where @vchZip1 = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchZip1 +  ' Has blank value test.';
Set @ExpectedResult = @vchZip1+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchZip1+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchZip1 + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchZip1 +  ' Has blank value test.';
Set @ExpectedResult = @vchZip1+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchZip1+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchZip1 + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment where @vchZip2 = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchZip2 +  ' Has blank value test.';
Set @ExpectedResult = @vchZip2+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @vchZip2+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchZip2 + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@vchZip2 +  ' Has blank value test.';
Set @ExpectedResult = @vchZip2+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @vchZip2+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchZip2 + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment where @loadDate = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' Has blank value test.';
Set @ExpectedResult = @loadDate+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @loadDate+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' Has blank value test.';
Set @ExpectedResult = @loadDate+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @loadDate+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment where @ClientIDENTIFIER = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ClientIDENTIFIER +  ' Has blank value test.';
Set @ExpectedResult = @ClientIDENTIFIER+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @ClientIDENTIFIER+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ClientIDENTIFIER + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ClientIDENTIFIER +  ' Has blank value test.';
Set @ExpectedResult = @ClientIDENTIFIER+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @ClientIDENTIFIER+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ClientIDENTIFIER + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment where @LinkedTenantNAFSequenceID = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@LinkedTenantNAFSequenceID +  ' Has blank value test.';
Set @ExpectedResult = @LinkedTenantNAFSequenceID+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @LinkedTenantNAFSequenceID+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LinkedTenantNAFSequenceID + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@LinkedTenantNAFSequenceID +  ' Has blank value test.';
Set @ExpectedResult = @LinkedTenantNAFSequenceID+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @LinkedTenantNAFSequenceID+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LinkedTenantNAFSequenceID + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM AccountMaster.PreDestination_Open_ClientEmployment where @LinkedTenantUniqueMATID = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@LinkedTenantUniqueMATID +  ' Has blank value test.';
Set @ExpectedResult = @LinkedTenantUniqueMATID+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @LinkedTenantUniqueMATID+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LinkedTenantUniqueMATID + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@LinkedTenantUniqueMATID +  ' Has blank value test.';
Set @ExpectedResult = @LinkedTenantUniqueMATID+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @LinkedTenantUniqueMATID+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LinkedTenantUniqueMATID + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where vchAddress1 = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAddress1' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchAddress1' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchAddress1' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAddress1' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchAddress1' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchAddress1' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchAddress1' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAddress1' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where vchAddress2 = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAddress2' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchAddress2' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchAddress2' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAddress2' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchAddress2' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchAddress2' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchAddress2' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAddress2' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where vchCity = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchCity' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchCity' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchCity' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchCity' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchCity' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchCity' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchCity' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchCity' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where vchEmploymentStatus = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchEmploymentStatus' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchEmploymentStatus' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchEmploymentStatus' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchEmploymentStatus' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchEmploymentStatus' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchEmploymentStatus' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchEmploymentStatus' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchEmploymentStatus' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where vchIncomeSource = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchIncomeSource' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchIncomeSource' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchIncomeSource' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchIncomeSource' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchIncomeSource' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchIncomeSource' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchIncomeSource' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchIncomeSource' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where vchOccupation = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchOccupation' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchOccupation' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchOccupation' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchOccupation' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchOccupation' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchOccupation' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchOccupation' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchOccupation' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where vchState = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchState' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchState' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchState' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchState' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchState' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchState' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchState' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchState' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where vchZip1 = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchZip1' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchZip1' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchZip1' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchZip1' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchZip1' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchZip1' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchZip1' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchZip1' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where vchZip2 = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchZip2' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchZip2' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='vchZip2' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchZip2' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'vchZip2' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'vchZip2' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchZip2' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchZip2' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where loadDate = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'loadDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'loadDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='loadDate' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'loadDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'loadDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'loadDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where ClientIDENTIFIER = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ClientIDENTIFIER' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ClientIDENTIFIER' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='ClientIDENTIFIER' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ClientIDENTIFIER' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'ClientIDENTIFIER' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ClientIDENTIFIER' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ClientIDENTIFIER' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ClientIDENTIFIER' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where LinkedTenantNAFSequenceID = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LinkedTenantNAFSequenceID' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'LinkedTenantNAFSequenceID' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='LinkedTenantNAFSequenceID' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LinkedTenantNAFSequenceID' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'LinkedTenantNAFSequenceID' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'LinkedTenantNAFSequenceID' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LinkedTenantNAFSequenceID' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LinkedTenantNAFSequenceID' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where LinkedTenantUniqueMATID = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LinkedTenantUniqueMATID' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'LinkedTenantUniqueMATID' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='LinkedTenantUniqueMATID' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LinkedTenantUniqueMATID' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'LinkedTenantUniqueMATID' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'LinkedTenantUniqueMATID' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LinkedTenantUniqueMATID' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LinkedTenantUniqueMATID' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where vchAddress1 is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAddress1' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchAddress1' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchAddress1' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAddress1' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAddress1' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchAddress1' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchAddress1' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAddress1' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where vchAddress2 is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAddress2' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchAddress2' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchAddress2' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAddress2' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAddress2' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchAddress2' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchAddress2' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAddress2' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where vchCity is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchCity' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchCity' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchCity' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchCity' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchCity' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchCity' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchCity' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchCity' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where vchEmploymentStatus is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchEmploymentStatus' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchEmploymentStatus' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchEmploymentStatus' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchEmploymentStatus' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchEmploymentStatus' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchEmploymentStatus' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchEmploymentStatus' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchEmploymentStatus' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where vchIncomeSource is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchIncomeSource' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchIncomeSource' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchIncomeSource' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchIncomeSource' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchIncomeSource' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchIncomeSource' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchIncomeSource' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchIncomeSource' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where vchOccupation is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchOccupation' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchOccupation' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchOccupation' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchOccupation' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchOccupation' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchOccupation' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchOccupation' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchOccupation' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where vchState is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchState' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchState' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchState' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchState' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchState' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchState' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchState' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchState' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where vchZip1 is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchZip1' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchZip1' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchZip1' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchZip1' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchZip1' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchZip1' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchZip1' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchZip1' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where vchZip2 is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchZip2' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchZip2' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchZip2' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchZip2' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchZip2' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'vchZip2' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'vchZip2' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchZip2' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where loadDate is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'loadDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'loadDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'loadDate' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'loadDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'loadDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'loadDate' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where ClientIDENTIFIER is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ClientIDENTIFIER' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ClientIDENTIFIER' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ClientIDENTIFIER' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ClientIDENTIFIER' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ClientIDENTIFIER' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ClientIDENTIFIER' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ClientIDENTIFIER' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ClientIDENTIFIER' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where LinkedTenantNAFSequenceID is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LinkedTenantNAFSequenceID' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'LinkedTenantNAFSequenceID' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LinkedTenantNAFSequenceID' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LinkedTenantNAFSequenceID' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LinkedTenantNAFSequenceID' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'LinkedTenantNAFSequenceID' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LinkedTenantNAFSequenceID' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LinkedTenantNAFSequenceID' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from AccountMaster.PreDestination_Open_ClientEmployment where LinkedTenantUniqueMATID is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LinkedTenantUniqueMATID' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'LinkedTenantUniqueMATID' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LinkedTenantUniqueMATID' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LinkedTenantUniqueMATID' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LinkedTenantUniqueMATID' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'LinkedTenantUniqueMATID' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LinkedTenantUniqueMATID' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LinkedTenantUniqueMATID' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
---------------------------------------------------
if not exists (select ClientIDENTIFIER 
from
AccountMaster.PreDestination_Open_ClientEmployment
where 
vchAddress1 like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAddress1'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchAddress1'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchAddress1' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAddress1'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAddress1'  + @Column+ @jvt;
Set @ExpectedResult = 'vchAddress1'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchAddress1'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAddress1'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select ClientIDENTIFIER 
from
AccountMaster.PreDestination_Open_ClientEmployment
where 
vchAddress2 like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAddress2'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchAddress2'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchAddress2' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAddress2'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchAddress2'  + @Column+ @jvt;
Set @ExpectedResult = 'vchAddress2'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchAddress2'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchAddress2'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select ClientIDENTIFIER 
from
AccountMaster.PreDestination_Open_ClientEmployment
where 
vchCity like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchCity'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchCity'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchCity' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchCity'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchCity'  + @Column+ @jvt;
Set @ExpectedResult = 'vchCity'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchCity'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchCity'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select ClientIDENTIFIER 
from
AccountMaster.PreDestination_Open_ClientEmployment
where 
vchEmploymentStatus like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchEmploymentStatus'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchEmploymentStatus'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchEmploymentStatus' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchEmploymentStatus'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchEmploymentStatus'  + @Column+ @jvt;
Set @ExpectedResult = 'vchEmploymentStatus'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchEmploymentStatus'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchEmploymentStatus'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select ClientIDENTIFIER 
from
AccountMaster.PreDestination_Open_ClientEmployment
where 
vchIncomeSource like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchIncomeSource'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchIncomeSource'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchIncomeSource' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchIncomeSource'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchIncomeSource'  + @Column+ @jvt;
Set @ExpectedResult = 'vchIncomeSource'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchIncomeSource'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchIncomeSource'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select ClientIDENTIFIER 
from
AccountMaster.PreDestination_Open_ClientEmployment
where 
vchOccupation like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchOccupation'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchOccupation'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchOccupation' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchOccupation'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchOccupation'  + @Column+ @jvt;
Set @ExpectedResult = 'vchOccupation'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchOccupation'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchOccupation'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select ClientIDENTIFIER 
from
AccountMaster.PreDestination_Open_ClientEmployment
where 
vchState like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchState'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchState'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchState' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchState'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchState'  + @Column+ @jvt;
Set @ExpectedResult = 'vchState'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchState'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchState'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select ClientIDENTIFIER 
from
AccountMaster.PreDestination_Open_ClientEmployment
where 
vchZip1 like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchZip1'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchZip1'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchZip1' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchZip1'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchZip1'  + @Column+ @jvt;
Set @ExpectedResult = 'vchZip1'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchZip1'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchZip1'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select ClientIDENTIFIER 
from
AccountMaster.PreDestination_Open_ClientEmployment
where 
vchZip2 like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchZip2'  + @Column+ @jvt ;
Set @ExpectedResult = 'vchZip2'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchZip2' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchZip2'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchZip2'  + @Column+ @jvt;
Set @ExpectedResult = 'vchZip2'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'vchZip2'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'vchZip2'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select ClientIDENTIFIER 
from
AccountMaster.PreDestination_Open_ClientEmployment
where 
loadDate like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'loadDate'  + @Column+ @jvt ;
Set @ExpectedResult = 'loadDate'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'loadDate' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'loadDate'  + @Column+ @jvt;
Set @ExpectedResult = 'loadDate'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'loadDate'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select ClientIDENTIFIER 
from
AccountMaster.PreDestination_Open_ClientEmployment
where 
ClientIDENTIFIER like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ClientIDENTIFIER'  + @Column+ @jvt ;
Set @ExpectedResult = 'ClientIDENTIFIER'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'ClientIDENTIFIER' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ClientIDENTIFIER'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ClientIDENTIFIER'  + @Column+ @jvt;
Set @ExpectedResult = 'ClientIDENTIFIER'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'ClientIDENTIFIER'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ClientIDENTIFIER'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select ClientIDENTIFIER 
from
AccountMaster.PreDestination_Open_ClientEmployment
where 
LinkedTenantNAFSequenceID like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LinkedTenantNAFSequenceID'  + @Column+ @jvt ;
Set @ExpectedResult = 'LinkedTenantNAFSequenceID'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'LinkedTenantNAFSequenceID' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LinkedTenantNAFSequenceID'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LinkedTenantNAFSequenceID'  + @Column+ @jvt;
Set @ExpectedResult = 'LinkedTenantNAFSequenceID'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'LinkedTenantNAFSequenceID'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LinkedTenantNAFSequenceID'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select ClientIDENTIFIER 
from
AccountMaster.PreDestination_Open_ClientEmployment
where 
LinkedTenantUniqueMATID like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LinkedTenantUniqueMATID'  + @Column+ @jvt ;
Set @ExpectedResult = 'LinkedTenantUniqueMATID'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'LinkedTenantUniqueMATID' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LinkedTenantUniqueMATID'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LinkedTenantUniqueMATID'  + @Column+ @jvt;
Set @ExpectedResult = 'LinkedTenantUniqueMATID'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'LinkedTenantUniqueMATID'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LinkedTenantUniqueMATID'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

select @BadValueCount = count(*)
from
AccountMaster.PreDestination_Open_ClientEmployment
where
vchEmploymentStatus not in (select vchCodeValue from Accounts.dbo.codeValues where vchCodeType = 'Employment.EmploymentStatus')
if @BadValueCount = @Zero
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Employment.EmploymentStatus values test.'   ;
Set @ExpectedResult = ' The Employment.EmploymentStatus values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The Employment.EmploymentStatus values are all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchEmploymentStatus + ' ' +  'Employment.EmploymentStatus values test.';
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Employment.EmploymentStatus values test.'   ;
Set @ExpectedResult = ' The Employment.EmploymentStatus values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The Employment.EmploymentStatus values are not all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchEmploymentStatus + ' ' + 'Employment.EmploymentStatus values test.';
Set @TestStatusCode = 1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------------
---------------------------------------------------------
---------------------------------------------------------
---------------------------------------------------------

select @BadValueCount = count(*)
from
AccountMaster.PreDestination_Open_ClientEmployment
where
vchState not in (select PostalCode from #StatesTerritoriesPostalCodes)
if @BadValueCount = @Zero
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'State.Citizenship values test.'   ;
Set @ExpectedResult = ' The State.Citizenship values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The State.Citizenship values are all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchState + ' ' +  'State.Citizenship values test.';
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'State.Citizenship values test.'   ;
Set @ExpectedResult = ' The State.Citizenship values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The State.Citizenship values are not all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchState + ' ' + 'State.Citizenship values test.';
Set @TestStatusCode = 1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------------
---------------------------------------------------------

select @BadValueCount = count(*)
from
AccountMaster.PreDestination_Open_ClientEmployment
where
vchIncomeSource not in ('EMPL','RETD','HOME','UEMP','SEMP','STDT','NULL','Employed','Retired','Unemployed')
if @BadValueCount = @Zero
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchIncomeSource values test.'   ;
Set @ExpectedResult = ' The vchIncomeSource values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The vchIncomeSource values are all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchIncomeSource + ' ' +  'vchIncomeSource values test.';
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'vchIncomeSource values test.'   ;
Set @ExpectedResult = ' The vchIncomeSource values are all valid.'+ trim(str(@Zero));
Set @ActualResult = ' The vchIncomeSource values are not all valid.' + trim(str(@BadValueCount));
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchIncomeSource + ' ' + 'vchIncomeSource values test.';
Set @TestStatusCode = 1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end

---------------------------------------------------------
---------------------------------------------------------

select @BadValueCount = count(*)
from
AccountMaster.PreDestination_Open_ClientEmployment
where
[vchAddress1] is null
and
[vchCity] is null
and
[vchState] is null
and
[vchZip1] is null
if @BadValueCount = @Zero
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Valid Address Check test.'   ;
Set @ExpectedResult = ' The Valid Address Check is successful.'+ trim(str(@Zero));
Set @ActualResult = ' The Valid Address Check is successful.' + trim(str(@BadValueCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchIncomeSource + ' ' +  'Valid Address Check test.';
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Valid Address Check test.'   ;
Set @ExpectedResult = ' The Valid Address Check is successful.'+ trim(str(@Zero));
Set @ActualResult = ' The Valid Address Check is not successful.' + trim(str(@BadValueCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'select * from AccountMaster.PreDestination_Open_ClientEmployment where [vchAddress1] is null and [vchCity] is null and [vchState] is null and [vchZip1] is null';
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------------
---------------------------------------------------------
if not exists(SELECT [ClientIDENTIFIER]
      ,[LinkedTenantNAFSequenceID]
      ,[LinkedTenantUniqueMATID]
		,[vchAddress1]
      ,[vchAddress2]
      ,[vchCity]
      ,[vchEmploymentStatus]
      ,[vchIncomeSource]
      ,[vchOccupation]
      ,[vchState]
      ,[vchZip1]
      ,[vchZip2]     
      ,count([ClientIDENTIFIER])Occurrences
  FROM [AccountMaster].[PreDestination_open_ClientEmployment]
  group by
  [ClientIDENTIFIER]
      ,[LinkedTenantNAFSequenceID]
      ,[LinkedTenantUniqueMATID]
		,[vchAddress1]
      ,[vchAddress2]
      ,[vchCity]
      ,[vchEmploymentStatus]
      ,[vchIncomeSource]
      ,[vchOccupation]
      ,[vchState]
      ,[vchZip1]
      ,[vchZip2]   
	having count([ClientIDENTIFIER])>1)
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Duplicate Records Check test.'   ;
Set @ExpectedResult = ' The Duplicate Records  Check is successful.';
Set @ActualResult = ' The Duplicate Records Check is successful.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @vchIncomeSource + ' ' +  'Duplicate Records Check test.';
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Valid Address Check test.'   ;
Set @ExpectedResult = ' The Duplicate Records Check is successful.';
Set @ActualResult = ' The Duplicate Records Check fails.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'select * from AccountMaster.PreDestination_Open_ClientEmployment where [vchAddress1] is null and [vchCity] is null and [vchState] is null and [vchZip1] is null';
Set @TestStatusCode = 1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------------
---------------------------------------------------------
select @TestDescription=(Select case WHEN
(select count(TestStatus) 
from [dbo].[TestResults] 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%PreDestination_Open_ClientEmployment%'
and TestStatus = @Passed
and TestId> @MaxTestID) = (select count(TestStatus) 
from [dbo].[TestResults] 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%PreDestination_Open_ClientEmployment%'
and TestId> @MaxTestID)
THEN 'ALL TESTS PASSED!'
WHEN
(select count(TestStatus) 
from [dbo].[TestResults] 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%PreDestination_Open_ClientEmployment%'
and TestStatus in(@Passed,@Warning)
and TestId> @MaxTestID) = (select count(TestStatus) 
from [dbo].[TestResults] 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%PreDestination_Open_ClientEmployment%'
and TestId> @MaxTestID)
THEN 'ALL TESTS PASSED. SOME PASS WITH WARNING!'
ELSE 'ALL TESTS DID NOT PASS!' END)

insert testLog values (@TestScenario,@EndingStatus,@TestDescription,SYSDATETIME(),System_User)

select
testStatus
,count(TestId)"NumberOfTests"
from
[dbo].[TestResults]
where
TestDateTime > convert(Date,GetDate())
and
TestDescription LIKE '%PreDestination_Open_ClientEmployment%'
and 
TestId> @MaxTestID
group by TestStatus

select 
TestLogId
,TestScenario
,TestDescription
,TestStatus
,TestExecutionTime
,ExecutedBy
 from 
 testLog 
 Where TestScenario LIKE '%PreDestination_Open_ClientEmployment%' 
--And TestExecutionTime > convert(Date,GetDate())
and TestLogId > @MaxTestLogId
Order by TestExecutionTime Desc

select
[TestId]
,[TestMessage]
,[TestDescription]
,[TestStatus]
,[ExpectedResult]
,[ActualResult]
,[TestDateTime]
,[TestStatusCode]
from
[dbo].[TestResults]
where
TestDateTime > convert(Date,GetDate())
and
TestDescription LIKE '%PreDestination_Open_ClientEmployment%'
and 
TestId> @MaxTestID
order by TestStatusCode asc

/*Drop temporary table*/
drop table #StatesTerritoriesPostalCodes
---------------------------------------------------


