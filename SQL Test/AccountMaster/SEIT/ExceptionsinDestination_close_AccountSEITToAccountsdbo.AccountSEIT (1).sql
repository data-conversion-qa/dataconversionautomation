Select 'AccountMaster.inDestination_close_AccountSEIT' as src, * from (select
ACCOUNTID,
COMPANY_NAME,
LAST_NAME,
FIRST_NAME,
STREET,
ADDRESS2,
ADDRESS3,
ADDRESS4,
ADDRESS5,
ADDRESS6,
CITY,
STATE,
ZIPCODE,
SSN_OR_TAXID,
ACCOUNTNUMBER,
ADVISORID,
TAXABLEFLAG,
Equities_Default_Sell_Method,
Fixed_Income_Default_Sell_Method,
Mutual_Funds_Default_Sell_Method,
User_Defined_Default_Sell_Method,
CDs_Default_Sell_Method,
Mortgaged_Back_Default_Sell_Method,
Unit_Trusts_Default_Sell_Method,
Options_Default_Sell_Method,
TBills_Default_Sell_Method,
Commercial_Paper_Default_Sell_Method,
Model_Type,
Holds_Non_SEI_Securities,
SEI_Managed_Account,
Model_Short_Description,
Model_Long_Description,
GWP_Account_Type,
ACCTKEY,
INSERTED,
UPDATED,
FILENAME,
FILE_DATE,
PlanStatusCode
from
AccountMaster.inDestination_close_AccountSEIT
except
select
ACCOUNTID,
COMPANY_NAME,
LAST_NAME,
FIRST_NAME,
STREET,
ADDRESS2,
ADDRESS3,
ADDRESS4,
ADDRESS5,
ADDRESS6,
CITY,
STATE,
ZIPCODE,
SSN_OR_TAXID,
ACCOUNTNUMBER,
ADVISORID,
TAXABLEFLAG,
Equities_Default_Sell_Method,
Fixed_Income_Default_Sell_Method,
Mutual_Funds_Default_Sell_Method,
User_Defined_Default_Sell_Method,
CDs_Default_Sell_Method,
Mortgaged_Back_Default_Sell_Method,
Unit_Trusts_Default_Sell_Method,
Options_Default_Sell_Method,
TBills_Default_Sell_Method,
Commercial_Paper_Default_Sell_Method,
Model_Type,
Holds_Non_SEI_Securities,
SEI_Managed_Account,
Model_Short_Description,
Model_Long_Description,
GWP_Account_Type,
ACCTKEY,
INSERTED,
UPDATED,
FILENAME,
FILE_DATE,
PlanStatusCode
from
Accounts.dbo.AccountSEIT) Left_Diffs
Union
Select 'Accounts.dbo.AccountSEIT' as src,* from(select
ACCOUNTID,
COMPANY_NAME,
LAST_NAME,
FIRST_NAME,
STREET,
ADDRESS2,
ADDRESS3,
ADDRESS4,
ADDRESS5,
ADDRESS6,
CITY,
STATE,
ZIPCODE,
SSN_OR_TAXID,
ACCOUNTNUMBER,
ADVISORID,
TAXABLEFLAG,
Equities_Default_Sell_Method,
Fixed_Income_Default_Sell_Method,
Mutual_Funds_Default_Sell_Method,
User_Defined_Default_Sell_Method,
CDs_Default_Sell_Method,
Mortgaged_Back_Default_Sell_Method,
Unit_Trusts_Default_Sell_Method,
Options_Default_Sell_Method,
TBills_Default_Sell_Method,
Commercial_Paper_Default_Sell_Method,
Model_Type,
Holds_Non_SEI_Securities,
SEI_Managed_Account,
Model_Short_Description,
Model_Long_Description,
GWP_Account_Type,
ACCTKEY,
INSERTED,
UPDATED,
FILENAME,
FILE_DATE,
PlanStatusCode
from
Accounts.dbo.AccountSEIT
except
select
ACCOUNTID,
COMPANY_NAME,
LAST_NAME,
FIRST_NAME,
STREET,
ADDRESS2,
ADDRESS3,
ADDRESS4,
ADDRESS5,
ADDRESS6,
CITY,
STATE,
ZIPCODE,
SSN_OR_TAXID,
ACCOUNTNUMBER,
ADVISORID,
TAXABLEFLAG,
Equities_Default_Sell_Method,
Fixed_Income_Default_Sell_Method,
Mutual_Funds_Default_Sell_Method,
User_Defined_Default_Sell_Method,
CDs_Default_Sell_Method,
Mortgaged_Back_Default_Sell_Method,
Unit_Trusts_Default_Sell_Method,
Options_Default_Sell_Method,
TBills_Default_Sell_Method,
Commercial_Paper_Default_Sell_Method,
Model_Type,
Holds_Non_SEI_Securities,
SEI_Managed_Account,
Model_Short_Description,
Model_Long_Description,
GWP_Account_Type,
ACCTKEY,
INSERTED,
UPDATED,
FILENAME,
FILE_DATE,
PlanStatusCode
from
AccountMaster.inDestination_close_AccountSEIT) Right_Diffs
