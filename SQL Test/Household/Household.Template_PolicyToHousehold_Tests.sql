Use DataConversion

declare @MaxTestID int; --Varaible to caputure Last Test ID in TestResult Table
declare @TestAreaBeginning varchar(30) = 'Household' + ' '
declare @MaxTestLogID int;
-- Test Name Declaration Starts Here --
declare @TableName varchar(40) = 'Template_PolicyToHousehold';
-- Test Name Declaration Ends Here --

-- Table Column Declaration Starts Here --

declare @HouseholdIdentifier varchar(19) = 'HouseholdIdentifier';
declare @InsurancePolicyIdentifier varchar(25) = 'InsurancePolicyIdentifier';
declare @FundCompanyCode varchar(15) = 'FundCompanyCode';
declare @InsurancePolicyNumber varchar(21) = 'InsurancePolicyNumber';
declare @SSNonPolicy varchar(11) = 'SSNonPolicy';
declare @loadDate varchar(8) = 'loadDate';
declare @checksum varchar(8) = 'checksum';
declare @checkDate varchar(9) = 'checkDate';
declare @num varchar(3) = 'num';


-- Table Column Declaration Ends Here --

--Fixed -- Do Not Change -- 
declare @CDNE varchar(75) = ' column does not exist.';
declare @CE varchar(25) = ' column exists.';
declare @TC varchar(25) = ' Test Case #';
declare @E varchar(25) = ' exists.'
declare @DNE varchar(25) = ' does not exist.';
declare @Table varchar(10) = 'Table';
Declare @Column varchar(25) = ' column ';
Declare @HTCT varchar(30) = 'has the correct type. ';
Declare @DNHTCT varchar(40) = 'does not have the correct type.';
Declare @CPIC varchar(50) = ' column position is correct.';
Declare @CPII varchar(50) = ' column position is incorrect.';
declare @CLIC varchar(40) = ' column length is correct.';
declare @CLII varchar(40) = ' column length is incorrect.';
declare @INPWDT varchar(40) = ' is not populated with date time.';
declare @IPWDT varchar(40) = ' is populated with date time.';
declare @TCDNED varchar(40) = 'does not exist, damn it!';
declare @somearenull varchar(25) = ' some columns are null.';
declare @tasbpwvda varchar(255) = ' should be populated with valid data. ';
declare @sbpwagcir varchar(255) = ' should be populated with a good contact identifier reference.'
declare @somecolumnsblank varchar(50) = ' some columns are blank.';
declare @somerhbd varchar(50) = ' some rows have bad dates.';
declare @sciraor varchar(175) = ' some contact identifier reference are oprhaned records';
declare @cchav varchar(50) = 'changeType column has appropriate values.';
declare @ccdnhav varchar(50) = 'changeType column does not have appropriate values.'
DECLARE @MyTestDescription varchar(500)
Declare @RC int;
DECLARE @TestStatusCode int
declare @NullCount int;
declare @Position int;
declare @TypeVarChar int = 167;
declare @TypeDateTime int = 61;
declare @TypeDate int = 40;
declare @TypeInt int = 56;
declare @TypeBit int = 104;
declare @TypeBigInt int = 127;
declare @TypeNVarChar int = 231;
declare @TypeChar int = 175;
declare @TypeMoney int = 60;
declare @TypeDecimal int = 106;
declare @TypeTinyInt int = 48;
declare @TypeTUserId int = 167;
declare @Passed varchar(10) = 'Passed.';
declare @Failed varchar(10) = 'Failed!';
declare @Warning varchar(10) = 'Warning!';
declare @PrimaryKey varchar(100) = 'PrimaryKey';
declare @SystemTypeId int;
declare @LengthDateTime int = 8;
declare @LengthInt int = 4;
declare @Zero int = 0;
declare @One int = 1;
declare @Two int = 2;
declare @Three int = 3;
declare @Four int = 4;
declare @Five int = 5;
declare @Seven int = 7;
declare @Six int = 6;
declare @Eight int = 8;
declare @Nine int = 9;
declare @Ten int = 10;
declare @eleven int = 11;
declare @Twelve int = 12;
declare @Thirteen int = 13;
declare @Fourteen int = 14;
declare @Fifteen int = 15;
declare @Sixteen int = 16;
declare @Seventeen int = 17;
declare @Eighteen int = 18;
declare @Nineteen int = 19;
declare @Twenty int = 20;
declare @TwentyOne int = 21;
declare @TwentyTwo int = 22;
declare @TwentyThree int = 23;
declare @TwentyFour int = 24;
declare @TwentyFive int = 25;
declare @TwentySix int = 26;
declare @TwentySeven int = 27;
declare @TwentyEight int = 28;
declare @TwentyNine int = 29;
declare @Thirty int = 30;
declare @ThirtyOne int = 31;
declare @ThirtyTwo int = 32;
declare @ThirtyThree int = 33;
declare @ThirtyFour int = 34;
declare @ThirtyFive int = 35;
declare @ThirtySix int = 36;
declare @ThirtySeven int = 37;
declare @ThirtyEight int = 38;
declare @ThirtyNine int = 39;
declare @Forty int = 40;
declare @FortyOne int = 41;
declare @FortyTwo int = 42;
declare @FortyThree int = 43;
declare @FortyFour int = 44;
declare @FortyFive int = 45;
declare @FortySix int = 46;
declare @FortySeven int = 47;
declare @FortyEight int = 48;
declare @FortyNine int = 49;
declare @Fifty int = 50;
declare @FiftyOne int = 51;
declare @FiftyTwo int = 52;
declare @FiftyThree int = 53;
declare @FiftyFour int = 54;
declare @FiftyFive int = 55;
declare @FiftySix int = 56;
declare @FiftySeven int = 57;
declare @FiftyEight int = 58;
declare @FiftyNine int = 59;
declare @Sixty int = 60;
declare @SixtyOne int = 61;
declare @SixtyTwo int = 62;
declare @SixtyThree int = 63;
declare @SixtyFour int = 64;
declare @SixtyFive int = 65;
declare @SixtySix int = 66;
declare @SixtySeven int = 67;
declare @SixtyEight int = 68;
declare @SixtyNine int = 69;
declare @Seventy int = 70;
declare @SeventyOne int = 71;
declare @SeventyTwo int = 72;
declare @SeventyThree int = 73;
declare @SeventyFour int = 74;
declare @SeventyFive int = 75;
declare @SeventySix int = 76;
declare @SeventySeven int = 77;
declare @SeventyEight int = 78;
declare @SeventyNine int = 79;
declare @Eighty int = 80;
declare @EightyOne int = 81;
declare @EightyTwo int = 82;
declare @EightyThree int = 83;
declare @EightyFour int = 84;
declare @EightyFive int = 85;
declare @EightySix int = 86;
declare @EightySeven int = 87;
declare @EightyEight int = 88;
declare @EightyNine int = 89;
declare @Ninety int = 90;
declare @NinetyOne int = 91;
declare @NinetyTwo int = 92;
declare @NinetyThree int = 93;
declare @NinetyFour int = 94;
declare @NinetyFive int = 95;
declare @NinetySix int = 96;
declare @NinetySeven int = 97;
declare @NinetyEight int = 98;
declare @NinetyNine int = 99;
declare @Hundred int = 100;
Declare @OneHundred int = 100;
declare @OneHundredTwentyFive int = 125;
declare @OneHundredTwentyEight int = 128;
declare @OneHundredThirtyThree int = 133;
declare @TwoHundred int = 200;
declare @TwoHundredFifty int = 250;
declare @TwoHundredFiftyFive int = 255;
declare @ThreeHundredFifty int = 350;
declare @FiveHundred int = 500;
declare @OneThousand int = 1000;
declare @ThirteenHundred int = 1300;
declare @FiveThousand int = 5000;
declare @onehundreddecimal decimal = 100.00;
declare @GreaterThan100Percent int;
declare @LessThanZeroPercent int;
declare @hapgt100 varchar(100) = ' held away percent greater than 100 percent. ';
declare @haplt100 varchar(100) = ' held away percent less than 100 percent. ';
declare @thapt varchar(100) = ' The held away percent test. ';
declare @hapltz varchar(100) = ' held away percent less than zero percent. ';
declare @hapnltz varchar(100) = ' held away percent not less than zero percent. '
declare @jvt varchar(50) = ' Junk values test. ';
declare @JVRCount int;
--declare @TestDescription varchar(200);
declare @ExpectedResult varchar(100);
declare @ActualResult varchar(100);
declare @TestStatus varchar(15);
declare @TestDateTime datetime;
declare @TestMessage varchar(200);
--declare @TableNameOne varchar(30) = 'staging_plan';
declare @Schema int = 17;
declare @Insert varchar(30) = 'I-insert';
Declare @Update varchar(30) = 'U-update';
Declare @Delete varchar(30) = 'D-delete';
declare @TableObject Int;
declare @ColumnObject Int;
declare @ColumnIsNullable Int;
declare @ICN varchar(30) = ' is column nullable.';
declare @TCIN varchar(40) = ' the column is nullable. ';
declare @TCINN varchar(50) = ' the column is not nullable. ';
declare @MaxLength Int;
declare @Return int;
declare @AllowsNull int;
declare @HasNull int;
declare @ColHasNull varchar(20) = ' column has null';
declare @ColHasNoNull varchar(20)= ' column has no null';
declare @HasBlank Bit;
declare @ColHasBlank varchar(25) = ' column has blank value '
declare @ColHasNoBlank varchar(25) = ' column has no blank value '
declare @HasBlankCount int;
declare @NumBlank int;
Declare @SchemaName varchar(50) = 'Household';
--Declare @TypeDecimal int = 106;
declare @LengthTinyInt int = 1;
declare @LengthDecimal int = 17;
declare @Eigthteen int = 18;
declare @TempTableName varchar(25) = '#testResults';
Declare @TestScenario varchar(80) = 'Household Template_PolicyToHousehold'
declare @BeginningStatus varchar(25) = 'Beginning';
Declare @EndingStatus varchar(25) = 'Ending';
Declare @TestDescription varchar(800) = 'Test the Template_PolicyToHousehold table and columns confirming the properties of the columns and some data verification.';

--Get the Schema ID for the table retrieval
select @Schema = dbo.fnGetSchemaId('Household');
--Get the TableObjectId for the table using the Table Name and the SchemaId
select @TableObject = dbo.fnGetTableObject(@Schema,@TableName);
-- Test the table exists.
select @MaxTestID = (select Max(Testid) from TestResults);

select @MaxTestLogId = (select Max(TestLogId) from TestLog)

insert testLog values (@TestScenario,@BeginningStatus,@TestDescription,SYSDATETIME(),System_User)

If @TableObject is not null
Begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table + ' exists test.';
Set @ExpectedResult =  'Table Exists.';
Set @ActualResult = 'Table Exists.';
Set @TestStatus =  @Passed;          
Set @TestDateTime = SYSDATETIME();
Set @TestMessage =  @TC + trim(str(@@IDENTITY)) +' '+@Table+' '+@TableName + @E;
Set @TestStatusCode =  3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
End
	else
Begin

Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' exists test.';
Set @ExpectedResult = 'Table Exists.';
Set @ActualResult = 'Table object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Table+' '+@TableName + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
		
End
--------------------------------------------------

set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@HouseholdIdentifier,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @HouseholdIdentifier + ' exists test.';
Set @ExpectedResult =  @HouseholdIdentifier + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @HouseholdIdentifier + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @HouseholdIdentifier + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @HouseholdIdentifier + ' exists test.';
Set @ExpectedResult = @HouseholdIdentifier + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@HouseholdIdentifier + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@HouseholdIdentifier + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@InsurancePolicyIdentifier,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @InsurancePolicyIdentifier + ' exists test.';
Set @ExpectedResult =  @InsurancePolicyIdentifier + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @InsurancePolicyIdentifier + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @InsurancePolicyIdentifier + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @InsurancePolicyIdentifier + ' exists test.';
Set @ExpectedResult = @InsurancePolicyIdentifier + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@InsurancePolicyIdentifier + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@InsurancePolicyIdentifier + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@FundCompanyCode,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @FundCompanyCode + ' exists test.';
Set @ExpectedResult =  @FundCompanyCode + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @FundCompanyCode + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @FundCompanyCode + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @FundCompanyCode + ' exists test.';
Set @ExpectedResult = @FundCompanyCode + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@FundCompanyCode + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@FundCompanyCode + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@InsurancePolicyNumber,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @InsurancePolicyNumber + ' exists test.';
Set @ExpectedResult =  @InsurancePolicyNumber + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @InsurancePolicyNumber + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @InsurancePolicyNumber + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @InsurancePolicyNumber + ' exists test.';
Set @ExpectedResult = @InsurancePolicyNumber + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@InsurancePolicyNumber + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@InsurancePolicyNumber + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@SSNonPolicy,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @SSNonPolicy + ' exists test.';
Set @ExpectedResult =  @SSNonPolicy + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @SSNonPolicy + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @SSNonPolicy + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @SSNonPolicy + ' exists test.';
Set @ExpectedResult = @SSNonPolicy + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@SSNonPolicy + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@SSNonPolicy + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@loadDate,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @loadDate + ' exists test.';
Set @ExpectedResult =  @loadDate + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @loadDate + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @loadDate + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @loadDate + ' exists test.';
Set @ExpectedResult = @loadDate + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@loadDate + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@loadDate + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@checksum,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @checksum + ' exists test.';
Set @ExpectedResult =  @checksum + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @checksum + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @checksum + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @checksum + ' exists test.';
Set @ExpectedResult = @checksum + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@checksum + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@checksum + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@checkDate,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @checkDate + ' exists test.';
Set @ExpectedResult =  @checkDate + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @checkDate + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @checkDate + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @checkDate + ' exists test.';
Set @ExpectedResult = @checkDate + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@checkDate + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@checkDate + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = dbo.fnGetColumnObjectId(@num,@TableObject)

if @ColumnObject is not null
begin		
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @num + ' exists test.';
Set @ExpectedResult =  @num + ' ' + @Column + ' ' +@E;
Set @ActualResult =   @num + ' ' + @Column + ' ' +@E;
Set @TestStatus =   @Passed;
Set @TestDateTime =  SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @num + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @num + ' exists test.';
Set @ExpectedResult = @num + ' ' + @Column + ' ' +@E;
Set @ActualResult =	@num + ' ' + @Column + ' ' +@DNE;
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage =	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@num + @DNE;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@HouseholdIdentifier)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @HouseholdIdentifier ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @HouseholdIdentifier + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeInt =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @HouseholdIdentifier;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeInt);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @HouseholdIdentifier + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @HouseholdIdentifier + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeInt);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @HouseholdIdentifier + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@InsurancePolicyIdentifier)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @InsurancePolicyIdentifier ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @InsurancePolicyIdentifier + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @InsurancePolicyIdentifier;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @InsurancePolicyIdentifier + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @InsurancePolicyIdentifier + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @InsurancePolicyIdentifier + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@FundCompanyCode)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @FundCompanyCode ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @FundCompanyCode + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @FundCompanyCode;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @FundCompanyCode + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @FundCompanyCode + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @FundCompanyCode + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@InsurancePolicyNumber)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @InsurancePolicyNumber ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @InsurancePolicyNumber + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @InsurancePolicyNumber;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @InsurancePolicyNumber + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @InsurancePolicyNumber + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @InsurancePolicyNumber + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@SSNonPolicy)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @SSNonPolicy ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @SSNonPolicy + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeVarChar =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @SSNonPolicy;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @SSNonPolicy + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @SSNonPolicy + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeVarChar);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @SSNonPolicy + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@loadDate)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @loadDate ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @loadDate + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeDateTime =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @loadDate;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeDateTime);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @loadDate + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @loadDate + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeDateTime);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @loadDate + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@checksum)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @checksum ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @checksum + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeBigInt =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @checksum;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeBigInt);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @checksum + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @checksum + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeBigInt);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @checksum + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@checkDate)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @checkDate ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @checkDate + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeDateTime =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @checkDate;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeDateTime);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @checkDate + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @checkDate + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeDateTime);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @checkDate + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


set @SystemTypeId = null
select
@SystemTypeId = dbo.fnGetSystemTypeId(@TableObject,@num)
if @SystemTypeId is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @num ;
Set @ExpectedResult =   'Column type matched.';
Set @ActualResult =   'Column object is null.';
Set @TestStatus =  @Failed;
Set @TestDateTime =   SysDatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY)) + ' ' + @num + @Column + @DNE;
Set @TestStatusCode =   1;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
	end
if @SystemTypeId is not null

if @TypeBigInt =  @SystemTypeId
begin	
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @num;
	Set @ExpectedResult = 'Column type matched.'+ ' System Type Id is ' + str(@TypeBigInt);
	Set @ActualResult =  'Column type matched.' + ' System Type Id is ' + str(@SystemTypeId); 
	Set @TestStatus =   @Passed; 
	Set @TestDateTime =  Sysdatetime();
	Set @TestMessage =    @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @num + @Column + @HTCT;
	Set @TestStatusCode =  3;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
	else
	begin
	Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @num + ' system type id test.';
	Set @ExpectedResult =   'Column system type id matched' + ' System Type Id is ' + str(@TypeBigInt);
	Set @ActualResult =  'Column system type id did not match!' + ' System Type Id is ' + str(@SystemTypeId);
	Set @TestStatus = 	@Failed;
	Set @TestDateTime =	 sysdatetime();
	Set @TestMessage =   @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @num + @Column + @DNHTCT;
    Set @TestStatusCode = 1;

   EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@HouseholdIdentifier,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @HouseholdIdentifier + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @HouseholdIdentifier + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@HouseholdIdentifier +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @HouseholdIdentifier + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@HouseholdIdentifier +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @HouseholdIdentifier + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@InsurancePolicyIdentifier,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @InsurancePolicyIdentifier + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyIdentifier + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Thirty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@InsurancePolicyIdentifier +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyIdentifier + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@InsurancePolicyIdentifier +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyIdentifier + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@FundCompanyCode,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @FundCompanyCode + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @FundCompanyCode + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@FundCompanyCode +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @FundCompanyCode + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@FundCompanyCode +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @FundCompanyCode + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@InsurancePolicyNumber,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @InsurancePolicyNumber + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyNumber + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Thirty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@InsurancePolicyNumber +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyNumber + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@InsurancePolicyNumber +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyNumber + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@SSNonPolicy,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @SSNonPolicy + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @SSNonPolicy + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@SSNonPolicy +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @SSNonPolicy + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@SSNonPolicy +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @SSNonPolicy + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@loadDate,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @loadDate + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @loadDate + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @loadDate + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@checksum,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @checksum + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @checksum + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Nineteen
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@checksum +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @checksum + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@checksum +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @checksum + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@checkDate,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @checkDate + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @checkDate + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@checkDate +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @checkDate + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@checkDate +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @checkDate + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@num,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @num + ' '+ @Column + ' length test';
Set @ExpectedResult =    'MaxLength does NOT match!';
Set @ActualResult =    'MaxLength set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @num + @CLII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @MaxLength is not null
If @MaxLength = @Nineteen
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@num +  ' length test.';
Set @ExpectedResult = 'MaxLength matches!';
Set @ActualResult = 'MaxLength matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @num + @CLIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@num +  ' length test.';
Set @ExpectedResult =    'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @num + @CLII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @HouseholdIdentifier;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @HouseholdIdentifier + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @HouseholdIdentifier + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @One
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@HouseholdIdentifier +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @HouseholdIdentifier + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@HouseholdIdentifier +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @HouseholdIdentifier + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @InsurancePolicyIdentifier;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @InsurancePolicyIdentifier + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyIdentifier + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Two
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@InsurancePolicyIdentifier +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyIdentifier + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@InsurancePolicyIdentifier +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyIdentifier + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @FundCompanyCode;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @FundCompanyCode + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @FundCompanyCode + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Three
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@FundCompanyCode +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @FundCompanyCode + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@FundCompanyCode +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @FundCompanyCode + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @InsurancePolicyNumber;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @InsurancePolicyNumber + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyNumber + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Four
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@InsurancePolicyNumber +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyNumber + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@InsurancePolicyNumber +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyNumber + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @SSNonPolicy;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @SSNonPolicy + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @SSNonPolicy + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Five
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@SSNonPolicy +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @SSNonPolicy + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@SSNonPolicy +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @SSNonPolicy + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @loadDate;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @loadDate + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @loadDate + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Seven
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @loadDate + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @checksum;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @checksum + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @checksum + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Nine
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@checksum +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @checksum + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@checksum +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @checksum + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @checkDate;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @checkDate + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @checkDate + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Eight
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@checkDate +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @checkDate + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@checkDate +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @checkDate + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
select
@Position = column_id
from
sys.all_columns
where
object_id = @TableObject 
and
name = @num;


if @Position  is null
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName + ' '+ @num + ' '+ @Column + ' position test';
Set @ExpectedResult =    'Position matches!';
Set @ActualResult =    'Position set to null, column does not exist.';
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC +trim(str(@@IDENTITY))+ ' ' + @num + @CPII;
Set @TestStatusCode =    1; 

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
If @Position  is not null
If @Position = @Six
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@num +  ' position test.';
Set @ExpectedResult = 'Position matches!';
Set @ActualResult = 'Position matches!';  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @num + @CPIC;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@num +  ' position test.';
Set @ExpectedResult =    'Position matches!'; 
Set @ActualResult =    ' Position is '+ trim(str(@Position)) + ' .';
Set @TestStatus =    @Failed; 
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =   @TC + trim(str(@@IDENTITY))+ ' ' + @num + @CPII;
Set @TestStatusCode =   1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@HouseholdIdentifier,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM Household.Template_PolicyToHousehold WHERE @HouseholdIdentifier IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@HouseholdIdentifier +  ' Has nullable test.';
Set @ExpectedResult = @HouseholdIdentifier+ @ColHasNull+trim(str(@One));
Set @ActualResult = @HouseholdIdentifier+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @HouseholdIdentifier + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@HouseholdIdentifier +  ' Has nullable test.'; 
Set @ExpectedResult = @HouseholdIdentifier+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@HouseholdIdentifier+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @HouseholdIdentifier + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@HouseholdIdentifier +  ' Has nullable test.';
Set @ExpectedResult =    @HouseholdIdentifier+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @HouseholdIdentifier+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @HouseholdIdentifier + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@HouseholdIdentifier +  ' Has nullable test.';
Set @ExpectedResult = @HouseholdIdentifier+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @HouseholdIdentifier+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @HouseholdIdentifier + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@InsurancePolicyIdentifier,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM Household.Template_PolicyToHousehold WHERE @InsurancePolicyIdentifier IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@InsurancePolicyIdentifier +  ' Has nullable test.';
Set @ExpectedResult = @InsurancePolicyIdentifier+ @ColHasNull+trim(str(@One));
Set @ActualResult = @InsurancePolicyIdentifier+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyIdentifier + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@InsurancePolicyIdentifier +  ' Has nullable test.'; 
Set @ExpectedResult = @InsurancePolicyIdentifier+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@InsurancePolicyIdentifier+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyIdentifier + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@InsurancePolicyIdentifier +  ' Has nullable test.';
Set @ExpectedResult =    @InsurancePolicyIdentifier+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @InsurancePolicyIdentifier+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyIdentifier + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@InsurancePolicyIdentifier +  ' Has nullable test.';
Set @ExpectedResult = @InsurancePolicyIdentifier+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @InsurancePolicyIdentifier+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyIdentifier + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@FundCompanyCode,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM Household.Template_PolicyToHousehold WHERE @FundCompanyCode IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@FundCompanyCode +  ' Has nullable test.';
Set @ExpectedResult = @FundCompanyCode+ @ColHasNull+trim(str(@One));
Set @ActualResult = @FundCompanyCode+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @FundCompanyCode + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@FundCompanyCode +  ' Has nullable test.'; 
Set @ExpectedResult = @FundCompanyCode+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@FundCompanyCode+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @FundCompanyCode + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@FundCompanyCode +  ' Has nullable test.';
Set @ExpectedResult =    @FundCompanyCode+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @FundCompanyCode+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @FundCompanyCode + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@FundCompanyCode +  ' Has nullable test.';
Set @ExpectedResult = @FundCompanyCode+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @FundCompanyCode+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @FundCompanyCode + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@InsurancePolicyNumber,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM Household.Template_PolicyToHousehold WHERE @InsurancePolicyNumber IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@InsurancePolicyNumber +  ' Has nullable test.';
Set @ExpectedResult = @InsurancePolicyNumber+ @ColHasNull+trim(str(@One));
Set @ActualResult = @InsurancePolicyNumber+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyNumber + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@InsurancePolicyNumber +  ' Has nullable test.'; 
Set @ExpectedResult = @InsurancePolicyNumber+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@InsurancePolicyNumber+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyNumber + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@InsurancePolicyNumber +  ' Has nullable test.';
Set @ExpectedResult =    @InsurancePolicyNumber+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @InsurancePolicyNumber+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyNumber + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@InsurancePolicyNumber +  ' Has nullable test.';
Set @ExpectedResult = @InsurancePolicyNumber+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @InsurancePolicyNumber+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyNumber + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@SSNonPolicy,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM Household.Template_PolicyToHousehold WHERE @SSNonPolicy IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@SSNonPolicy +  ' Has nullable test.';
Set @ExpectedResult = @SSNonPolicy+ @ColHasNull+trim(str(@One));
Set @ActualResult = @SSNonPolicy+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @SSNonPolicy + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@SSNonPolicy +  ' Has nullable test.'; 
Set @ExpectedResult = @SSNonPolicy+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@SSNonPolicy+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @SSNonPolicy + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@SSNonPolicy +  ' Has nullable test.';
Set @ExpectedResult =    @SSNonPolicy+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @SSNonPolicy+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @SSNonPolicy + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@SSNonPolicy +  ' Has nullable test.';
Set @ExpectedResult = @SSNonPolicy+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @SSNonPolicy+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @SSNonPolicy + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@loadDate,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM Household.Template_PolicyToHousehold WHERE @loadDate IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' Has nullable test.';
Set @ExpectedResult = @loadDate+ @ColHasNull+trim(str(@One));
Set @ActualResult = @loadDate+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' Has nullable test.'; 
Set @ExpectedResult = @loadDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@loadDate+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' Has nullable test.';
Set @ExpectedResult =    @loadDate+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @loadDate+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' Has nullable test.';
Set @ExpectedResult = @loadDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @loadDate+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@checksum,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM Household.Template_PolicyToHousehold WHERE @checksum IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@checksum +  ' Has nullable test.';
Set @ExpectedResult = @checksum+ @ColHasNull+trim(str(@One));
Set @ActualResult = @checksum+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @checksum + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@checksum +  ' Has nullable test.'; 
Set @ExpectedResult = @checksum+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@checksum+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @checksum + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@checksum +  ' Has nullable test.';
Set @ExpectedResult =    @checksum+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @checksum+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @checksum + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@checksum +  ' Has nullable test.';
Set @ExpectedResult = @checksum+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @checksum+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @checksum + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@checkDate,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM Household.Template_PolicyToHousehold WHERE @checkDate IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@checkDate +  ' Has nullable test.';
Set @ExpectedResult = @checkDate+ @ColHasNull+trim(str(@One));
Set @ActualResult = @checkDate+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @checkDate + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@checkDate +  ' Has nullable test.'; 
Set @ExpectedResult = @checkDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@checkDate+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @checkDate + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@checkDate +  ' Has nullable test.';
Set @ExpectedResult =    @checkDate+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @checkDate+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @checkDate + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@checkDate +  ' Has nullable test.';
Set @ExpectedResult = @checkDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @checkDate+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @checkDate + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@num,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM Household.Template_PolicyToHousehold WHERE @num IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@num +  ' Has nullable test.';
Set @ExpectedResult = @num+ @ColHasNull+trim(str(@One));
Set @ActualResult = @num+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @num + @ColHasNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 1 AND @HasNull = 0
begin	
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@num +  ' Has nullable test.'; 
Set @ExpectedResult = @num+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =@num+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @num + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	

end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@num +  ' Has nullable test.';
Set @ExpectedResult =    @num+ @ColHasNull+trim(str(@One));
Set @ActualResult =    @num+ @ColHasNull +trim(str(isnull(@ColumnIsNullable,0)));
Set @TestStatus =    @Failed;
Set @TestDateTime =    Sysdatetime();
Set @TestMessage =    @TC+ trim(str(@@IDENTITY))+ ' ' + @num + @ColHasNull;
Set @TestStatusCode =    1;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;	
end
if @AllowsNull = 0 AND @HasNull = 0
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@num +  ' Has nullable test.';
Set @ExpectedResult = @num+ @ColHasNoNull+trim(str(@One));
Set @ActualResult = @num+ @ColHasNoNull +trim(str(isnull(@ColumnIsNullable,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @num + @ColHasNoNull;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------
-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM Household.Template_PolicyToHousehold where @HouseholdIdentifier = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@HouseholdIdentifier +  ' Has blank value test.';
Set @ExpectedResult = @HouseholdIdentifier+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @HouseholdIdentifier+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @HouseholdIdentifier + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@HouseholdIdentifier +  ' Has blank value test.';
Set @ExpectedResult = @HouseholdIdentifier+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @HouseholdIdentifier+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @HouseholdIdentifier + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM Household.Template_PolicyToHousehold where @InsurancePolicyIdentifier = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@InsurancePolicyIdentifier +  ' Has blank value test.';
Set @ExpectedResult = @InsurancePolicyIdentifier+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @InsurancePolicyIdentifier+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyIdentifier + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@InsurancePolicyIdentifier +  ' Has blank value test.';
Set @ExpectedResult = @InsurancePolicyIdentifier+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @InsurancePolicyIdentifier+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyIdentifier + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM Household.Template_PolicyToHousehold where @FundCompanyCode = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@FundCompanyCode +  ' Has blank value test.';
Set @ExpectedResult = @FundCompanyCode+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @FundCompanyCode+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @FundCompanyCode + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@FundCompanyCode +  ' Has blank value test.';
Set @ExpectedResult = @FundCompanyCode+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @FundCompanyCode+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @FundCompanyCode + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM Household.Template_PolicyToHousehold where @InsurancePolicyNumber = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@InsurancePolicyNumber +  ' Has blank value test.';
Set @ExpectedResult = @InsurancePolicyNumber+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @InsurancePolicyNumber+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyNumber + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@InsurancePolicyNumber +  ' Has blank value test.';
Set @ExpectedResult = @InsurancePolicyNumber+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @InsurancePolicyNumber+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsurancePolicyNumber + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM Household.Template_PolicyToHousehold where @SSNonPolicy = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@SSNonPolicy +  ' Has blank value test.';
Set @ExpectedResult = @SSNonPolicy+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @SSNonPolicy+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @SSNonPolicy + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@SSNonPolicy +  ' Has blank value test.';
Set @ExpectedResult = @SSNonPolicy+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @SSNonPolicy+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @SSNonPolicy + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM Household.Template_PolicyToHousehold where @loadDate = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' Has blank value test.';
Set @ExpectedResult = @loadDate+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @loadDate+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@loadDate +  ' Has blank value test.';
Set @ExpectedResult = @loadDate+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @loadDate+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loadDate + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM Household.Template_PolicyToHousehold where @checksum = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@checksum +  ' Has blank value test.';
Set @ExpectedResult = @checksum+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @checksum+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @checksum + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@checksum +  ' Has blank value test.';
Set @ExpectedResult = @checksum+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @checksum+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @checksum + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM Household.Template_PolicyToHousehold where @checkDate = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@checkDate +  ' Has blank value test.';
Set @ExpectedResult = @checkDate+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @checkDate+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @checkDate + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@checkDate +  ' Has blank value test.';
Set @ExpectedResult = @checkDate+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @checkDate+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @checkDate + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

-------------------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM Household.Template_PolicyToHousehold where @num = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@num +  ' Has blank value test.';
Set @ExpectedResult = @num+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @num+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @num + @ColHasNoBlank;
Set @TestStatusCode = 3;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@num +  ' Has blank value test.';
Set @ExpectedResult = @num+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @num+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @num + @ColHasBlank;
Set @TestStatusCode = 2;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------

------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from Household.Template_PolicyToHousehold where HouseholdIdentifier = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'HouseholdIdentifier' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'HouseholdIdentifier' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='HouseholdIdentifier' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'HouseholdIdentifier' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'HouseholdIdentifier' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'HouseholdIdentifier' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'HouseholdIdentifier' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'HouseholdIdentifier' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from Household.Template_PolicyToHousehold where InsurancePolicyIdentifier = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'InsurancePolicyIdentifier' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'InsurancePolicyIdentifier' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='InsurancePolicyIdentifier' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsurancePolicyIdentifier' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'InsurancePolicyIdentifier' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'InsurancePolicyIdentifier' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'InsurancePolicyIdentifier' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsurancePolicyIdentifier' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from Household.Template_PolicyToHousehold where FundCompanyCode = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'FundCompanyCode' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'FundCompanyCode' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='FundCompanyCode' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'FundCompanyCode' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'FundCompanyCode' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'FundCompanyCode' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'FundCompanyCode' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'FundCompanyCode' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from Household.Template_PolicyToHousehold where InsurancePolicyNumber = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'InsurancePolicyNumber' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'InsurancePolicyNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='InsurancePolicyNumber' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsurancePolicyNumber' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'InsurancePolicyNumber' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'InsurancePolicyNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'InsurancePolicyNumber' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsurancePolicyNumber' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from Household.Template_PolicyToHousehold where SSNonPolicy = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'SSNonPolicy' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'SSNonPolicy' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='SSNonPolicy' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'SSNonPolicy' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'SSNonPolicy' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'SSNonPolicy' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'SSNonPolicy' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'SSNonPolicy' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from Household.Template_PolicyToHousehold where loadDate = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'loadDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'loadDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='loadDate' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'loadDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'loadDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'loadDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from Household.Template_PolicyToHousehold where checksum = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'checksum' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'checksum' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='checksum' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'checksum' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'checksum' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'checksum' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'checksum' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'checksum' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from Household.Template_PolicyToHousehold where checkDate = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'checkDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'checkDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='checkDate' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'checkDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'checkDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'checkDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'checkDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'checkDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from Household.Template_PolicyToHousehold where num = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'num' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'num' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult ='num' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'num' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'num' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'num' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'num' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(isnull(@NumBlank,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'num' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from Household.Template_PolicyToHousehold where HouseholdIdentifier is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'HouseholdIdentifier' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'HouseholdIdentifier' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'HouseholdIdentifier' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'HouseholdIdentifier' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'HouseholdIdentifier' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'HouseholdIdentifier' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'HouseholdIdentifier' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'HouseholdIdentifier' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from Household.Template_PolicyToHousehold where InsurancePolicyIdentifier is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'InsurancePolicyIdentifier' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'InsurancePolicyIdentifier' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'InsurancePolicyIdentifier' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsurancePolicyIdentifier' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'InsurancePolicyIdentifier' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'InsurancePolicyIdentifier' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'InsurancePolicyIdentifier' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsurancePolicyIdentifier' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from Household.Template_PolicyToHousehold where FundCompanyCode is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'FundCompanyCode' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'FundCompanyCode' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'FundCompanyCode' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'FundCompanyCode' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'FundCompanyCode' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'FundCompanyCode' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'FundCompanyCode' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'FundCompanyCode' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from Household.Template_PolicyToHousehold where InsurancePolicyNumber is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'InsurancePolicyNumber' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'InsurancePolicyNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'InsurancePolicyNumber' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsurancePolicyNumber' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'InsurancePolicyNumber' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'InsurancePolicyNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'InsurancePolicyNumber' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsurancePolicyNumber' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from Household.Template_PolicyToHousehold where SSNonPolicy is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'SSNonPolicy' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'SSNonPolicy' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'SSNonPolicy' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'SSNonPolicy' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'SSNonPolicy' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'SSNonPolicy' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'SSNonPolicy' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'SSNonPolicy' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from Household.Template_PolicyToHousehold where loadDate is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'loadDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'loadDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'loadDate' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'loadDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'loadDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'loadDate' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from Household.Template_PolicyToHousehold where checksum is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'checksum' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'checksum' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'checksum' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'checksum' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'checksum' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'checksum' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'checksum' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'checksum' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from Household.Template_PolicyToHousehold where checkDate is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'checkDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'checkDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'checkDate' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'checkDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'checkDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'checkDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'checkDate' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'checkDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from Household.Template_PolicyToHousehold where num is null

If @NullCount = 0

Begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'num' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'num' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'num' + @Column + @somearenull   + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'num' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'num' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'num' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'num' + @Column + @somearenull  + @tasbpwvda +trim(str(isNull(@NullCount,0)));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'num' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;
EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
---------------------------------------------------
if not exists (select HouseholdIDENTIFIER 
from
Household.Template_PolicyToHousehold
where 
InsurancePolicyIdentifier like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'InsurancePolicyIdentifier'  + @Column+ @jvt ;
Set @ExpectedResult = 'InsurancePolicyIdentifier'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'InsurancePolicyIdentifier' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsurancePolicyIdentifier'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'InsurancePolicyIdentifier'  + @Column+ @jvt;
Set @ExpectedResult = 'InsurancePolicyIdentifier'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'InsurancePolicyIdentifier'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsurancePolicyIdentifier'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select HouseholdIDENTIFIER 
from
Household.Template_PolicyToHousehold
where 
FundCompanyCode like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'FundCompanyCode'  + @Column+ @jvt ;
Set @ExpectedResult = 'FundCompanyCode'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'FundCompanyCode' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'FundCompanyCode'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'FundCompanyCode'  + @Column+ @jvt;
Set @ExpectedResult = 'FundCompanyCode'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'FundCompanyCode'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'FundCompanyCode'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select HouseholdIDENTIFIER 
from
Household.Template_PolicyToHousehold
where 
InsurancePolicyNumber like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'InsurancePolicyNumber'  + @Column+ @jvt ;
Set @ExpectedResult = 'InsurancePolicyNumber'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'InsurancePolicyNumber' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsurancePolicyNumber'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'InsurancePolicyNumber'  + @Column+ @jvt;
Set @ExpectedResult = 'InsurancePolicyNumber'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'InsurancePolicyNumber'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsurancePolicyNumber'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select HouseholdIDENTIFIER 
from
Household.Template_PolicyToHousehold
where 
SSNonPolicy like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'SSNonPolicy'  + @Column+ @jvt ;
Set @ExpectedResult = 'SSNonPolicy'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'SSNonPolicy' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'SSNonPolicy'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'SSNonPolicy'  + @Column+ @jvt;
Set @ExpectedResult = 'SSNonPolicy'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'SSNonPolicy'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'SSNonPolicy'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select HouseholdIDENTIFIER 
from
Household.Template_PolicyToHousehold
where 
loadDate like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'loadDate'  + @Column+ @jvt ;
Set @ExpectedResult = 'loadDate'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'loadDate' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'loadDate'  + @Column+ @jvt;
Set @ExpectedResult = 'loadDate'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'loadDate'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loadDate'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select HouseholdIDENTIFIER 
from
Household.Template_PolicyToHousehold
where 
checksum like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'checksum'  + @Column+ @jvt ;
Set @ExpectedResult = 'checksum'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'checksum' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'checksum'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'checksum'  + @Column+ @jvt;
Set @ExpectedResult = 'checksum'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'checksum'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'checksum'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select HouseholdIDENTIFIER 
from
Household.Template_PolicyToHousehold
where 
checkDate like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'checkDate'  + @Column+ @jvt ;
Set @ExpectedResult = 'checkDate'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'checkDate' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'checkDate'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'checkDate'  + @Column+ @jvt;
Set @ExpectedResult = 'checkDate'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'checkDate'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'checkDate'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
if not exists (select HouseholdIDENTIFIER 
from
Household.Template_PolicyToHousehold
where 
num like '%|%')
begin
--Select 'The data in the field is clean.' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'num'  + @Column+ @jvt ;
Set @ExpectedResult = 'num'  + @Column + ' The data in the field is clean.';
Set @ActualResult = 'num' + @Column + ' The data in the field is clean.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'num'  + @Column + @jvt;
Set @TestStatusCode = 3;

EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--Select 'There is an invalid character in the field.' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'num'  + @Column+ @jvt;
Set @ExpectedResult = 'num'  +  @Column + ' The data in the field is clean.';
Set @ActualResult = 'num'  + @Column + ' There is an invalid character in the field.';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'num'  + @Column + @jvt;
Set @TestStatusCode = 1;


EXECUTE @RC = [DataConversion].[dbo].[insertTestResult] 
   @MyTestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------------------------------------
---------------------------------------------------
select @TestDescription=(Select case WHEN
(select count(TestStatus) 
from [dbo].[TestResults] 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Template_PolicyToHousehold%'
and TestStatus = @Passed
and TestId> @MaxTestID) = (select count(TestStatus) 
from [dbo].[TestResults] 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Template_PolicyToHousehold%'
and TestId> @MaxTestID)
THEN 'ALL TESTS PASSED!'
WHEN
(select count(TestStatus) 
from [dbo].[TestResults] 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Template_PolicyToHousehold%'
and TestStatus in(@Passed,@Warning)
and TestId> @MaxTestID) = (select count(TestStatus) 
from [dbo].[TestResults] 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Template_PolicyToHousehold%'
and TestId> @MaxTestID)
THEN 'ALL TESTS PASSED. SOME PASS WITH WARNING!'
ELSE 'ALL TESTS DID NOT PASS!' END)

insert testLog values (@TestScenario,@EndingStatus,@TestDescription,SYSDATETIME(),System_User)

select
testStatus
,count(TestId)"NumberOfTests"
from
[dbo].[TestResults]
where
TestDateTime > convert(Date,GetDate())
and
TestDescription LIKE '%Template_PolicyToHousehold%'
and 
TestId> @MaxTestID
group by TestStatus

select 
TestLogId
,TestScenario
,TestDescription
,TestStatus
,TestExecutionTime
,ExecutedBy
 from 
 testLog 
 Where TestScenario LIKE '%Template_PolicyToHousehold%' 
--And TestExecutionTime > convert(Date,GetDate())
and TestLogId > @MaxTestLogId
Order by TestExecutionTime Desc

select
[TestId]
,[TestMessage]
,[TestDescription]
,[TestStatus]
,[ExpectedResult]
,[ActualResult]
,[TestDateTime]
,[TestStatusCode]
from
[dbo].[TestResults]
where
TestDateTime > convert(Date,GetDate())
and
TestDescription LIKE '%Template_PolicyToHousehold%'
and 
TestId> @MaxTestID
order by TestStatusCode asc

---------------------------------------------------
