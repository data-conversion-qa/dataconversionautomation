Select 'Household.PreDestination_HouseholdAccessList' as src, * from (select
HHID,
PlanID
from
Household.PreDestination_HouseholdAccessList
except
select
HHID,
PlanID
from
CFNAccountDB.dbo.HouseholdAccessList) Left_Diffs
Union
Select 'Household.HouseholdAccessList' as src,* from(select
HHID,
PlanID
from
CFNAccountDB.dbo.HouseholdAccessList
except
select
HHID,
PlanID
from
Household.PreDestination_HouseholdAccessList) Right_Diffs
