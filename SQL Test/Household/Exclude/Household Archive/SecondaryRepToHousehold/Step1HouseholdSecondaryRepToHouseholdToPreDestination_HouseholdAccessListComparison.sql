Select 'Household.Template_SecondaryRepToHousehold' as src, * from (select
HouseholdIDENTIFIER,
AdvisorRepIDENTIFIER
from
Household.Template_SecondaryRepToHousehold
except
select
HHID,
PlanID
from
Household.PreDestination_HouseholdAccessList) Left_Diffs
Union
Select 'Household.PreDestination_HouseholdAccessList' as src,* from(select
HHID,
PlanID
from
Household.PreDestination_HouseholdAccessList
except
select
HouseholdIDENTIFIER,
AdvisorRepIDENTIFIER
from
Household.Template_SecondaryRepToHousehold) Right_Diffs
