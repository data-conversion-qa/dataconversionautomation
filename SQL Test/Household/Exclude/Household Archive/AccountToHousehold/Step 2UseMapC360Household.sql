Use DataConversion

select
[HouseholdIDENTIFIER]
,cfn.iHHID
,[AccountDataSource]
,cfn.vchHHName
,[FundCode]
,cfn.vchHHPlanID
,[FundCompanyCode]
,[BranchIDENTIFIER]
,[AccountNumber]
,[TINPrimaryAccountHolderSSNumber]
,[num]
,[loadDate]
,[checkDate]
,[checkSum]
from
[Household].[Template_AccountToHouseHold] tath inner join CFNAccountDb.dbo.CFNAccount cfn on cfn.vchSource=tath.AccountDataSource
and
tath.AccountNumber=cfn.vchAccountNumber
and
tath.TINPrimaryAccountHolderSSNumber=cfn.vchPrimarySSNumber;

USE [DataConversion]
GO

SELECT [iConversionID]
      ,acct.[iHHID]
      ,[vchHouseholdName]
      ,acct.[vchPlanID]
      ,[dtCreated]
  FROM [CRM].[Map_C360Household] map inner join CFNAccountDB.dbo.CFNAccount acct on acct.iHHID=map.IHHID
  and
  acct.vchHHName=map.vchHouseholdName
  and
  acct.vchHHPlanID=map.vchPlanID

GO

