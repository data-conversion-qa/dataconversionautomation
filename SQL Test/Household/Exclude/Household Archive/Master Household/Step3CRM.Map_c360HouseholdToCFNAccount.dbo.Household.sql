Select 'CRM.Map_c360Household' as src, * from (select
iHHID,
vchHouseholdName,
vchPlanID,
dtCreated
from
CRM.Map_c360Household
except
select
iHHID,
vchHHName,
vchPlanID,
dtCreated
from
CFNAccountDB.dbo.Household) Left_Diffs
Union
Select 'CFNAccountDB.dbo.Household' as src,* from(select
iHHID,
vchHHName,
vchPlanID,
dtCreated
from
CFNAccountDB.dbo.Household
except
select
iHHID,
vchHouseholdName,
vchPlanID,
dtCreated
from
CRM.Map_c360Household) Right_Diffs
