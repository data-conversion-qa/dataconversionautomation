Select 'Household.Template_MasterHousehold' as src, * from (select
HouseholdIDENTIFIER,
HouseholdName,
FormalName,
AdvisorRepIDENTIFIER
from
Household.Template_MasterHousehold
except
select
iConversionID,
vchHHName,
vchDescr,
vchPlanID
from
CRM.Convert_C360Household) Left_Diffs
Union
Select 'CRM.Convert_C360Household' as src,* from(select
iConversionID,
vchHHName,
vchDescr,
vchPlanID
from
CRM.Convert_C360Household
except
select
HouseholdIDENTIFIER,
HouseholdName,
FormalName,
AdvisorRepIDENTIFIER
from
Household.Template_MasterHousehold) Right_Diffs
