Select 'CRM.Convert_C360Household' as src, * from (select
iConversionID,
vchHHName,
vchDescr,
vchPlanID
from
CRM.Convert_C360Household
except
select
iHHID,
vchHouseholdName,
vchPlanID,
dtCreated
from
CRM.Map_c360Household) Left_Diffs
Union
Select 'CRM.Map_c360Household' as src,* from(select
iHHID,
vchHouseholdName,
vchPlanID,
dtCreated
from
CRM.Map_c360Household
except
select
iConversionID,
vchHHName,
vchDescr,
vchPlanID
from
CRM.Convert_C360Household) Right_Diffs
