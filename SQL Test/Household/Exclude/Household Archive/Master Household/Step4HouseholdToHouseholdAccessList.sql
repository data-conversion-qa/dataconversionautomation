Use CFNAccountDb

Select 'dbo.Household' as src, * from (select
iHHID,
vchPlanID
from
dbo.Household
except
select
HHID,
PlanID
from
dbo.HouseholdAccessList) Left_Diffs
Union
Select 'dbo.HouseholdAccessList' as src,* from(select
HHID,
PlanID
from
dbo.HouseholdAccessList
except
select
iHHID,
vchPlanID
from
dbo.Household) Right_Diffs
