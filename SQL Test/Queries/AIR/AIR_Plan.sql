SELECT 
[ContactIdentifierReference]
,[PlanIdTypecode]
,[Plan_Id]
,[MainAdvisorOnThePlan]
,(CASE [SplitPercentage] WHEN '0.00' THEN '.00' ELSE CONVERT(varchar,CONVERT(decimal(5,2),[SplitPercentage])) END ) as SplitPercentage
,[CommissionTypeCode]
,NULLIF(CONVERT(varchar,[PlanIdTerminationDate], 23),'1753-01-01') as PlanIdTerminationDate
,[ServiceModelsForThePlanId]
,[AUMDiscount]
,NULLIF(CONVERT(varchar,[MSAExclusionDate], 23),'1753-01-01') as MSAExclusionDate
,NULLIF(CONVERT(varchar,[ProgramElectionDate], 23),'1753-01-01') as ProgramElectionDate
,[12B1HousePaymentCode]
,[ExcludedFromMSA]
FROM [DataIntegrationStage].[air].[Staging_Plan]