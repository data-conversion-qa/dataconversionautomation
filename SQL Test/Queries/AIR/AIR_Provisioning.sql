SELECT 
    [ContactIdentifier]
    ,[LoginId]
    ,[WealthscapeId]
    ,[CRMPrimaryBusUnit]
    ,[CRMSecondaryBusUnit]
    ,[CasperRoles]
    ,[Status]
    ,[UPN]
FROM [DataIntegrationStage].[air].[Staging_Provisioning]