SELECT 
	[ContactIdentifierReference]
	,[AddressType]
	,[AddressLine1]
	,[AddressLine2]
	,[Country]
	,[City]
	,[StateOrProvince]
	,[ZipCode]
	,[OfficePhoneNumber]
	,[FaxNumber]
	,[BranchId]
	,[FinraBranchNumber]
	,[MainAdvisorForTheBranch]
FROM [DataIntegrationStage].[air].[Staging_Address]