SELECT 
	SS.[ContactIdentifierReference]
	,STUFF((SELECT ',' + US.[DRPQuestionCode]
        FROM [DataIntegrationStage].[air].[Staging_DRP]  US
        WHERE [DRPQuestionCode] <> ''
		AND (US.[ContactIdentifierReference] = SS.[ContactIdentifierReference])
		AND ((US.[DRPEventDate] = SS.[DRPEventDate]) OR [DRPEventDate] is null)
		AND ((US.[DRPReportedDate] = SS.[DRPReportedDate]) OR [DRPReportedDate] is null)
		AND ((US.[DRPResolutionDate]= SS.[DRPResolutionDate]) OR [DRPResolutionDate] is null)
		AND ((US.[IsDRPActive] = SS.[IsDRPActive]) OR [IsDRPActive] is null)
        FOR XML PATH('')), 1, 1, '') [DRPQuestionCode]
,NULLIF(SS.[DRPStatus],'') as [DRPStatus]
,NULLIF(CONVERT(varchar,[DRPEventDate], 23),'1753-01-01') as[DRPEventDate]
,NULLIF(CONVERT(varchar,[DRPReportedDate], 23),'1753-01-01') as[DRPReportedDate]
,NULLIF(CONVERT(varchar,[DRPResolutionDate], 23),'1753-01-01') as[DRPResolutionDate]
-- ,(CASE [IsDRPActive] WHEN 1 THEN '1' ELSE '0' END) as [IsDRPActive]
FROM [DataIntegrationStage].[air].[Staging_DRP]  SS
GROUP BY SS.[ContactIdentifierReference], SS.[DRPStatus], SS.[DRPEventDate], SS.[DRPReportedDate], [DRPResolutionDate], [IsDRPActive]
ORDER BY 1