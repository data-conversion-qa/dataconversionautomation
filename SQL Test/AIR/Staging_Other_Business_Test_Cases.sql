Use DataIntegrationStage

declare @MaxTestID int; --Variable to caputure Last Test ID in TestResult Table
declare @TestAreaBeginning varchar(30) = 'air'

-- Test Name Declaration Starts Here --
declare @TableName varchar(100) = 'Staging_Other_Business';
-- Test Name Declaration Ends Here --
declare @SchemaName varchar(25) = 'air';
-- Table Column Declaration Starts Here --

declare @OtherBusinessId varchar(15) = 'OtherBusinessId';
declare @CompanyName varchar(11) = 'CompanyName';
declare @ProcessedDatetime varchar(17) = 'ProcessedDatetime';
declare @InsertDateTime varchar(14) = 'InsertDateTime';
declare @CountryCode varchar(11) = 'CountryCode';
declare @ContactIdentifierReference varchar(26) = 'ContactIdentifierReference';
declare @BusinessDescription varchar(19) = 'BusinessDescription';
declare @ChangeType varchar(10) = 'ChangeType';
declare @AddressLine1 varchar(12) = 'AddressLine1';
declare @AddressLine2 varchar(12) = 'AddressLine2';
declare @City varchar(4) = 'City';
declare @StateOrProvince varchar(15) = 'StateOrProvince';
declare @ZipCode varchar(7) = 'ZipCode';
declare @MeetsConditionsForApproval varchar(26) = 'MeetsConditionsForApproval';
declare @OtherBusinessApprovalCode varchar(25) = 'OtherBusinessApprovalCode';
declare @ComplianceTrackingCode varchar(22) = 'ComplianceTrackingCode';
declare @U4CodeForOutsideBusiness varchar(24) = 'U4CodeForOutsideBusiness';
declare @OtherBusinessApprovalDate varchar(25) = 'OtherBusinessApprovalDate';
declare @OtherBusinessEndDate varchar(20) = 'OtherBusinessEndDate';
declare @ADVForm varchar(7) = 'ADVForm';
declare @BranchOfficeRegistration varchar(24) = 'BranchOfficeRegistration';


-- Table Column Declaration Ends Here --

--Fixed -- Do Not Change -- 
declare @CDNE varchar(75) = ' column does not exist.';
declare @CE varchar(25) = ' column exists.';
declare @TC varchar(25) = ' Test Case #';
declare @E varchar(25) = ' exists.'
declare @DNE varchar(25) = ' does not exist.';
declare @Table varchar(10) = 'Table';
Declare @Column varchar(25) = ' column ';
Declare @HTCT varchar(30) = 'has the correct type. ';
Declare @DNHTCT varchar(40) = 'does not have the correct type.';
declare @CLIC varchar(40) = ' column length is correct.';
declare @CLII varchar(40) = ' column length is incorrect.';
declare @INPWDT varchar(40) = ' is not populated with date time.';
declare @IPWDT varchar(40) = ' is populated with date time.';
declare @TCDNED varchar(40) = 'does not exist, damn it!';
declare @somearenull varchar(25) = ' some columns are null.';
declare @tasbpwvda varchar(255) = ' should be populated with valid data. ';
declare @sbpwagcir varchar(255) = ' should be populated with a good contact identifier reference.'
declare @somecolumnsblank varchar(50) = ' some columns are blank.';
declare @somerhbd varchar(50) = ' some rows have bad dates.';
declare @sciraor varchar(175) = ' some contact identifier reference are oprhaned records';
declare @cchav varchar(50) = 'changeType column has appropriate values.';
declare @ccdnhav varchar(50) = 'changeType column does not have appropriate values.';
DECLARE @MyTestDescription varchar(500);
Declare @RC int;
DECLARE @TestStatusCode int;
declare @NullCount int;
declare @NumBlank int;
declare @TypeVarChar int = 167;
declare @TypeDateTime int = 61;
declare @TypeDate int = 167;
declare @TypeInt int = 56;
declare @TypeBit int = 104;
declare @TypeBigInt int = 127;
declare @TypeNVarChar int = 231;
declare @TypeChar int = 175;
declare @TypeMoney int = 60;
declare @TypeDecimal int = 106;
declare @TypeTinyInt int = 48;
declare @Passed varchar(10) = 'Passed.';
declare @Failed varchar(10) = 'Failed!';
declare @Warning varchar(10) = 'Warning!';
declare @PrimaryKey varchar(100) = 'PrimaryKey';
declare @SystemTypeId int;
declare @LengthDateTime int = 8;
declare @LengthInt int = 4;
declare @Zero int = 0;
declare @One int = 1;
declare @Two int = 2;
declare @Three int = 3;
declare @Four int = 4;
declare @Five int = 5;
declare @Seven int = 7;
declare @Six int = 6;
declare @Eight int = 8;
declare @Nine int = 9;
declare @Ten int = 10;
declare @Twelve int = 12;
declare @Fifteen int = 15;
declare @Eighteen int = 18;
declare @Nineteen int = 19;
declare @Twenty int = 20;
declare @TwentyThree int = 23;
declare @TwentyFour int = 24;
declare @TwentyFive int = 25;
declare @Thirty int = 30;
declare @ThirtyTwo int = 32;
declare @ThirtyFive int =35;
declare @Forty int = 40;
declare @FortyOne int = 41;
declare @Fifty int = 50;
declare @Sixty int = 60;
declare @Eighty int = 80;
declare @Hundred int = 100;
declare @OneHundred int = 100;
declare @OneHundredThirtyThree int = 133;
declare @OneHundredTwentyFive int = 125;
declare @TwoHundred int = 200;
declare @TwoHundredFifty int = 250;
declare @TwoHundredFiftyFive int = 255;
declare @ThreeHundredFifty int = 350;
declare @FiveHundred int = 500;
declare @OneThousand int = 1000;
declare @ThirteenHundred int = 1300;
--declare @TestDescription varchar(200);
declare @ExpectedResult varchar(100);
declare @ActualResult varchar(100);
declare @TestStatus varchar(15);
declare @TestDateTime datetime;
declare @TestMessage varchar(200);
--declare @TableNameOne varchar(30) = 'staging_plan';
declare @Schema int = 17;
declare @Insert varchar(30) = 'I-insert';
Declare @Update varchar(30) = 'U-update';
Declare @Delete varchar(30) = 'D-delete';
declare @TableObject Int;
declare @ColumnObject Int;
declare @ColumnIsNullable Int;
declare @ICN varchar(30) = ' is column nullable.';
declare @TCIN varchar(40) = ' the column is nullable. ';
declare @TCINN varchar(50) = ' the column is not nullable. ';
declare @TCIPK varchar(50) = ' the column is a Primary Key. ';
declare @MaxLength Int;
declare @MaxTestLogID Int;
declare @Return int;
declare @AllowsNull int;
declare @HasNull int;
declare @ColHasNull varchar(20) = ' column has null';
declare @ColHasNoNull varchar(20)= ' column has no null';
declare @HasBlank Bit;
declare @ColHasBlank varchar(25) = ' column has blank value '
declare @ColHasNoBlank varchar(25) = ' column has no blank value '
declare @HasBlankCount int;
--Declare @TypeDecimal int = 106;
declare @LengthTinyInt int = 1;
declare @LengthDecimal int = 17;
declare @Eigthteen int = 18;
Declare @IsIdentity bit;
declare @TempTableName varchar(25) = '#testResults';
Declare @TestScenario varchar(80) = 'Staging_Other_Business'
declare @BeginningStatus varchar(25) = 'Beginning';
Declare @EndingStatus varchar(25) = 'Ending';
Declare @TestDescription varchar(800) = 'Test the Staging_Other_Business table and columns confirming the properties of the columns and some data verification.';
declare @p1 varchar(25) = 'Air';
declare @p2 varchar(25) = 'AccountMaster';

select @MaxTestLogID = max(TestLogID) from dbo.TestLog;
select @MaxTestID = max(Testid) from dbo.TestResults;
--Get the Schema ID for the table retrieval


select @Schema  = schema_id
from
sys.schemas
where
name = @p1;

--Get the TableObjectId for the table using the Table Name and the SchemaId
select @TableObject = [object_id]
	from
	sys.tables
	where
	name = @TableName
	and
	schema_id = @Schema;
	
select
@PrimaryKey = name
from
sys.all_columns
where
is_identity = 1
and
object_id = @TableObject;


insert TestLog values (@TestScenario,@BeginningStatus,@TestDescription,SYSDATETIME(),System_User)

If @TableObject is not null
begin
Set @TestDescription =	@TestAreaBeginning + @TableName + '' + @Table + ' exists test.';
Set @ExpectedResult =    'Table Exists.';
Set @ActualResult = 'Table Exists.';
Set @TestStatus =  @Passed;
Set @TestDateTime = SYSDATETIME();
Set @TestMessage = @TC + trim(str(@@IDENTITY)) +' '+@Table+' '+@TableName + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode
end
	else
begin	
Set @TestDescription =	@TestAreaBeginning + @TableName+ '' + @Table + ' exists test.';
set @ExpectedResult = 	'Table Exists.';
Set @ActualResult =	'Table object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage = 	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Table+' '+@TableName + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode
end
--------------------------------------------------

set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @OtherBusinessId and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @OtherBusinessId + ' exists test.';
Set @ExpectedResult =  @OtherBusinessId + ' ' + @Column + ' ' +@E;
Set @ActualResult = @OtherBusinessId + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @OtherBusinessId + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @OtherBusinessId + ' exists test.';
Set @ExpectedResult =  @OtherBusinessId + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @OtherBusinessId + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@OtherBusinessId + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @CompanyName and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @CompanyName + ' exists test.';
Set @ExpectedResult =  @CompanyName + ' ' + @Column + ' ' +@E;
Set @ActualResult = @CompanyName + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @CompanyName + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @CompanyName + ' exists test.';
Set @ExpectedResult =  @CompanyName + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @CompanyName + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@CompanyName + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @ProcessedDatetime and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @ProcessedDatetime + ' exists test.';
Set @ExpectedResult =  @ProcessedDatetime + ' ' + @Column + ' ' +@E;
Set @ActualResult = @ProcessedDatetime + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @ProcessedDatetime + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @ProcessedDatetime + ' exists test.';
Set @ExpectedResult =  @ProcessedDatetime + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @ProcessedDatetime + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@ProcessedDatetime + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @InsertDateTime and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @InsertDateTime + ' exists test.';
Set @ExpectedResult =  @InsertDateTime + ' ' + @Column + ' ' +@E;
Set @ActualResult = @InsertDateTime + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @InsertDateTime + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @InsertDateTime + ' exists test.';
Set @ExpectedResult =  @InsertDateTime + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @InsertDateTime + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@InsertDateTime + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @CountryCode and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @CountryCode + ' exists test.';
Set @ExpectedResult =  @CountryCode + ' ' + @Column + ' ' +@E;
Set @ActualResult = @CountryCode + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @CountryCode + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @CountryCode + ' exists test.';
Set @ExpectedResult =  @CountryCode + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @CountryCode + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@CountryCode + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @ContactIdentifierReference and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @ContactIdentifierReference + ' exists test.';
Set @ExpectedResult =  @ContactIdentifierReference + ' ' + @Column + ' ' +@E;
Set @ActualResult = @ContactIdentifierReference + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @ContactIdentifierReference + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @ContactIdentifierReference + ' exists test.';
Set @ExpectedResult =  @ContactIdentifierReference + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @ContactIdentifierReference + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@ContactIdentifierReference + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @BusinessDescription and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @BusinessDescription + ' exists test.';
Set @ExpectedResult =  @BusinessDescription + ' ' + @Column + ' ' +@E;
Set @ActualResult = @BusinessDescription + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @BusinessDescription + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @BusinessDescription + ' exists test.';
Set @ExpectedResult =  @BusinessDescription + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @BusinessDescription + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@BusinessDescription + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @ChangeType and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @ChangeType + ' exists test.';
Set @ExpectedResult =  @ChangeType + ' ' + @Column + ' ' +@E;
Set @ActualResult = @ChangeType + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @ChangeType + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @ChangeType + ' exists test.';
Set @ExpectedResult =  @ChangeType + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @ChangeType + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@ChangeType + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @AddressLine1 and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @AddressLine1 + ' exists test.';
Set @ExpectedResult =  @AddressLine1 + ' ' + @Column + ' ' +@E;
Set @ActualResult = @AddressLine1 + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @AddressLine1 + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @AddressLine1 + ' exists test.';
Set @ExpectedResult =  @AddressLine1 + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @AddressLine1 + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@AddressLine1 + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @AddressLine2 and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @AddressLine2 + ' exists test.';
Set @ExpectedResult =  @AddressLine2 + ' ' + @Column + ' ' +@E;
Set @ActualResult = @AddressLine2 + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @AddressLine2 + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @AddressLine2 + ' exists test.';
Set @ExpectedResult =  @AddressLine2 + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @AddressLine2 + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@AddressLine2 + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @City and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @City + ' exists test.';
Set @ExpectedResult =  @City + ' ' + @Column + ' ' +@E;
Set @ActualResult = @City + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @City + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @City + ' exists test.';
Set @ExpectedResult =  @City + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @City + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@City + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @StateOrProvince and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @StateOrProvince + ' exists test.';
Set @ExpectedResult =  @StateOrProvince + ' ' + @Column + ' ' +@E;
Set @ActualResult = @StateOrProvince + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @StateOrProvince + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @StateOrProvince + ' exists test.';
Set @ExpectedResult =  @StateOrProvince + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @StateOrProvince + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@StateOrProvince + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @ZipCode and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @ZipCode + ' exists test.';
Set @ExpectedResult =  @ZipCode + ' ' + @Column + ' ' +@E;
Set @ActualResult = @ZipCode + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @ZipCode + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @ZipCode + ' exists test.';
Set @ExpectedResult =  @ZipCode + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @ZipCode + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@ZipCode + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @MeetsConditionsForApproval and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @MeetsConditionsForApproval + ' exists test.';
Set @ExpectedResult =  @MeetsConditionsForApproval + ' ' + @Column + ' ' +@E;
Set @ActualResult = @MeetsConditionsForApproval + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @MeetsConditionsForApproval + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @MeetsConditionsForApproval + ' exists test.';
Set @ExpectedResult =  @MeetsConditionsForApproval + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @MeetsConditionsForApproval + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@MeetsConditionsForApproval + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @OtherBusinessApprovalCode and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @OtherBusinessApprovalCode + ' exists test.';
Set @ExpectedResult =  @OtherBusinessApprovalCode + ' ' + @Column + ' ' +@E;
Set @ActualResult = @OtherBusinessApprovalCode + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @OtherBusinessApprovalCode + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @OtherBusinessApprovalCode + ' exists test.';
Set @ExpectedResult =  @OtherBusinessApprovalCode + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @OtherBusinessApprovalCode + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@OtherBusinessApprovalCode + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @ComplianceTrackingCode and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @ComplianceTrackingCode + ' exists test.';
Set @ExpectedResult =  @ComplianceTrackingCode + ' ' + @Column + ' ' +@E;
Set @ActualResult = @ComplianceTrackingCode + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @ComplianceTrackingCode + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @ComplianceTrackingCode + ' exists test.';
Set @ExpectedResult =  @ComplianceTrackingCode + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @ComplianceTrackingCode + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@ComplianceTrackingCode + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @U4CodeForOutsideBusiness and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @U4CodeForOutsideBusiness + ' exists test.';
Set @ExpectedResult =  @U4CodeForOutsideBusiness + ' ' + @Column + ' ' +@E;
Set @ActualResult = @U4CodeForOutsideBusiness + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @U4CodeForOutsideBusiness + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @U4CodeForOutsideBusiness + ' exists test.';
Set @ExpectedResult =  @U4CodeForOutsideBusiness + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @U4CodeForOutsideBusiness + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@U4CodeForOutsideBusiness + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @OtherBusinessApprovalDate and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @OtherBusinessApprovalDate + ' exists test.';
Set @ExpectedResult =  @OtherBusinessApprovalDate + ' ' + @Column + ' ' +@E;
Set @ActualResult = @OtherBusinessApprovalDate + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @OtherBusinessApprovalDate + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @OtherBusinessApprovalDate + ' exists test.';
Set @ExpectedResult =  @OtherBusinessApprovalDate + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @OtherBusinessApprovalDate + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@OtherBusinessApprovalDate + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @OtherBusinessEndDate and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @OtherBusinessEndDate + ' exists test.';
Set @ExpectedResult =  @OtherBusinessEndDate + ' ' + @Column + ' ' +@E;
Set @ActualResult = @OtherBusinessEndDate + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @OtherBusinessEndDate + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @OtherBusinessEndDate + ' exists test.';
Set @ExpectedResult =  @OtherBusinessEndDate + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @OtherBusinessEndDate + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@OtherBusinessEndDate + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @ADVForm and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @ADVForm + ' exists test.';
Set @ExpectedResult =  @ADVForm + ' ' + @Column + ' ' +@E;
Set @ActualResult = @ADVForm + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @ADVForm + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @ADVForm + ' exists test.';
Set @ExpectedResult =  @ADVForm + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @ADVForm + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@ADVForm + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @BranchOfficeRegistration and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @BranchOfficeRegistration + ' exists test.';
Set @ExpectedResult =  @BranchOfficeRegistration + ' ' + @Column + ' ' +@E;
Set @ActualResult = @BranchOfficeRegistration + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @BranchOfficeRegistration + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @BranchOfficeRegistration + ' exists test.';
Set @ExpectedResult =  @BranchOfficeRegistration + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @BranchOfficeRegistration + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@BranchOfficeRegistration + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @OtherBusinessId	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @OtherBusinessId; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @OtherBusinessId + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeInt =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @OtherBusinessId;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @OtherBusinessId + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @OtherBusinessId + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @OtherBusinessId + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @CompanyName	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @CompanyName; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @CompanyName + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @CompanyName;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @CompanyName + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @CompanyName + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @CompanyName + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @ProcessedDatetime	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @ProcessedDatetime; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @ProcessedDatetime + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeDateTime =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ProcessedDatetime;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ProcessedDatetime + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ProcessedDatetime + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ProcessedDatetime + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @InsertDateTime	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @InsertDateTime; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @InsertDateTime + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeDateTime =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @InsertDateTime;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @InsertDateTime + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @InsertDateTime + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @InsertDateTime + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @CountryCode	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @CountryCode; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @CountryCode + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeInt =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @CountryCode;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @CountryCode + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @CountryCode + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @CountryCode + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @ContactIdentifierReference	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @ContactIdentifierReference; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @ContactIdentifierReference + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ContactIdentifierReference;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ContactIdentifierReference + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ContactIdentifierReference + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ContactIdentifierReference + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @BusinessDescription	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @BusinessDescription; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @BusinessDescription + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @BusinessDescription;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @BusinessDescription + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @BusinessDescription + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @BusinessDescription + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @ChangeType	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @ChangeType; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @ChangeType + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ChangeType;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ChangeType + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ChangeType + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ChangeType + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @AddressLine1	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @AddressLine1; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @AddressLine1 + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @AddressLine1;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @AddressLine1 + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @AddressLine1 + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @AddressLine1 + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @AddressLine2	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @AddressLine2; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @AddressLine2 + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @AddressLine2;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @AddressLine2 + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @AddressLine2 + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @AddressLine2 + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @City	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @City; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @City + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @City;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @City + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @City + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @City + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @StateOrProvince	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @StateOrProvince; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @StateOrProvince + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @StateOrProvince;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @StateOrProvince + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @StateOrProvince + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @StateOrProvince + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @ZipCode	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @ZipCode; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @ZipCode + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ZipCode;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ZipCode + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ZipCode + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ZipCode + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @MeetsConditionsForApproval	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @MeetsConditionsForApproval; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @MeetsConditionsForApproval + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeTinyInt =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @MeetsConditionsForApproval;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @MeetsConditionsForApproval + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @MeetsConditionsForApproval + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @MeetsConditionsForApproval + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @OtherBusinessApprovalCode	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @OtherBusinessApprovalCode; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @OtherBusinessApprovalCode + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeInt =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @OtherBusinessApprovalCode;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @OtherBusinessApprovalCode + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @OtherBusinessApprovalCode + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @OtherBusinessApprovalCode + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @ComplianceTrackingCode	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @ComplianceTrackingCode; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @ComplianceTrackingCode + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeInt =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ComplianceTrackingCode;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ComplianceTrackingCode + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ComplianceTrackingCode + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ComplianceTrackingCode + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @U4CodeForOutsideBusiness	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @U4CodeForOutsideBusiness; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @U4CodeForOutsideBusiness + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeInt =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @U4CodeForOutsideBusiness;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @U4CodeForOutsideBusiness + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @U4CodeForOutsideBusiness + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @U4CodeForOutsideBusiness + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @OtherBusinessApprovalDate	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @OtherBusinessApprovalDate; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @OtherBusinessApprovalDate + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeDateTime =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @OtherBusinessApprovalDate;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @OtherBusinessApprovalDate + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @OtherBusinessApprovalDate + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @OtherBusinessApprovalDate + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @OtherBusinessEndDate	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @OtherBusinessEndDate; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @OtherBusinessEndDate + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeDateTime =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @OtherBusinessEndDate;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @OtherBusinessEndDate + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @OtherBusinessEndDate + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @OtherBusinessEndDate + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @ADVForm	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @ADVForm; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @ADVForm + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeTinyInt =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ADVForm;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ADVForm + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ADVForm + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ADVForm + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @BranchOfficeRegistration	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @BranchOfficeRegistration; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @BranchOfficeRegistration + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeTinyInt =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @BranchOfficeRegistration;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @BranchOfficeRegistration + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @BranchOfficeRegistration + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @BranchOfficeRegistration + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@OtherBusinessId,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @OtherBusinessId + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @OtherBusinessId + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@OtherBusinessId +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessId + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@OtherBusinessId +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @OtherBusinessId + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@CompanyName,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @CompanyName + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @CompanyName + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @TwoHundredFiftyFive
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@CompanyName +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @CompanyName + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@CompanyName +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @CompanyName + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@ProcessedDatetime,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @ProcessedDatetime + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@InsertDateTime,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @InsertDateTime + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@CountryCode,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @CountryCode + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @CountryCode + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@CountryCode +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @CountryCode + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@CountryCode +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @CountryCode + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@ContactIdentifierReference,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @ContactIdentifierReference + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@BusinessDescription,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @BusinessDescription + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @BusinessDescription + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @TwoHundredFiftyFive
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@BusinessDescription +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @BusinessDescription + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@BusinessDescription +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @BusinessDescription + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@ChangeType,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @ChangeType + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @ChangeType + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @One
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @ChangeType + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@AddressLine1,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @AddressLine1 + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @AddressLine1 + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @TwoHundred
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@AddressLine1 +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @AddressLine1 + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@AddressLine1 +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @AddressLine1 + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@AddressLine2,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @AddressLine2 + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @AddressLine2 + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @TwoHundred
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@AddressLine2 +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @AddressLine2 + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@AddressLine2 +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @AddressLine2 + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@City,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @City + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @City + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Thirty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@City +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @City + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@City +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @City + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@StateOrProvince,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @StateOrProvince + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @StateOrProvince + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Forty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@StateOrProvince +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @StateOrProvince + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@StateOrProvince +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @StateOrProvince + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@ZipCode,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @ZipCode + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @ZipCode + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Twenty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ZipCode +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ZipCode + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ZipCode +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @ZipCode + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@MeetsConditionsForApproval,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @MeetsConditionsForApproval + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @MeetsConditionsForApproval + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Three
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@MeetsConditionsForApproval +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @MeetsConditionsForApproval + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@MeetsConditionsForApproval +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @MeetsConditionsForApproval + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@OtherBusinessApprovalCode,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @OtherBusinessApprovalCode + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @OtherBusinessApprovalCode + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@OtherBusinessApprovalCode +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessApprovalCode + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@OtherBusinessApprovalCode +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @OtherBusinessApprovalCode + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@ComplianceTrackingCode,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @ComplianceTrackingCode + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @ComplianceTrackingCode + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ComplianceTrackingCode +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ComplianceTrackingCode + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ComplianceTrackingCode +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @ComplianceTrackingCode + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@U4CodeForOutsideBusiness,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @U4CodeForOutsideBusiness + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @U4CodeForOutsideBusiness + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@U4CodeForOutsideBusiness +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @U4CodeForOutsideBusiness + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@U4CodeForOutsideBusiness +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @U4CodeForOutsideBusiness + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@OtherBusinessApprovalDate,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @OtherBusinessApprovalDate + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @OtherBusinessApprovalDate + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@OtherBusinessApprovalDate +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessApprovalDate + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@OtherBusinessApprovalDate +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @OtherBusinessApprovalDate + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@OtherBusinessEndDate,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @OtherBusinessEndDate + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @OtherBusinessEndDate + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@OtherBusinessEndDate +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessEndDate + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@OtherBusinessEndDate +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @OtherBusinessEndDate + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@ADVForm,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @ADVForm + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @ADVForm + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Three
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ADVForm +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ADVForm + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ADVForm +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @ADVForm + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@BranchOfficeRegistration,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @BranchOfficeRegistration + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @BranchOfficeRegistration + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Three
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@BranchOfficeRegistration +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @BranchOfficeRegistration + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@BranchOfficeRegistration +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @BranchOfficeRegistration + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

Select @AllowsNull = COLUMNPROPERTY(@TableObject,@OtherBusinessId,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business WHERE @OtherBusinessId IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@OtherBusinessId +  ' Has nullable test.';
Set @ExpectedResult =  @OtherBusinessId+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @OtherBusinessId+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessId + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@OtherBusinessId +  ' Has nullable test.'; 
Set @ExpectedResult =  @OtherBusinessId+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @OtherBusinessId+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessId + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@OtherBusinessId +  ' Has nullable test.';
Set @ExpectedResult =     @OtherBusinessId+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @OtherBusinessId+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessId + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@OtherBusinessId +  ' Has nullable test.';
Set @ExpectedResult =     @OtherBusinessId+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @OtherBusinessId+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessId + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@CompanyName,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business WHERE @CompanyName IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@CompanyName +  ' Has nullable test.';
Set @ExpectedResult =  @CompanyName+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @CompanyName+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @CompanyName + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@CompanyName +  ' Has nullable test.'; 
Set @ExpectedResult =  @CompanyName+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @CompanyName+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @CompanyName + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@CompanyName +  ' Has nullable test.';
Set @ExpectedResult =     @CompanyName+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @CompanyName+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @CompanyName + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@CompanyName +  ' Has nullable test.';
Set @ExpectedResult =     @CompanyName+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @CompanyName+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @CompanyName + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@ProcessedDatetime,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business WHERE @ProcessedDatetime IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has nullable test.';
Set @ExpectedResult =  @ProcessedDatetime+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @ProcessedDatetime+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has nullable test.'; 
Set @ExpectedResult =  @ProcessedDatetime+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @ProcessedDatetime+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has nullable test.';
Set @ExpectedResult =     @ProcessedDatetime+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @ProcessedDatetime+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has nullable test.';
Set @ExpectedResult =     @ProcessedDatetime+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @ProcessedDatetime+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@InsertDateTime,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business WHERE @InsertDateTime IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has nullable test.';
Set @ExpectedResult =  @InsertDateTime+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @InsertDateTime+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has nullable test.'; 
Set @ExpectedResult =  @InsertDateTime+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @InsertDateTime+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has nullable test.';
Set @ExpectedResult =     @InsertDateTime+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @InsertDateTime+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has nullable test.';
Set @ExpectedResult =     @InsertDateTime+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @InsertDateTime+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@CountryCode,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business WHERE @CountryCode IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@CountryCode +  ' Has nullable test.';
Set @ExpectedResult =  @CountryCode+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @CountryCode+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @CountryCode + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@CountryCode +  ' Has nullable test.'; 
Set @ExpectedResult =  @CountryCode+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @CountryCode+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @CountryCode + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@CountryCode +  ' Has nullable test.';
Set @ExpectedResult =     @CountryCode+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @CountryCode+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @CountryCode + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@CountryCode +  ' Has nullable test.';
Set @ExpectedResult =     @CountryCode+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @CountryCode+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @CountryCode + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@ContactIdentifierReference,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business WHERE @ContactIdentifierReference IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' Has nullable test.';
Set @ExpectedResult =  @ContactIdentifierReference+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @ContactIdentifierReference+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' Has nullable test.'; 
Set @ExpectedResult =  @ContactIdentifierReference+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @ContactIdentifierReference+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' Has nullable test.';
Set @ExpectedResult =     @ContactIdentifierReference+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @ContactIdentifierReference+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' Has nullable test.';
Set @ExpectedResult =     @ContactIdentifierReference+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @ContactIdentifierReference+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@BusinessDescription,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business WHERE @BusinessDescription IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@BusinessDescription +  ' Has nullable test.';
Set @ExpectedResult =  @BusinessDescription+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @BusinessDescription+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @BusinessDescription + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@BusinessDescription +  ' Has nullable test.'; 
Set @ExpectedResult =  @BusinessDescription+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @BusinessDescription+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @BusinessDescription + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@BusinessDescription +  ' Has nullable test.';
Set @ExpectedResult =     @BusinessDescription+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @BusinessDescription+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @BusinessDescription + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@BusinessDescription +  ' Has nullable test.';
Set @ExpectedResult =     @BusinessDescription+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @BusinessDescription+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @BusinessDescription + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@ChangeType,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business WHERE @ChangeType IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has nullable test.';
Set @ExpectedResult =  @ChangeType+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @ChangeType+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has nullable test.'; 
Set @ExpectedResult =  @ChangeType+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @ChangeType+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has nullable test.';
Set @ExpectedResult =     @ChangeType+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @ChangeType+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has nullable test.';
Set @ExpectedResult =     @ChangeType+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @ChangeType+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@AddressLine1,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business WHERE @AddressLine1 IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@AddressLine1 +  ' Has nullable test.';
Set @ExpectedResult =  @AddressLine1+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @AddressLine1+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AddressLine1 + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@AddressLine1 +  ' Has nullable test.'; 
Set @ExpectedResult =  @AddressLine1+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @AddressLine1+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @AddressLine1 + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@AddressLine1 +  ' Has nullable test.';
Set @ExpectedResult =     @AddressLine1+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @AddressLine1+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @AddressLine1 + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@AddressLine1 +  ' Has nullable test.';
Set @ExpectedResult =     @AddressLine1+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @AddressLine1+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @AddressLine1 + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@AddressLine2,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business WHERE @AddressLine2 IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@AddressLine2 +  ' Has nullable test.';
Set @ExpectedResult =  @AddressLine2+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @AddressLine2+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AddressLine2 + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@AddressLine2 +  ' Has nullable test.'; 
Set @ExpectedResult =  @AddressLine2+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @AddressLine2+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @AddressLine2 + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@AddressLine2 +  ' Has nullable test.';
Set @ExpectedResult =     @AddressLine2+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @AddressLine2+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @AddressLine2 + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@AddressLine2 +  ' Has nullable test.';
Set @ExpectedResult =     @AddressLine2+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @AddressLine2+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @AddressLine2 + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@City,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business WHERE @City IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@City +  ' Has nullable test.';
Set @ExpectedResult =  @City+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @City+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @City + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@City +  ' Has nullable test.'; 
Set @ExpectedResult =  @City+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @City+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @City + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@City +  ' Has nullable test.';
Set @ExpectedResult =     @City+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @City+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @City + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@City +  ' Has nullable test.';
Set @ExpectedResult =     @City+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @City+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @City + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@StateOrProvince,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business WHERE @StateOrProvince IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@StateOrProvince +  ' Has nullable test.';
Set @ExpectedResult =  @StateOrProvince+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @StateOrProvince+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @StateOrProvince + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@StateOrProvince +  ' Has nullable test.'; 
Set @ExpectedResult =  @StateOrProvince+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @StateOrProvince+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @StateOrProvince + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@StateOrProvince +  ' Has nullable test.';
Set @ExpectedResult =     @StateOrProvince+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @StateOrProvince+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @StateOrProvince + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@StateOrProvince +  ' Has nullable test.';
Set @ExpectedResult =     @StateOrProvince+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @StateOrProvince+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @StateOrProvince + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@ZipCode,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business WHERE @ZipCode IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ZipCode +  ' Has nullable test.';
Set @ExpectedResult =  @ZipCode+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @ZipCode+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ZipCode + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ZipCode +  ' Has nullable test.'; 
Set @ExpectedResult =  @ZipCode+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @ZipCode+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ZipCode + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@ZipCode +  ' Has nullable test.';
Set @ExpectedResult =     @ZipCode+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @ZipCode+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ZipCode + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@ZipCode +  ' Has nullable test.';
Set @ExpectedResult =     @ZipCode+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @ZipCode+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ZipCode + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@MeetsConditionsForApproval,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business WHERE @MeetsConditionsForApproval IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@MeetsConditionsForApproval +  ' Has nullable test.';
Set @ExpectedResult =  @MeetsConditionsForApproval+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @MeetsConditionsForApproval+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @MeetsConditionsForApproval + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@MeetsConditionsForApproval +  ' Has nullable test.'; 
Set @ExpectedResult =  @MeetsConditionsForApproval+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @MeetsConditionsForApproval+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @MeetsConditionsForApproval + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@MeetsConditionsForApproval +  ' Has nullable test.';
Set @ExpectedResult =     @MeetsConditionsForApproval+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @MeetsConditionsForApproval+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @MeetsConditionsForApproval + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@MeetsConditionsForApproval +  ' Has nullable test.';
Set @ExpectedResult =     @MeetsConditionsForApproval+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @MeetsConditionsForApproval+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @MeetsConditionsForApproval + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@OtherBusinessApprovalCode,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business WHERE @OtherBusinessApprovalCode IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@OtherBusinessApprovalCode +  ' Has nullable test.';
Set @ExpectedResult =  @OtherBusinessApprovalCode+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @OtherBusinessApprovalCode+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessApprovalCode + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@OtherBusinessApprovalCode +  ' Has nullable test.'; 
Set @ExpectedResult =  @OtherBusinessApprovalCode+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @OtherBusinessApprovalCode+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessApprovalCode + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@OtherBusinessApprovalCode +  ' Has nullable test.';
Set @ExpectedResult =     @OtherBusinessApprovalCode+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @OtherBusinessApprovalCode+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessApprovalCode + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@OtherBusinessApprovalCode +  ' Has nullable test.';
Set @ExpectedResult =     @OtherBusinessApprovalCode+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @OtherBusinessApprovalCode+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessApprovalCode + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@ComplianceTrackingCode,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business WHERE @ComplianceTrackingCode IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ComplianceTrackingCode +  ' Has nullable test.';
Set @ExpectedResult =  @ComplianceTrackingCode+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @ComplianceTrackingCode+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ComplianceTrackingCode + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ComplianceTrackingCode +  ' Has nullable test.'; 
Set @ExpectedResult =  @ComplianceTrackingCode+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @ComplianceTrackingCode+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ComplianceTrackingCode + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@ComplianceTrackingCode +  ' Has nullable test.';
Set @ExpectedResult =     @ComplianceTrackingCode+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @ComplianceTrackingCode+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ComplianceTrackingCode + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@ComplianceTrackingCode +  ' Has nullable test.';
Set @ExpectedResult =     @ComplianceTrackingCode+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @ComplianceTrackingCode+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ComplianceTrackingCode + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@U4CodeForOutsideBusiness,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business WHERE @U4CodeForOutsideBusiness IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@U4CodeForOutsideBusiness +  ' Has nullable test.';
Set @ExpectedResult =  @U4CodeForOutsideBusiness+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @U4CodeForOutsideBusiness+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @U4CodeForOutsideBusiness + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@U4CodeForOutsideBusiness +  ' Has nullable test.'; 
Set @ExpectedResult =  @U4CodeForOutsideBusiness+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @U4CodeForOutsideBusiness+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @U4CodeForOutsideBusiness + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@U4CodeForOutsideBusiness +  ' Has nullable test.';
Set @ExpectedResult =     @U4CodeForOutsideBusiness+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @U4CodeForOutsideBusiness+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @U4CodeForOutsideBusiness + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@U4CodeForOutsideBusiness +  ' Has nullable test.';
Set @ExpectedResult =     @U4CodeForOutsideBusiness+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @U4CodeForOutsideBusiness+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @U4CodeForOutsideBusiness + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@OtherBusinessApprovalDate,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business WHERE @OtherBusinessApprovalDate IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@OtherBusinessApprovalDate +  ' Has nullable test.';
Set @ExpectedResult =  @OtherBusinessApprovalDate+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @OtherBusinessApprovalDate+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessApprovalDate + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@OtherBusinessApprovalDate +  ' Has nullable test.'; 
Set @ExpectedResult =  @OtherBusinessApprovalDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @OtherBusinessApprovalDate+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessApprovalDate + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@OtherBusinessApprovalDate +  ' Has nullable test.';
Set @ExpectedResult =     @OtherBusinessApprovalDate+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @OtherBusinessApprovalDate+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessApprovalDate + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@OtherBusinessApprovalDate +  ' Has nullable test.';
Set @ExpectedResult =     @OtherBusinessApprovalDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @OtherBusinessApprovalDate+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessApprovalDate + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@OtherBusinessEndDate,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business WHERE @OtherBusinessEndDate IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@OtherBusinessEndDate +  ' Has nullable test.';
Set @ExpectedResult =  @OtherBusinessEndDate+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @OtherBusinessEndDate+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessEndDate + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@OtherBusinessEndDate +  ' Has nullable test.'; 
Set @ExpectedResult =  @OtherBusinessEndDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @OtherBusinessEndDate+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessEndDate + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@OtherBusinessEndDate +  ' Has nullable test.';
Set @ExpectedResult =     @OtherBusinessEndDate+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @OtherBusinessEndDate+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessEndDate + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@OtherBusinessEndDate +  ' Has nullable test.';
Set @ExpectedResult =     @OtherBusinessEndDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @OtherBusinessEndDate+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessEndDate + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@ADVForm,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business WHERE @ADVForm IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ADVForm +  ' Has nullable test.';
Set @ExpectedResult =  @ADVForm+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @ADVForm+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ADVForm + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ADVForm +  ' Has nullable test.'; 
Set @ExpectedResult =  @ADVForm+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @ADVForm+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ADVForm + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@ADVForm +  ' Has nullable test.';
Set @ExpectedResult =     @ADVForm+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @ADVForm+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ADVForm + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@ADVForm +  ' Has nullable test.';
Set @ExpectedResult =     @ADVForm+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @ADVForm+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ADVForm + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@BranchOfficeRegistration,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business WHERE @BranchOfficeRegistration IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@BranchOfficeRegistration +  ' Has nullable test.';
Set @ExpectedResult =  @BranchOfficeRegistration+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @BranchOfficeRegistration+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @BranchOfficeRegistration + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@BranchOfficeRegistration +  ' Has nullable test.'; 
Set @ExpectedResult =  @BranchOfficeRegistration+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @BranchOfficeRegistration+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @BranchOfficeRegistration + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@BranchOfficeRegistration +  ' Has nullable test.';
Set @ExpectedResult =     @BranchOfficeRegistration+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @BranchOfficeRegistration+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @BranchOfficeRegistration + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@BranchOfficeRegistration +  ' Has nullable test.';
Set @ExpectedResult =     @BranchOfficeRegistration+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @BranchOfficeRegistration+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @BranchOfficeRegistration + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business where @OtherBusinessId = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@OtherBusinessId +  ' Has blank value test.'; 
Set @ExpectedResult = @OtherBusinessId+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @OtherBusinessId+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessId + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@OtherBusinessId +  ' Has blank value test.'; 
Set @ExpectedResult = @OtherBusinessId+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @OtherBusinessId+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessId + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business where @CompanyName = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@CompanyName +  ' Has blank value test.'; 
Set @ExpectedResult = @CompanyName+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @CompanyName+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @CompanyName + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@CompanyName +  ' Has blank value test.'; 
Set @ExpectedResult = @CompanyName+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @CompanyName+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @CompanyName + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business where @ProcessedDatetime = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has blank value test.'; 
Set @ExpectedResult = @ProcessedDatetime+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @ProcessedDatetime+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has blank value test.'; 
Set @ExpectedResult = @ProcessedDatetime+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @ProcessedDatetime+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business where @InsertDateTime = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has blank value test.'; 
Set @ExpectedResult = @InsertDateTime+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @InsertDateTime+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has blank value test.'; 
Set @ExpectedResult = @InsertDateTime+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @InsertDateTime+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business where @CountryCode = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@CountryCode +  ' Has blank value test.'; 
Set @ExpectedResult = @CountryCode+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @CountryCode+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @CountryCode + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@CountryCode +  ' Has blank value test.'; 
Set @ExpectedResult = @CountryCode+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @CountryCode+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @CountryCode + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business where @ContactIdentifierReference = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' Has blank value test.'; 
Set @ExpectedResult = @ContactIdentifierReference+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @ContactIdentifierReference+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' Has blank value test.'; 
Set @ExpectedResult = @ContactIdentifierReference+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @ContactIdentifierReference+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business where @BusinessDescription = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@BusinessDescription +  ' Has blank value test.'; 
Set @ExpectedResult = @BusinessDescription+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @BusinessDescription+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @BusinessDescription + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@BusinessDescription +  ' Has blank value test.'; 
Set @ExpectedResult = @BusinessDescription+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @BusinessDescription+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @BusinessDescription + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business where @ChangeType = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has blank value test.'; 
Set @ExpectedResult = @ChangeType+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @ChangeType+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has blank value test.'; 
Set @ExpectedResult = @ChangeType+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @ChangeType+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business where @AddressLine1 = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@AddressLine1 +  ' Has blank value test.'; 
Set @ExpectedResult = @AddressLine1+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @AddressLine1+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AddressLine1 + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@AddressLine1 +  ' Has blank value test.'; 
Set @ExpectedResult = @AddressLine1+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @AddressLine1+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AddressLine1 + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business where @AddressLine2 = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@AddressLine2 +  ' Has blank value test.'; 
Set @ExpectedResult = @AddressLine2+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @AddressLine2+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AddressLine2 + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@AddressLine2 +  ' Has blank value test.'; 
Set @ExpectedResult = @AddressLine2+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @AddressLine2+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @AddressLine2 + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business where @City = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@City +  ' Has blank value test.'; 
Set @ExpectedResult = @City+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @City+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @City + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@City +  ' Has blank value test.'; 
Set @ExpectedResult = @City+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @City+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @City + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business where @StateOrProvince = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@StateOrProvince +  ' Has blank value test.'; 
Set @ExpectedResult = @StateOrProvince+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @StateOrProvince+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @StateOrProvince + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@StateOrProvince +  ' Has blank value test.'; 
Set @ExpectedResult = @StateOrProvince+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @StateOrProvince+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @StateOrProvince + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business where @ZipCode = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@ZipCode +  ' Has blank value test.'; 
Set @ExpectedResult = @ZipCode+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @ZipCode+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ZipCode + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ZipCode +  ' Has blank value test.'; 
Set @ExpectedResult = @ZipCode+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @ZipCode+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ZipCode + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business where @MeetsConditionsForApproval = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@MeetsConditionsForApproval +  ' Has blank value test.'; 
Set @ExpectedResult = @MeetsConditionsForApproval+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @MeetsConditionsForApproval+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @MeetsConditionsForApproval + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@MeetsConditionsForApproval +  ' Has blank value test.'; 
Set @ExpectedResult = @MeetsConditionsForApproval+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @MeetsConditionsForApproval+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @MeetsConditionsForApproval + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business where @OtherBusinessApprovalCode = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@OtherBusinessApprovalCode +  ' Has blank value test.'; 
Set @ExpectedResult = @OtherBusinessApprovalCode+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @OtherBusinessApprovalCode+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessApprovalCode + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@OtherBusinessApprovalCode +  ' Has blank value test.'; 
Set @ExpectedResult = @OtherBusinessApprovalCode+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @OtherBusinessApprovalCode+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessApprovalCode + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business where @ComplianceTrackingCode = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@ComplianceTrackingCode +  ' Has blank value test.'; 
Set @ExpectedResult = @ComplianceTrackingCode+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @ComplianceTrackingCode+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ComplianceTrackingCode + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ComplianceTrackingCode +  ' Has blank value test.'; 
Set @ExpectedResult = @ComplianceTrackingCode+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @ComplianceTrackingCode+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ComplianceTrackingCode + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business where @U4CodeForOutsideBusiness = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@U4CodeForOutsideBusiness +  ' Has blank value test.'; 
Set @ExpectedResult = @U4CodeForOutsideBusiness+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @U4CodeForOutsideBusiness+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @U4CodeForOutsideBusiness + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@U4CodeForOutsideBusiness +  ' Has blank value test.'; 
Set @ExpectedResult = @U4CodeForOutsideBusiness+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @U4CodeForOutsideBusiness+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @U4CodeForOutsideBusiness + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business where @OtherBusinessApprovalDate = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@OtherBusinessApprovalDate +  ' Has blank value test.'; 
Set @ExpectedResult = @OtherBusinessApprovalDate+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @OtherBusinessApprovalDate+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessApprovalDate + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@OtherBusinessApprovalDate +  ' Has blank value test.'; 
Set @ExpectedResult = @OtherBusinessApprovalDate+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @OtherBusinessApprovalDate+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessApprovalDate + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business where @OtherBusinessEndDate = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@OtherBusinessEndDate +  ' Has blank value test.'; 
Set @ExpectedResult = @OtherBusinessEndDate+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @OtherBusinessEndDate+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessEndDate + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@OtherBusinessEndDate +  ' Has blank value test.'; 
Set @ExpectedResult = @OtherBusinessEndDate+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @OtherBusinessEndDate+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @OtherBusinessEndDate + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business where @ADVForm = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@ADVForm +  ' Has blank value test.'; 
Set @ExpectedResult = @ADVForm+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @ADVForm+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ADVForm + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ADVForm +  ' Has blank value test.'; 
Set @ExpectedResult = @ADVForm+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @ADVForm+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ADVForm + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Other_Business where @BranchOfficeRegistration = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@BranchOfficeRegistration +  ' Has blank value test.'; 
Set @ExpectedResult = @BranchOfficeRegistration+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @BranchOfficeRegistration+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @BranchOfficeRegistration + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@BranchOfficeRegistration +  ' Has blank value test.'; 
Set @ExpectedResult = @BranchOfficeRegistration+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @BranchOfficeRegistration+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @BranchOfficeRegistration + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Other_Business where OtherBusinessId = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OtherBusinessId' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'OtherBusinessId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OtherBusinessId' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OtherBusinessId' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'OtherBusinessId' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'OtherBusinessId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OtherBusinessId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OtherBusinessId' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


--select @NumBlank = count(@PrimaryKey) from air.Staging_Other_Business where CompanyName = '';

--If @NumBlank = 0
--begin
--Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'CompanyName' + @Column+ @somecolumnsblank + 'test.';
--Set @ExpectedResult = 'CompanyName' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
--Set @ActualResult = 'CompanyName' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
--Set @TestStatus =@Passed;
--Set @TestDateTime = Sysdatetime();
--Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CompanyName' + @Column + @somecolumnsblank + @tasbpwvda;
--Set @TestStatusCode = 3;

--EXECUTE @RC = [dbo].[insertTestResult] 
--   @TestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
-- end 
--else
--begin
--Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'CompanyName' + @Column+ @somecolumnsblank + 'test.';
--Set @ExpectedResult = 'CompanyName' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
--Set @ActualResult = 'CompanyName' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
--Set @TestStatus = @Warning;
--Set @TestDateTime = Sysdatetime();
--Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CompanyName' + @Column + @somecolumnsblank + @tasbpwvda;
--Set @TestStatusCode = 2;

--EXECUTE @RC = [dbo].[insertTestResult] 
--   @TestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end
--set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Other_Business where ProcessedDatetime = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ProcessedDatetime' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ProcessedDatetime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ProcessedDatetime' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ProcessedDatetime' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'ProcessedDatetime' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ProcessedDatetime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ProcessedDatetime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ProcessedDatetime' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Other_Business where InsertDateTime = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'InsertDateTime' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'InsertDateTime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'InsertDateTime' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsertDateTime' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'InsertDateTime' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'InsertDateTime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'InsertDateTime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsertDateTime' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Other_Business where CountryCode = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'CountryCode' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'CountryCode' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'CountryCode' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CountryCode' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'CountryCode' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'CountryCode' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'CountryCode' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CountryCode' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Other_Business where ContactIdentifierReference = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ContactIdentifierReference' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ContactIdentifierReference' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ContactIdentifierReference' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ContactIdentifierReference' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'ContactIdentifierReference' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ContactIdentifierReference' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ContactIdentifierReference' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ContactIdentifierReference' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Other_Business where BusinessDescription = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'BusinessDescription' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'BusinessDescription' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'BusinessDescription' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'BusinessDescription' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'BusinessDescription' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'BusinessDescription' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'BusinessDescription' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'BusinessDescription' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Other_Business where ChangeType = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ChangeType' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ChangeType' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ChangeType' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ChangeType' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'ChangeType' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ChangeType' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ChangeType' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ChangeType' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Other_Business where AddressLine1 = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'AddressLine1' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'AddressLine1' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'AddressLine1' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AddressLine1' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'AddressLine1' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'AddressLine1' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'AddressLine1' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AddressLine1' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Other_Business where AddressLine2 = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'AddressLine2' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'AddressLine2' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'AddressLine2' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AddressLine2' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'AddressLine2' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'AddressLine2' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'AddressLine2' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AddressLine2' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Other_Business where City = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'City' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'City' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'City' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'City' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'City' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'City' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'City' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'City' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Other_Business where StateOrProvince = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'StateOrProvince' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'StateOrProvince' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'StateOrProvince' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'StateOrProvince' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'StateOrProvince' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'StateOrProvince' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'StateOrProvince' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'StateOrProvince' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Other_Business where ZipCode = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ZipCode' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ZipCode' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ZipCode' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ZipCode' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'ZipCode' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ZipCode' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ZipCode' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ZipCode' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Other_Business where MeetsConditionsForApproval = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'MeetsConditionsForApproval' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'MeetsConditionsForApproval' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'MeetsConditionsForApproval' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'MeetsConditionsForApproval' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'MeetsConditionsForApproval' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'MeetsConditionsForApproval' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'MeetsConditionsForApproval' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'MeetsConditionsForApproval' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Other_Business where OtherBusinessApprovalCode = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OtherBusinessApprovalCode' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'OtherBusinessApprovalCode' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OtherBusinessApprovalCode' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OtherBusinessApprovalCode' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'OtherBusinessApprovalCode' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'OtherBusinessApprovalCode' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OtherBusinessApprovalCode' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OtherBusinessApprovalCode' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Other_Business where ComplianceTrackingCode = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ComplianceTrackingCode' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ComplianceTrackingCode' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ComplianceTrackingCode' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ComplianceTrackingCode' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'ComplianceTrackingCode' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ComplianceTrackingCode' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ComplianceTrackingCode' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ComplianceTrackingCode' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Other_Business where U4CodeForOutsideBusiness = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'U4CodeForOutsideBusiness' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'U4CodeForOutsideBusiness' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'U4CodeForOutsideBusiness' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'U4CodeForOutsideBusiness' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'U4CodeForOutsideBusiness' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'U4CodeForOutsideBusiness' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'U4CodeForOutsideBusiness' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'U4CodeForOutsideBusiness' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Other_Business where OtherBusinessApprovalDate = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OtherBusinessApprovalDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'OtherBusinessApprovalDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OtherBusinessApprovalDate' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OtherBusinessApprovalDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'OtherBusinessApprovalDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'OtherBusinessApprovalDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OtherBusinessApprovalDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OtherBusinessApprovalDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Other_Business where OtherBusinessEndDate = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OtherBusinessEndDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'OtherBusinessEndDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OtherBusinessEndDate' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OtherBusinessEndDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'OtherBusinessEndDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'OtherBusinessEndDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OtherBusinessEndDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OtherBusinessEndDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Other_Business where ADVForm = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ADVForm' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ADVForm' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ADVForm' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ADVForm' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'ADVForm' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ADVForm' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ADVForm' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ADVForm' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Other_Business where BranchOfficeRegistration = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'BranchOfficeRegistration' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'BranchOfficeRegistration' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'BranchOfficeRegistration' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'BranchOfficeRegistration' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'BranchOfficeRegistration' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'BranchOfficeRegistration' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'BranchOfficeRegistration' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'BranchOfficeRegistration' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Other_Business where OtherBusinessId is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OtherBusinessId' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'OtherBusinessId' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OtherBusinessId' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OtherBusinessId' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OtherBusinessId' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'OtherBusinessId' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OtherBusinessId' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OtherBusinessId' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


--select
--@NullCount = count(@PrimaryKey) from air.Staging_Other_Business where CompanyName is null

--If @NullCount = 0
--begin
--Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'CompanyName' + @Column+ @somearenull + 'test.';
--Set @ExpectedResult = 'CompanyName' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
--Set @ActualResult = 'CompanyName' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
--Set @TestStatus = @Passed;
--Set @TestDateTime = Sysdatetime();
--Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CompanyName' + @Column + @somearenull + @tasbpwvda;
--Set @TestStatusCode = 3;

--EXECUTE @RC = [dbo].[insertTestResult] 
--   @TestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end
--else
--begin
--Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'CompanyName' + @Column+ @somearenull + 'test.';
--Set @ExpectedResult = 'CompanyName' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
--Set @ActualResult = 'CompanyName' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
--Set @TestStatus = @Warning;
--Set @TestDateTime = Sysdatetime();
--Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CompanyName' + @Column + @somearenull + @tasbpwvda;
--Set @TestStatusCode = 2;

--EXECUTE @RC = [dbo].[insertTestResult] 
--   @TestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end
--set @NullCount = NULL

-------------------------------------------------------------------
--------------------------------------------------------------------


--select
--@NullCount = count(@PrimaryKey) from air.Staging_Other_Business where ProcessedDatetime is null

--If @NullCount = 0
--begin
--Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ProcessedDatetime' + @Column+ @somearenull + 'test.';
--Set @ExpectedResult = 'ProcessedDatetime' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
--Set @ActualResult = 'ProcessedDatetime' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
--Set @TestStatus = @Passed;
--Set @TestDateTime = Sysdatetime();
--Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ProcessedDatetime' + @Column + @somearenull + @tasbpwvda;
--Set @TestStatusCode = 3;

--EXECUTE @RC = [dbo].[insertTestResult] 
--   @TestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end
--else
--begin
--Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ProcessedDatetime' + @Column+ @somearenull + 'test.';
--Set @ExpectedResult = 'ProcessedDatetime' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
--Set @ActualResult = 'ProcessedDatetime' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
--Set @TestStatus = @Warning;
--Set @TestDateTime = Sysdatetime();
--Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ProcessedDatetime' + @Column + @somearenull + @tasbpwvda;
--Set @TestStatusCode = 2;

--EXECUTE @RC = [dbo].[insertTestResult] 
--   @TestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end
--set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Other_Business where InsertDateTime is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'InsertDateTime' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'InsertDateTime' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'InsertDateTime' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsertDateTime' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'InsertDateTime' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'InsertDateTime' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'InsertDateTime' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsertDateTime' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Other_Business where CountryCode is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'CountryCode' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'CountryCode' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'CountryCode' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CountryCode' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'CountryCode' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'CountryCode' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'CountryCode' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CountryCode' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Other_Business where ContactIdentifierReference is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ContactIdentifierReference' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ContactIdentifierReference' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ContactIdentifierReference' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ContactIdentifierReference' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ContactIdentifierReference' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ContactIdentifierReference' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ContactIdentifierReference' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ContactIdentifierReference' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Other_Business where BusinessDescription is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'BusinessDescription' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'BusinessDescription' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'BusinessDescription' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'BusinessDescription' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'BusinessDescription' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'BusinessDescription' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'BusinessDescription' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'BusinessDescription' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Other_Business where ChangeType is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ChangeType' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ChangeType' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ChangeType' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ChangeType' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ChangeType' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ChangeType' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ChangeType' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ChangeType' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Other_Business where AddressLine1 is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'AddressLine1' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'AddressLine1' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'AddressLine1' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AddressLine1' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'AddressLine1' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'AddressLine1' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'AddressLine1' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AddressLine1' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Other_Business where AddressLine2 is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'AddressLine2' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'AddressLine2' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'AddressLine2' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AddressLine2' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'AddressLine2' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'AddressLine2' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'AddressLine2' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'AddressLine2' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Other_Business where City is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'City' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'City' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'City' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'City' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'City' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'City' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'City' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'City' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Other_Business where StateOrProvince is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'StateOrProvince' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'StateOrProvince' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'StateOrProvince' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'StateOrProvince' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'StateOrProvince' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'StateOrProvince' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'StateOrProvince' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'StateOrProvince' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Other_Business where ZipCode is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ZipCode' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ZipCode' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ZipCode' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ZipCode' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ZipCode' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ZipCode' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ZipCode' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ZipCode' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Other_Business where MeetsConditionsForApproval is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'MeetsConditionsForApproval' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'MeetsConditionsForApproval' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'MeetsConditionsForApproval' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'MeetsConditionsForApproval' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'MeetsConditionsForApproval' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'MeetsConditionsForApproval' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'MeetsConditionsForApproval' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'MeetsConditionsForApproval' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Other_Business where OtherBusinessApprovalCode is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OtherBusinessApprovalCode' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'OtherBusinessApprovalCode' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OtherBusinessApprovalCode' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OtherBusinessApprovalCode' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OtherBusinessApprovalCode' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'OtherBusinessApprovalCode' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OtherBusinessApprovalCode' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OtherBusinessApprovalCode' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Other_Business where ComplianceTrackingCode is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ComplianceTrackingCode' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ComplianceTrackingCode' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ComplianceTrackingCode' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ComplianceTrackingCode' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ComplianceTrackingCode' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ComplianceTrackingCode' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ComplianceTrackingCode' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ComplianceTrackingCode' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Other_Business where U4CodeForOutsideBusiness is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'U4CodeForOutsideBusiness' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'U4CodeForOutsideBusiness' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'U4CodeForOutsideBusiness' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'U4CodeForOutsideBusiness' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'U4CodeForOutsideBusiness' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'U4CodeForOutsideBusiness' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'U4CodeForOutsideBusiness' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'U4CodeForOutsideBusiness' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Other_Business where OtherBusinessApprovalDate is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OtherBusinessApprovalDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'OtherBusinessApprovalDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OtherBusinessApprovalDate' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OtherBusinessApprovalDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OtherBusinessApprovalDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'OtherBusinessApprovalDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OtherBusinessApprovalDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OtherBusinessApprovalDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Other_Business where OtherBusinessEndDate is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OtherBusinessEndDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'OtherBusinessEndDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OtherBusinessEndDate' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OtherBusinessEndDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'OtherBusinessEndDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'OtherBusinessEndDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'OtherBusinessEndDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'OtherBusinessEndDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Other_Business where ADVForm is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ADVForm' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ADVForm' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ADVForm' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ADVForm' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ADVForm' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ADVForm' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ADVForm' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ADVForm' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Other_Business where BranchOfficeRegistration is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'BranchOfficeRegistration' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'BranchOfficeRegistration' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'BranchOfficeRegistration' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'BranchOfficeRegistration' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'BranchOfficeRegistration' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'BranchOfficeRegistration' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'BranchOfficeRegistration' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'BranchOfficeRegistration' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
---------------------------------------------------

select @TestDescription=(Select case WHEN
(select count(TestStatus) 
from dbo.TestResults 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Staging_Other_Business%'
and TestStatus = @Passed
) = (select count(TestStatus) 
from dbo.TestResults 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Staging_Other_Business%'
)
THEN 'ALL TESTS PASSED!'
WHEN
(select count(TestStatus) 
from dbo.TestResults 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Staging_Other_Business%'
and TestStatus in (@Passed,@Warning)
) = (select count(TestStatus) 
from dbo.TestResults 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Staging_Other_Business%'
)
THEN 'ALL TESTS PASSED. SOME PASS WITH WARNING!'
ELSE 'ALL TESTS DID NOT PASS!' END)

insert dbo.TestLog values (@TestScenario,@EndingStatus,@TestDescription,SYSDATETIME(),System_User)

select
testStatus
,TestStatusCode
,count(TestId)"Count"
from
dbo.TestResults
where
TestDateTime > convert(Date,GetDate())
and
TestDescription LIKE '%Staging_Other_Business%'
and
TestID > @MaxTestID
group by TestStatus,TestStatusCode
order by TestStatusCode
select * from dbo.TestLog Where TestScenario LIKE '%Staging_Other_Business%' 
--And TestExecutionTime > convert(Date,GetDate())
and
TestLogID > @MaxTestLogID
Order by TestExecutionTime Desc

select
[TestId]
,[TestMessage]
,[TestDescription]
,[TestStatus]
,[ExpectedResult]
,[ActualResult]
,[TestDateTime]
,[TestStatusCode]
from
dbo.TestResults
where
TestDateTime > convert(Date,GetDate())
and
TestDescription LIKE '%Staging_Other_Business%'
and
TestID > @MaxTestID
order by TestStatusCode asc
---------------------------------------------------
