Use DataIntegrationStage

declare @MaxTestID int; --Variable to caputure Last Test ID in TestResult Table
declare @TestAreaBeginning varchar(30) = 'air'

-- Test Name Declaration Starts Here --
declare @TableName varchar(100) = 'Staging_FINRA_License';
-- Test Name Declaration Ends Here --
declare @SchemaName varchar(25) = 'air';
-- Table Column Declaration Starts Here --

declare @FinraLicenseId varchar(14) = 'FinraLicenseId';
declare @LicenseRequestDate varchar(18) = 'LicenseRequestDate';
declare @LicenseEffectivedate varchar(20) = 'LicenseEffectivedate';
declare @LicenseTerminatedDate varchar(21) = 'LicenseTerminatedDate';
declare @LicenseRenewalDate varchar(18) = 'LicenseRenewalDate';
declare @BeginningOfWindowDateRange varchar(26) = 'BeginningOfWindowDateRange';
declare @EndOfWindowDateRange varchar(20) = 'EndOfWindowDateRange';
declare @ProcessedDatetime varchar(17) = 'ProcessedDatetime';
declare @InsertDateTime varchar(14) = 'InsertDateTime';
declare @ContactIdentifierReference varchar(26) = 'ContactIdentifierReference';
declare @FinraLicenseType varchar(16) = 'FinraLicenseType';
declare @LicenseState varchar(12) = 'LicenseState';
declare @LicenseSeries varchar(13) = 'LicenseSeries';
declare @ChangeType varchar(10) = 'ChangeType';


-- Table Column Declaration Ends Here --

--Fixed -- Do Not Change -- 
declare @MyRowcount int;
declare @WHD varchar(30) = 'We have duplicates! ';
declare @WDHD varchar(40) = 'We do not have duplicates. ';
declare @WHOR varchar(40) = 'We have orphaned records! ';
Declare @WDNHOR varchar(40) = 'We do not have orphaned records. '
declare @CDNE varchar(75) = ' column does not exist.';
declare @CE varchar(25) = ' column exists.';
declare @TC varchar(25) = ' Test Case #';
declare @E varchar(25) = ' exists.'
declare @DNE varchar(25) = ' does not exist.';
declare @Table varchar(10) = 'Table';
Declare @Column varchar(25) = ' column ';
Declare @HTCT varchar(30) = 'has the correct type. ';
Declare @DNHTCT varchar(40) = 'does not have the correct type.';
declare @CLIC varchar(40) = ' column length is correct.';
declare @CLII varchar(40) = ' column length is incorrect.';
declare @INPWDT varchar(40) = ' is not populated with date time.';
declare @IPWDT varchar(40) = ' is populated with date time.';
declare @TCDNED varchar(40) = 'does not exist, damn it!';
declare @somearenull varchar(25) = ' some columns are null.';
declare @tasbpwvda varchar(255) = ' should be populated with valid data. ';
declare @sbpwagcir varchar(255) = ' should be populated with a good contact identifier reference.'
declare @somecolumnsblank varchar(50) = ' some columns are blank.';
declare @somerhbd varchar(50) = ' some rows have bad dates.';
declare @sciraor varchar(175) = ' some contact identifier reference are oprhaned records';
declare @cchav varchar(50) = 'changeType column has appropriate values.';
declare @ccdnhav varchar(50) = 'changeType column does not have appropriate values.';
DECLARE @MyTestDescription varchar(500);
Declare @RC int;
DECLARE @TestStatusCode int;
declare @NullCount int;
declare @NumBlank int;
declare @TypeVarChar int = 167;
declare @TypeDateTime int = 61;
declare @TypeDate int = 167;
declare @TypeInt int = 56;
declare @TypeBit int = 104;
declare @TypeBigInt int = 127;
declare @TypeNVarChar int = 231;
declare @TypeChar int = 175;
declare @TypeMoney int = 60;
declare @TypeDecimal int = 106;
declare @TypeTinyInt int = 48;
declare @Passed varchar(10) = 'Passed.';
declare @Failed varchar(10) = 'Failed!';
declare @Warning varchar(10) = 'Warning!';
declare @PrimaryKey varchar(100) = 'PrimaryKey';
declare @SystemTypeId int;
declare @LengthDateTime int = 8;
declare @LengthInt int = 4;
declare @Zero int = 0;
declare @One int = 1;
declare @Two int = 2;
declare @Three int = 3;
declare @Four int = 4;
declare @Five int = 5;
declare @Seven int = 7;
declare @Six int = 6;
declare @Eight int = 8;
declare @Nine int = 9;
declare @Ten int = 10;
declare @Twelve int = 12;
declare @Fifteen int = 15;
declare @Eighteen int = 18;
declare @Nineteen int = 19;
declare @Twenty int = 20;
declare @TwentyThree int = 23;
declare @TwentyFour int = 24;
declare @TwentyFive int = 25;
declare @Thirty int = 30;
declare @ThirtyTwo int = 32;
declare @ThirtyFive int =35;
declare @Forty int = 40;
declare @FortyOne int = 41;
declare @Fifty int = 50;
declare @Sixty int = 60;
declare @Eighty int = 80;
declare @Hundred int = 100;
declare @OneHundred int = 100;
declare @OneHundredThirtyThree int = 133;
declare @OneHundredTwentyFive int = 125;
declare @TwoHundred int = 200;
declare @TwoHundredFifty int = 250;
declare @TwoHundredFiftyFive int = 255;
declare @ThreeHundredFifty int = 350;
declare @FiveHundred int = 500;
declare @OneThousand int = 1000;
declare @ThirteenHundred int = 1300;
--declare @TestDescription varchar(200);
declare @ExpectedResult varchar(100);
declare @ActualResult varchar(100);
declare @TestStatus varchar(15);
declare @TestDateTime datetime;
declare @TestMessage varchar(200);
--declare @TableNameOne varchar(30) = 'staging_plan';
declare @Schema int = 17;
declare @Insert varchar(30) = 'I-insert';
Declare @Update varchar(30) = 'U-update';
Declare @Delete varchar(30) = 'D-delete';
declare @TableObject Int;
declare @ColumnObject Int;
declare @ColumnIsNullable Int;
declare @ICN varchar(30) = ' is column nullable.';
declare @TCIN varchar(40) = ' the column is nullable. ';
declare @TCINN varchar(50) = ' the column is not nullable. ';
declare @TCIPK varchar(50) = ' the column is a Primary Key. ';
declare @MaxLength Int;
declare @Return int;
declare @AllowsNull int;
declare @HasNull int;
declare @ColHasNull varchar(20) = ' column has null';
declare @ColHasNoNull varchar(20)= ' column has no null';
declare @HasBlank Bit;
declare @ColHasBlank varchar(25) = ' column has blank value '
declare @ColHasNoBlank varchar(25) = ' column has no blank value '
declare @HasBlankCount int;
--Declare @TypeDecimal int = 106;
Declare @MaxTestLogID int;
declare @LengthTinyInt int = 1;
declare @LengthDecimal int = 17;
declare @Eigthteen int = 18;
Declare @IsIdentity bit;
declare @TempTableName varchar(25) = '#testResults';
Declare @TestScenario varchar(80) = 'Staging_FINRA_License'
declare @BeginningStatus varchar(25) = 'Beginning';
Declare @EndingStatus varchar(25) = 'Ending';
Declare @TestDescription varchar(800) = 'Test the Staging_FINRA_License table and columns confirming the properties of the columns and some data verification.';
declare @p1 varchar(25) = 'Air';
declare @p2 varchar(25) = 'AccountMaster';


select @MaxTestID = max(Testid) from dbo.TestResults;

select @MaxTestLogID = max(TestLogId) from dbo.TestLog;

--Get the Schema ID for the table retrieval


select @Schema  = schema_id
from
sys.schemas
where
name = @p1;

--Get the TableObjectId for the table using the Table Name and the SchemaId
select @TableObject = [object_id]
	from
	sys.tables
	where
	name = @TableName
	and
	schema_id = @Schema;
	
select
@PrimaryKey = name
from
sys.all_columns
where
is_identity = 1
and
object_id = @TableObject;


insert TestLog values (@TestScenario,@BeginningStatus,@TestDescription,SYSDATETIME(),System_User)

If @TableObject is not null
begin
Set @TestDescription =	@TestAreaBeginning + @TableName + '' + @Table + ' exists test.';
Set @ExpectedResult =    'Table Exists.';
Set @ActualResult = 'Table Exists.';
Set @TestStatus =  @Passed;
Set @TestDateTime = SYSDATETIME();
Set @TestMessage = @TC + trim(str(@@IDENTITY)) +' '+@Table+' '+@TableName + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode
end
	else
begin	
Set @TestDescription =	@TestAreaBeginning + @TableName+ '' + @Table + ' exists test.';
set @ExpectedResult = 	'Table Exists.';
Set @ActualResult =	'Table object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage = 	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Table+' '+@TableName + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode
end
--------------------------------------------------

set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @FinraLicenseId and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @FinraLicenseId + ' exists test.';
Set @ExpectedResult =  @FinraLicenseId + ' ' + @Column + ' ' +@E;
Set @ActualResult = @FinraLicenseId + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @FinraLicenseId + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @FinraLicenseId + ' exists test.';
Set @ExpectedResult =  @FinraLicenseId + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @FinraLicenseId + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@FinraLicenseId + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @LicenseRequestDate and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @LicenseRequestDate + ' exists test.';
Set @ExpectedResult =  @LicenseRequestDate + ' ' + @Column + ' ' +@E;
Set @ActualResult = @LicenseRequestDate + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @LicenseRequestDate + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @LicenseRequestDate + ' exists test.';
Set @ExpectedResult =  @LicenseRequestDate + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @LicenseRequestDate + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@LicenseRequestDate + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @LicenseEffectivedate and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @LicenseEffectivedate + ' exists test.';
Set @ExpectedResult =  @LicenseEffectivedate + ' ' + @Column + ' ' +@E;
Set @ActualResult = @LicenseEffectivedate + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @LicenseEffectivedate + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @LicenseEffectivedate + ' exists test.';
Set @ExpectedResult =  @LicenseEffectivedate + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @LicenseEffectivedate + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@LicenseEffectivedate + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @LicenseTerminatedDate and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @LicenseTerminatedDate + ' exists test.';
Set @ExpectedResult =  @LicenseTerminatedDate + ' ' + @Column + ' ' +@E;
Set @ActualResult = @LicenseTerminatedDate + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @LicenseTerminatedDate + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @LicenseTerminatedDate + ' exists test.';
Set @ExpectedResult =  @LicenseTerminatedDate + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @LicenseTerminatedDate + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@LicenseTerminatedDate + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @LicenseRenewalDate and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @LicenseRenewalDate + ' exists test.';
Set @ExpectedResult =  @LicenseRenewalDate + ' ' + @Column + ' ' +@E;
Set @ActualResult = @LicenseRenewalDate + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @LicenseRenewalDate + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @LicenseRenewalDate + ' exists test.';
Set @ExpectedResult =  @LicenseRenewalDate + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @LicenseRenewalDate + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@LicenseRenewalDate + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @BeginningOfWindowDateRange and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @BeginningOfWindowDateRange + ' exists test.';
Set @ExpectedResult =  @BeginningOfWindowDateRange + ' ' + @Column + ' ' +@E;
Set @ActualResult = @BeginningOfWindowDateRange + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @BeginningOfWindowDateRange + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @BeginningOfWindowDateRange + ' exists test.';
Set @ExpectedResult =  @BeginningOfWindowDateRange + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @BeginningOfWindowDateRange + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@BeginningOfWindowDateRange + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @EndOfWindowDateRange and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @EndOfWindowDateRange + ' exists test.';
Set @ExpectedResult =  @EndOfWindowDateRange + ' ' + @Column + ' ' +@E;
Set @ActualResult = @EndOfWindowDateRange + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @EndOfWindowDateRange + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @EndOfWindowDateRange + ' exists test.';
Set @ExpectedResult =  @EndOfWindowDateRange + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @EndOfWindowDateRange + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@EndOfWindowDateRange + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @ProcessedDatetime and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @ProcessedDatetime + ' exists test.';
Set @ExpectedResult =  @ProcessedDatetime + ' ' + @Column + ' ' +@E;
Set @ActualResult = @ProcessedDatetime + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @ProcessedDatetime + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @ProcessedDatetime + ' exists test.';
Set @ExpectedResult =  @ProcessedDatetime + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @ProcessedDatetime + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@ProcessedDatetime + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @InsertDateTime and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @InsertDateTime + ' exists test.';
Set @ExpectedResult =  @InsertDateTime + ' ' + @Column + ' ' +@E;
Set @ActualResult = @InsertDateTime + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @InsertDateTime + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @InsertDateTime + ' exists test.';
Set @ExpectedResult =  @InsertDateTime + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @InsertDateTime + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@InsertDateTime + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @ContactIdentifierReference and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @ContactIdentifierReference + ' exists test.';
Set @ExpectedResult =  @ContactIdentifierReference + ' ' + @Column + ' ' +@E;
Set @ActualResult = @ContactIdentifierReference + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @ContactIdentifierReference + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @ContactIdentifierReference + ' exists test.';
Set @ExpectedResult =  @ContactIdentifierReference + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @ContactIdentifierReference + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@ContactIdentifierReference + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @FinraLicenseType and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @FinraLicenseType + ' exists test.';
Set @ExpectedResult =  @FinraLicenseType + ' ' + @Column + ' ' +@E;
Set @ActualResult = @FinraLicenseType + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @FinraLicenseType + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @FinraLicenseType + ' exists test.';
Set @ExpectedResult =  @FinraLicenseType + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @FinraLicenseType + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@FinraLicenseType + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @LicenseState and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @LicenseState + ' exists test.';
Set @ExpectedResult =  @LicenseState + ' ' + @Column + ' ' +@E;
Set @ActualResult = @LicenseState + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @LicenseState + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @LicenseState + ' exists test.';
Set @ExpectedResult =  @LicenseState + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @LicenseState + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@LicenseState + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @LicenseSeries and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @LicenseSeries + ' exists test.';
Set @ExpectedResult =  @LicenseSeries + ' ' + @Column + ' ' +@E;
Set @ActualResult = @LicenseSeries + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @LicenseSeries + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @LicenseSeries + ' exists test.';
Set @ExpectedResult =  @LicenseSeries + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @LicenseSeries + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@LicenseSeries + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @ChangeType and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @ChangeType + ' exists test.';
Set @ExpectedResult =  @ChangeType + ' ' + @Column + ' ' +@E;
Set @ActualResult = @ChangeType + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @ChangeType + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @ChangeType + ' exists test.';
Set @ExpectedResult =  @ChangeType + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @ChangeType + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@ChangeType + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @FinraLicenseId	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @FinraLicenseId; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @FinraLicenseId + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeInt =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @FinraLicenseId;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @FinraLicenseId + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @FinraLicenseId + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @FinraLicenseId + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @LicenseRequestDate	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @LicenseRequestDate; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @LicenseRequestDate + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeDateTime =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @LicenseRequestDate;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @LicenseRequestDate + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @LicenseRequestDate + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @LicenseRequestDate + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @LicenseEffectivedate	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @LicenseEffectivedate; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @LicenseEffectivedate + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeDateTime =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @LicenseEffectivedate;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @LicenseEffectivedate + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @LicenseEffectivedate + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @LicenseEffectivedate + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @LicenseTerminatedDate	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @LicenseTerminatedDate; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @LicenseTerminatedDate + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeDateTime =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @LicenseTerminatedDate;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @LicenseTerminatedDate + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @LicenseTerminatedDate + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @LicenseTerminatedDate + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @LicenseRenewalDate	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @LicenseRenewalDate; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @LicenseRenewalDate + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeDateTime =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @LicenseRenewalDate;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @LicenseRenewalDate + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @LicenseRenewalDate + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @LicenseRenewalDate + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @BeginningOfWindowDateRange	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @BeginningOfWindowDateRange; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @BeginningOfWindowDateRange + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeDateTime =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @BeginningOfWindowDateRange;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @BeginningOfWindowDateRange + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @BeginningOfWindowDateRange + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @BeginningOfWindowDateRange + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @EndOfWindowDateRange	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @EndOfWindowDateRange; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @EndOfWindowDateRange + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeDateTime =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @EndOfWindowDateRange;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @EndOfWindowDateRange + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @EndOfWindowDateRange + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @EndOfWindowDateRange + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @ProcessedDatetime	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @ProcessedDatetime; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @ProcessedDatetime + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeDateTime =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ProcessedDatetime;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ProcessedDatetime + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ProcessedDatetime + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ProcessedDatetime + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @InsertDateTime	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @InsertDateTime; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @InsertDateTime + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeDateTime =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @InsertDateTime;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @InsertDateTime + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @InsertDateTime + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @InsertDateTime + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @ContactIdentifierReference	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @ContactIdentifierReference; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @ContactIdentifierReference + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ContactIdentifierReference;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ContactIdentifierReference + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ContactIdentifierReference + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ContactIdentifierReference + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @FinraLicenseType	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @FinraLicenseType; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @FinraLicenseType + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @FinraLicenseType;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @FinraLicenseType + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @FinraLicenseType + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @FinraLicenseType + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @LicenseState	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @LicenseState; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @LicenseState + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @LicenseState;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @LicenseState + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @LicenseState + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @LicenseState + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @LicenseSeries	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @LicenseSeries; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @LicenseSeries + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @LicenseSeries;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @LicenseSeries + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @LicenseSeries + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @LicenseSeries + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @ChangeType	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @ChangeType; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @ChangeType + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ChangeType;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ChangeType + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ChangeType + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ChangeType + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@FinraLicenseId,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @FinraLicenseId + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @FinraLicenseId + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@FinraLicenseId +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @FinraLicenseId + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@FinraLicenseId +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @FinraLicenseId + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@LicenseRequestDate,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @LicenseRequestDate + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @LicenseRequestDate + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LicenseRequestDate +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseRequestDate + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LicenseRequestDate +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @LicenseRequestDate + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@LicenseEffectivedate,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @LicenseEffectivedate + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @LicenseEffectivedate + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LicenseEffectivedate +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseEffectivedate + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LicenseEffectivedate +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @LicenseEffectivedate + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@LicenseTerminatedDate,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @LicenseTerminatedDate + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @LicenseTerminatedDate + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LicenseTerminatedDate +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseTerminatedDate + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LicenseTerminatedDate +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @LicenseTerminatedDate + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@LicenseRenewalDate,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @LicenseRenewalDate + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @LicenseRenewalDate + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LicenseRenewalDate +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseRenewalDate + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LicenseRenewalDate +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @LicenseRenewalDate + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@BeginningOfWindowDateRange,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @BeginningOfWindowDateRange + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @BeginningOfWindowDateRange + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@BeginningOfWindowDateRange +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @BeginningOfWindowDateRange + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@BeginningOfWindowDateRange +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @BeginningOfWindowDateRange + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@EndOfWindowDateRange,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @EndOfWindowDateRange + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @EndOfWindowDateRange + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@EndOfWindowDateRange +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @EndOfWindowDateRange + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@EndOfWindowDateRange +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @EndOfWindowDateRange + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@ProcessedDatetime,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @ProcessedDatetime + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@InsertDateTime,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @InsertDateTime + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@ContactIdentifierReference,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @ContactIdentifierReference + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@FinraLicenseType,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @FinraLicenseType + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @FinraLicenseType + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Fifty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@FinraLicenseType +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @FinraLicenseType + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@FinraLicenseType +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @FinraLicenseType + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@LicenseState,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @LicenseState + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @LicenseState + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Six
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LicenseState +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseState + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LicenseState +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @LicenseState + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@LicenseSeries,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @LicenseSeries + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @LicenseSeries + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LicenseSeries +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseSeries + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@LicenseSeries +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @LicenseSeries + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@ChangeType,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @ChangeType + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @ChangeType + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @One
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @ChangeType + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

Select @AllowsNull = COLUMNPROPERTY(@TableObject,@FinraLicenseId,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License WHERE @FinraLicenseId IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@FinraLicenseId +  ' Has nullable test.';
Set @ExpectedResult =  @FinraLicenseId+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @FinraLicenseId+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @FinraLicenseId + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@FinraLicenseId +  ' Has nullable test.'; 
Set @ExpectedResult =  @FinraLicenseId+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @FinraLicenseId+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @FinraLicenseId + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@FinraLicenseId +  ' Has nullable test.';
Set @ExpectedResult =     @FinraLicenseId+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @FinraLicenseId+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @FinraLicenseId + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@FinraLicenseId +  ' Has nullable test.';
Set @ExpectedResult =     @FinraLicenseId+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @FinraLicenseId+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @FinraLicenseId + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@LicenseRequestDate,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License WHERE @LicenseRequestDate IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@LicenseRequestDate +  ' Has nullable test.';
Set @ExpectedResult =  @LicenseRequestDate+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @LicenseRequestDate+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseRequestDate + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@LicenseRequestDate +  ' Has nullable test.'; 
Set @ExpectedResult =  @LicenseRequestDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @LicenseRequestDate+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseRequestDate + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@LicenseRequestDate +  ' Has nullable test.';
Set @ExpectedResult =     @LicenseRequestDate+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @LicenseRequestDate+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseRequestDate + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@LicenseRequestDate +  ' Has nullable test.';
Set @ExpectedResult =     @LicenseRequestDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @LicenseRequestDate+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseRequestDate + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@LicenseEffectivedate,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License WHERE @LicenseEffectivedate IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@LicenseEffectivedate +  ' Has nullable test.';
Set @ExpectedResult =  @LicenseEffectivedate+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @LicenseEffectivedate+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseEffectivedate + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@LicenseEffectivedate +  ' Has nullable test.'; 
Set @ExpectedResult =  @LicenseEffectivedate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @LicenseEffectivedate+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseEffectivedate + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@LicenseEffectivedate +  ' Has nullable test.';
Set @ExpectedResult =     @LicenseEffectivedate+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @LicenseEffectivedate+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseEffectivedate + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@LicenseEffectivedate +  ' Has nullable test.';
Set @ExpectedResult =     @LicenseEffectivedate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @LicenseEffectivedate+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseEffectivedate + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@LicenseTerminatedDate,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License WHERE @LicenseTerminatedDate IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@LicenseTerminatedDate +  ' Has nullable test.';
Set @ExpectedResult =  @LicenseTerminatedDate+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @LicenseTerminatedDate+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseTerminatedDate + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@LicenseTerminatedDate +  ' Has nullable test.'; 
Set @ExpectedResult =  @LicenseTerminatedDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @LicenseTerminatedDate+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseTerminatedDate + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@LicenseTerminatedDate +  ' Has nullable test.';
Set @ExpectedResult =     @LicenseTerminatedDate+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @LicenseTerminatedDate+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseTerminatedDate + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@LicenseTerminatedDate +  ' Has nullable test.';
Set @ExpectedResult =     @LicenseTerminatedDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @LicenseTerminatedDate+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseTerminatedDate + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@LicenseRenewalDate,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License WHERE @LicenseRenewalDate IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@LicenseRenewalDate +  ' Has nullable test.';
Set @ExpectedResult =  @LicenseRenewalDate+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @LicenseRenewalDate+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseRenewalDate + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@LicenseRenewalDate +  ' Has nullable test.'; 
Set @ExpectedResult =  @LicenseRenewalDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @LicenseRenewalDate+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseRenewalDate + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@LicenseRenewalDate +  ' Has nullable test.';
Set @ExpectedResult =     @LicenseRenewalDate+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @LicenseRenewalDate+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseRenewalDate + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@LicenseRenewalDate +  ' Has nullable test.';
Set @ExpectedResult =     @LicenseRenewalDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @LicenseRenewalDate+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseRenewalDate + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@BeginningOfWindowDateRange,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License WHERE @BeginningOfWindowDateRange IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@BeginningOfWindowDateRange +  ' Has nullable test.';
Set @ExpectedResult =  @BeginningOfWindowDateRange+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @BeginningOfWindowDateRange+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @BeginningOfWindowDateRange + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@BeginningOfWindowDateRange +  ' Has nullable test.'; 
Set @ExpectedResult =  @BeginningOfWindowDateRange+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @BeginningOfWindowDateRange+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @BeginningOfWindowDateRange + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@BeginningOfWindowDateRange +  ' Has nullable test.';
Set @ExpectedResult =     @BeginningOfWindowDateRange+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @BeginningOfWindowDateRange+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @BeginningOfWindowDateRange + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@BeginningOfWindowDateRange +  ' Has nullable test.';
Set @ExpectedResult =     @BeginningOfWindowDateRange+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @BeginningOfWindowDateRange+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @BeginningOfWindowDateRange + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@EndOfWindowDateRange,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License WHERE @EndOfWindowDateRange IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@EndOfWindowDateRange +  ' Has nullable test.';
Set @ExpectedResult =  @EndOfWindowDateRange+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @EndOfWindowDateRange+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @EndOfWindowDateRange + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@EndOfWindowDateRange +  ' Has nullable test.'; 
Set @ExpectedResult =  @EndOfWindowDateRange+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @EndOfWindowDateRange+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @EndOfWindowDateRange + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@EndOfWindowDateRange +  ' Has nullable test.';
Set @ExpectedResult =     @EndOfWindowDateRange+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @EndOfWindowDateRange+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @EndOfWindowDateRange + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@EndOfWindowDateRange +  ' Has nullable test.';
Set @ExpectedResult =     @EndOfWindowDateRange+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @EndOfWindowDateRange+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @EndOfWindowDateRange + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@ProcessedDatetime,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License WHERE @ProcessedDatetime IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has nullable test.';
Set @ExpectedResult =  @ProcessedDatetime+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @ProcessedDatetime+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has nullable test.'; 
Set @ExpectedResult =  @ProcessedDatetime+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @ProcessedDatetime+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has nullable test.';
Set @ExpectedResult =     @ProcessedDatetime+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @ProcessedDatetime+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has nullable test.';
Set @ExpectedResult =     @ProcessedDatetime+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @ProcessedDatetime+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@InsertDateTime,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License WHERE @InsertDateTime IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has nullable test.';
Set @ExpectedResult =  @InsertDateTime+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @InsertDateTime+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has nullable test.'; 
Set @ExpectedResult =  @InsertDateTime+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @InsertDateTime+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has nullable test.';
Set @ExpectedResult =     @InsertDateTime+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @InsertDateTime+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has nullable test.';
Set @ExpectedResult =     @InsertDateTime+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @InsertDateTime+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@ContactIdentifierReference,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License WHERE @ContactIdentifierReference IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' Has nullable test.';
Set @ExpectedResult =  @ContactIdentifierReference+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @ContactIdentifierReference+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' Has nullable test.'; 
Set @ExpectedResult =  @ContactIdentifierReference+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @ContactIdentifierReference+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' Has nullable test.';
Set @ExpectedResult =     @ContactIdentifierReference+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @ContactIdentifierReference+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' Has nullable test.';
Set @ExpectedResult =     @ContactIdentifierReference+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @ContactIdentifierReference+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@FinraLicenseType,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License WHERE @FinraLicenseType IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@FinraLicenseType +  ' Has nullable test.';
Set @ExpectedResult =  @FinraLicenseType+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @FinraLicenseType+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @FinraLicenseType + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@FinraLicenseType +  ' Has nullable test.'; 
Set @ExpectedResult =  @FinraLicenseType+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @FinraLicenseType+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @FinraLicenseType + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@FinraLicenseType +  ' Has nullable test.';
Set @ExpectedResult =     @FinraLicenseType+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @FinraLicenseType+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @FinraLicenseType + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@FinraLicenseType +  ' Has nullable test.';
Set @ExpectedResult =     @FinraLicenseType+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @FinraLicenseType+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @FinraLicenseType + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@LicenseState,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License WHERE @LicenseState IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@LicenseState +  ' Has nullable test.';
Set @ExpectedResult =  @LicenseState+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @LicenseState+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseState + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@LicenseState +  ' Has nullable test.'; 
Set @ExpectedResult =  @LicenseState+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @LicenseState+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseState + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@LicenseState +  ' Has nullable test.';
Set @ExpectedResult =     @LicenseState+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @LicenseState+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseState + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@LicenseState +  ' Has nullable test.';
Set @ExpectedResult =     @LicenseState+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @LicenseState+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseState + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@LicenseSeries,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License WHERE @LicenseSeries IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@LicenseSeries +  ' Has nullable test.';
Set @ExpectedResult =  @LicenseSeries+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @LicenseSeries+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseSeries + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@LicenseSeries +  ' Has nullable test.'; 
Set @ExpectedResult =  @LicenseSeries+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @LicenseSeries+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseSeries + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@LicenseSeries +  ' Has nullable test.';
Set @ExpectedResult =     @LicenseSeries+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @LicenseSeries+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseSeries + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@LicenseSeries +  ' Has nullable test.';
Set @ExpectedResult =     @LicenseSeries+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @LicenseSeries+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseSeries + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@ChangeType,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License WHERE @ChangeType IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has nullable test.';
Set @ExpectedResult =  @ChangeType+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @ChangeType+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has nullable test.'; 
Set @ExpectedResult =  @ChangeType+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @ChangeType+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has nullable test.';
Set @ExpectedResult =     @ChangeType+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @ChangeType+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has nullable test.';
Set @ExpectedResult =     @ChangeType+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @ChangeType+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License where @FinraLicenseId = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@FinraLicenseId +  ' Has blank value test.'; 
Set @ExpectedResult = @FinraLicenseId+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @FinraLicenseId+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @FinraLicenseId + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@FinraLicenseId +  ' Has blank value test.'; 
Set @ExpectedResult = @FinraLicenseId+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @FinraLicenseId+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @FinraLicenseId + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License where @LicenseRequestDate = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@LicenseRequestDate +  ' Has blank value test.'; 
Set @ExpectedResult = @LicenseRequestDate+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @LicenseRequestDate+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseRequestDate + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@LicenseRequestDate +  ' Has blank value test.'; 
Set @ExpectedResult = @LicenseRequestDate+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @LicenseRequestDate+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseRequestDate + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License where @LicenseEffectivedate = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@LicenseEffectivedate +  ' Has blank value test.'; 
Set @ExpectedResult = @LicenseEffectivedate+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @LicenseEffectivedate+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseEffectivedate + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@LicenseEffectivedate +  ' Has blank value test.'; 
Set @ExpectedResult = @LicenseEffectivedate+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @LicenseEffectivedate+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseEffectivedate + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License where @LicenseTerminatedDate = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@LicenseTerminatedDate +  ' Has blank value test.'; 
Set @ExpectedResult = @LicenseTerminatedDate+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @LicenseTerminatedDate+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseTerminatedDate + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@LicenseTerminatedDate +  ' Has blank value test.'; 
Set @ExpectedResult = @LicenseTerminatedDate+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @LicenseTerminatedDate+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseTerminatedDate + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License where @LicenseRenewalDate = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@LicenseRenewalDate +  ' Has blank value test.'; 
Set @ExpectedResult = @LicenseRenewalDate+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @LicenseRenewalDate+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseRenewalDate + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@LicenseRenewalDate +  ' Has blank value test.'; 
Set @ExpectedResult = @LicenseRenewalDate+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @LicenseRenewalDate+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseRenewalDate + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License where @BeginningOfWindowDateRange = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@BeginningOfWindowDateRange +  ' Has blank value test.'; 
Set @ExpectedResult = @BeginningOfWindowDateRange+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @BeginningOfWindowDateRange+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @BeginningOfWindowDateRange + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@BeginningOfWindowDateRange +  ' Has blank value test.'; 
Set @ExpectedResult = @BeginningOfWindowDateRange+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @BeginningOfWindowDateRange+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @BeginningOfWindowDateRange + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License where @EndOfWindowDateRange = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@EndOfWindowDateRange +  ' Has blank value test.'; 
Set @ExpectedResult = @EndOfWindowDateRange+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @EndOfWindowDateRange+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @EndOfWindowDateRange + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@EndOfWindowDateRange +  ' Has blank value test.'; 
Set @ExpectedResult = @EndOfWindowDateRange+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @EndOfWindowDateRange+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @EndOfWindowDateRange + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License where @ProcessedDatetime = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has blank value test.'; 
Set @ExpectedResult = @ProcessedDatetime+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @ProcessedDatetime+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has blank value test.'; 
Set @ExpectedResult = @ProcessedDatetime+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @ProcessedDatetime+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License where @InsertDateTime = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has blank value test.'; 
Set @ExpectedResult = @InsertDateTime+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @InsertDateTime+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has blank value test.'; 
Set @ExpectedResult = @InsertDateTime+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @InsertDateTime+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License where @ContactIdentifierReference = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' Has blank value test.'; 
Set @ExpectedResult = @ContactIdentifierReference+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @ContactIdentifierReference+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' Has blank value test.'; 
Set @ExpectedResult = @ContactIdentifierReference+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @ContactIdentifierReference+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License where @FinraLicenseType = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@FinraLicenseType +  ' Has blank value test.'; 
Set @ExpectedResult = @FinraLicenseType+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @FinraLicenseType+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @FinraLicenseType + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@FinraLicenseType +  ' Has blank value test.'; 
Set @ExpectedResult = @FinraLicenseType+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @FinraLicenseType+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @FinraLicenseType + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License where @LicenseState = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@LicenseState +  ' Has blank value test.'; 
Set @ExpectedResult = @LicenseState+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @LicenseState+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseState + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@LicenseState +  ' Has blank value test.'; 
Set @ExpectedResult = @LicenseState+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @LicenseState+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseState + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License where @LicenseSeries = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@LicenseSeries +  ' Has blank value test.'; 
Set @ExpectedResult = @LicenseSeries+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @LicenseSeries+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseSeries + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@LicenseSeries +  ' Has blank value test.'; 
Set @ExpectedResult = @LicenseSeries+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @LicenseSeries+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @LicenseSeries + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_FINRA_License where @ChangeType = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has blank value test.'; 
Set @ExpectedResult = @ChangeType+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @ChangeType+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has blank value test.'; 
Set @ExpectedResult = @ChangeType+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @ChangeType+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_FINRA_License where FinraLicenseId = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'FinraLicenseId' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'FinraLicenseId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'FinraLicenseId' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'FinraLicenseId' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'FinraLicenseId' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'FinraLicenseId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'FinraLicenseId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'FinraLicenseId' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_FINRA_License where LicenseRequestDate = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LicenseRequestDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'LicenseRequestDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseRequestDate' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseRequestDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'LicenseRequestDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'LicenseRequestDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseRequestDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseRequestDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_FINRA_License where LicenseEffectivedate = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LicenseEffectivedate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'LicenseEffectivedate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseEffectivedate' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseEffectivedate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'LicenseEffectivedate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'LicenseEffectivedate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseEffectivedate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseEffectivedate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_FINRA_License where LicenseTerminatedDate = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LicenseTerminatedDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'LicenseTerminatedDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseTerminatedDate' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseTerminatedDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'LicenseTerminatedDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'LicenseTerminatedDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseTerminatedDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseTerminatedDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_FINRA_License where LicenseRenewalDate = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LicenseRenewalDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'LicenseRenewalDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseRenewalDate' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseRenewalDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'LicenseRenewalDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'LicenseRenewalDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseRenewalDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseRenewalDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_FINRA_License where BeginningOfWindowDateRange = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'BeginningOfWindowDateRange' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'BeginningOfWindowDateRange' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'BeginningOfWindowDateRange' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'BeginningOfWindowDateRange' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'BeginningOfWindowDateRange' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'BeginningOfWindowDateRange' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'BeginningOfWindowDateRange' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'BeginningOfWindowDateRange' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_FINRA_License where EndOfWindowDateRange = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'EndOfWindowDateRange' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'EndOfWindowDateRange' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'EndOfWindowDateRange' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'EndOfWindowDateRange' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'EndOfWindowDateRange' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'EndOfWindowDateRange' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'EndOfWindowDateRange' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'EndOfWindowDateRange' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_FINRA_License where ProcessedDatetime = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ProcessedDatetime' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ProcessedDatetime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ProcessedDatetime' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ProcessedDatetime' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'ProcessedDatetime' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ProcessedDatetime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ProcessedDatetime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ProcessedDatetime' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_FINRA_License where InsertDateTime = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'InsertDateTime' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'InsertDateTime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'InsertDateTime' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsertDateTime' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'InsertDateTime' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'InsertDateTime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'InsertDateTime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsertDateTime' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_FINRA_License where ContactIdentifierReference = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ContactIdentifierReference' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ContactIdentifierReference' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ContactIdentifierReference' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ContactIdentifierReference' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'ContactIdentifierReference' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ContactIdentifierReference' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ContactIdentifierReference' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ContactIdentifierReference' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_FINRA_License where FinraLicenseType = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'FinraLicenseType' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'FinraLicenseType' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'FinraLicenseType' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'FinraLicenseType' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'FinraLicenseType' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'FinraLicenseType' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'FinraLicenseType' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'FinraLicenseType' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_FINRA_License where LicenseState = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LicenseState' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'LicenseState' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseState' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseState' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'LicenseState' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'LicenseState' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseState' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseState' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_FINRA_License where LicenseSeries = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LicenseSeries' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'LicenseSeries' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseSeries' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseSeries' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'LicenseSeries' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'LicenseSeries' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseSeries' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseSeries' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_FINRA_License where ChangeType = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ChangeType' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ChangeType' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ChangeType' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ChangeType' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'ChangeType' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ChangeType' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ChangeType' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ChangeType' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_FINRA_License where FinraLicenseId is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'FinraLicenseId' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'FinraLicenseId' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'FinraLicenseId' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'FinraLicenseId' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'FinraLicenseId' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'FinraLicenseId' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'FinraLicenseId' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'FinraLicenseId' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_FINRA_License where LicenseRequestDate is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LicenseRequestDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'LicenseRequestDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseRequestDate' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseRequestDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LicenseRequestDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'LicenseRequestDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseRequestDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseRequestDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_FINRA_License where LicenseEffectivedate is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LicenseEffectivedate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'LicenseEffectivedate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseEffectivedate' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseEffectivedate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LicenseEffectivedate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'LicenseEffectivedate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseEffectivedate' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseEffectivedate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_FINRA_License where LicenseTerminatedDate is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LicenseTerminatedDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'LicenseTerminatedDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseTerminatedDate' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseTerminatedDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LicenseTerminatedDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'LicenseTerminatedDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseTerminatedDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseTerminatedDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_FINRA_License where LicenseRenewalDate is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LicenseRenewalDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'LicenseRenewalDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseRenewalDate' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseRenewalDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LicenseRenewalDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'LicenseRenewalDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseRenewalDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseRenewalDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_FINRA_License where BeginningOfWindowDateRange is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'BeginningOfWindowDateRange' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'BeginningOfWindowDateRange' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'BeginningOfWindowDateRange' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'BeginningOfWindowDateRange' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'BeginningOfWindowDateRange' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'BeginningOfWindowDateRange' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'BeginningOfWindowDateRange' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'BeginningOfWindowDateRange' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_FINRA_License where EndOfWindowDateRange is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'EndOfWindowDateRange' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'EndOfWindowDateRange' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'EndOfWindowDateRange' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'EndOfWindowDateRange' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'EndOfWindowDateRange' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'EndOfWindowDateRange' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'EndOfWindowDateRange' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'EndOfWindowDateRange' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_FINRA_License where ProcessedDatetime is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ProcessedDatetime' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ProcessedDatetime' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ProcessedDatetime' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ProcessedDatetime' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ProcessedDatetime' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ProcessedDatetime' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ProcessedDatetime' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ProcessedDatetime' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_FINRA_License where InsertDateTime is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'InsertDateTime' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'InsertDateTime' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'InsertDateTime' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsertDateTime' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'InsertDateTime' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'InsertDateTime' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'InsertDateTime' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsertDateTime' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_FINRA_License where ContactIdentifierReference is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ContactIdentifierReference' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ContactIdentifierReference' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ContactIdentifierReference' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ContactIdentifierReference' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ContactIdentifierReference' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ContactIdentifierReference' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ContactIdentifierReference' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ContactIdentifierReference' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_FINRA_License where FinraLicenseType is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'FinraLicenseType' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'FinraLicenseType' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'FinraLicenseType' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'FinraLicenseType' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'FinraLicenseType' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'FinraLicenseType' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'FinraLicenseType' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'FinraLicenseType' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_FINRA_License where LicenseState is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LicenseState' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'LicenseState' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseState' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseState' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LicenseState' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'LicenseState' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseState' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseState' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_FINRA_License where LicenseSeries is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LicenseSeries' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'LicenseSeries' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseSeries' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseSeries' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'LicenseSeries' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'LicenseSeries' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'LicenseSeries' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'LicenseSeries' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_FINRA_License where ChangeType is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ChangeType' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ChangeType' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ChangeType' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ChangeType' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ChangeType' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ChangeType' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ChangeType' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ChangeType' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL
-----------------------------------------------------------------
---------------------------------------------------
IF (SELECT 
    Count(*) FROM [air].[Staging_FINRA_License]
	Where [ContactIdentifierReference] not in (SELECT
      Distinct([ContactIdentifier])
	FROM [DataIntegrationStage].[air].[Staging_Contact]))>0
begin
--Select 'All FINRA License records are NOT associated to the Contacts defined on the Contact template' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' BR 031 '+ 'All FINRA License records must be associated to the Contacts defined on the Contact template'  + ' test.';
Set @ExpectedResult = 'All FINRA License records are associated to the Contacts defined on the Contact template';
Set @ActualResult = 'All FINRA License records are NOT associated to the Contacts defined on the Contact template';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' BR 031 ' + 'All FINRA License records must be associated to the Contacts defined on the Contact template.';
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Else
begin
--Select 'All FINRA License records are associated to the Contacts defined on the Contact template' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' BR 031 '+ 'All FINRA License records must be associated to the Contacts defined on the Contact template'  + ' test.';
Set @ExpectedResult = 'All FINRA License records are associated to the Contacts defined on the Contact template.';
Set @ActualResult = 'All FINRA License records are associated to the Contacts defined on the Contact template.';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' BR 031 ' + 'All FINRA License records must be associated to the Contacts defined on the Contact template.';
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end


-----------------------------------------------------------------
---------------------------------------------------
IF (SELECT Count(*)
	  FROM [DataIntegrationStage].[air].[Staging_FINRA_License]
	  Where FinraLicenseType='State'
	  And LicenseState is null OR LEN(Trim(LicenseState))=0)>0
begin
--Select 'For State License Type, License State is NOT populated' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' BR 032 '+ 'For State License Type, License State must be populated'  + ' test.';
Set @ExpectedResult = 'For State License Type, License State is populated';
Set @ActualResult = 'For State License Type, License State is NOT populated';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' BR 032 ' + 'For State License Type, License State must be populated.';
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Else
begin
--Select 'For State License Type, License State is populated' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' BR 032 '+ 'For State License Type, License State must be populated'  + ' test.';
Set @ExpectedResult = 'For State License Type, License State is populated';
Set @ActualResult = 'For State License Type, License State is populated';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' BR 032 ' + 'For State License Type, License State must be populated.';
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end

-----------------------------------------------------------------
---------------------------------------------------
IF (SELECT Count(*)
	  FROM [DataIntegrationStage].[air].[Staging_FINRA_License]
	  Where FinraLicenseType='State'
	  And LicenseSeries is null OR LEN(Trim(LicenseSeries))=0)>0
begin
--Select 'For Series License Type, License Series is NOT populated' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' BR 033 '+ 'For Series License Type, License Series is populated'  + ' test.';
Set @ExpectedResult = 'For Series License Type, License Series is populated';
Set @ActualResult = 'For Series License Type, License Series is NOT populated';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' BR 033 ' + 'For Series License Type, License Series is populated';
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Else
begin
--Select 'For Series License Type, License Series is populated' Pass
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' BR 033 '+ 'For Series License Type, License Series is populated'  + ' test.';
Set @ExpectedResult = 'For Series License Type, License Series is populated';
Set @ActualResult = 'For Series License Type, License Series is populated';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' BR 033 ' + 'For Series License Type, License Series is populated';
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end

-----------------------------------------------------------------
---------------------------------------------------
if (SELECT count(*)
  FROM [DataIntegrationStage].[air].[Staging_FINRA_License]
  Where
  LicenseEffectivedate is null
  AND 
  LicenseTerminatedDate is null)>0
  begin
  --select 'Effective Date or Terminated Date must be populated' Fail
  Set @MyTestDescription = @SchemaName + ' ' + @TableName +' BR 034 '+ 'Effective Date or Terminated Date must be populated'  + ' test.';
Set @ExpectedResult = 'Effective Date or Terminated Date are populated';
Set @ActualResult = 'Effective Date or Terminated Date must be populated';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' BR 034 ' + 'Effective Date or Terminated Date must be populated';
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
  end
  else
  begin
  --select 'Effective Date or Terminated Date are be populated' Pass
  Set @MyTestDescription = @SchemaName + ' ' + @TableName +' BR 034 '+ 'Effective Date or Terminated Date are be populated'  + ' test.';
Set @ExpectedResult = 'Effective Date or Terminated Date are populated';
Set @ActualResult = 'Effective Date or Terminated Date are be populated';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' BR 034 ' + 'Effective Date or Terminated Date are be populated';
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
  end

-----------------------------------------------------------------
---------------------------------------------------
SELECT [ContactIdentifierReference]
      ,[FinraLicenseType]
      ,[LicenseState]
      ,[LicenseSeries]
      ,[LicenseRequestDate]
      ,[LicenseEffectivedate]
      ,[LicenseTerminatedDate]
      ,[LicenseRenewalDate]
      ,[BeginningOfWindowDateRange]
      ,[EndOfWindowDateRange]
      ,count(FinralicenseId)finralicenseoccurrences
  FROM [air].[Staging_FINRA_License]
  group by [ContactIdentifierReference]
      ,[FinraLicenseType]
      ,[LicenseState]
      ,[LicenseSeries]
      ,[LicenseRequestDate]
      ,[LicenseEffectivedate]
      ,[LicenseTerminatedDate]
      ,[LicenseRenewalDate]
      ,[BeginningOfWindowDateRange]
      ,[EndOfWindowDateRange]
having count(finralicenseId) > 1
select @MyRowCount = @@ROWCOUNT

if @MyRowCount = 0
begin
--insert 
--#testResults
--([TestDescription]
--           ,[ExpectedResult]
--           ,[ActualResult]
--           ,[TestStatus]
--           ,[TestDateTime]
--           ,[TestMessage]
--	   ,[TestStatusCode])  
--values 
--(@SchemaName + ' ' + @TableName +' '+ 'Duplicate record check on' + @TableName+ @Table + 'test.',
--'Duplicate record check on' + @TableName+ @Table  + @WDHD +trim(str(@Zero)),
--'Duplicate record check on' + @TableName+ @Table   + @WDHD +trim(str(isNull(@MyRowcount,0))),
--@Passed,
--Sysdatetime(),
--@TC+ trim(str(@@IDENTITY))+ ' ' + 'Duplicate record check on' + @TableName+ @Table   + @WDHD ,3)
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Duplicate record check on' + @TableName+ @Table + 'test.';
Set @ExpectedResult = 'Duplicate record check on' + @TableName+ @Table  + @WDHD +trim(str(@Zero));
Set @ActualResult = 'Duplicate record check on' + @TableName+ @Table   + @WDHD +trim(str(isNull(@MyRowcount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'Duplicate record check on' + @TableName+ @Table   + @WDHD;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
--insert 
--#testResults
--([TestDescription]
--           ,[ExpectedResult]
--           ,[ActualResult]
--           ,[TestStatus]
--           ,[TestDateTime]
--           ,[TestMessage]
--	   ,[TestStatusCode]) 
--values 
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Duplicate record check on' + @TableName+ @Table + 'test.';
Set @ExpectedResult = 'Duplicate record check on' + @TableName+ @Table  + @WHD +trim(str(@Zero));
Set @ActualResult ='Duplicate record check on' + @TableName+ @Table   + @WHD +trim(str(isNull(@MyRowcount,0)));
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage =@TC+ trim(str(@@IDENTITY))+ ' ' + 'Duplicate record check on' + @TableName+ @Table   + @WHD ;
Set @TestStatusCode = 1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end

-----------------------------------------------------------------
---------------------------------------------------


select @TestDescription=(Select case WHEN
(select count(TestStatus) 
from dbo.TestResults 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Staging_FINRA_License%'
and TestStatus = @Passed
) = (select count(TestStatus) 
from dbo.TestResults 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Staging_FINRA_License%'
)
THEN 'ALL TESTS PASSED!'
WHEN
(select count(TestStatus) 
from dbo.TestResults 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Staging_FINRA_License%'
and TestStatus in (@Passed,@Warning)
) = (select count(TestStatus) 
from dbo.TestResults 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Staging_FINRA_License%'
)
THEN 'ALL TESTS PASSED. SOME PASS WITH WARNING!'
ELSE 'ALL TESTS DID NOT PASS!' END)

insert dbo.TestLog values (@TestScenario,@EndingStatus,@TestDescription,SYSDATETIME(),System_User)

select
testStatus
,TestStatusCode
,count(TestId)"Count"
from
dbo.TestResults
where
TestDateTime > convert(Date,GetDate())
and
TestDescription LIKE '%Staging_FINRA_License%'
and
testid > @MaxTestID
group by TestStatus,TestStatusCode
order by TestStatusCode
select * from dbo.TestLog Where TestScenario LIKE '%Staging_FINRA_License%' 
--And TestExecutionTime > convert(Date,GetDate())
and TestLogId > @MaxTestLogID
Order by TestExecutionTime Desc

select
[TestId]
,[TestMessage]
,[TestDescription]
,[TestStatus]
,[ExpectedResult]
,[ActualResult]
,[TestDateTime]
,[TestStatusCode]
from
dbo.TestResults
where
TestDateTime > convert(Date,GetDate())
and
TestDescription LIKE '%Staging_FINRA_License%'
and
testid > @MaxTestID
order by TestStatusCode asc
---------------------------------------------------
