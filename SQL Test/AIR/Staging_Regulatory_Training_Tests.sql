Use DataIntegrationStage

declare @MaxTestID int; --Variable to caputure Last Test ID in TestResult Table
declare @TestAreaBeginning varchar(30) = 'air'

-- Test Name Declaration Starts Here --
declare @TableName varchar(100) = 'Staging_Regulatory_Training';
-- Test Name Declaration Ends Here --
declare @SchemaName varchar(25) = 'air';
-- Table Column Declaration Starts Here --

declare @RegulatoryTrainingId varchar(20) = 'RegulatoryTrainingId';
declare @CECompletedDate varchar(15) = 'CECompletedDate';
declare @CEAnniversaryDate varchar(17) = 'CEAnniversaryDate';
declare @CECutoffDate varchar(12) = 'CECutoffDate';
declare @ProcessedDatetime varchar(17) = 'ProcessedDatetime';
declare @InsertDateTime varchar(14) = 'InsertDateTime';
declare @ContactIdentifierReference varchar(26) = 'ContactIdentifierReference';
declare @CERegistrationStatus varchar(20) = 'CERegistrationStatus';
declare @ChangeType varchar(10) = 'ChangeType';


-- Table Column Declaration Ends Here --

--Fixed -- Do Not Change -- 
declare @CDNE varchar(75) = ' column does not exist.';
declare @CE varchar(25) = ' column exists.';
declare @TC varchar(25) = ' Test Case #';
declare @E varchar(25) = ' exists.'
declare @DNE varchar(25) = ' does not exist.';
declare @Table varchar(10) = 'Table';
Declare @Column varchar(25) = ' column ';
Declare @HTCT varchar(30) = 'has the correct type. ';
Declare @DNHTCT varchar(40) = 'does not have the correct type.';
declare @CLIC varchar(40) = ' column length is correct.';
declare @CLII varchar(40) = ' column length is incorrect.';
declare @INPWDT varchar(40) = ' is not populated with date time.';
declare @IPWDT varchar(40) = ' is populated with date time.';
declare @TCDNED varchar(40) = 'does not exist, damn it!';
declare @somearenull varchar(25) = ' some columns are null.';
declare @tasbpwvda varchar(255) = ' should be populated with valid data. ';
declare @sbpwagcir varchar(255) = ' should be populated with a good contact identifier reference.'
declare @somecolumnsblank varchar(50) = ' some columns are blank.';
declare @somerhbd varchar(50) = ' some rows have bad dates.';
declare @sciraor varchar(175) = ' some contact identifier reference are oprhaned records';
declare @cchav varchar(50) = 'changeType column has appropriate values.';
declare @ccdnhav varchar(50) = 'changeType column does not have appropriate values.';
DECLARE @MyTestDescription varchar(500);
Declare @RC int;
DECLARE @TestStatusCode int;
declare @NullCount int;
declare @NumBlank int;
declare @TypeVarChar int = 167;
declare @TypeDateTime int = 61;
declare @TypeDate int = 167;
declare @TypeInt int = 56;
declare @TypeBit int = 104;
declare @TypeBigInt int = 127;
declare @TypeNVarChar int = 231;
declare @TypeChar int = 175;
declare @TypeMoney int = 60;
declare @TypeDecimal int = 106;
declare @TypeTinyInt int = 48;
declare @Passed varchar(10) = 'Passed.';
declare @Failed varchar(10) = 'Failed!';
declare @Warning varchar(10) = 'Warning!';
declare @PrimaryKey varchar(100) = 'PrimaryKey';
declare @SystemTypeId int;
declare @LengthDateTime int = 8;
declare @LengthInt int = 4;
declare @Zero int = 0;
declare @One int = 1;
declare @Two int = 2;
declare @Three int = 3;
declare @Four int = 4;
declare @Five int = 5;
declare @Seven int = 7;
declare @Six int = 6;
declare @Eight int = 8;
declare @Nine int = 9;
declare @Ten int = 10;
declare @Twelve int = 12;
declare @Fifteen int = 15;
declare @Eighteen int = 18;
declare @Nineteen int = 19;
declare @Twenty int = 20;
declare @TwentyThree int = 23;
declare @TwentyFour int = 24;
declare @TwentyFive int = 25;
declare @Thirty int = 30;
declare @ThirtyTwo int = 32;
declare @ThirtyFive int =35;
declare @Forty int = 40;
declare @FortyOne int = 41;
declare @Fifty int = 50;
declare @Sixty int = 60;
declare @Eighty int = 80;
declare @Hundred int = 100;
declare @OneHundred int = 100;
declare @OneHundredThirtyThree int = 133;
declare @OneHundredTwentyFive int = 125;
declare @TwoHundred int = 200;
declare @TwoHundredFifty int = 250;
declare @TwoHundredFiftyFive int = 255;
declare @ThreeHundredFifty int = 350;
declare @FiveHundred int = 500;
declare @OneThousand int = 1000;
declare @ThirteenHundred int = 1300;
--declare @TestDescription varchar(200);
declare @ExpectedResult varchar(100);
declare @ActualResult varchar(100);
declare @TestStatus varchar(15);
declare @TestDateTime datetime;
declare @TestMessage varchar(200);
--declare @TableNameOne varchar(30) = 'staging_plan';
declare @Schema int = 17;
declare @Insert varchar(30) = 'I-insert';
Declare @Update varchar(30) = 'U-update';
Declare @Delete varchar(30) = 'D-delete';
declare @TableObject Int;
declare @ColumnObject Int;
declare @ColumnIsNullable Int;
declare @ICN varchar(30) = ' is column nullable.';
declare @TCIN varchar(40) = ' the column is nullable. ';
declare @TCINN varchar(50) = ' the column is not nullable. ';
declare @TCIPK varchar(50) = ' the column is a Primary Key. ';
declare @MaxLength Int;
declare @Return int;
declare @AllowsNull int;
declare @HasNull int;
declare @ColHasNull varchar(20) = ' column has null';
declare @ColHasNoNull varchar(20)= ' column has no null';
declare @HasBlank Bit;
declare @ColHasBlank varchar(25) = ' column has blank value '
declare @ColHasNoBlank varchar(25) = ' column has no blank value '
declare @HasBlankCount int;
--Declare @TypeDecimal int = 106;
Declare @MaxTestLogID int;
declare @LengthTinyInt int = 1;
declare @LengthDecimal int = 17;
declare @Eigthteen int = 18;
Declare @IsIdentity bit;
declare @TempTableName varchar(25) = '#testResults';
Declare @TestScenario varchar(80) = 'Staging_Regulatory_Training'
declare @BeginningStatus varchar(25) = 'Beginning';
Declare @EndingStatus varchar(25) = 'Ending';
Declare @TestDescription varchar(800) = 'Test the Staging_Regulatory_Training table and columns confirming the properties of the columns and some data verification.';
declare @p1 varchar(25) = 'Air';
declare @p2 varchar(25) = 'AccountMaster';


select @MaxTestID = max(Testid) from dbo.TestResults;

select @MaxTestLogID = max(TestLogId) from dbo.TestLog;

--Get the Schema ID for the table retrieval


select @Schema  = schema_id
from
sys.schemas
where
name = @p1;

--Get the TableObjectId for the table using the Table Name and the SchemaId
select @TableObject = [object_id]
	from
	sys.tables
	where
	name = @TableName
	and
	schema_id = @Schema;
	
select
@PrimaryKey = name
from
sys.all_columns
where
is_identity = 1
and
object_id = @TableObject;


insert TestLog values (@TestScenario,@BeginningStatus,@TestDescription,SYSDATETIME(),System_User)

If @TableObject is not null
begin
Set @TestDescription =	@TestAreaBeginning + @TableName + '' + @Table + ' exists test.';
Set @ExpectedResult =    'Table Exists.';
Set @ActualResult = 'Table Exists.';
Set @TestStatus =  @Passed;
Set @TestDateTime = SYSDATETIME();
Set @TestMessage = @TC + trim(str(@@IDENTITY)) +' '+@Table+' '+@TableName + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode
end
	else
begin	
Set @TestDescription =	@TestAreaBeginning + @TableName+ '' + @Table + ' exists test.';
set @ExpectedResult = 	'Table Exists.';
Set @ActualResult =	'Table object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage = 	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Table+' '+@TableName + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode
end
--------------------------------------------------

set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @RegulatoryTrainingId and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @RegulatoryTrainingId + ' exists test.';
Set @ExpectedResult =  @RegulatoryTrainingId + ' ' + @Column + ' ' +@E;
Set @ActualResult = @RegulatoryTrainingId + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @RegulatoryTrainingId + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @RegulatoryTrainingId + ' exists test.';
Set @ExpectedResult =  @RegulatoryTrainingId + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @RegulatoryTrainingId + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@RegulatoryTrainingId + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @CECompletedDate and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @CECompletedDate + ' exists test.';
Set @ExpectedResult =  @CECompletedDate + ' ' + @Column + ' ' +@E;
Set @ActualResult = @CECompletedDate + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @CECompletedDate + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @CECompletedDate + ' exists test.';
Set @ExpectedResult =  @CECompletedDate + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @CECompletedDate + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@CECompletedDate + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @CEAnniversaryDate and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @CEAnniversaryDate + ' exists test.';
Set @ExpectedResult =  @CEAnniversaryDate + ' ' + @Column + ' ' +@E;
Set @ActualResult = @CEAnniversaryDate + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @CEAnniversaryDate + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @CEAnniversaryDate + ' exists test.';
Set @ExpectedResult =  @CEAnniversaryDate + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @CEAnniversaryDate + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@CEAnniversaryDate + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @CECutoffDate and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @CECutoffDate + ' exists test.';
Set @ExpectedResult =  @CECutoffDate + ' ' + @Column + ' ' +@E;
Set @ActualResult = @CECutoffDate + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @CECutoffDate + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @CECutoffDate + ' exists test.';
Set @ExpectedResult =  @CECutoffDate + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @CECutoffDate + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@CECutoffDate + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @ProcessedDatetime and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @ProcessedDatetime + ' exists test.';
Set @ExpectedResult =  @ProcessedDatetime + ' ' + @Column + ' ' +@E;
Set @ActualResult = @ProcessedDatetime + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @ProcessedDatetime + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @ProcessedDatetime + ' exists test.';
Set @ExpectedResult =  @ProcessedDatetime + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @ProcessedDatetime + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@ProcessedDatetime + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @InsertDateTime and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @InsertDateTime + ' exists test.';
Set @ExpectedResult =  @InsertDateTime + ' ' + @Column + ' ' +@E;
Set @ActualResult = @InsertDateTime + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @InsertDateTime + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @InsertDateTime + ' exists test.';
Set @ExpectedResult =  @InsertDateTime + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @InsertDateTime + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@InsertDateTime + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @ContactIdentifierReference and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @ContactIdentifierReference + ' exists test.';
Set @ExpectedResult =  @ContactIdentifierReference + ' ' + @Column + ' ' +@E;
Set @ActualResult = @ContactIdentifierReference + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @ContactIdentifierReference + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @ContactIdentifierReference + ' exists test.';
Set @ExpectedResult =  @ContactIdentifierReference + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @ContactIdentifierReference + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@ContactIdentifierReference + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @CERegistrationStatus and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @CERegistrationStatus + ' exists test.';
Set @ExpectedResult =  @CERegistrationStatus + ' ' + @Column + ' ' +@E;
Set @ActualResult = @CERegistrationStatus + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @CERegistrationStatus + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @CERegistrationStatus + ' exists test.';
Set @ExpectedResult =  @CERegistrationStatus + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @CERegistrationStatus + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@CERegistrationStatus + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @ChangeType and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @ChangeType + ' exists test.';
Set @ExpectedResult =  @ChangeType + ' ' + @Column + ' ' +@E;
Set @ActualResult = @ChangeType + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @ChangeType + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @ChangeType + ' exists test.';
Set @ExpectedResult =  @ChangeType + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @ChangeType + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@ChangeType + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @RegulatoryTrainingId	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @RegulatoryTrainingId; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @RegulatoryTrainingId + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeInt =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @RegulatoryTrainingId;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @RegulatoryTrainingId + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @RegulatoryTrainingId + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @RegulatoryTrainingId + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @CECompletedDate	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @CECompletedDate; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @CECompletedDate + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeDateTime =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @CECompletedDate;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @CECompletedDate + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @CECompletedDate + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @CECompletedDate + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @CEAnniversaryDate	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @CEAnniversaryDate; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @CEAnniversaryDate + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeDateTime =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @CEAnniversaryDate;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @CEAnniversaryDate + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @CEAnniversaryDate + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @CEAnniversaryDate + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @CECutoffDate	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @CECutoffDate; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @CECutoffDate + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeDateTime =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @CECutoffDate;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @CECutoffDate + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @CECutoffDate + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @CECutoffDate + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @ProcessedDatetime	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @ProcessedDatetime; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @ProcessedDatetime + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeDateTime =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ProcessedDatetime;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ProcessedDatetime + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ProcessedDatetime + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ProcessedDatetime + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @InsertDateTime	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @InsertDateTime; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @InsertDateTime + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeDateTime =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @InsertDateTime;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @InsertDateTime + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @InsertDateTime + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @InsertDateTime + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @ContactIdentifierReference	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @ContactIdentifierReference; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @ContactIdentifierReference + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ContactIdentifierReference;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ContactIdentifierReference + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ContactIdentifierReference + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ContactIdentifierReference + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @CERegistrationStatus	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @CERegistrationStatus; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @CERegistrationStatus + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @CERegistrationStatus;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @CERegistrationStatus + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @CERegistrationStatus + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @CERegistrationStatus + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @ChangeType	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @ChangeType; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @ChangeType + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ChangeType;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ChangeType + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ChangeType + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ChangeType + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@RegulatoryTrainingId,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @RegulatoryTrainingId + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @RegulatoryTrainingId + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@RegulatoryTrainingId +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @RegulatoryTrainingId + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@RegulatoryTrainingId +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @RegulatoryTrainingId + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@CECompletedDate,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @CECompletedDate + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @CECompletedDate + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@CECompletedDate +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @CECompletedDate + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@CECompletedDate +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @CECompletedDate + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@CEAnniversaryDate,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @CEAnniversaryDate + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @CEAnniversaryDate + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@CEAnniversaryDate +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @CEAnniversaryDate + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@CEAnniversaryDate +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @CEAnniversaryDate + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@CECutoffDate,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @CECutoffDate + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @CECutoffDate + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@CECutoffDate +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @CECutoffDate + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@CECutoffDate +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @CECutoffDate + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@ProcessedDatetime,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @ProcessedDatetime + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@InsertDateTime,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @InsertDateTime + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@ContactIdentifierReference,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @ContactIdentifierReference + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@CERegistrationStatus,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @CERegistrationStatus + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @CERegistrationStatus + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@CERegistrationStatus +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @CERegistrationStatus + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@CERegistrationStatus +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @CERegistrationStatus + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@ChangeType,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @ChangeType + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @ChangeType + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @One
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @ChangeType + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

Select @AllowsNull = COLUMNPROPERTY(@TableObject,@RegulatoryTrainingId,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Regulatory_Training WHERE @RegulatoryTrainingId IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@RegulatoryTrainingId +  ' Has nullable test.';
Set @ExpectedResult =  @RegulatoryTrainingId+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @RegulatoryTrainingId+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @RegulatoryTrainingId + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@RegulatoryTrainingId +  ' Has nullable test.'; 
Set @ExpectedResult =  @RegulatoryTrainingId+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @RegulatoryTrainingId+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @RegulatoryTrainingId + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@RegulatoryTrainingId +  ' Has nullable test.';
Set @ExpectedResult =     @RegulatoryTrainingId+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @RegulatoryTrainingId+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @RegulatoryTrainingId + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@RegulatoryTrainingId +  ' Has nullable test.';
Set @ExpectedResult =     @RegulatoryTrainingId+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @RegulatoryTrainingId+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @RegulatoryTrainingId + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@CECompletedDate,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Regulatory_Training WHERE @CECompletedDate IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@CECompletedDate +  ' Has nullable test.';
Set @ExpectedResult =  @CECompletedDate+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @CECompletedDate+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @CECompletedDate + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@CECompletedDate +  ' Has nullable test.'; 
Set @ExpectedResult =  @CECompletedDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @CECompletedDate+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @CECompletedDate + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@CECompletedDate +  ' Has nullable test.';
Set @ExpectedResult =     @CECompletedDate+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @CECompletedDate+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @CECompletedDate + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@CECompletedDate +  ' Has nullable test.';
Set @ExpectedResult =     @CECompletedDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @CECompletedDate+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @CECompletedDate + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@CEAnniversaryDate,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Regulatory_Training WHERE @CEAnniversaryDate IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@CEAnniversaryDate +  ' Has nullable test.';
Set @ExpectedResult =  @CEAnniversaryDate+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @CEAnniversaryDate+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @CEAnniversaryDate + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@CEAnniversaryDate +  ' Has nullable test.'; 
Set @ExpectedResult =  @CEAnniversaryDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @CEAnniversaryDate+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @CEAnniversaryDate + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@CEAnniversaryDate +  ' Has nullable test.';
Set @ExpectedResult =     @CEAnniversaryDate+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @CEAnniversaryDate+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @CEAnniversaryDate + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@CEAnniversaryDate +  ' Has nullable test.';
Set @ExpectedResult =     @CEAnniversaryDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @CEAnniversaryDate+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @CEAnniversaryDate + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@CECutoffDate,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Regulatory_Training WHERE @CECutoffDate IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@CECutoffDate +  ' Has nullable test.';
Set @ExpectedResult =  @CECutoffDate+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @CECutoffDate+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @CECutoffDate + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@CECutoffDate +  ' Has nullable test.'; 
Set @ExpectedResult =  @CECutoffDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @CECutoffDate+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @CECutoffDate + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@CECutoffDate +  ' Has nullable test.';
Set @ExpectedResult =     @CECutoffDate+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @CECutoffDate+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @CECutoffDate + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@CECutoffDate +  ' Has nullable test.';
Set @ExpectedResult =     @CECutoffDate+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @CECutoffDate+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @CECutoffDate + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@ProcessedDatetime,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Regulatory_Training WHERE @ProcessedDatetime IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has nullable test.';
Set @ExpectedResult =  @ProcessedDatetime+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @ProcessedDatetime+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has nullable test.'; 
Set @ExpectedResult =  @ProcessedDatetime+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @ProcessedDatetime+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has nullable test.';
Set @ExpectedResult =     @ProcessedDatetime+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @ProcessedDatetime+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has nullable test.';
Set @ExpectedResult =     @ProcessedDatetime+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @ProcessedDatetime+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@InsertDateTime,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Regulatory_Training WHERE @InsertDateTime IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has nullable test.';
Set @ExpectedResult =  @InsertDateTime+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @InsertDateTime+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has nullable test.'; 
Set @ExpectedResult =  @InsertDateTime+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @InsertDateTime+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has nullable test.';
Set @ExpectedResult =     @InsertDateTime+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @InsertDateTime+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has nullable test.';
Set @ExpectedResult =     @InsertDateTime+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @InsertDateTime+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@ContactIdentifierReference,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Regulatory_Training WHERE @ContactIdentifierReference IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' Has nullable test.';
Set @ExpectedResult =  @ContactIdentifierReference+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @ContactIdentifierReference+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' Has nullable test.'; 
Set @ExpectedResult =  @ContactIdentifierReference+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @ContactIdentifierReference+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' Has nullable test.';
Set @ExpectedResult =     @ContactIdentifierReference+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @ContactIdentifierReference+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' Has nullable test.';
Set @ExpectedResult =     @ContactIdentifierReference+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @ContactIdentifierReference+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@CERegistrationStatus,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Regulatory_Training WHERE @CERegistrationStatus IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@CERegistrationStatus +  ' Has nullable test.';
Set @ExpectedResult =  @CERegistrationStatus+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @CERegistrationStatus+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @CERegistrationStatus + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@CERegistrationStatus +  ' Has nullable test.'; 
Set @ExpectedResult =  @CERegistrationStatus+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @CERegistrationStatus+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @CERegistrationStatus + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@CERegistrationStatus +  ' Has nullable test.';
Set @ExpectedResult =     @CERegistrationStatus+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @CERegistrationStatus+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @CERegistrationStatus + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@CERegistrationStatus +  ' Has nullable test.';
Set @ExpectedResult =     @CERegistrationStatus+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @CERegistrationStatus+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @CERegistrationStatus + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@ChangeType,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Regulatory_Training WHERE @ChangeType IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has nullable test.';
Set @ExpectedResult =  @ChangeType+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @ChangeType+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has nullable test.'; 
Set @ExpectedResult =  @ChangeType+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @ChangeType+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has nullable test.';
Set @ExpectedResult =     @ChangeType+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @ChangeType+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has nullable test.';
Set @ExpectedResult =     @ChangeType+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @ChangeType+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Regulatory_Training where @RegulatoryTrainingId = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@RegulatoryTrainingId +  ' Has blank value test.'; 
Set @ExpectedResult = @RegulatoryTrainingId+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @RegulatoryTrainingId+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @RegulatoryTrainingId + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@RegulatoryTrainingId +  ' Has blank value test.'; 
Set @ExpectedResult = @RegulatoryTrainingId+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @RegulatoryTrainingId+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @RegulatoryTrainingId + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Regulatory_Training where @CECompletedDate = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@CECompletedDate +  ' Has blank value test.'; 
Set @ExpectedResult = @CECompletedDate+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @CECompletedDate+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @CECompletedDate + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@CECompletedDate +  ' Has blank value test.'; 
Set @ExpectedResult = @CECompletedDate+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @CECompletedDate+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @CECompletedDate + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Regulatory_Training where @CEAnniversaryDate = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@CEAnniversaryDate +  ' Has blank value test.'; 
Set @ExpectedResult = @CEAnniversaryDate+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @CEAnniversaryDate+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @CEAnniversaryDate + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@CEAnniversaryDate +  ' Has blank value test.'; 
Set @ExpectedResult = @CEAnniversaryDate+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @CEAnniversaryDate+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @CEAnniversaryDate + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Regulatory_Training where @CECutoffDate = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@CECutoffDate +  ' Has blank value test.'; 
Set @ExpectedResult = @CECutoffDate+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @CECutoffDate+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @CECutoffDate + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@CECutoffDate +  ' Has blank value test.'; 
Set @ExpectedResult = @CECutoffDate+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @CECutoffDate+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @CECutoffDate + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Regulatory_Training where @ProcessedDatetime = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has blank value test.'; 
Set @ExpectedResult = @ProcessedDatetime+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @ProcessedDatetime+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has blank value test.'; 
Set @ExpectedResult = @ProcessedDatetime+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @ProcessedDatetime+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Regulatory_Training where @InsertDateTime = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has blank value test.'; 
Set @ExpectedResult = @InsertDateTime+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @InsertDateTime+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has blank value test.'; 
Set @ExpectedResult = @InsertDateTime+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @InsertDateTime+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Regulatory_Training where @ContactIdentifierReference = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' Has blank value test.'; 
Set @ExpectedResult = @ContactIdentifierReference+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @ContactIdentifierReference+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ContactIdentifierReference +  ' Has blank value test.'; 
Set @ExpectedResult = @ContactIdentifierReference+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @ContactIdentifierReference+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ContactIdentifierReference + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Regulatory_Training where @CERegistrationStatus = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@CERegistrationStatus +  ' Has blank value test.'; 
Set @ExpectedResult = @CERegistrationStatus+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @CERegistrationStatus+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @CERegistrationStatus + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@CERegistrationStatus +  ' Has blank value test.'; 
Set @ExpectedResult = @CERegistrationStatus+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @CERegistrationStatus+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @CERegistrationStatus + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Regulatory_Training where @ChangeType = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has blank value test.'; 
Set @ExpectedResult = @ChangeType+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @ChangeType+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has blank value test.'; 
Set @ExpectedResult = @ChangeType+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @ChangeType+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Regulatory_Training where RegulatoryTrainingId = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'RegulatoryTrainingId' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'RegulatoryTrainingId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'RegulatoryTrainingId' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'RegulatoryTrainingId' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'RegulatoryTrainingId' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'RegulatoryTrainingId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'RegulatoryTrainingId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'RegulatoryTrainingId' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Regulatory_Training where CECompletedDate = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'CECompletedDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'CECompletedDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'CECompletedDate' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CECompletedDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'CECompletedDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'CECompletedDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'CECompletedDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CECompletedDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Regulatory_Training where CEAnniversaryDate = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'CEAnniversaryDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'CEAnniversaryDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'CEAnniversaryDate' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CEAnniversaryDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'CEAnniversaryDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'CEAnniversaryDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'CEAnniversaryDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CEAnniversaryDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Regulatory_Training where CECutoffDate = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'CECutoffDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'CECutoffDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'CECutoffDate' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CECutoffDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'CECutoffDate' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'CECutoffDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'CECutoffDate' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CECutoffDate' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Regulatory_Training where ProcessedDatetime = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ProcessedDatetime' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ProcessedDatetime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ProcessedDatetime' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ProcessedDatetime' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'ProcessedDatetime' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ProcessedDatetime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ProcessedDatetime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ProcessedDatetime' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Regulatory_Training where InsertDateTime = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'InsertDateTime' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'InsertDateTime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'InsertDateTime' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsertDateTime' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'InsertDateTime' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'InsertDateTime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'InsertDateTime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsertDateTime' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Regulatory_Training where ContactIdentifierReference = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ContactIdentifierReference' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ContactIdentifierReference' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ContactIdentifierReference' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ContactIdentifierReference' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'ContactIdentifierReference' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ContactIdentifierReference' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ContactIdentifierReference' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ContactIdentifierReference' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Regulatory_Training where CERegistrationStatus = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'CERegistrationStatus' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'CERegistrationStatus' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'CERegistrationStatus' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CERegistrationStatus' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'CERegistrationStatus' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'CERegistrationStatus' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'CERegistrationStatus' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CERegistrationStatus' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Regulatory_Training where ChangeType = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ChangeType' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ChangeType' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ChangeType' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ChangeType' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'ChangeType' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ChangeType' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ChangeType' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ChangeType' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Regulatory_Training where RegulatoryTrainingId is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'RegulatoryTrainingId' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'RegulatoryTrainingId' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'RegulatoryTrainingId' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'RegulatoryTrainingId' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'RegulatoryTrainingId' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'RegulatoryTrainingId' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'RegulatoryTrainingId' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'RegulatoryTrainingId' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Regulatory_Training where CECompletedDate is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'CECompletedDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'CECompletedDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'CECompletedDate' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CECompletedDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'CECompletedDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'CECompletedDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'CECompletedDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CECompletedDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Regulatory_Training where CEAnniversaryDate is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'CEAnniversaryDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'CEAnniversaryDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'CEAnniversaryDate' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CEAnniversaryDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'CEAnniversaryDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'CEAnniversaryDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'CEAnniversaryDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CEAnniversaryDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Regulatory_Training where CECutoffDate is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'CECutoffDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'CECutoffDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'CECutoffDate' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CECutoffDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'CECutoffDate' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'CECutoffDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'CECutoffDate' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CECutoffDate' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Regulatory_Training where ProcessedDatetime is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ProcessedDatetime' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ProcessedDatetime' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ProcessedDatetime' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ProcessedDatetime' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ProcessedDatetime' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ProcessedDatetime' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ProcessedDatetime' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ProcessedDatetime' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Regulatory_Training where InsertDateTime is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'InsertDateTime' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'InsertDateTime' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'InsertDateTime' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsertDateTime' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'InsertDateTime' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'InsertDateTime' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'InsertDateTime' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsertDateTime' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Regulatory_Training where ContactIdentifierReference is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ContactIdentifierReference' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ContactIdentifierReference' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ContactIdentifierReference' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ContactIdentifierReference' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ContactIdentifierReference' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ContactIdentifierReference' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ContactIdentifierReference' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ContactIdentifierReference' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Regulatory_Training where CERegistrationStatus is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'CERegistrationStatus' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'CERegistrationStatus' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'CERegistrationStatus' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CERegistrationStatus' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'CERegistrationStatus' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'CERegistrationStatus' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'CERegistrationStatus' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'CERegistrationStatus' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Regulatory_Training where ChangeType is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ChangeType' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ChangeType' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ChangeType' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ChangeType' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ChangeType' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ChangeType' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ChangeType' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ChangeType' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
---------------------------------------------------
IF (SELECT 
    Count(*) FROM [air].[Staging_Firm_Training]
	Where ContactIdentifierReference not in (SELECT
      Distinct([ContactIdentifier])
	FROM [DataIntegrationStage].[air].[Staging_Contact]))>0
begin
--Select 'All Regulatory Training records NOT associated to the Contacts defined on the Contact template' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' BR 039 '+ 'All Regulatory Training records must be associated to the Contacts defined on the Contact template ' + 'test.';
Set @ExpectedResult = 'All Regulatory Training records is associated to the Contacts defined on the Contact template';
Set @ActualResult = 'All Regulatory Training records are NOT associated to the Contacts defined on the Contact template';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' BR 039 ' + 'All Regulatory Training records is associated to the Contacts defined on the Contact template' ;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Else
begin
--Select 'All Regulatory Training records is associated to the Contacts defined on the Contact template' Pass

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' BR 039 '+ 'All Regulatory Training records must be associated to the Contacts defined on the Contact template ' + 'test.';
Set @ExpectedResult = 'All Regulatory Training records is associated to the Contacts defined on the Contact template';
Set @ActualResult = 'All Regulatory Training records is associated to the Contacts defined on the Contact template';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' BR 039 ' + 'All Regulatory Training records is associated to the Contacts defined on the Contact template' ;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
-----------------------------------------------------------------
---------------------------------------------------

select @TestDescription=(Select case WHEN
(select count(TestStatus) 
from dbo.TestResults 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Staging_Regulatory_Training%'
and TestStatus = @Passed
) = (select count(TestStatus) 
from dbo.TestResults 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Staging_Regulatory_Training%'
)
THEN 'ALL TESTS PASSED!'
WHEN
(select count(TestStatus) 
from dbo.TestResults 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Staging_Regulatory_Training%'
and TestStatus in (@Passed,@Warning)
) = (select count(TestStatus) 
from dbo.TestResults 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Staging_Regulatory_Training%'
)
THEN 'ALL TESTS PASSED. SOME PASS WITH WARNING!'
ELSE 'ALL TESTS DID NOT PASS!' END)

insert dbo.TestLog values (@TestScenario,@EndingStatus,@TestDescription,SYSDATETIME(),System_User)

select
testStatus
,TestStatusCode
,count(TestId)"Count"
from
dbo.TestResults
where
TestDateTime > convert(Date,GetDate())
and
TestDescription LIKE '%Staging_Regulatory_Training%'
and
testid > @MaxTestID
group by TestStatus,TestStatusCode
order by TestStatusCode
select * from dbo.TestLog Where TestScenario LIKE '%Staging_Regulatory_Training%' 
--And TestExecutionTime > convert(Date,GetDate())
and TestLogId > @MaxTestLogID
Order by TestExecutionTime Desc

select
[TestId]
,[TestMessage]
,[TestDescription]
,[TestStatus]
,[ExpectedResult]
,[ActualResult]
,[TestDateTime]
,[TestStatusCode]
from
dbo.TestResults
where
TestDateTime > convert(Date,GetDate())
and
TestDescription LIKE '%Staging_Regulatory_Training%'
and
testid > @MaxTestID
order by TestStatusCode asc
---------------------------------------------------
