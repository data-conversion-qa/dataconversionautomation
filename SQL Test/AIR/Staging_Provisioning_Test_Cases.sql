Use DataIntegrationStage

declare @MaxTestID int; --Variable to caputure Last Test ID in TestResult Table
declare @TestAreaBeginning varchar(30) = 'air'

-- Test Name Declaration Starts Here --
declare @TableName varchar(100) = 'Staging_Provisioning';
-- Test Name Declaration Ends Here --
declare @SchemaName varchar(25) = 'air';
-- Table Column Declaration Starts Here --

declare @contactIdentifier varchar(17) = 'contactIdentifier';
declare @loginId varchar(7) = 'loginId';
declare @wealthscapeId varchar(13) = 'wealthscapeId';
declare @crmPrimaryBusUnit varchar(17) = 'crmPrimaryBusUnit';
declare @crmSecondaryBusUnit varchar(19) = 'crmSecondaryBusUnit';
declare @casperRoles varchar(11) = 'casperRoles';
declare @status varchar(6) = 'status';
declare @changeType varchar(10) = 'changeType';
declare @processedDatetime varchar(17) = 'processedDatetime';


-- Table Column Declaration Ends Here --

--Fixed -- Do Not Change -- 
declare @MyOrphanCount int;
declare @WHOR varchar(40) = 'We have orphaned records! ';
Declare @WDNHOR varchar(40) = 'We do not have orphaned records. '
declare @MyRowcount int;
declare @WHD varchar(30) = 'We have duplicates! ';
declare @WDHD varchar(40) = 'We do not have duplicates. ';
declare @CDNE varchar(75) = ' column does not exist.';
declare @CE varchar(25) = ' column exists.';
declare @TC varchar(25) = ' Test Case #';
declare @E varchar(25) = ' exists.'
declare @DNE varchar(25) = ' does not exist.';
declare @Table varchar(10) = 'Table';
Declare @Column varchar(25) = ' column ';
Declare @HTCT varchar(30) = 'has the correct type. ';
Declare @DNHTCT varchar(40) = 'does not have the correct type.';
declare @CLIC varchar(40) = ' column length is correct.';
declare @CLII varchar(40) = ' column length is incorrect.';
declare @INPWDT varchar(40) = ' is not populated with date time.';
declare @IPWDT varchar(40) = ' is populated with date time.';
declare @TCDNED varchar(40) = 'does not exist, damn it!';
declare @somearenull varchar(25) = ' some columns are null.';
declare @tasbpwvda varchar(255) = ' should be populated with valid data. ';
declare @sbpwagcir varchar(255) = ' should be populated with a good contact identifier reference.'
declare @somecolumnsblank varchar(50) = ' some columns are blank.';
declare @somerhbd varchar(50) = ' some rows have bad dates.';
declare @sciraor varchar(175) = ' some contact identifier reference are oprhaned records';
declare @cchav varchar(50) = 'changeType column has appropriate values.';
declare @ccdnhav varchar(50) = 'changeType column does not have appropriate values.';
DECLARE @MyTestDescription varchar(500);
Declare @RC int;
DECLARE @TestStatusCode int;
declare @NullCount int;
declare @NumBlank int;
declare @TypeVarChar int = 167;
declare @TypeDateTime int = 61;
declare @TypeDate int = 167;
declare @TypeInt int = 56;
declare @TypeBit int = 104;
declare @TypeBigInt int = 127;
declare @TypeNVarChar int = 231;
declare @TypeChar int = 175;
declare @TypeMoney int = 60;
declare @TypeDecimal int = 106;
declare @TypeTinyInt int = 48;
declare @Passed varchar(10) = 'Passed.';
declare @Failed varchar(10) = 'Failed!';
declare @Warning varchar(10) = 'Warning!';
declare @PrimaryKey varchar(100) = 'PrimaryKey';
declare @SystemTypeId int;
declare @LengthDateTime int = 8;
declare @LengthInt int = 4;
declare @Zero int = 0;
declare @One int = 1;
declare @Two int = 2;
declare @Three int = 3;
declare @Four int = 4;
declare @Five int = 5;
declare @Seven int = 7;
declare @Six int = 6;
declare @Eight int = 8;
declare @Nine int = 9;
declare @Ten int = 10;
declare @Twelve int = 12;
declare @Fifteen int = 15;
declare @Eighteen int = 18;
declare @Nineteen int = 19;
declare @Twenty int = 20;
declare @TwentyThree int = 23;
declare @TwentyFour int = 24;
declare @TwentyFive int = 25;
declare @Thirty int = 30;
declare @ThirtyTwo int = 32;
declare @ThirtyFive int =35;
declare @Forty int = 40;
declare @FortyOne int = 41;
declare @Fifty int = 50;
declare @Sixty int = 60;
declare @Eighty int = 80;
declare @Hundred int = 100;
declare @OneHundred int = 100;
declare @OneHundredThirtyThree int = 133;
declare @OneHundredTwentyFive int = 125;
declare @TwoHundred int = 200;
declare @TwoHundredFifty int = 250;
declare @TwoHundredFiftyFive int = 255;
declare @ThreeHundredFifty int = 350;
declare @FiveHundred int = 500;
declare @OneThousand int = 1000;
declare @ThirteenHundred int = 1300;
--declare @TestDescription varchar(200);
declare @ExpectedResult varchar(100);
declare @ActualResult varchar(100);
declare @TestStatus varchar(15);
declare @TestDateTime datetime;
declare @TestMessage varchar(200);
--declare @TableNameOne varchar(30) = 'staging_plan';
declare @Schema int = 17;
declare @Insert varchar(30) = 'I-insert';
Declare @Update varchar(30) = 'U-update';
Declare @Delete varchar(30) = 'D-delete';
declare @TableObject Int;
declare @ColumnObject Int;
declare @ColumnIsNullable Int;
declare @ICN varchar(30) = ' is column nullable.';
declare @TCIN varchar(40) = ' the column is nullable. ';
declare @TCINN varchar(50) = ' the column is not nullable. ';
declare @TCIPK varchar(50) = ' the column is a Primary Key. ';
declare @MaxLength Int;
declare @Return int;
declare @AllowsNull int;
declare @HasNull int;
declare @ColHasNull varchar(20) = ' column has null';
declare @ColHasNoNull varchar(20)= ' column has no null';
declare @HasBlank Bit;
declare @ColHasBlank varchar(25) = ' column has blank value '
declare @ColHasNoBlank varchar(25) = ' column has no blank value '
declare @HasBlankCount int;
--Declare @TypeDecimal int = 106;
Declare @MaxTestLogID int;
declare @LengthTinyInt int = 1;
declare @LengthDecimal int = 17;
declare @Eigthteen int = 18;
Declare @IsIdentity bit;
declare @TempTableName varchar(25) = '#testResults';
Declare @TestScenario varchar(80) = 'Staging_Provisioning'
declare @BeginningStatus varchar(25) = 'Beginning';
Declare @EndingStatus varchar(25) = 'Ending';
Declare @TestDescription varchar(800) = 'Test the Staging_Provisioning table and columns confirming the properties of the columns and some data verification.';
declare @p1 varchar(25) = 'Air';
declare @p2 varchar(25) = 'AccountMaster';


select @MaxTestID = max(Testid) from dbo.TestResults;

select @MaxTestLogID = max(TestLogId) from dbo.TestLog;

--Get the Schema ID for the table retrieval


select @Schema  = schema_id
from
sys.schemas
where
name = @p1;

--Get the TableObjectId for the table using the Table Name and the SchemaId
select @TableObject = [object_id]
	from
	sys.tables
	where
	name = @TableName
	and
	schema_id = @Schema;
	
select
@PrimaryKey = name
from
sys.all_columns
where
is_identity = 1
and
object_id = @TableObject;


insert TestLog values (@TestScenario,@BeginningStatus,@TestDescription,SYSDATETIME(),System_User)

If @TableObject is not null
begin
Set @TestDescription =	@TestAreaBeginning + @TableName + '' + @Table + ' exists test.';
Set @ExpectedResult =    'Table Exists.';
Set @ActualResult = 'Table Exists.';
Set @TestStatus =  @Passed;
Set @TestDateTime = SYSDATETIME();
Set @TestMessage = @TC + trim(str(@@IDENTITY)) +' '+@Table+' '+@TableName + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode
end
	else
begin	
Set @TestDescription =	@TestAreaBeginning + @TableName+ '' + @Table + ' exists test.';
set @ExpectedResult = 	'Table Exists.';
Set @ActualResult =	'Table object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage = 	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Table+' '+@TableName + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode
end
--------------------------------------------------

set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @contactIdentifier and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @contactIdentifier + ' exists test.';
Set @ExpectedResult =  @contactIdentifier + ' ' + @Column + ' ' +@E;
Set @ActualResult = @contactIdentifier + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @contactIdentifier + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @contactIdentifier + ' exists test.';
Set @ExpectedResult =  @contactIdentifier + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @contactIdentifier + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@contactIdentifier + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @loginId and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @loginId + ' exists test.';
Set @ExpectedResult =  @loginId + ' ' + @Column + ' ' +@E;
Set @ActualResult = @loginId + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @loginId + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @loginId + ' exists test.';
Set @ExpectedResult =  @loginId + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @loginId + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@loginId + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @wealthscapeId and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @wealthscapeId + ' exists test.';
Set @ExpectedResult =  @wealthscapeId + ' ' + @Column + ' ' +@E;
Set @ActualResult = @wealthscapeId + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @wealthscapeId + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @wealthscapeId + ' exists test.';
Set @ExpectedResult =  @wealthscapeId + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @wealthscapeId + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@wealthscapeId + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @crmPrimaryBusUnit and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @crmPrimaryBusUnit + ' exists test.';
Set @ExpectedResult =  @crmPrimaryBusUnit + ' ' + @Column + ' ' +@E;
Set @ActualResult = @crmPrimaryBusUnit + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @crmPrimaryBusUnit + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @crmPrimaryBusUnit + ' exists test.';
Set @ExpectedResult =  @crmPrimaryBusUnit + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @crmPrimaryBusUnit + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@crmPrimaryBusUnit + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @crmSecondaryBusUnit and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @crmSecondaryBusUnit + ' exists test.';
Set @ExpectedResult =  @crmSecondaryBusUnit + ' ' + @Column + ' ' +@E;
Set @ActualResult = @crmSecondaryBusUnit + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @crmSecondaryBusUnit + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @crmSecondaryBusUnit + ' exists test.';
Set @ExpectedResult =  @crmSecondaryBusUnit + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @crmSecondaryBusUnit + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@crmSecondaryBusUnit + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @casperRoles and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @casperRoles + ' exists test.';
Set @ExpectedResult =  @casperRoles + ' ' + @Column + ' ' +@E;
Set @ActualResult = @casperRoles + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @casperRoles + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @casperRoles + ' exists test.';
Set @ExpectedResult =  @casperRoles + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @casperRoles + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@casperRoles + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @status and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @status + ' exists test.';
Set @ExpectedResult =  @status + ' ' + @Column + ' ' +@E;
Set @ActualResult = @status + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @status + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @status + ' exists test.';
Set @ExpectedResult =  @status + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @status + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@status + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @changeType and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @changeType + ' exists test.';
Set @ExpectedResult =  @changeType + ' ' + @Column + ' ' +@E;
Set @ActualResult = @changeType + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @changeType + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @changeType + ' exists test.';
Set @ExpectedResult =  @changeType + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @changeType + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@changeType + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @processedDatetime and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @processedDatetime + ' exists test.';
Set @ExpectedResult =  @processedDatetime + ' ' + @Column + ' ' +@E;
Set @ActualResult = @processedDatetime + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @processedDatetime + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @processedDatetime + ' exists test.';
Set @ExpectedResult =  @processedDatetime + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @processedDatetime + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@processedDatetime + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @contactIdentifier	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @contactIdentifier; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @contactIdentifier + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @contactIdentifier;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @contactIdentifier + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @contactIdentifier + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @contactIdentifier + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @loginId	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @loginId; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @loginId + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @loginId;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @loginId + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @loginId + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @loginId + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @wealthscapeId	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @wealthscapeId; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @wealthscapeId + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @wealthscapeId;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @wealthscapeId + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @wealthscapeId + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @wealthscapeId + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @crmPrimaryBusUnit	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @crmPrimaryBusUnit; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @crmPrimaryBusUnit + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @crmPrimaryBusUnit;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @crmPrimaryBusUnit + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @crmPrimaryBusUnit + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @crmPrimaryBusUnit + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @crmSecondaryBusUnit	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @crmSecondaryBusUnit; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @crmSecondaryBusUnit + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @crmSecondaryBusUnit;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @crmSecondaryBusUnit + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @crmSecondaryBusUnit + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @crmSecondaryBusUnit + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @casperRoles	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @casperRoles; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @casperRoles + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @casperRoles;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @casperRoles + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @casperRoles + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @casperRoles + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @status	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @status; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @status + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @status;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @status + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @status + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @status + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @changeType	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @changeType; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @changeType + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @changeType;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @changeType + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @changeType + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @changeType + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @processedDatetime	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @processedDatetime; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @processedDatetime + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeDateTime =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @processedDatetime;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @processedDatetime + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @processedDatetime + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @processedDatetime + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@contactIdentifier,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @contactIdentifier + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @contactIdentifier + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@contactIdentifier +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @contactIdentifier + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@contactIdentifier +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @contactIdentifier + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@loginId,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @loginId + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @loginId + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @OneHundred
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@loginId +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @loginId + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@loginId +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @loginId + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@wealthscapeId,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @wealthscapeId + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @wealthscapeId + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Twenty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@wealthscapeId +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @wealthscapeId + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@wealthscapeId +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @wealthscapeId + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@crmPrimaryBusUnit,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @crmPrimaryBusUnit + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @crmPrimaryBusUnit + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @OneHundred
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@crmPrimaryBusUnit +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @crmPrimaryBusUnit + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@crmPrimaryBusUnit +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @crmPrimaryBusUnit + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@crmSecondaryBusUnit,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @crmSecondaryBusUnit + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @crmSecondaryBusUnit + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @OneThousand
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@crmSecondaryBusUnit +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @crmSecondaryBusUnit + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@crmSecondaryBusUnit +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @crmSecondaryBusUnit + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@casperRoles,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @casperRoles + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @casperRoles + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @OneThousand
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@casperRoles +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @casperRoles + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@casperRoles +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @casperRoles + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@status,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @status + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @status + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@status +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @status + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@status +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @status + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@changeType,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @changeType + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @changeType + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @One
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@changeType +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @changeType + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@changeType +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @changeType + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

Select @AllowsNull = COLUMNPROPERTY(@TableObject,@contactIdentifier,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Provisioning WHERE @contactIdentifier IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@contactIdentifier +  ' Has nullable test.';
Set @ExpectedResult =  @contactIdentifier+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @contactIdentifier+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @contactIdentifier + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@contactIdentifier +  ' Has nullable test.'; 
Set @ExpectedResult =  @contactIdentifier+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @contactIdentifier+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @contactIdentifier + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@contactIdentifier +  ' Has nullable test.';
Set @ExpectedResult =     @contactIdentifier+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @contactIdentifier+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @contactIdentifier + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@contactIdentifier +  ' Has nullable test.';
Set @ExpectedResult =     @contactIdentifier+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @contactIdentifier+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @contactIdentifier + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@loginId,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Provisioning WHERE @loginId IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@loginId +  ' Has nullable test.';
Set @ExpectedResult =  @loginId+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @loginId+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loginId + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@loginId +  ' Has nullable test.'; 
Set @ExpectedResult =  @loginId+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @loginId+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @loginId + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@loginId +  ' Has nullable test.';
Set @ExpectedResult =     @loginId+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @loginId+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @loginId + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@loginId +  ' Has nullable test.';
Set @ExpectedResult =     @loginId+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @loginId+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @loginId + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@wealthscapeId,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Provisioning WHERE @wealthscapeId IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@wealthscapeId +  ' Has nullable test.';
Set @ExpectedResult =  @wealthscapeId+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @wealthscapeId+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @wealthscapeId + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@wealthscapeId +  ' Has nullable test.'; 
Set @ExpectedResult =  @wealthscapeId+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @wealthscapeId+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @wealthscapeId + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@wealthscapeId +  ' Has nullable test.';
Set @ExpectedResult =     @wealthscapeId+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @wealthscapeId+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @wealthscapeId + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@wealthscapeId +  ' Has nullable test.';
Set @ExpectedResult =     @wealthscapeId+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @wealthscapeId+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @wealthscapeId + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@crmPrimaryBusUnit,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Provisioning WHERE @crmPrimaryBusUnit IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@crmPrimaryBusUnit +  ' Has nullable test.';
Set @ExpectedResult =  @crmPrimaryBusUnit+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @crmPrimaryBusUnit+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @crmPrimaryBusUnit + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@crmPrimaryBusUnit +  ' Has nullable test.'; 
Set @ExpectedResult =  @crmPrimaryBusUnit+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @crmPrimaryBusUnit+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @crmPrimaryBusUnit + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@crmPrimaryBusUnit +  ' Has nullable test.';
Set @ExpectedResult =     @crmPrimaryBusUnit+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @crmPrimaryBusUnit+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @crmPrimaryBusUnit + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@crmPrimaryBusUnit +  ' Has nullable test.';
Set @ExpectedResult =     @crmPrimaryBusUnit+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @crmPrimaryBusUnit+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @crmPrimaryBusUnit + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@crmSecondaryBusUnit,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Provisioning WHERE @crmSecondaryBusUnit IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@crmSecondaryBusUnit +  ' Has nullable test.';
Set @ExpectedResult =  @crmSecondaryBusUnit+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @crmSecondaryBusUnit+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @crmSecondaryBusUnit + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@crmSecondaryBusUnit +  ' Has nullable test.'; 
Set @ExpectedResult =  @crmSecondaryBusUnit+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @crmSecondaryBusUnit+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @crmSecondaryBusUnit + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@crmSecondaryBusUnit +  ' Has nullable test.';
Set @ExpectedResult =     @crmSecondaryBusUnit+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @crmSecondaryBusUnit+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @crmSecondaryBusUnit + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@crmSecondaryBusUnit +  ' Has nullable test.';
Set @ExpectedResult =     @crmSecondaryBusUnit+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @crmSecondaryBusUnit+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @crmSecondaryBusUnit + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@casperRoles,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Provisioning WHERE @casperRoles IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@casperRoles +  ' Has nullable test.';
Set @ExpectedResult =  @casperRoles+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @casperRoles+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @casperRoles + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@casperRoles +  ' Has nullable test.'; 
Set @ExpectedResult =  @casperRoles+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @casperRoles+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @casperRoles + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@casperRoles +  ' Has nullable test.';
Set @ExpectedResult =     @casperRoles+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @casperRoles+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @casperRoles + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@casperRoles +  ' Has nullable test.';
Set @ExpectedResult =     @casperRoles+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @casperRoles+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @casperRoles + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@status,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Provisioning WHERE @status IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@status +  ' Has nullable test.';
Set @ExpectedResult =  @status+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @status+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @status + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@status +  ' Has nullable test.'; 
Set @ExpectedResult =  @status+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @status+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @status + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@status +  ' Has nullable test.';
Set @ExpectedResult =     @status+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @status+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @status + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@status +  ' Has nullable test.';
Set @ExpectedResult =     @status+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @status+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @status + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@changeType,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Provisioning WHERE @changeType IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@changeType +  ' Has nullable test.';
Set @ExpectedResult =  @changeType+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @changeType+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @changeType + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@changeType +  ' Has nullable test.'; 
Set @ExpectedResult =  @changeType+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @changeType+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @changeType + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@changeType +  ' Has nullable test.';
Set @ExpectedResult =     @changeType+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @changeType+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @changeType + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@changeType +  ' Has nullable test.';
Set @ExpectedResult =     @changeType+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @changeType+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @changeType + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@processedDatetime,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Provisioning WHERE @processedDatetime IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@processedDatetime +  ' Has nullable test.';
Set @ExpectedResult =  @processedDatetime+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @processedDatetime+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @processedDatetime + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@processedDatetime +  ' Has nullable test.'; 
Set @ExpectedResult =  @processedDatetime+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @processedDatetime+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @processedDatetime + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@processedDatetime +  ' Has nullable test.';
Set @ExpectedResult =     @processedDatetime+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @processedDatetime+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @processedDatetime + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@processedDatetime +  ' Has nullable test.';
Set @ExpectedResult =     @processedDatetime+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @processedDatetime+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @processedDatetime + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Provisioning where @contactIdentifier = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@contactIdentifier +  ' Has blank value test.'; 
Set @ExpectedResult = @contactIdentifier+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @contactIdentifier+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @contactIdentifier + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@contactIdentifier +  ' Has blank value test.'; 
Set @ExpectedResult = @contactIdentifier+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @contactIdentifier+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @contactIdentifier + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Provisioning where @loginId = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@loginId +  ' Has blank value test.'; 
Set @ExpectedResult = @loginId+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @loginId+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loginId + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@loginId +  ' Has blank value test.'; 
Set @ExpectedResult = @loginId+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @loginId+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @loginId + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Provisioning where @wealthscapeId = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@wealthscapeId +  ' Has blank value test.'; 
Set @ExpectedResult = @wealthscapeId+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @wealthscapeId+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @wealthscapeId + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@wealthscapeId +  ' Has blank value test.'; 
Set @ExpectedResult = @wealthscapeId+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @wealthscapeId+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @wealthscapeId + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Provisioning where @crmPrimaryBusUnit = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@crmPrimaryBusUnit +  ' Has blank value test.'; 
Set @ExpectedResult = @crmPrimaryBusUnit+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @crmPrimaryBusUnit+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @crmPrimaryBusUnit + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@crmPrimaryBusUnit +  ' Has blank value test.'; 
Set @ExpectedResult = @crmPrimaryBusUnit+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @crmPrimaryBusUnit+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @crmPrimaryBusUnit + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Provisioning where @crmSecondaryBusUnit = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@crmSecondaryBusUnit +  ' Has blank value test.'; 
Set @ExpectedResult = @crmSecondaryBusUnit+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @crmSecondaryBusUnit+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @crmSecondaryBusUnit + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@crmSecondaryBusUnit +  ' Has blank value test.'; 
Set @ExpectedResult = @crmSecondaryBusUnit+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @crmSecondaryBusUnit+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @crmSecondaryBusUnit + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Provisioning where @casperRoles = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@casperRoles +  ' Has blank value test.'; 
Set @ExpectedResult = @casperRoles+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @casperRoles+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @casperRoles + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@casperRoles +  ' Has blank value test.'; 
Set @ExpectedResult = @casperRoles+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @casperRoles+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @casperRoles + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Provisioning where @status = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@status +  ' Has blank value test.'; 
Set @ExpectedResult = @status+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @status+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @status + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@status +  ' Has blank value test.'; 
Set @ExpectedResult = @status+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @status+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @status + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Provisioning where @changeType = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@changeType +  ' Has blank value test.'; 
Set @ExpectedResult = @changeType+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @changeType+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @changeType + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@changeType +  ' Has blank value test.'; 
Set @ExpectedResult = @changeType+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @changeType+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @changeType + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Provisioning where @processedDatetime = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@processedDatetime +  ' Has blank value test.'; 
Set @ExpectedResult = @processedDatetime+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @processedDatetime+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @processedDatetime + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@processedDatetime +  ' Has blank value test.'; 
Set @ExpectedResult = @processedDatetime+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @processedDatetime+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @processedDatetime + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Provisioning where contactIdentifier = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'contactIdentifier' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'contactIdentifier' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'contactIdentifier' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'contactIdentifier' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'contactIdentifier' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'contactIdentifier' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'contactIdentifier' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'contactIdentifier' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Provisioning where loginId = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'loginId' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'loginId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'loginId' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loginId' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'loginId' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'loginId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'loginId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loginId' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Provisioning where wealthscapeId = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'wealthscapeId' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'wealthscapeId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'wealthscapeId' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'wealthscapeId' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'wealthscapeId' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'wealthscapeId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'wealthscapeId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'wealthscapeId' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Provisioning where crmPrimaryBusUnit = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'crmPrimaryBusUnit' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'crmPrimaryBusUnit' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'crmPrimaryBusUnit' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'crmPrimaryBusUnit' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'crmPrimaryBusUnit' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'crmPrimaryBusUnit' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'crmPrimaryBusUnit' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'crmPrimaryBusUnit' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Provisioning where crmSecondaryBusUnit = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'crmSecondaryBusUnit' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'crmSecondaryBusUnit' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'crmSecondaryBusUnit' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'crmSecondaryBusUnit' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'crmSecondaryBusUnit' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'crmSecondaryBusUnit' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'crmSecondaryBusUnit' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'crmSecondaryBusUnit' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Provisioning where casperRoles = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'casperRoles' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'casperRoles' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'casperRoles' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'casperRoles' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'casperRoles' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'casperRoles' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'casperRoles' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'casperRoles' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Provisioning where status = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'status' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'status' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'status' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'status' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'status' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'status' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'status' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'status' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Provisioning where changeType = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'changeType' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'changeType' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'changeType' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'changeType' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'changeType' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'changeType' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'changeType' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'changeType' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Provisioning where processedDatetime = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'processedDatetime' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'processedDatetime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'processedDatetime' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'processedDatetime' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'processedDatetime' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'processedDatetime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'processedDatetime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'processedDatetime' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Provisioning where contactIdentifier is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'contactIdentifier' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'contactIdentifier' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'contactIdentifier' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'contactIdentifier' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'contactIdentifier' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'contactIdentifier' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'contactIdentifier' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'contactIdentifier' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Provisioning where loginId is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'loginId' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'loginId' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'loginId' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loginId' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'loginId' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'loginId' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'loginId' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'loginId' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Provisioning where wealthscapeId is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'wealthscapeId' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'wealthscapeId' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'wealthscapeId' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'wealthscapeId' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'wealthscapeId' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'wealthscapeId' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'wealthscapeId' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'wealthscapeId' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Provisioning where crmPrimaryBusUnit is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'crmPrimaryBusUnit' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'crmPrimaryBusUnit' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'crmPrimaryBusUnit' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'crmPrimaryBusUnit' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'crmPrimaryBusUnit' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'crmPrimaryBusUnit' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'crmPrimaryBusUnit' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'crmPrimaryBusUnit' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Provisioning where crmSecondaryBusUnit is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'crmSecondaryBusUnit' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'crmSecondaryBusUnit' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'crmSecondaryBusUnit' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'crmSecondaryBusUnit' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'crmSecondaryBusUnit' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'crmSecondaryBusUnit' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'crmSecondaryBusUnit' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'crmSecondaryBusUnit' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Provisioning where casperRoles is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'casperRoles' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'casperRoles' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'casperRoles' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'casperRoles' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'casperRoles' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'casperRoles' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'casperRoles' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'casperRoles' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Provisioning where status is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'status' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'status' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'status' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'status' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'status' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'status' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'status' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'status' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Provisioning where changeType is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'changeType' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'changeType' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'changeType' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'changeType' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'changeType' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'changeType' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'changeType' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'changeType' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Provisioning where processedDatetime is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'processedDatetime' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'processedDatetime' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'processedDatetime' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'processedDatetime' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'processedDatetime' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'processedDatetime' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'processedDatetime' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'processedDatetime' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
---------------------------------------------------
-- duplicate record checks test ---------------------------------
SELECT [ContactIdentifier]
      ,[LoginId]
      ,[WealthscapeId]
      ,[CRMPrimaryBusUnit]
      ,[CRMSecondaryBusUnit]
      ,[CasperRoles]
      ,[Status]
      ,[UPN]
      ,count(ProvisionId)ProvisioningOccurrences
  FROM [air].[Staging_Provisioning]
  group by [ContactIdentifier]
      ,[LoginId]
      ,[WealthscapeId]
      ,[CRMPrimaryBusUnit]
      ,[CRMSecondaryBusUnit]
      ,[CasperRoles]
      ,[Status]
      ,[UPN]
Having count(ProvisionId) > 1

select @MyRowCount = @@ROWCOUNT

if @MyRowCount = 0
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Duplicate record check on' + @TableName+ @Table + 'test.';
Set @ExpectedResult = 'Duplicate record check on' + @TableName+ @Table  + @WDHD +trim(str(@Zero));
Set @ActualResult = 'Duplicate record check on' + @TableName+ @Table   + @WDHD +trim(str(isNull(@MyRowcount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'Duplicate record check on' + @TableName+ @Table   + @WDHD ;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Duplicate record check on' + @TableName+ @Table + 'test.';
Set @ExpectedResult = 'Duplicate record check on' + @TableName+ @Table  + @WHD +trim(str(@Zero));
Set @ActualResult = 'Duplicate record check on' + @TableName+ @Table   + @WHD +trim(str(isNull(@MyRowcount,0)));
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'Duplicate record check on' + @TableName+ @Table   + @WHD ;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
-----------------------------------------------------------------
---------------------------------------------------
SELECT distinct [ContactIdentifier] PContactIdentifier
      
     
  FROM [air].[Staging_Provisioning]
  where
  [ContactIdentifier]
  not in (Select ContactIdentifier from [air].[Staging_Contact])

select
@MyOrphanCount = count([ProvisionId])
from
[air].[Staging_Provisioning]
where
  ContactIdentifier not in (Select ContactIdentifier from [air].[Staging_Contact])
--Select @MyOrphanCount = @@ROWCOUNT

If @MyOrphanCount = @Zero
begin

Set @MyTestDescription =  @SchemaName + ' ' + @TableName +' BR 044 '+ 'Orphan record check on' + @TableName+ @Table + 'test.';
Set @ExpectedResult = 'Orphan record check on' + @TableName+ @Table  + @WDNHOR +trim(str(@Zero));
Set @ActualResult = 'Orphan record check on' + @TableName+ @Table   + @WDNHOR +trim(str(isNull(@MyOrphanCount,0)));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' BR 044 ' + 'Orphan record check on' + @TableName+ @Table   + @WDNHOR ;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription =@SchemaName + ' ' + @TableName +' BR 044 '+ 'Orphan record check on' + @TableName+ @Table + 'test.';
Set @ExpectedResult ='Orphan record check on' + @TableName+ @Table  + @WHOR +trim(str(@Zero));
Set @ActualResult = 'Orphan record check on' + @TableName+ @Table   + @WHOR +trim(str(isNull(@MyOrphancount,0)));
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' BR 044 ' + 'Orphan record check on' + @TableName+ @Table   + @WHOR ;
Set @TestStatusCode = 1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
-----------------------------------------------------------------
---------------------------------------------------
IF (SELECT 
count(*) 
FROM 
[air].[Staging_Provisioning] 
where  
LoginId is null 
OR 
LEN(Trim(LoginId))=0)>0
begin
--Select 'LoginID NOT populated for all records' Fail
Set @MyTestDescription =@SchemaName + ' ' + @TableName +' BR 045 '+ 'LoginID must be populated' +  ' test.';
Set @ExpectedResult = 'LoginID is populated for all records';
Set @ActualResult = 'LoginID NOT populated for all records';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' BR 045 ' + 'LoginID must be populated' +  ' test.';
Set @TestStatusCode = 1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Else
begin
--Select 'LoginID is populated for all records' Pass
Set @MyTestDescription =@SchemaName + ' ' + @TableName +' BR 045 '+ 'LoginID must be populated' +  ' test.';
Set @ExpectedResult = 'LoginID is populated for all records';
Set @ActualResult = 'LoginID is populated for all records';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' BR 045 ' + 'LoginID must be populated' +  ' test.';
Set @TestStatusCode = 3;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
-----------------------------------------------------------------
---------------------------------------------------
IF (SELECT count(*) FROM [air].[Staging_Provisioning] where  [Status] is null OR LEN(Trim([Status]))=0)>0
begin
--Select 'Status NOT populated for all records' Fail
Set @MyTestDescription =@SchemaName + ' ' + @TableName +' BR 046 '+ 'Status is populated for all records' +  ' test.';
Set @ExpectedResult = 'Status is populated for all records';
Set @ActualResult = 'Status NOT populated for all records';
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' BR 046 ' + 'Status is populated for all records' +  ' test.';
Set @TestStatusCode = 1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Else
begin
--Select 'Status is populated for all records' Pass
Set @MyTestDescription =@SchemaName + ' ' + @TableName +' BR 046 '+ 'Status is populated for all records' +  ' test.';
Set @ExpectedResult = 'Status is populated for all records';
Set @ActualResult = 'Status is populated for all records';
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' BR 046 ' + 'Status is populated for all records' +  ' test.';
Set @TestStatusCode = 3;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end

-----------------------------------------------------------------
---------------------------------------------------
select @TestDescription=(Select case WHEN
(select count(TestStatus) 
from dbo.TestResults 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Staging_Provisioning%'
and TestStatus = @Passed
) = (select count(TestStatus) 
from dbo.TestResults 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Staging_Provisioning%'
)
THEN 'ALL TESTS PASSED!'
WHEN
(select count(TestStatus) 
from dbo.TestResults 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Staging_Provisioning%'
and TestStatus in (@Passed,@Warning)
) = (select count(TestStatus) 
from dbo.TestResults 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Staging_Provisioning%'
)
THEN 'ALL TESTS PASSED. SOME PASS WITH WARNING!'
ELSE 'ALL TESTS DID NOT PASS!' END)

insert dbo.TestLog values (@TestScenario,@EndingStatus,@TestDescription,SYSDATETIME(),System_User)

select
testStatus
,TestStatusCode
,count(TestId)"Count"
from
dbo.TestResults
where
TestDateTime > convert(Date,GetDate())
and
TestDescription LIKE '%Staging_Provisioning%'
and
testid > @MaxTestID
group by TestStatus,TestStatusCode
order by TestStatusCode
select * from dbo.TestLog Where TestScenario LIKE '%Staging_Provisioning%' 
--And TestExecutionTime > convert(Date,GetDate())
and TestLogId > @MaxTestLogID
Order by TestExecutionTime Desc

select
[TestId]
,[TestMessage]
,[TestDescription]
,[TestStatus]
,[ExpectedResult]
,[ActualResult]
,[TestDateTime]
,[TestStatusCode]
from
dbo.TestResults
where
TestDateTime > convert(Date,GetDate())
and
TestDescription LIKE '%Staging_Provisioning%'
and
testid > @MaxTestID
order by TestStatusCode asc
---------------------------------------------------
