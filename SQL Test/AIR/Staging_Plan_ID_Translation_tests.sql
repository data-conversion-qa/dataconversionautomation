Use DataIntegrationStage

declare @MaxTestID int; --Variable to caputure Last Test ID in TestResult Table
declare @TestAreaBeginning varchar(30) = 'air '

-- Test Name Declaration Starts Here --
declare @TableName varchar(100) = 'Staging_Plan_ID_Translation';
-- Test Name Declaration Ends Here --
declare @SchemaName varchar(25) = 'air ';
-- Table Column Declaration Starts Here --

declare @PlanIdTranslationId varchar(19) = 'PlanIdTranslationId';
declare @ProcessedDatetime varchar(17) = 'ProcessedDatetime';
declare @InsertDateTime varchar(14) = 'InsertDateTime';
declare @Source varchar(6) = 'Source';
declare @SponsorCompanyCode varchar(18) = 'SponsorCompanyCode';
declare @SponsorPlanId varchar(13) = 'SponsorPlanId';
declare @MMLPlanId varchar(9) = 'MMLPlanId';
declare @ChangeType varchar(10) = 'ChangeType';


-- Table Column Declaration Ends Here --

--Fixed -- Do Not Change -- 
declare @CDNE varchar(75) = ' column does not exist.';
declare @CE varchar(25) = ' column exists.';
declare @TC varchar(25) = ' Test Case #';
declare @E varchar(25) = ' exists.'
declare @DNE varchar(25) = ' does not exist.';
declare @Table varchar(10) = 'Table';
Declare @Column varchar(25) = ' column ';
Declare @HTCT varchar(30) = 'has the correct type. ';
Declare @DNHTCT varchar(40) = 'does not have the correct type.';
declare @CLIC varchar(40) = ' column length is correct.';
declare @CLII varchar(40) = ' column length is incorrect.';
declare @INPWDT varchar(40) = ' is not populated with date time.';
declare @IPWDT varchar(40) = ' is populated with date time.';
declare @TCDNED varchar(40) = 'does not exist, damn it!';
declare @somearenull varchar(25) = ' some columns are null.';
declare @tasbpwvda varchar(255) = ' should be populated with valid data. ';
declare @sbpwagcir varchar(255) = ' should be populated with a good contact identifier reference.'
declare @somecolumnsblank varchar(50) = ' some columns are blank.';
declare @somerhbd varchar(50) = ' some rows have bad dates.';
declare @sciraor varchar(175) = ' some contact identifier reference are oprhaned records';
declare @cchav varchar(50) = 'changeType column has appropriate values.';
declare @ccdnhav varchar(50) = 'changeType column does not have appropriate values.';
DECLARE @MyTestDescription varchar(500);
Declare @RC int;
DECLARE @TestStatusCode int;
declare @NullCount int;
declare @NumBlank int;
declare @TypeVarChar int = 167;
declare @TypeDateTime int = 61;
declare @TypeDate int = 167;
declare @TypeInt int = 56;
declare @TypeBit int = 104;
declare @TypeBigInt int = 127;
declare @TypeNVarChar int = 231;
declare @TypeChar int = 175;
declare @TypeMoney int = 60;
declare @TypeDecimal int = 106;
declare @TypeTinyInt int = 48;
declare @Passed varchar(10) = 'Passed.';
declare @Failed varchar(10) = 'Failed!';
declare @Warning varchar(10) = 'Warning!';
declare @PrimaryKey varchar(100) = 'PrimaryKey';
declare @SystemTypeId int;
declare @LengthDateTime int = 8;
declare @LengthInt int = 4;
declare @Zero int = 0;
declare @One int = 1;
declare @Two int = 2;
declare @Three int = 3;
declare @Four int = 4;
declare @Five int = 5;
declare @Seven int = 7;
declare @Six int = 6;
declare @Eight int = 8;
declare @Nine int = 9;
declare @Ten int = 10;
declare @Twelve int = 12;
declare @Fifteen int = 15;
declare @Eighteen int = 18;
declare @Nineteen int = 19;
declare @Twenty int = 20;
declare @TwentyThree int = 23;
declare @TwentyFour int = 24;
declare @TwentyFive int = 25;
declare @Thirty int = 30;
declare @ThirtyTwo int = 32;
declare @ThirtyFive int =35;
declare @Forty int = 40;
declare @FortyOne int = 41;
declare @Fifty int = 50;
declare @Sixty int = 60;
declare @Eighty int = 80;
declare @Hundred int = 100;
declare @OneHundred int = 100;
declare @OneHundredThirtyThree int = 133;
declare @OneHundredTwentyFive int = 125;
declare @TwoHundred int = 200;
declare @TwoHundredFifty int = 250;
declare @TwoHundredFiftyFive int = 255;
declare @ThreeHundredFifty int = 350;
declare @FiveHundred int = 500;
declare @OneThousand int = 1000;
declare @ThirteenHundred int = 1300;
--declare @TestDescription varchar(200);
declare @ExpectedResult varchar(100);
declare @ActualResult varchar(100);
declare @TestStatus varchar(15);
declare @TestDateTime datetime;
declare @TestMessage varchar(200);
--declare @TableNameOne varchar(30) = 'staging_plan';
declare @Schema int = 17;
declare @Insert varchar(30) = 'I-insert';
Declare @Update varchar(30) = 'U-update';
Declare @Delete varchar(30) = 'D-delete';
declare @TableObject Int;
declare @ColumnObject Int;
declare @ColumnIsNullable Int;
declare @ICN varchar(30) = ' is column nullable.';
declare @TCIN varchar(40) = ' the column is nullable. ';
declare @TCINN varchar(50) = ' the column is not nullable. ';
declare @TCIPK varchar(50) = ' the column is a Primary Key. ';
declare @MaxLength Int;
declare @Return int;
declare @AllowsNull int;
declare @HasNull int;
declare @ColHasNull varchar(20) = ' column has null';
declare @ColHasNoNull varchar(20)= ' column has no null';
declare @HasBlank Bit;
declare @ColHasBlank varchar(25) = ' column has blank value '
declare @ColHasNoBlank varchar(25) = ' column has no blank value '
declare @HasBlankCount int;
--Declare @TypeDecimal int = 106;
Declare @MaxTestLogID int;
declare @LengthTinyInt int = 1;
declare @LengthDecimal int = 17;
declare @Eigthteen int = 18;
Declare @IsIdentity bit;
declare @TempTableName varchar(25) = '#testResults';
Declare @TestScenario varchar(80) = 'Staging_Plan_ID_Translation'
declare @BeginningStatus varchar(25) = 'Beginning';
Declare @EndingStatus varchar(25) = 'Ending';
Declare @TestDescription varchar(800) = 'Test the Staging_Plan_ID_Translation table and columns confirming the properties of the columns and some data verification.';
declare @p1 varchar(25) = 'Air';
declare @p2 varchar(25) = 'AccountMaster';

--Get the Schema ID for the table retrieval
select @MaxTestID = max(Testid) from dbo.TestResults;

select @MaxTestLogID = max(TestLogId) from dbo.TestLog;

select @Schema  = schema_id
from
sys.schemas
where
name = @p1;

--Get the TableObjectId for the table using the Table Name and the SchemaId
select @TableObject = [object_id]
	from
	sys.tables
	where
	name = @TableName
	and
	schema_id = @Schema;
	
select
@PrimaryKey = name
from
sys.all_columns
where
is_identity = 1
and
object_id = @TableObject;


insert TestLog values (@TestScenario,@BeginningStatus,@TestDescription,SYSDATETIME(),System_User)

If @TableObject is not null
begin
Set @TestDescription =	@TestAreaBeginning + @TableName + '' + @Table + ' exists test.';
Set @ExpectedResult =    'Table Exists.';
Set @ActualResult = 'Table Exists.';
Set @TestStatus =  @Passed;
Set @TestDateTime = SYSDATETIME();
Set @TestMessage = @TC + trim(str(@@IDENTITY)) +' '+@Table+' '+@TableName + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode
end
	else
begin	
Set @TestDescription =	@TestAreaBeginning + @TableName+ '' + @Table + ' exists test.';
set @ExpectedResult = 	'Table Exists.';
Set @ActualResult =	'Table object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime =	SysDatetime();
Set @TestMessage = 	@TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Table+' '+@TableName + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode
end
--------------------------------------------------

set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @PlanIdTranslationId and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @PlanIdTranslationId + ' exists test.';
Set @ExpectedResult =  @PlanIdTranslationId + ' ' + @Column + ' ' +@E;
Set @ActualResult = @PlanIdTranslationId + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @PlanIdTranslationId + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @PlanIdTranslationId + ' exists test.';
Set @ExpectedResult =  @PlanIdTranslationId + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @PlanIdTranslationId + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@PlanIdTranslationId + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @ProcessedDatetime and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @ProcessedDatetime + ' exists test.';
Set @ExpectedResult =  @ProcessedDatetime + ' ' + @Column + ' ' +@E;
Set @ActualResult = @ProcessedDatetime + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @ProcessedDatetime + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @ProcessedDatetime + ' exists test.';
Set @ExpectedResult =  @ProcessedDatetime + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @ProcessedDatetime + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@ProcessedDatetime + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @InsertDateTime and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @InsertDateTime + ' exists test.';
Set @ExpectedResult =  @InsertDateTime + ' ' + @Column + ' ' +@E;
Set @ActualResult = @InsertDateTime + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @InsertDateTime + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @InsertDateTime + ' exists test.';
Set @ExpectedResult =  @InsertDateTime + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @InsertDateTime + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@InsertDateTime + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @Source and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @Source + ' exists test.';
Set @ExpectedResult =  @Source + ' ' + @Column + ' ' +@E;
Set @ActualResult = @Source + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @Source + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @Source + ' exists test.';
Set @ExpectedResult =  @Source + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @Source + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@Source + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @SponsorCompanyCode and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @SponsorCompanyCode + ' exists test.';
Set @ExpectedResult =  @SponsorCompanyCode + ' ' + @Column + ' ' +@E;
Set @ActualResult = @SponsorCompanyCode + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @SponsorCompanyCode + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @SponsorCompanyCode + ' exists test.';
Set @ExpectedResult =  @SponsorCompanyCode + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @SponsorCompanyCode + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@SponsorCompanyCode + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @SponsorPlanId and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @SponsorPlanId + ' exists test.';
Set @ExpectedResult =  @SponsorPlanId + ' ' + @Column + ' ' +@E;
Set @ActualResult = @SponsorPlanId + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @SponsorPlanId + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @SponsorPlanId + ' exists test.';
Set @ExpectedResult =  @SponsorPlanId + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @SponsorPlanId + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@SponsorPlanId + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @MMLPlanId and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @MMLPlanId + ' exists test.';
Set @ExpectedResult =  @MMLPlanId + ' ' + @Column + ' ' +@E;
Set @ActualResult = @MMLPlanId + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @MMLPlanId + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @MMLPlanId + ' exists test.';
Set @ExpectedResult =  @MMLPlanId + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @MMLPlanId + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@MMLPlanId + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


set @ColumnObject = null
select
		@ColumnObject = column_ID from sys.all_columns where name = @ChangeType and [object_id] = @TableObject

if @ColumnObject is not null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table + ' ' +@Column +' '+ @ChangeType + ' exists test.';
Set @ExpectedResult =  @ChangeType + ' ' + @Column + ' ' +@E;
Set @ActualResult = @ChangeType + ' ' + @Column + ' ' +@E;
Set @TestStatus = @Passed;
Set @TestDateTime = SysDateTime();
Set @TestMessage = @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+ @ChangeType + @E;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
		else
begin
Set @MyTestDescription = @TestAreaBeginning + trim(@TableName) + ' ' + @Table + ' ' +@Column +' '+ @ChangeType + ' exists test.';
Set @ExpectedResult =  @ChangeType + ' ' + @Column + ' ' +@E;
Set @ActualResult =  @ChangeType + ' ' + @Column + ' ' +@DNE;
Set @TestStatus =  @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage =  @TC + trim(str(isnull(@@IDENTITY,1)))+' ' +@Column+' '+@ChangeType + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @PlanIdTranslationId	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @PlanIdTranslationId; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @PlanIdTranslationId + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeInt =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @PlanIdTranslationId;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @PlanIdTranslationId + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @PlanIdTranslationId + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @PlanIdTranslationId + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @ProcessedDatetime	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @ProcessedDatetime; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @ProcessedDatetime + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeDateTime =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ProcessedDatetime;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ProcessedDatetime + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ProcessedDatetime + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ProcessedDatetime + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @InsertDateTime	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @InsertDateTime; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @InsertDateTime + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeDateTime =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @InsertDateTime;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @InsertDateTime + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @InsertDateTime + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @InsertDateTime + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @Source	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @Source; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @Source + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @Source;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @Source + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @Source + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @Source + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @SponsorCompanyCode	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @SponsorCompanyCode; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @SponsorCompanyCode + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @SponsorCompanyCode;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @SponsorCompanyCode + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @SponsorCompanyCode + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @SponsorCompanyCode + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @SponsorPlanId	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @SponsorPlanId; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @SponsorPlanId + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @SponsorPlanId;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @SponsorPlanId + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @SponsorPlanId + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @SponsorPlanId + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @MMLPlanId	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @MMLPlanId; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @MMLPlanId + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeVarChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @MMLPlanId;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @MMLPlanId + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @MMLPlanId + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @MMLPlanId + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

----------------------------------------------------------------------
set @SystemTypeId = null
select @SystemTypeId = system_type_id	from	sys.all_columns	where	name = @ChangeType	and	[object_id]= @TableObject;
if @SystemTypeId is null
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName + ' ' + @Table  + ' ' +@Column +' '+ @ChangeType; 
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult = 'Column object is null.';
Set @TestStatus = @Failed;
Set @TestDateTime = SysDatetime();
Set @TestMessage = 	@TC+ trim(str(@@IDENTITY)) + ' ' + @ChangeType + @Column + @DNE;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	
if @SystemTypeId is not null
if @TypeChar =  @SystemTypeId
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ChangeType;
Set @ExpectedResult = 'Column type matched.';
Set @ActualResult =  'Column type matched.';
Set @TestStatus =  @Passed; 
Set @TestDateTime = Sysdatetime();
Set @TestMessage =  @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ChangeType + @Column + @HTCT;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' + @Table  + ' ' +@Column +' '+ @ChangeType + ' system type id test.';
Set @ExpectedResult = 'Column system type id matched';
Set @ActualResult = 'Column system type id did not match!';
Set @TestStatus =  @Failed;
Set @TestDateTime = sysdatetime();
Set @TestMessage = @TC+ trim(str(isnull(@@IDENTITY,1))) + ' ' + @ChangeType + @Column + @DNHTCT;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@PlanIdTranslationId,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @PlanIdTranslationId + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @PlanIdTranslationId + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@PlanIdTranslationId +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @PlanIdTranslationId + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@PlanIdTranslationId +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @PlanIdTranslationId + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@ProcessedDatetime,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @ProcessedDatetime + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@InsertDateTime,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @InsertDateTime + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @TwentyThree
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@Source,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @Source + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @Source + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@Source +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @Source + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@Source +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @Source + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@SponsorCompanyCode,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @SponsorCompanyCode + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @SponsorCompanyCode + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Ten
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@SponsorCompanyCode +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @SponsorCompanyCode + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@SponsorCompanyCode +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @SponsorCompanyCode + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@SponsorPlanId,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @SponsorPlanId + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @SponsorPlanId + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Thirty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@SponsorPlanId +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @SponsorPlanId + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@SponsorPlanId +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @SponsorPlanId + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@MMLPlanId,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @MMLPlanId + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @MMLPlanId + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @Thirty
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@MMLPlanId +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @MMLPlanId + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@MMLPlanId +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @MMLPlanId + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------


select @MaxLength = COLUMNPROPERTY(@TableObject,@ChangeType,'Precision')
if @MaxLength is null
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName + ' '+ @ChangeType + ' '+ @Column + ' length test';
Set @ExpectedResult =  'MaxLength does NOT match!';
Set @ActualResult =  'MaxLength set to null, column does not exist.';
Set @TestStatus =  @Failed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC +trim(str(@@IDENTITY))+ ' ' + @ChangeType + @CLII;
Set @TestStatusCode =  1;
EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
If @MaxLength is not null
If @MaxLength = @One
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' length test.'; 
Set @ExpectedResult =     'MaxLength matches!';
Set @ActualResult =     'MaxLength matches!';  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @CLIC;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
	else
begin
Set @MyTestDescription =	@TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' length test.';
Set @ExpectedResult =     'MaxLength does NOT match!'; 
Set @ActualResult =    ' Length is '+ trim(str(@MaxLength)) + ' ';
Set @TestStatus =     @Failed; 
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC + trim(str(@@IDENTITY))+ ' ' + @ChangeType + @CLII;
Set @TestStatusCode =     1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
---------------------------------------------------

Select @AllowsNull = COLUMNPROPERTY(@TableObject,@PlanIdTranslationId,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Plan_ID_Translation WHERE @PlanIdTranslationId IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@PlanIdTranslationId +  ' Has nullable test.';
Set @ExpectedResult =  @PlanIdTranslationId+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @PlanIdTranslationId+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @PlanIdTranslationId + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@PlanIdTranslationId +  ' Has nullable test.'; 
Set @ExpectedResult =  @PlanIdTranslationId+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @PlanIdTranslationId+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @PlanIdTranslationId + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@PlanIdTranslationId +  ' Has nullable test.';
Set @ExpectedResult =     @PlanIdTranslationId+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @PlanIdTranslationId+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @PlanIdTranslationId + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@PlanIdTranslationId +  ' Has nullable test.';
Set @ExpectedResult =     @PlanIdTranslationId+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @PlanIdTranslationId+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @PlanIdTranslationId + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@ProcessedDatetime,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Plan_ID_Translation WHERE @ProcessedDatetime IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has nullable test.';
Set @ExpectedResult =  @ProcessedDatetime+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @ProcessedDatetime+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has nullable test.'; 
Set @ExpectedResult =  @ProcessedDatetime+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @ProcessedDatetime+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has nullable test.';
Set @ExpectedResult =     @ProcessedDatetime+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @ProcessedDatetime+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has nullable test.';
Set @ExpectedResult =     @ProcessedDatetime+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @ProcessedDatetime+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@InsertDateTime,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Plan_ID_Translation WHERE @InsertDateTime IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has nullable test.';
Set @ExpectedResult =  @InsertDateTime+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @InsertDateTime+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has nullable test.'; 
Set @ExpectedResult =  @InsertDateTime+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @InsertDateTime+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has nullable test.';
Set @ExpectedResult =     @InsertDateTime+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @InsertDateTime+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has nullable test.';
Set @ExpectedResult =     @InsertDateTime+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @InsertDateTime+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@Source,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Plan_ID_Translation WHERE @Source IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@Source +  ' Has nullable test.';
Set @ExpectedResult =  @Source+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @Source+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @Source + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@Source +  ' Has nullable test.'; 
Set @ExpectedResult =  @Source+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @Source+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @Source + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@Source +  ' Has nullable test.';
Set @ExpectedResult =     @Source+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @Source+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @Source + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@Source +  ' Has nullable test.';
Set @ExpectedResult =     @Source+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @Source+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @Source + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@SponsorCompanyCode,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Plan_ID_Translation WHERE @SponsorCompanyCode IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@SponsorCompanyCode +  ' Has nullable test.';
Set @ExpectedResult =  @SponsorCompanyCode+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @SponsorCompanyCode+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @SponsorCompanyCode + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@SponsorCompanyCode +  ' Has nullable test.'; 
Set @ExpectedResult =  @SponsorCompanyCode+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @SponsorCompanyCode+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @SponsorCompanyCode + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@SponsorCompanyCode +  ' Has nullable test.';
Set @ExpectedResult =     @SponsorCompanyCode+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @SponsorCompanyCode+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @SponsorCompanyCode + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@SponsorCompanyCode +  ' Has nullable test.';
Set @ExpectedResult =     @SponsorCompanyCode+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @SponsorCompanyCode+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @SponsorCompanyCode + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@SponsorPlanId,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Plan_ID_Translation WHERE @SponsorPlanId IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@SponsorPlanId +  ' Has nullable test.';
Set @ExpectedResult =  @SponsorPlanId+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @SponsorPlanId+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @SponsorPlanId + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@SponsorPlanId +  ' Has nullable test.'; 
Set @ExpectedResult =  @SponsorPlanId+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @SponsorPlanId+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @SponsorPlanId + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@SponsorPlanId +  ' Has nullable test.';
Set @ExpectedResult =     @SponsorPlanId+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @SponsorPlanId+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @SponsorPlanId + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@SponsorPlanId +  ' Has nullable test.';
Set @ExpectedResult =     @SponsorPlanId+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @SponsorPlanId+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @SponsorPlanId + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@MMLPlanId,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Plan_ID_Translation WHERE @MMLPlanId IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@MMLPlanId +  ' Has nullable test.';
Set @ExpectedResult =  @MMLPlanId+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @MMLPlanId+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @MMLPlanId + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@MMLPlanId +  ' Has nullable test.'; 
Set @ExpectedResult =  @MMLPlanId+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @MMLPlanId+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @MMLPlanId + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@MMLPlanId +  ' Has nullable test.';
Set @ExpectedResult =     @MMLPlanId+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @MMLPlanId+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @MMLPlanId + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@MMLPlanId +  ' Has nullable test.';
Set @ExpectedResult =     @MMLPlanId+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @MMLPlanId+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @MMLPlanId + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @AllowsNull = COLUMNPROPERTY(@TableObject,@ChangeType,'AllowsNull')
	
Select @HasNull = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Plan_ID_Translation WHERE @ChangeType IS NULL)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @AllowsNull = 1 AND @HasNull = 1
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has nullable test.';
Set @ExpectedResult =  @ChangeType+ @ColHasNull+trim(str(@One));
Set @ActualResult =  @ChangeType+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =  @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 1 AND @HasNull = 0
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has nullable test.'; 
Set @ExpectedResult =  @ChangeType+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =  @ChangeType+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =  @Passed;
Set @TestDateTime =  Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasNoNull;
Set @TestStatusCode =  3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 1
begin
Set @MyTestDescription = 	@TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has nullable test.';
Set @ExpectedResult =     @ChangeType+ @ColHasNull+trim(str(@One));
Set @ActualResult =     @ChangeType+ @ColHasNull +trim(str(@HasNull));
Set @TestStatus =     @Failed;
Set @TestDateTime =   Sysdatetime();
Set @TestMessage =  @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasNull;
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
if @AllowsNull = 0 AND @HasNull = 0
	begin
Set @MyTestDescription =    @TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has nullable test.';
Set @ExpectedResult =     @ChangeType+ @ColHasNoNull+trim(str(@One));
Set @ActualResult =     @ChangeType+ @ColHasNoNull +trim(str(@HasNull));  
Set @TestStatus =     @Passed;
Set @TestDateTime =     Sysdatetime();
Set @TestMessage =     @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasNoNull;
Set @TestStatusCode =     3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
    end


------------------------------------------------------
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Plan_ID_Translation where @PlanIdTranslationId = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@PlanIdTranslationId +  ' Has blank value test.'; 
Set @ExpectedResult = @PlanIdTranslationId+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @PlanIdTranslationId+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @PlanIdTranslationId + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@PlanIdTranslationId +  ' Has blank value test.'; 
Set @ExpectedResult = @PlanIdTranslationId+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @PlanIdTranslationId+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @PlanIdTranslationId + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Plan_ID_Translation where @ProcessedDatetime = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has blank value test.'; 
Set @ExpectedResult = @ProcessedDatetime+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @ProcessedDatetime+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ProcessedDatetime +  ' Has blank value test.'; 
Set @ExpectedResult = @ProcessedDatetime+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @ProcessedDatetime+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ProcessedDatetime + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Plan_ID_Translation where @InsertDateTime = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has blank value test.'; 
Set @ExpectedResult = @InsertDateTime+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @InsertDateTime+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@InsertDateTime +  ' Has blank value test.'; 
Set @ExpectedResult = @InsertDateTime+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @InsertDateTime+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @InsertDateTime + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Plan_ID_Translation where @Source = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@Source +  ' Has blank value test.'; 
Set @ExpectedResult = @Source+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @Source+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @Source + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@Source +  ' Has blank value test.'; 
Set @ExpectedResult = @Source+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @Source+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @Source + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Plan_ID_Translation where @SponsorCompanyCode = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@SponsorCompanyCode +  ' Has blank value test.'; 
Set @ExpectedResult = @SponsorCompanyCode+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @SponsorCompanyCode+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @SponsorCompanyCode + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@SponsorCompanyCode +  ' Has blank value test.'; 
Set @ExpectedResult = @SponsorCompanyCode+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @SponsorCompanyCode+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @SponsorCompanyCode + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Plan_ID_Translation where @SponsorPlanId = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@SponsorPlanId +  ' Has blank value test.'; 
Set @ExpectedResult = @SponsorPlanId+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @SponsorPlanId+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @SponsorPlanId + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@SponsorPlanId +  ' Has blank value test.'; 
Set @ExpectedResult = @SponsorPlanId+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @SponsorPlanId+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @SponsorPlanId + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Plan_ID_Translation where @MMLPlanId = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@MMLPlanId +  ' Has blank value test.'; 
Set @ExpectedResult = @MMLPlanId+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @MMLPlanId+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @MMLPlanId + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@MMLPlanId +  ' Has blank value test.'; 
Set @ExpectedResult = @MMLPlanId+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @MMLPlanId+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @MMLPlanId + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
Select @HasBlank = (Select CASE WHEN EXISTS (SELECT 1 FROM air.Staging_Plan_ID_Translation where @ChangeType = '')
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END)

if @HasBlank = 0
begin
Set @MyTestDescription =@TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has blank value test.'; 
Set @ExpectedResult = @ChangeType+ @ColHasNoBlank+trim(str(@Zero));
Set @ActualResult = @ChangeType+ @ColHasNoBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasNoBlank;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @TestAreaBeginning + @TableName+ ' ' +@ChangeType +  ' Has blank value test.'; 
Set @ExpectedResult = @ChangeType+ @ColHasBlank+trim(str(@Zero));
Set @ActualResult = @ChangeType+ @ColHasBlank +trim(str(isnull(@HasBlank,0)));  
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + @ChangeType + @ColHasBlank;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Plan_ID_Translation where PlanIdTranslationId = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'PlanIdTranslationId' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'PlanIdTranslationId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'PlanIdTranslationId' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'PlanIdTranslationId' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'PlanIdTranslationId' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'PlanIdTranslationId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'PlanIdTranslationId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'PlanIdTranslationId' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Plan_ID_Translation where ProcessedDatetime = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ProcessedDatetime' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ProcessedDatetime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ProcessedDatetime' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ProcessedDatetime' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'ProcessedDatetime' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ProcessedDatetime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ProcessedDatetime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ProcessedDatetime' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Plan_ID_Translation where InsertDateTime = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'InsertDateTime' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'InsertDateTime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'InsertDateTime' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsertDateTime' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'InsertDateTime' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'InsertDateTime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'InsertDateTime' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsertDateTime' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Plan_ID_Translation where Source = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Source' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'Source' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'Source' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'Source' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'Source' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'Source' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'Source' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'Source' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Plan_ID_Translation where SponsorCompanyCode = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'SponsorCompanyCode' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'SponsorCompanyCode' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'SponsorCompanyCode' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'SponsorCompanyCode' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'SponsorCompanyCode' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'SponsorCompanyCode' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'SponsorCompanyCode' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'SponsorCompanyCode' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Plan_ID_Translation where SponsorPlanId = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'SponsorPlanId' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'SponsorPlanId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'SponsorPlanId' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'SponsorPlanId' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'SponsorPlanId' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'SponsorPlanId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'SponsorPlanId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'SponsorPlanId' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Plan_ID_Translation where MMLPlanId = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'MMLPlanId' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'MMLPlanId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'MMLPlanId' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'MMLPlanId' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'MMLPlanId' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'MMLPlanId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'MMLPlanId' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'MMLPlanId' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------


select @NumBlank = count(@PrimaryKey) from air.Staging_Plan_ID_Translation where ChangeType = '';

If @NumBlank = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ChangeType' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ChangeType' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ChangeType' + @Column + @somecolumnsblank   + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus =@Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ChangeType' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
 end 
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+'ChangeType' + @Column+ @somecolumnsblank + 'test.';
Set @ExpectedResult = 'ChangeType' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ChangeType' + @Column + @somecolumnsblank  + @tasbpwvda +trim(str(@NumBlank));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ChangeType' + @Column + @somecolumnsblank + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NumBlank = null


------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Plan_ID_Translation where PlanIdTranslationId is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'PlanIdTranslationId' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'PlanIdTranslationId' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'PlanIdTranslationId' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'PlanIdTranslationId' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'PlanIdTranslationId' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'PlanIdTranslationId' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'PlanIdTranslationId' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'PlanIdTranslationId' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Plan_ID_Translation where ProcessedDatetime is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ProcessedDatetime' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ProcessedDatetime' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ProcessedDatetime' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ProcessedDatetime' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ProcessedDatetime' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ProcessedDatetime' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ProcessedDatetime' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ProcessedDatetime' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Plan_ID_Translation where InsertDateTime is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'InsertDateTime' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'InsertDateTime' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'InsertDateTime' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsertDateTime' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'InsertDateTime' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'InsertDateTime' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'InsertDateTime' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'InsertDateTime' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Plan_ID_Translation where Source is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Source' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'Source' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'Source' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'Source' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Source' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'Source' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'Source' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'Source' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Plan_ID_Translation where SponsorCompanyCode is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'SponsorCompanyCode' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'SponsorCompanyCode' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'SponsorCompanyCode' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'SponsorCompanyCode' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'SponsorCompanyCode' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'SponsorCompanyCode' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'SponsorCompanyCode' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'SponsorCompanyCode' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Plan_ID_Translation where SponsorPlanId is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'SponsorPlanId' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'SponsorPlanId' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'SponsorPlanId' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'SponsorPlanId' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'SponsorPlanId' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'SponsorPlanId' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'SponsorPlanId' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'SponsorPlanId' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Plan_ID_Translation where MMLPlanId is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'MMLPlanId' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'MMLPlanId' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'MMLPlanId' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'MMLPlanId' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'MMLPlanId' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'MMLPlanId' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'MMLPlanId' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'MMLPlanId' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
------------------------------------------------------------------


select
@NullCount = count(@PrimaryKey) from air.Staging_Plan_ID_Translation where ChangeType is null

If @NullCount = 0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ChangeType' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ChangeType' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ChangeType' + @Column + @somearenull   + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ChangeType' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'ChangeType' + @Column+ @somearenull + 'test.';
Set @ExpectedResult = 'ChangeType' + @Column + @somearenull  + @tasbpwvda +trim(str(@Zero));
Set @ActualResult = 'ChangeType' + @Column + @somearenull  + @tasbpwvda +trim(str(@NullCount));
Set @TestStatus = @Warning;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' ' + 'ChangeType' + @Column + @somearenull + @tasbpwvda;
Set @TestStatusCode = 2;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
set @NullCount = NULL

-----------------------------------------------------------------
---------------------------------------------------
--Check [SponsorPlanId] value which DOES NOT maps to exactly 1 [MMLPlanId] value
if exists (select
[source]
,SponsorCompanyCode
,SponsorPlanId
,MMLPlanId
,count(PlanIdTranslationId)
FROM [DataIntegrationStage].[air].[Staging_Plan_ID_Translation]
group by [source]
,SponsorCompanyCode
,SponsorPlanId
,MMLPlanId
having count([planIdTranslationId]) > 1)
begin
--Select 'All [SponsorPlanId] value DO NOT map to exactly 1 [MMLPlanId] value' Fail
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Check [SponsorPlanId] value which DOES NOT maps to exactly 1 [MMLPlanId] value ' +  'test.';
Set @ExpectedResult = 'All [SponsorPlanId] value maps to exactly 1 [MMLPlanId] value ';
Set @ActualResult = 'All [SponsorPlanId] value DO NOT map to exactly 1 [MMLPlanId] value ' ;
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' BR 047 ' + 'Check [SponsorPlanId] value which DOES NOT maps to exactly 1 [MMLPlanId] value ' +  'test.';
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;

end
Else
begin
--Select 'All [SponsorPlanId] value maps to exactly 1 [MMLPlanId] value' Pass

Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Check [SponsorPlanId] value which DOES NOT maps to exactly 1 [MMLPlanId] value ' +  'test.';
Set @ExpectedResult = 'All [SponsorPlanId] value maps to exactly 1 [MMLPlanId] value ';
Set @ActualResult = 'All [SponsorPlanId] value maps to exactly 1 [MMLPlanId] value ' ;
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' BR 047 ' + 'Check [SponsorPlanId] value which DOES NOT maps to exactly 1 [MMLPlanId] value ' +  'test.';
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end

-----------------------------------------------------------------
---------------------------------------------------
--Check [MMLPlanId] value which DOES NOT maps to exactly 1 [SponsorPlanId] value
--IF (Select Count(t.[MMLPlanId]) from 
--(SELECT [MMLPlanId], COUNT([SponsorPlanId]) as [CountOFSponsorPlanId] FROM [DataIntegrationStage].[air].[Staging_Plan_ID_Translation] 
--GROUP BY [MMLPlanId] HAVING COUNT([SponsorPlanId]) <> 1) as t)>0
--begin
----Select 'Check [MMLPlanId] value which DOES NOT maps to exactly 1 [SponsorPlanId] value' Fail
--Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Check [MMLPlanId] value which DOES NOT maps to exactly 1 [SponsorPlanId] value ' +  'test.';
--Set @ExpectedResult = 'All [MMLPlanId] values map to exactly 1 [SponsorPlanId] value ';
--Set @ActualResult = 'All [MMLPlanId] values do not map to exactly 1 [SponsorPlanId] value ' ;
--Set @TestStatus = @Failed;
--Set @TestDateTime = Sysdatetime();
--Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' BR 048 ' + 'Check [MMLPlanId] value which DOES NOT maps to exactly 1 [SponsorPlanId] value ' +  'test.';
--Set @TestStatusCode = 1;

--EXECUTE @RC = [dbo].[insertTestResult] 
--   @TestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end
--Else
--begin
----Select 'All [MMLPlanId] values map to exactly 1 [SponsorPlanId] value' Pass
--Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Check [MMLPlanId] value which DOES NOT maps to exactly 1 [SponsorPlanId] value ' +  'test.';
--Set @ExpectedResult = 'All [MMLPlanId] values map to exactly 1 [SponsorPlanId] value ';
--Set @ActualResult = 'All [MMLPlanId] values map to exactly 1 [SponsorPlanId] value ' ;
--Set @TestStatus = @Passed;
--Set @TestDateTime = Sysdatetime();
--Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' BR 048 ' + 'Check [MMLPlanId] value which DOES NOT maps to exactly 1 [SponsorPlanId] value ' +  'test.';
--Set @TestStatusCode = 3;

--EXECUTE @RC = [dbo].[insertTestResult] 
--   @TestDescription
--  ,@TestMessage
--  ,@ExpectedResult
--  ,@ActualResult
--  ,@TestDateTime
--  ,@TestStatus
--  ,@TestStatusCode;
--end

-----------------------------------------------------------------
---------------------------------------------------
If exists(SELECT [PlanIdTranslationId]
      ,[Source]
      ,[SponsorCompanyCode]
      ,[SponsorPlanId]
      ,[MMLPlanId]
      ,[ChangeType]
      ,[ProcessedDatetime]
      ,[InsertDateTime]
  FROM [air].[Staging_Plan_ID_Translation]
  where source not in ('Pershing','SEI','SCHWAB','SEIT','DAZL','IWS','TDA','ETRADE','BRINKER','Genworth','Morningstar')
  and
  SponsorCompanyCode not in ('Schwab','SEI','Brinker','IWS','3MS','TDA','ETRADE','3CO','4RW','SJW','6U7','GVK','BKU','DJQ','DJR','DJS','HJ2','JUF','NO4','N84','NBU','XB8','XTC'))

begin
Set @MyTestDescription = @SchemaName + ' BR 048 ' + @TableName +' '+ 'Distinct list of sources and sponsor company code for Plan ID Translation ' +  'test.';
Set @ExpectedResult = 'All sources and sponsor company codes meet the distinct list ';
Set @ActualResult = 'Some sources and sponsor company codes fall outside the distinct list of sources ' ;
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' BR 048 ' + 'Distinct list of sources and sponsor company code for Plan ID Translation ' +  'test.';
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' BR 048 ' + @TableName +' '+ 'Distinct list of sources and sponsor company code for Plan ID Translation ' +  'test.';
Set @ExpectedResult = 'All sources and sponsor company codes meet the distinct list ';
Set @ActualResult = 'All sources and sponsor company codes meet the distinct list ' ;
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' BR 048 ' + ' Distinct list of sources and sponsor company code for Plan ID Translation ' +  'test.';
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end

-----------------------------------------------------------------
---------------------------------------------------
if (SELECT count([PlanIdTranslationId])
      
  FROM [air].[Staging_Plan_ID_Translation])>0
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Test the Staging_Plan_ID_Translation table has records. ' +  'test.';
Set @ExpectedResult = 'Staging_Plan_ID_Translation table has records. ';
Set @ActualResult = 'Staging_Plan_ID_Translation table has records. ' ;
Set @TestStatus = @Passed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ ' Test the Staging_Plan_ID_Translation table has records. ' +  'test.';
Set @TestStatusCode = 3;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
else
begin
Set @MyTestDescription = @SchemaName + ' ' + @TableName +' '+ 'Test the Staging_Plan_ID_Translation table has records. ' +  'test.';
Set @ExpectedResult = 'Staging_Plan_ID_Translation table has records. ';
Set @ActualResult = 'Staging_Plan_ID_Translation table does not have records.' ;
Set @TestStatus = @Failed;
Set @TestDateTime = Sysdatetime();
Set @TestMessage = @TC+ trim(str(@@IDENTITY))+ 'Test the Staging_Plan_ID_Translation table has records. ' +  'test.';
Set @TestStatusCode = 1;

EXECUTE @RC = [dbo].[insertTestResult] 
   @TestDescription
  ,@TestMessage
  ,@ExpectedResult
  ,@ActualResult
  ,@TestDateTime
  ,@TestStatus
  ,@TestStatusCode;
end
-----------------------------------------------------------------
---------------------------------------------------

select @TestDescription=(Select case WHEN
(select count(TestStatus) 
from dbo.TestResults 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Staging_Plan_ID_Translation%'
and TestStatus = @Passed
) = (select count(TestStatus) 
from dbo.TestResults 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Staging_Plan_ID_Translation%'
)
THEN 'ALL TESTS PASSED!'
WHEN
(select count(TestStatus) 
from dbo.TestResults 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Staging_Plan_ID_Translation%'
and TestStatus in (@Passed,@Warning)
) = (select count(TestStatus) 
from dbo.TestResults 
where TestDateTime > convert(Date,GetDate()) 
and TestDescription LIKE '%Staging_Plan_ID_Translation%'
)
THEN 'ALL TESTS PASSED. SOME PASS WITH WARNING!'
ELSE 'ALL TESTS DID NOT PASS!' END)

insert dbo.TestLog values (@TestScenario,@EndingStatus,@TestDescription,SYSDATETIME(),System_User)

select
testStatus
,TestStatusCode
,count(TestId)"Count"
from
dbo.TestResults
where
TestDateTime > convert(Date,GetDate())
and
TestDescription LIKE '%Staging_Plan_ID_Translation%'
and
testid > @MaxTestID
group by TestStatus,TestStatusCode
order by TestStatusCode
select * from dbo.TestLog Where TestScenario LIKE '%Staging_Plan_ID_Translation%' 
and TestLogId > @MaxTestLogID
--And TestExecutionTime > convert(Date,GetDate())
Order by TestExecutionTime Desc

select
[TestId]
,[TestMessage]
,[TestDescription]
,[TestStatus]
,[ExpectedResult]
,[ActualResult]
,[TestDateTime]
,[TestStatusCode]
from
dbo.TestResults
where
TestDateTime > convert(Date,GetDate())
and
TestDescription LIKE '%Staging_Plan_ID_Translation%'
and
testid > @MaxTestID
order by TestStatusCode asc
---------------------------------------------------
