﻿using NUnit.Framework.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataConversionAutomation
{
    class QueryManager
    {
        public static string GetQuery(string queryFilepath="none")
        {
            
            if (queryFilepath.Equals("none"))
                {
                TestSetup.strSQLFile = TestSetup.DBQueriesRootPath + TestSetup.module + @"\" + TestSetup.module + "_" + TestSetup.section + ".sql";
                }
            else
            {
                TestSetup.strSQLFile = queryFilepath;
            }
            ;
            if (File.Exists(TestSetup.strSQLFile))
            {
                // This path is a file
                TestSetup.strQuery = Database.ProcessFile(TestSetup.strSQLFile);
            }
            else
            {
                Console.WriteLine(".sql file not found. Query is not a valid. : ", TestSetup.strSQLFile);
            }
            return TestSetup.strQuery;
        }

        public static string GetResults()
        {
            string strQuery = String.Empty;
            string strSQLFile = TestSetup.SQLTestRootPath + @"\GetResults\" + TestSetup.module + @"_GetResults" + ".sql";
            if (File.Exists(strSQLFile))
            {
                // This path is a file
                strQuery = Database.ProcessFile(strSQLFile);
            }
            else
            {
                Console.WriteLine(".sql file not found. Query is not a valid. : ", strSQLFile);
            }
            
            return strQuery;
        }

    }
}
