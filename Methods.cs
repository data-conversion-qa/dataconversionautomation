﻿
using NUnit.Framework;
using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using System.IO;
using Microsoft.VisualBasic.FileIO;
using System.Security.Cryptography;
using System.Collections;
using System.Text.RegularExpressions;

namespace DataConversionAutomation
{
    class Methods
    {
        public static int numberOfRecords = 0;
        //private static object watch;
        public TestContext testContextInstance;
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }
        public string WriteErrorToFile(System.IO.TextWriter textwriter, string errorMessage)
        {
            textwriter.WriteLine(errorMessage);
            return errorMessage;
        }

        public static void PrintRunReport()
        {
            if (TestSetup.StepFail == false)
            {
                Trace.WriteLine("Test Passed");
                Debug.WriteLine("Test Passed");
            }
            else
            {
                Trace.WriteLine("Test Failed");
                Debug.WriteLine("Test Failed");
                Assert.Fail("One or more Step Failed to Run");
            }
        }
        public void PrintCollection(List<string> list)
        {
            foreach (string i in list)
            {
                TestContext.WriteLine(i.ToString());
            }
        }
        public static string GetCurrentTime(String strFormat = "yyyy-MM-dd HH:mm:ss.000")
        {
            DateTime time = DateTime.Now.AddMinutes(-1);
            //strFormat = "yyyy-MM-dd HH:mm:ss.000";
            var usCulture = new System.Globalization.CultureInfo("en-US");
            return time.ToString(strFormat, usCulture);
        }

        public static string GenerateRowChecksum(String aggregateRowData)
        {
            // Serialize the table
            var serializer = new DataContractSerializer(typeof(DataTable));
            var memoryStream = new MemoryStream();
            serializer.WriteObject(memoryStream, aggregateRowData);
            byte[] serializedData = memoryStream.ToArray();
            // Calculte the serialized data's hash value
            var SHA = new SHA1CryptoServiceProvider();
            byte[] hash = SHA.ComputeHash(serializedData);
            Console.Write(hash.ToString());
            // Convert the hash to a base 64 string
            string hashAsText = Convert.ToBase64String(hash);
            return hashAsText;
            //return hash;
        }

        public static byte[] checkMD5(string text)
        {
            //byte[] byteArray = Encoding.ASCII.GetBytes();
            System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] bs = System.Text.Encoding.Unicode.GetBytes(text);
            bs = x.ComputeHash(bs);
            PrintByteArray(bs);
            return bs;
        }

        public static string SQLSHA256Hash(string input)
        {
            using (SHA256 hasher = SHA256.Create())
            {
                // Convert the input string to a byte array and compute the hash.
                byte[] data = hasher.ComputeHash(Encoding.Unicode.GetBytes(input));
                // Create a new Stringbuilder to collect the bytes and create a string.
                StringBuilder sBuilder = new StringBuilder();
                // Loop through each byte of the hashed data and format each one as a hexadecimal string.
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("X2"));
                }
                // Return the hexadecimal string.
                return sBuilder.ToString();
            }
        }

        public static  int SQLBinaryChecksum(string text)
        {
            byte[] byteArray = Encoding.ASCII.GetBytes(text); 
            long sum = 0;
            byte overflow;
            for (int i = 0; i < text.Length; i++)
            {
                sum = (long)((16 * sum) ^ Convert.ToUInt32(text[i]));
                overflow = (byte)(sum / 4294967296);
                sum = sum - overflow * 4294967296;
                sum = sum ^ overflow;
            }

            if (sum > 2147483647)
                sum = sum - 4294967296;
            else if (sum >= 32768 && sum <= 65535)
                sum = sum - 65536;
            else if (sum >= 128 && sum <= 255)
                sum = sum - 256;

            return (int)sum;
        }

        public static void PrintByteArray(byte[] bytes)
        {
            var sb = new StringBuilder("new byte[] { ");
            foreach (var b in bytes)
            {
                sb.Append(b);
            }
            Console.WriteLine(sb.ToString());
        }

        public static String CheckColumnDatatype(string strModule, string TargetTable, string TargetColName, Type ExpectedDataType)
        {
            //Console.WriteLine("Test : Checking Column Datatype for :" + TargetServer + " >> " + TargetDatabase + " >> " + TargetTable);
            var DataTable = new DataTable();
            bool found = false;
            bool match = false;
            DataTable = Database.PullData(strModule, TargetTable);
            Type ActualDataType = null;

            foreach (System.Data.DataColumn column in DataTable.Columns)
            {
                ActualDataType = column.DataType;
                //Compare column name
                if (column.ColumnName == TargetColName)
                { //match datatype}
                    found = true;
                    if (ActualDataType == ExpectedDataType)
                    {
                        match = true;
                        break;
                    }
                    else
                    {
                        match = false;
                        break;
                    }
                }
            }

            if (found == true && match == true)
            {
                // Test Passed
                Console.WriteLine("Passed | Column " + TargetColName + " as Expected :" + ExpectedDataType.ToString() + " matches Actual Datatype: " + ActualDataType.ToString());
                return "Pass";
            }
            else if (found == true && match == false)
            {
                // Test Failed
                Console.WriteLine("Failed | Column " + TargetColName + " Not as Expected :" + ExpectedDataType.ToString() + " Does NOT matche Actual Datatype: " + ActualDataType.ToString());
                return "Fail";
            }
            else
            {
                // Test Failed
                Console.WriteLine("Failed | Column " + TargetColName + " Not Found!");
                return "Fail";

            }

        }


       public static String CheckColumnMaxLength(string strModule, string TargetTable, string TargetColName, int ExpectedLength)
        {
            //Console.WriteLine("Test : Checking Max Allowable Column Lenght for :" + TargetServer + " >> " + TargetDatabase + " >> " + TargetTable);
            var DataTable = new DataTable();
            bool found = false;
            bool match = false;
            DataTable = Database.PullData(strModule, TargetTable);
            int ActualLenght = -1;

            foreach (System.Data.DataColumn column in DataTable.Columns)
            {
                ActualLenght = column.MaxLength;
                //Compare column name
                if (column.ColumnName == TargetColName)
                { //match datatype}
                    found = true;
                    if (ActualLenght == ExpectedLength)
                    {
                        match = true;
                        break;
                    }
                    else
                    {
                        match = false;
                        break;
                    }
                }
            }

            if (found == true && match == true)
            {
                // Test Passed
                Console.WriteLine("Passed | Column " + TargetColName + " as Expected :" + ExpectedLength + " matches Actual Length: " + ActualLenght);
                return "Pass";
            }
            else if (found == true && match == false)
            {
                // Test Failed
                Console.WriteLine("Failed | Column " + TargetColName + " Not as Expected :" + ExpectedLength + " Does NOT matche Actual Length: " + ActualLenght);
                return "Fail";
            }
            else
            {
                // Test Failed
                Console.WriteLine("Failed | Expected Column : " + TargetColName + " Not Found!");
                return "Fail";
            }
        }

        public static String DataConversionNullCheck(string strModule, string TargetTable, string TargetColName)
        {
            //Console.WriteLine("Test : Primary Key Test - Checking if Column has Null Value for :" + TargetServer + " >> " + TargetDatabase + " >> " + TargetTable);
            var DataTable = new DataTable();
            DataTable = Database.PullData(strModule, TargetTable);
            //DataColumn col = null;
            foreach (System.Data.DataRow row in DataTable.Rows)
            {
                if (row[TargetColName] == DBNull.Value || row[TargetColName].ToString() == "")
                {
                    Console.WriteLine("!!! Null or Empty value found in Column : " + TargetColName);
                    return "Fail";
                }
            }
            Console.WriteLine("No Null value found in Column : " + TargetColName);
            return "Pass";

        }
        static String DataConversionNumberFormatCheck()
        {
            return "Pass";
        }

        public static List<string> GetAllRowChecksumArray(DataTable dt)
        {
            string AggregateRowStr = string.Empty;
            string OutputChecksum= string.Empty;
            Int32 counter = 0;
            List<string> ChecksumList = new List<string>();
            foreach (DataRow dataRow in dt.Rows)
            {
                counter++;
                AggregateRowStr = string.Join("", dataRow.ItemArray).Trim();
                OutputChecksum = SQLSHA256Hash(AggregateRowStr);
                ChecksumList.Add(String.Join(",", dataRow.ItemArray) + " | " + OutputChecksum);
                //ChecksumList.Add("Row # "+ (dt.Rows.IndexOf(dataRow)+1) + " | " + String.Join(",", dataRow.ItemArray) + " | " +OutputChecksum);
            }
            return ChecksumList;

        }
        static void CompareChecksumExtractToDB(String filepath, DataTable tarDataTable)
        {
            bool headerRow = true;
            var parser = new TextFieldParser(filepath);
            var theDataTable = new DataTable();
            string aggregateRowStr = String.Empty;
            string outputChecksum = String.Empty;
            string tarAggregateRowStr = string.Empty;
            string tarOutputChecksum = string.Empty;
            List<string> sourceChecksumList = new List<string>();
            List<string> targetChecksumList = new List<string>();

            parser.SetDelimiters("|");

            while (!parser.EndOfData)
            {
                var currentRow = parser.ReadFields();
                if (headerRow)
                {
                    foreach (var field in currentRow)
                    {
                        theDataTable.Columns.Add(field, typeof(object));
                    }
                    headerRow = false;
                }
                else
                {
                    theDataTable.Rows.Add(currentRow);
                }
            }
            headerRow = true;
            var diff = theDataTable.AsEnumerable().Except(tarDataTable.AsEnumerable(), DataRowComparer.Default);
            //Console.WriteLine(diff);

            foreach (DataRow dataRow in theDataTable.Rows)
            {
                aggregateRowStr = string.Join("|", dataRow.ItemArray.Select(p => p.ToString()).ToArray());
                outputChecksum = SQLSHA256Hash(aggregateRowStr);
                Console.WriteLine(outputChecksum);
                sourceChecksumList.Add(outputChecksum);
            }

            foreach (DataRow dataRow in tarDataTable.Rows)
            {
                tarAggregateRowStr = string.Join("|", dataRow.ItemArray.Select(p => p.ToString()).ToArray());
                tarOutputChecksum = SQLSHA256Hash(tarAggregateRowStr);
                Console.WriteLine(tarOutputChecksum);
                targetChecksumList.Add(tarOutputChecksum);
            }

            if (sourceChecksumList.SequenceEqual(targetChecksumList))
            {
                Console.WriteLine("Pass");
                System.Threading.Thread.Sleep(10000);
            }
            else
            {
                Console.WriteLine("Fail");
                System.Threading.Thread.Sleep(10000);
            }

        }

        public static DataTable MergeDataSet(DataSet ds)
        {
            DataTable dtFinal = new DataTable();
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                if (i == 0)
                    dtFinal = ds.Tables[i].Copy();
                else
                    dtFinal.Merge(ds.Tables[i]);
            }

            return dtFinal;
        }
        public static String AreTablesTheSame(DataTable tbl1, DataTable tbl2)
        {

            if (tbl1.Columns.Count != tbl2.Columns.Count)
            {
                Console.WriteLine("Raw Table ColCount: " + tbl1.Columns.Count + " | Final Table ColCount: " + tbl2.Columns.Count);
                Console.WriteLine("Column Count MissMatched!");
                return "Fail";
            }
            else if (tbl1.Rows.Count != tbl2.Rows.Count)
            {
                Console.WriteLine("Raw Table RowCount: " + tbl1.Rows.Count + " | Final Table RowCount: " + tbl2.Rows.Count);
                Console.WriteLine("Record Count MissMatched!");

               /* var datarows_in_dt1_only = tbl1.AsEnumerable()
                    .Where(dr1 => !tbl2.AsEnumerable().Any(dr2 => dr1.Field<string>("BranchIDENTIFIER") != dr2.Field<string>("BranchIDENTIFIER") && dr1.Field<string>("AccountIDENTIFIER") != dr2.Field<string>("AccountIDENTIFIER")));


                foreach (var row in datarows_in_dt1_only.ToList())
                {
                    Console.WriteLine(row);
                }
                */

                return "Fail";
            }
            else
            {
                for (int i = 0; i < tbl1.Rows.Count; i++)
                {
                    for (int c = 0; c < tbl1.Columns.Count; c++)
                    {
                        if (!Equals(tbl1.Rows[i][c], tbl2.Rows[i][c]))
                        {
                            Console.WriteLine("Table Data MissMatched!");
                            return "Fail";
                        }
                        else
                            continue;
                    }
                }
                Console.WriteLine("Table Match Passed!");
                return "Pass";
            }

        }

        private DataTable NoMatches(DataTable MainTable, DataTable SecondaryTable)
        {
            var matched = from table1 in MainTable.AsEnumerable()
                          join table2 in SecondaryTable.AsEnumerable()
                          on table1.Field<string>("ColumnA") equals table2.Field<string>("ColumnC")
                          where (table1.Field<string>("ColumnB").Equals(table2.Field<string>("ColumnA")))
                          || (table1.Field<int>("ColumnC") == table2.Field<int>("ColumnD"))
                          select table1;

            return MainTable.AsEnumerable().Except(matched).CopyToDataTable();
        }

        public static Dictionary<T,int> CompareLists<T>(List<T> list1, List<T> list2)
        {
            IEqualityComparer<T> comparer = null;
            var itemCounts = new Dictionary<T, int>(comparer);

            /*if (list1 is ICollection<T> ilist1 && list2 is ICollection<T> ilist2 && ilist1.Count != ilist2.Count)
                itemCounts.Add('N', 1);
                goto EndCompare;
                */
            if (comparer == null)
                comparer = EqualityComparer<T>.Default;

            foreach (T s in list1)
            {
                if (itemCounts.ContainsKey(s))
                {
                    itemCounts[s]++;
                }
                else
                {
                    itemCounts.Add(s, 1);
                }
            }
            foreach (T s in list2)
            {
                if (itemCounts.ContainsKey(s))
                {
                    itemCounts[s]--;
                }
                else
                {
                    return itemCounts;
                }
            }
       return itemCounts;
        }

    }
}