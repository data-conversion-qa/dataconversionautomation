﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.SqlServer.Management.Smo;
using System.Runtime.Remoting.Messaging;

namespace DataConversionAutomation
{
    class Database
    {
        public static string GetConnectionString(string strModule, string env)
        {
            
            if (strModule.ToUpper().Equals("ACCOUNTMASTER") || strModule.ToUpper().Equals("AM"))
            {
                switch (env.ToUpper())
                {
                    case "DEV":
                        {
                            TestSetup.strServer = "DC1T1DB-D008SQL.tenant01dev.com";
                            TestSetup.strDB = "DataConversion";
                            break;
                        }
                    case "DEV-EDIT":
                        {
                            TestSetup.strServer = "dc1t1ag-d008sql.tenant01dev.com";
                            TestSetup.strDB = "DataConversion";
                            break;
                        }

                    case "QA":
                        {
                            TestSetup.strServer = "DC1T1AG-Q009SQL.tenant01qax.com";
                            TestSetup.strDB = "DataConversion";
                            break;
                        }
                    case "PRE-PROD":
                        {
                            TestSetup.strServer = "DC1T1AG-S009SQL.tenant01stg.com";
                            TestSetup.strDB = "DataConversion";
                            break;
                        }
                    case "PROD":
                        {
                            TestSetup.strServer = "DC1T1AG-P009SQL.tenant01prd.com";
                            if (string.IsNullOrEmpty(TestSetup.strDB))
                            {
                                TestSetup.strDB = "DataConversion";
                            } 
                            break;
                        }
                    default:
                        Console.WriteLine("Error~!");
                        break;
                }
            }
            else if (strModule.ToUpper().Equals("HOUSEHOLD") || strModule.ToUpper().Equals("HOUSE HOLD") || strModule.ToUpper().Equals("HOUSE-HOLD"))
            {
                switch (env.ToUpper())
                {
                    case "DEV":
                        {
                            TestSetup.strServer = "DC1T1DB-D008SQL.tenant01dev.com";
                            TestSetup.strDB = "DataConversion";
                            break;
                        }
                    case "DEV-EDIT":
                        {
                            TestSetup.strServer = "dc1t1ag-d008sql.tenant01dev.com";
                            TestSetup.strDB = "DataConversion";
                            break;
                        }

                    case "QA":
                        {
                            TestSetup.strServer = "DC1T1AG-Q009SQL.tenant01qax.com";
                            TestSetup.strDB = "DataConversion";
                            break;
                        }
                    case "PRE-PROD":
                        {
                            TestSetup.strServer = "DC1T1AG-S009SQL.tenant01stg.com";
                            TestSetup.strDB = "DataConversion";
                            break;
                        }
                    case "PROD":
                        {
                            TestSetup.strServer = "DC1T1AG-P009SQL.tenant01prd.com";
                            TestSetup.strDB = "DataConversion";
                            break;
                        }
                    default:
                        Console.WriteLine("Error~!");
                        break;
                }
            }
            else if (strModule.ToUpper().Equals("MANUALINSURANCE") || strModule.ToUpper().Equals("MANUAL INSURANCE"))
            {
                switch (env.ToUpper())
                {
                    case "DEV":
                        {
                            TestSetup.strServer = "DC1T1DB-D008SQL.tenant01dev.com";
                            TestSetup.strDB = "DataConversion";
                            break;
                        }
                    case "DEV-EDIT":
                        {
                            TestSetup.strServer = "dc1t1ag-d008sql.tenant01dev.com";
                            TestSetup.strDB = "DataConversion";
                            break;
                        }

                    case "QA":
                        {
                            TestSetup.strServer = "DC1T1AG-Q009SQL.tenant01qax.com";
                            TestSetup.strDB = "DataConversion";
                            break;
                        }
                    case "PRE-PROD":
                        {
                            TestSetup.strServer = "DC1T1AG-S009SQL.tenant01stg.com";
                            TestSetup.strDB = "DataConversion";
                            break;
                        }
                    case "PROD":
                        {
                            TestSetup.strServer = "DC1T1AG-P009SQL.tenant01prd.com";
                            TestSetup.strDB = "DataConversion";
                            break;
                        }
                    default:
                        Console.WriteLine("Error~!");
                        break;
                }
            }
            else if (strModule.ToUpper().Equals("PPS"))
            {
                switch (env.ToUpper())
                {
                    case "DEV":
                        {
                            TestSetup.strServer = "DC1T1DB-D008SQL.tenant01dev.com";
                            TestSetup.strDB = "DataConversion";
                            break;
                        }
                    case "DEV-EDIT":
                        {
                            TestSetup.strServer = "dc1t1ag-d008sql.tenant01dev.com";
                            TestSetup.strDB = "DataConversion";
                            break;
                        }

                    case "QA":
                        {
                            TestSetup.strServer = "DC1T1AG-Q009SQL.tenant01qax.com";
                            TestSetup.strDB = "DataConversion";
                            break;
                        }
                    case "PRE-PROD":
                        {
                            TestSetup.strServer = "DC1T1AG-S009SQL.tenant01stg.com";
                            TestSetup.strDB = "DataConversion";
                            break;
                        }
                    case "PROD":
                        {
                            TestSetup.strServer = "DC1T1AG-P009SQL.tenant01prd.com";
                            TestSetup.strDB = "DataConversion";
                            break;
                        }
                    default:
                        Console.WriteLine("Error~!");
                        break;
                }
            }
            else if (strModule.ToUpper().Equals("AIR"))
            {
                switch (env.ToUpper())
                {
                    case "DEV":
                        {
                            TestSetup.strServer = "dc1t1db-d010sql.tenant01dev.com";
                            TestSetup.strDB = "DataIntegrationStage";
                            break;
                        }

                    case "QA":
                        {
                            TestSetup.strServer = "";
                            TestSetup.strDB = "DataIntegrationStage";
                            break;
                        }
                    case "PRE-PROD":
                        {
                            TestSetup.strServer = "DC1T1AG-S011SQL.tenant01stg.com";
                            TestSetup.strDB = "DataIntegrationStage";
                            break;
                        }
                    case "PROD":
                        {
                            TestSetup.strServer = "DC1T1AG-P011SQL.tenant01prd.com";
                            TestSetup.strDB = "DataIntegrationStage";
                            break;
                        }
                    default:
                        Console.WriteLine("Error~!");
                        break;
                }
            }
            else
            {
                Console.WriteLine("Error~!: Enivironment Unknown");
            }

            string connString = @"Server=" + TestSetup.strServer + ";Database=" + TestSetup.strDB + ";Trusted_Connection=true;Connection Timeout=240";
            return connString;
        }

        public static DataTable RunQuery(string queryString, string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                connectionString = Database.GetConnectionString(TestSetup.module, TestSetup.environment);
            }
            using (SqlConnection connection = new SqlConnection(connectionString))
            {   // Create DataTable Object
                DataTable dt = new DataTable();
                //connection.ConnectionTimeout(120);
                connection.Open();
                using (SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection))
                {
                    adapter.Fill(dt);
                };

                return dt;
            }
        }
        public static DataTable PullData(String strModule, String strTable)
        {
            DataTable dataTable = new DataTable();
            string connString = Database.GetConnectionString(strModule, TestSetup.environment);
            string query = "select * from " + strTable;
            return (Database.RunQuery(query, connString));
        }

        public static void RunDotSQLFile(String strFileFolder)
        {
            if (File.Exists(strFileFolder))
            {
                // This path is a file
                ProcessFile(strFileFolder);
            }
            else if (Directory.Exists(strFileFolder))
            {
                // This path is a directory
                ProcessDirectory(strFileFolder);
            }
            else
            {
                Console.WriteLine("{0} is not a valid file or directory. ", strFileFolder);
            }
        }

        public static void ExecuteNonQuery(String script)
        {
            String ConnString = GetConnectionString(TestSetup.module, TestSetup.environment);
            using (var connection = new SqlConnection(ConnString))
            {
                using (var command = new SqlCommand(script, connection))
                {
                    command.CommandTimeout = 240;
                    connection.Open();
                    try
                    {
                        command.ExecuteNonQuery();
                        //Console.WriteLine("Connection : " + connection + " | Query Executed : " + script);
                    }
                    catch (Exception e)
                    {
                        //Console.Write("executeSQLUpdate: problem with command:" + command + "e= " + e);
                        Console.Out.Flush();
                        TestSetup.StepFail = true;
                        Console.Write("executeSQLUpdate: problem with command:" + command + "e= " + e);
                        //throw new Exception("ExecuteSQLUpdate: problem with command:" + command, e);
                    }
                }
                connection.Close();
            }
        }
        public static void ProcessDirectory(string targetDirectory)
        {
            // Process the list of files found in the directory.
            string[] fileEntries = Directory.GetFiles(targetDirectory);

            foreach (string fileName in fileEntries)
            {
                Console.WriteLine("Trying to Execute File : " + fileName);
                try
                {
                    ExecuteNonQuery(ProcessFile(fileName));
                    Console.WriteLine("Executed File : " + fileName);
                }
                catch (Exception e)
                {
                    Console.WriteLine(" with Error");
                    Console.WriteLine("ExecuteSQLUpdate: problem with command : " + e);
                }
                Console.Write(" with No Errors");
            }

            // Recurse into subdirectories of this directory.
            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
            DirectoryInfo d = new DirectoryInfo(targetDirectory);
            string isExcludeFolder = String.Empty;
            foreach (string subdirectory in subdirectoryEntries)
            {
                d = new DirectoryInfo(subdirectory);
                isExcludeFolder = d.Name;
                if (isExcludeFolder.Equals("Exclude"))
                {
                    //do Nothing
                }
                else
                {
                    ProcessDirectory(subdirectory);
                }
            }
        }

        // Insert logic for processing found files here.
        public static string ProcessFile(string path)
        {
            return File.ReadAllText(path);
        }


        public static string GetQuery()
        {
            string strFileFolder = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\Queries\";
            if (File.Exists(strFileFolder))
            {
                // This path is a file
                ProcessFile(strFileFolder);
            }
            else if (Directory.Exists(strFileFolder))
            {
                // This path is a directory
                ProcessDirectory(strFileFolder);
            }
            else
            {
                Console.WriteLine("{0} is not a valid file or directory. ", strFileFolder);
            }
            return "Yes";
        }

        public static DataTable GetTableSchema(String strTableName)
        { 
        String query = @"SELECT 
                                    c.name 'Column Name',
                                    t.Name 'Data type',
                                    c.max_length 'Max Length',
                                    c.precision ,
                                    c.scale ,
                                    c.is_nullable,
                                    ISNULL(i.is_primary_key, 0) 'Primary Key',
	                                c.column_id
                                        FROM    
                                            sys.columns c
                                        INNER JOIN 
                                            sys.types t ON c.user_type_id = t.user_type_id
                                        LEFT OUTER JOIN 
                                            sys.index_columns ic ON ic.object_id = c.object_id AND ic.column_id = c.column_id
                                        LEFT OUTER JOIN 
                                            sys.indexes i ON ic.object_id = i.object_id AND ic.index_id = i.index_id
                                        WHERE
                                            c.object_id = OBJECT_ID('" + strTableName + @"')
                                        Order by c.column_id";
        DataTable dt = Database.RunQuery(query, "");
        return dt;
    }
}
}
