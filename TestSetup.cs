﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataConversionAutomation
{
    public class TestSetup
    {
        //Template root Folder Path
        public static Boolean PartialRun = false;
        public static string module = string.Empty;
        public static string environment = string.Empty;
        public static string templateFilePath = string.Empty;
        public static string section = string.Empty;
        public static string table = string.Empty;
        public static string query = string.Empty;
        public static string Expected = "Pass";
        public static Boolean StepFail = false;
        public static string TempFilePath = string.Empty;
        public static string strQuery = String.Empty;
        public static string strSQLFile = String.Empty;

        //Template File Path
        public static string SourceChecksumFilePath;
        public static string TargetChecksumFilePath;
        public static string ResultFilePath;
        public static readonly string ProjectPath = AppDomain.CurrentDomain.BaseDirectory.Substring(0, AppDomain.CurrentDomain.BaseDirectory.Length - 13);
        //Root Checksum Path
        public static readonly string checksumStorePath = ProjectPath + @"ChecksumStore";

        public static Boolean txt2db = false;
        //Root SQL Query Path
        public const string SQLTestRootPath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\";
        //Root Checksum Query Path
        public const string DBQueriesRootPath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\DB Queries\";

        public const string SQLTestResultPath = @"\\ADVISOR360CORP.COM\public\ITShare\Development\DataConversions\QA Automation\SQL Test\Results\";

        public static List<string> file1Lines;
        public static List<string> file2Lines;
        public static IEnumerable<string> missingInDB;
        public static IEnumerable<string> missingInRaw;

        public static string strServer = String.Empty;
        public static string strDB = String.Empty;

    }
 }
